package search;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import libes.globalData;
import noti.notificationAdmin;
import profamily.designsite.in.profamilies.R;
import profamily.designsite.in.profamilies.notidetails;
import profamily.designsite.in.profamilies.serviceSearchResult;

/**
 * Created by mac on 4/18/17.
 */

public class searchHistoryAdapter extends RecyclerView.Adapter<searchHistoryAdapter.ViewHolder> {
    private Context v;
    private List<searchHistory> my_data;
    public searchHistoryAdapter(Context v, List<searchHistory> data_list) {
        this.v = v;
        this.my_data = data_list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_search, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.title.setText(my_data.get(position).getTitle());
        holder.count.setText(my_data.get(position).getCount());
        if(position % 2 == 1) {
            holder.rowData.setBackgroundColor(Color.parseColor("#E9EBEE"));
        }else{
            holder.rowData.setBackgroundColor(Color.parseColor("#ffffff"));
        }
        holder.rowData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(v.getContext(), serviceSearchResult.class);
                i.putExtra("txtSearch", my_data.get(position).getTitle());
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                v.getContext().startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return my_data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView title;
        public TextView count;
        public LinearLayout rowData;

        public ViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            title.setTypeface(Typeface.createFromAsset(itemView.getContext().getAssets(), globalData.getFont()));
            count = (TextView) itemView.findViewById(R.id.count);
            count.setTypeface(Typeface.createFromAsset(itemView.getContext().getAssets(), globalData.getFont()));
            rowData = (LinearLayout) itemView.findViewById(R.id.rowData);
        }
    }

}
