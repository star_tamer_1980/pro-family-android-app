package libes;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

import cartSQLiteData.CartSQLite;
import cartSQLiteData.CartSQLiteMarket;

/**
 * Created by mac on 6/7/17.
 */

public class SQLiteConnection extends SQLiteOpenHelper {
    public static final String DbName = "mycart.db";
    public static final int Verson = 1;


    public SQLiteConnection(Context context) {
        super(context, DbName, null, Verson);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE if not exists 'products' ( `id` INTEGER PRIMARY KEY AUTOINCREMENT, `idProduct` INTEGER, `idMarket` INTEGER, 'marketName' TEXT, `productName` TEXT, `productQuantity` INTEGER, `productPriceFinish` INTEGER, `img` TEXT )");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("Drob table if not exists products");
        onCreate(db);
    }

    public boolean UpdateRowCart(Integer idProduct, String productQuantity, String productPriceFinish){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("productQuantity", productQuantity);
        contentValues.put("productPriceFinish", productPriceFinish);
        if (db.update("products", contentValues, "idProduct = ?", new String[]{Integer.toString(idProduct)}) != 0) {
            return true;
        }
        return false;

    }
    public void InsertRowCart(Integer idProduct, Integer idMarket, String marketName, String productName, String productQuantity, String productPriceFinish, String img){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("idProduct", idProduct);
        contentValues.put("idMarket", idMarket);
        contentValues.put("marketName", marketName);
        contentValues.put("productName", productName);
        contentValues.put("productQuantity", productQuantity);
        contentValues.put("productPriceFinish", productPriceFinish);
        contentValues.put("img", img);
        db.insert("products", null, contentValues);
    }
    public List<CartSQLite> getAllRecord(){
        List<CartSQLite> cartList = new ArrayList<CartSQLite>();
        String selectQuery = "select * from products";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        cursor.moveToFirst();
        while(cursor.isAfterLast() == false){
            CartSQLite cartSQLite = new CartSQLite();
            cartSQLite.setId(cursor.getString(cursor.getColumnIndex("id")));
            cartSQLite.setIdProduct(cursor.getString(cursor.getColumnIndex("idProduct")));
            cartSQLite.setIdMarket(cursor.getString(cursor.getColumnIndex("idMarket")));
            cartSQLite.setProductName(cursor.getString(cursor.getColumnIndex("productName")));
            cartSQLite.setProductPriceFinish(cursor.getString(cursor.getColumnIndex("productPriceFinish")));
            cartSQLite.setProductQuantity(cursor.getString(cursor.getColumnIndex("productQuantity")));
            cartSQLite.setImg(cursor.getString(cursor.getColumnIndex("img")));
            cartList.add(cartSQLite);
            cursor.moveToNext();
        }
        return cartList;
    }
    public List<CartSQLiteMarket> getAllMarketForCart(){
        List<CartSQLiteMarket> cartList = new ArrayList<CartSQLiteMarket>();
        String selectQuery = "select * from products group by idMarket";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        cursor.moveToFirst();
        while(cursor.isAfterLast() == false){
            CartSQLiteMarket cartSQLite = new CartSQLiteMarket();
            cartSQLite.setId(cursor.getString(cursor.getColumnIndex("id")));
            cartSQLite.setIdMarket(cursor.getString(cursor.getColumnIndex("idMarket")));
            cartSQLite.setProductPriceFinish(cursor.getString(cursor.getColumnIndex("productPriceFinish")));
            cartSQLite.setProductQuantity(cursor.getString(cursor.getColumnIndex("productQuantity")));
            cartSQLite.setImg(cursor.getString(cursor.getColumnIndex("img")));
            cartList.add(cartSQLite);
            cursor.moveToNext();
        }
        return cartList;
    }
    public List<CartSQLite> getCart(){
        List<CartSQLite> cartList = new ArrayList<CartSQLite>();
        String selectQuery = "select * from products";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        cursor.moveToFirst();
        while(cursor.isAfterLast() == false){
            CartSQLite cartSQLite = new CartSQLite();
            cartSQLite.setIdProduct(cursor.getString(cursor.getColumnIndex("idProduct")));
            cartSQLite.setProductQuantity(cursor.getString(cursor.getColumnIndex("productQuantity")));
            cartList.add(cartSQLite);
            cursor.moveToNext();
        }
        return cartList;
    }
    public String getQuantity(String idProduct){
        String selectQuery = "select productQuantity as quantity from products where idProduct = " + idProduct;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if(cursor.moveToFirst()) {
            return cursor.getString(cursor.getColumnIndex("quantity"));
        }
        return "0";
    }
    public String getAllPrice(){
        String selectQuery = "select sum(productPriceFinish) as price from products";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if(cursor.moveToFirst()) {
            return cursor.getString(cursor.getColumnIndex("price"));
        }
        return "0";
    }
    public int deleteAllRecord(){
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("products", "1", null);

    }
    public int deleteOneRecord(String id){
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("products", "id=" + Integer.valueOf(id), null);

    }
    public boolean isProductFound(String idProduct){
        String selectQuery = "select * from products where idProduct =" + idProduct;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if(cursor!=null && cursor.getCount()>0){
            return true;
        }
        return false;
    }

}
