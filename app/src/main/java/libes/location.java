package libes;

import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.format.Formatter;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import profamily.designsite.in.profamilies.Manifest;

import static android.content.Context.WIFI_SERVICE;

/**
 * Created by mac on 3/18/17.
 */

public class location {
    private GoogleMap googlemap;
    private double maplat;
    private double maplon;
    LocationManager lmgr;
    LocationListener listener;


    public String Langitude = "0";
    public String Longitude = "0";


    Context mContext;
    public location(Context mContext) {
        this.mContext = mContext;
        setUpMap();
        Langitude = KeyValueShared.getMapLat(mContext);
        Longitude = KeyValueShared.getMapLon(mContext);
    }
    public String getIPAddress(){
        WifiManager wifiMgr = (WifiManager) mContext.getSystemService(WIFI_SERVICE);
        WifiInfo wifiInfo = wifiMgr.getConnectionInfo();
        int ip = wifiInfo.getIpAddress();
        return Formatter.formatIpAddress(ip);
    }


    public void setUpMap() {
        LocationManager locationManager = (LocationManager) mContext.getSystemService(mContext.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        String provider = locationManager.getBestProvider(criteria, true);
        if (ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        Location locations = locationManager.getLastKnownLocation(provider);
        if(locations != null) {
            Langitude = String.valueOf(locations.getLatitude());
            Longitude = String.valueOf(locations.getLongitude());
        }else{
            Langitude = KeyValueShared.getMapLat(mContext);
            Longitude = KeyValueShared.getMapLon(mContext);
        }
        Log.e("data is ", Longitude.toString());
    }

    public static String getCompleteAddress(Context context, String LATITUDE, String LONGITUDE) throws IOException {
        Geocoder geocoder;
        List<Address> addresses;
        Locale Arabic = new Locale("ar");
        geocoder = new Geocoder(context, Arabic);
        addresses = geocoder.getFromLocation(Double.parseDouble(LATITUDE), Double.parseDouble(LONGITUDE), 3); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
        Log.e("LATITUDE", LATITUDE.toString());
        Log.e("LONGITUDE", LONGITUDE.toString());
        if(addresses.size() != 0) {
            String address = addresses.get(1).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(2).getLocality();
            String state = addresses.get(1).getAdminArea();
            String country = addresses.get(1).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL
            if(address != null){
                city = city + " - " + address;
            }
            return city;
        }else{
            return "الموقع غير محدد";
        }
    }
}
//https://maps.googleapis.com/maps/api/geocode/json?latlng=24.404805000000003,46.8398466666666&key=myKeyCode