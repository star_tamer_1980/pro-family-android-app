package libes;

import android.content.Context;
import android.content.Intent;

import profamily.designsite.in.profamilies.HarajSettings;
import profamily.designsite.in.profamilies.MainHaraj;
import profamily.designsite.in.profamilies.haraj_search_list;
import profamily.designsite.in.profamilies.notifi;
import profamily.designsite.in.profamilies.notification_alerts;
import profamily.designsite.in.profamilies.serviceMain;
import profamily.designsite.in.profamilies.serviceMenu;
import profamily.designsite.in.profamilies.serviceNotificationAlert;
import profamily.designsite.in.profamilies.serviceSearchList;
import profamily.designsite.in.profamilies.settingsMarket;

/**
 * Created by mac on 3/19/18.
 */

public class footerServiceAction {
    public static void footerHome(Context context){
        Intent i = new Intent(context, serviceMain.class);
        context.startActivity(i);
    }
    public static void footerSearch(Context context){
        Intent i = new Intent(context, serviceSearchList.class);
        context.startActivity(i);
    }
    public static void footerNoti(Context context){
        Intent i = new Intent(context, serviceNotificationAlert.class);
        context.startActivity(i);
    }
    public static void footerMore(Context context){
        Intent i = new Intent(context, serviceMenu.class);
        context.startActivity(i);
    }
}
