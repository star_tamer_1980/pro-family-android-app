package libes;

import android.app.Activity;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.view.View;

import profamily.designsite.in.profamilies.HarajAdd;
import profamily.designsite.in.profamilies.HarajAddSwicher;
import profamily.designsite.in.profamilies.HarajProductDetails;
import profamily.designsite.in.profamilies.R;
import profamily.designsite.in.profamilies.userLogin;
import profamily.designsite.in.profamilies.userRegister;

/**
 * Created by mac on 3/7/18.
 */

public class alertsApp {
    public static void mustLogin(final View view){
        AlertDialog.Builder builder1 = new AlertDialog.Builder(view.getContext());
        builder1.setMessage(view.getResources().getString(R.string.alertMessageMustLogin));
        builder1.setTitle(view.getResources().getString(R.string.alertTitleMustLogin));
        builder1.setCancelable(true);

        builder1.setNeutralButton(
                view.getResources().getString(R.string.alertButtonMustRegister),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent i = new Intent(view.getContext(), userRegister.class);
                        view.getContext().startActivity(i);
                        dialog.cancel();
                    }
                });

        builder1.setNegativeButton(
                view.getResources().getString(R.string.alertButtonMustLogin),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent i = new Intent(view.getContext(), userLogin.class);
                        view.getContext().startActivity(i);
                        dialog.cancel();
                    }
                });
        builder1.setCancelable(false).setPositiveButton(view.getResources().getString(R.string.alertButtonCancel), new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }


}
