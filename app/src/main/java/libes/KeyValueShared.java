package libes;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by mac on 5/17/17.
 */

public class KeyValueShared {
    private SharedPreferences sharedPreferences;
    private static String getKeyAccountLogin = "accountLogin";
    private static String MEMBER_NAME = "member_name";
    private static String MEMBER_TEL = "member_tel";
    private static String MEMBER_MAIL = "member_mail";
    private static String MEMBER_STATUE = "member_statue";
    private static String MEMBER_id = "member_id";
    private static String MEMBER_RAND_id = "member_rand_id";    // this is temp number to use on upload images and vedio until login
    private static String MAP_LAT = "mapLat";
    private static String MAP_LON = "mapLon";
    private static String MARKET_MIN_PRICE = "market_min_price";
    private static String MARKET_DELIVERY_PRICE = "market_delivery_price";
    private static String WORD_SEARCH = "word_search";
    private static String isAdd = "isAdd";
    private static String settingsAppNoNoti = "settingsApp_notificationNumbers";
    private static String settingsAppCityId = "settingsApp_countryId";
    private static String settingsAppCityName = "settingsApp_cityName";
    private static String settingsAppCountryId = "settingsApp_countryId";
    private static String settingsAppCountryName = "settingsApp_countryName";
    private static String settingsAppUserDetailsIsOpen = "settingsAppUserDetailsIsOpen";
    private static String settingsAppCountryIsOpen = "settingsAppCountryIsOpen";
    private static String settingsAppMyCityName = "settingsAppMyCityName";
    private static String settingsAppMyCityId = "settingsAppMyCityId";
    private static String settingsAppFirebaseToken = "settingsAppFirebaseToken";
    private static String settingsAppisSMSOrders = "isSMSOrders";
    private static String settingsAppisSMSComments = "isSMSComments";
    private static String settingsAppisNotifiOrders= "isNotifiOrders";
    private static String settingsAppisNotifiComments= "isNotifiComments";
    private static String market_remove= "false";
    private static String product_remove= "product_remove";
    private static String haraj_main_cat= "haraj_main_cat";
    // when add market use this data
    private static String MARKET_MARKET_ID = "market_id";
    private static String market_mainCat= "marketMainCat";
    private static String market_mainCat_tab= "marketMainCatTab";
    private static String market_add_is_true= "add_is_true";
    // 0 = not add
    // 1 = add
    // 2 = add location
    private static String idLocationForOrder = "id_Location_For_Order";

    public KeyValueShared(){

    }
    private static SharedPreferences getAccountData(Context context){
        return context.getSharedPreferences(getKeyAccountLogin, Context.MODE_PRIVATE);
    }
    public static String getStatue(Context context){
        return getAccountData(context).getString(MEMBER_STATUE, "0");
    }
    public static void setStatue(Context context, String input){
        SharedPreferences.Editor editor = getAccountData(context).edit();
        editor.putString(MEMBER_STATUE, input);
        editor.commit();
    }
    public static String getTel(Context context){
        return getAccountData(context).getString(MEMBER_TEL, "");
    }
    public static void setTel(Context context, String input){
        SharedPreferences.Editor editor = getAccountData(context).edit();
        editor.putString(MEMBER_TEL, input);
        editor.commit();
    }
    public static String getName(Context context){
        return getAccountData(context).getString(MEMBER_NAME, "");
    }
    public static void setName(Context context, String input){
        SharedPreferences.Editor editor = getAccountData(context).edit();
        editor.putString(MEMBER_NAME, input);
        editor.commit();
    }
    public static String getEmail(Context context){
        return getAccountData(context).getString(MEMBER_MAIL, "");
    }
    public static void setEmail(Context context, String input){
        SharedPreferences.Editor editor = getAccountData(context).edit();
        editor.putString(MEMBER_MAIL, input);
        editor.commit();
    }
    public static String getId(Context context){
        return getAccountData(context).getString(MEMBER_id, "0");
    }
    public static void setId(Context context, String input){
        SharedPreferences.Editor editor = getAccountData(context).edit();
        editor.putString(MEMBER_id, input);
        editor.commit();
    }
    public static void setMEMBER_RAND_id(Context context, String input) {
        SharedPreferences.Editor editor = getAccountData(context).edit();
        editor.putString(MEMBER_RAND_id, input);
        editor.commit();
    }
    public static String getIdForMember(Context context){
        String no = getId(context);
        if(no == "0"){
            return getAccountData(context).getString(MEMBER_RAND_id, "10000");
        }else{
            return no;
        }

    }
    public static String getMapLat(Context context){
        return getAccountData(context).getString(MAP_LAT, "0");
    }
    public static void setMapLat(Context context, String input){
        SharedPreferences.Editor editor = getAccountData(context).edit();
        editor.putString(MAP_LAT, input);
        editor.commit();
    }
    public static String getMapLon(Context context){
        return getAccountData(context).getString(MAP_LON, "0");
    }
    public static void setMapLon(Context context, String input){
        SharedPreferences.Editor editor = getAccountData(context).edit();
        editor.putString(MAP_LON, input);
        editor.commit();
    }
    public static String getMarketMinPrice(Context context){
        return getAccountData(context).getString(MARKET_MIN_PRICE, "");
    }
    public static void setMarketMinPrice(Context context, String input){
        SharedPreferences.Editor editor = getAccountData(context).edit();
        editor.putString(MARKET_MIN_PRICE, input);
        editor.commit();
    }
    public static String getMarketDeliveryPrice(Context context){
        return getAccountData(context).getString(MARKET_DELIVERY_PRICE, "");
    }
    public static void setMarketDeliveryPrice(Context context, String input){
        SharedPreferences.Editor editor = getAccountData(context).edit();
        editor.putString(MARKET_DELIVERY_PRICE, input);
        editor.commit();
    }
    public static String getMarkeId(Context context){
        return getAccountData(context).getString(MARKET_MARKET_ID, "");
    }
    public static void setMarketId(Context context, String input){
        SharedPreferences.Editor editor = getAccountData(context).edit();
        editor.putString(MARKET_MARKET_ID, input);
        editor.commit();
    }
    public static String getWordSearch(Context context){
        return getAccountData(context).getString(WORD_SEARCH, "");
    }
    public static void setWordSearch(Context context, String input){
        SharedPreferences.Editor editor = getAccountData(context).edit();
        editor.putString(WORD_SEARCH, input);
        editor.commit();
    }
    public static String getIsAdd(Context context){
        return getAccountData(context).getString(isAdd, "0");
    }
    public static void setIsAdd(Context context, String input){
        SharedPreferences.Editor editor = getAccountData(context).edit();
        editor.putString(isAdd, input);
        editor.commit();
    }
    public static String getSettingsAppNoNoti(Context context){
        return getAccountData(context).getString(settingsAppNoNoti, "0");
    }
    public static void setSettingsAppNoNoti(Context context, String input){
        SharedPreferences.Editor editor = getAccountData(context).edit();
        editor.putString(settingsAppNoNoti, input);
        editor.commit();
    }
    public static String getSettingsAppCityId(Context context){
        return getAccountData(context).getString(settingsAppCityId, "0");
    }
    public static void setSettingsAppCityId(Context context, String input){
        SharedPreferences.Editor editor = getAccountData(context).edit();
        editor.putString(settingsAppCityId, input);
        editor.commit();
    }
    public static String getSettingsAppCityName(Context context){
        return getAccountData(context).getString(settingsAppCityName, "");
    }
    public static void setSettingsAppCityName(Context context, String input){
        SharedPreferences.Editor editor = getAccountData(context).edit();
        editor.putString(settingsAppCityName, input);
        editor.commit();
    }
    public static String getSettingsAppCountryId(Context context){
        return getAccountData(context).getString(settingsAppCountryId, "");
    }
    public static void setSettingsAppCountryId(Context context, String input){
        SharedPreferences.Editor editor = getAccountData(context).edit();
        editor.putString(settingsAppCountryId, input);
        editor.commit();
    }
    public static String getSettingsAppCountryName(Context context){
        return getAccountData(context).getString(settingsAppCountryName, "");
    }
    public static void setSettingsAppCountryName(Context context, String input){
        SharedPreferences.Editor editor = getAccountData(context).edit();
        editor.putString(settingsAppCountryName, input);
        editor.commit();
    }
    public static String getSettingsAppUserDetailsIsOpen(Context context){
        return getAccountData(context).getString(settingsAppUserDetailsIsOpen, "0");
    }
    public static void setSettingsAppUserDetailsIsOpen(Context context, String input){
        SharedPreferences.Editor editor = getAccountData(context).edit();
        editor.putString(settingsAppUserDetailsIsOpen, input);
        editor.commit();
    }
    public static String getSettingsAppCountryIsOpen(Context context){
        return getAccountData(context).getString(settingsAppCountryIsOpen, "0");
    }
    public static void setSettingsAppCountryIsOpen(Context context, String input){
        SharedPreferences.Editor editor = getAccountData(context).edit();
        editor.putString(settingsAppCountryIsOpen, input);
        editor.commit();
    }
    public static String getSettingsAppMyCityName(Context context){
        return getAccountData(context).getString(settingsAppMyCityName, "");
    }
    public static void setSettingsAppMyCityName(Context context, String input){
        SharedPreferences.Editor editor = getAccountData(context).edit();
        editor.putString(settingsAppMyCityName, input);
        editor.commit();
    }
    public static String getSettingsAppMyCityId(Context context){
        return getAccountData(context).getString(settingsAppMyCityId, "0");
    }
    public static void setSettingsAppMyCityId(Context context, String input){
        SharedPreferences.Editor editor = getAccountData(context).edit();
        editor.putString(settingsAppMyCityId, input);
        editor.commit();
    }
    public static String getSettingsAppFirebaseToken(Context context){
        return getAccountData(context).getString(settingsAppFirebaseToken, "0");
    }
    public static void setSettingsAppFirebaseToken(Context context, String input){
        SharedPreferences.Editor editor = getAccountData(context).edit();
        editor.putString(settingsAppFirebaseToken, input);
        editor.commit();
    }
    public static String getSettingsAppisSMSOrders(Context context){
        return getAccountData(context).getString(settingsAppisSMSOrders, "0");
    }
    public static void setSettingsAppisSMSOrders(Context context, String input){
        SharedPreferences.Editor editor = getAccountData(context).edit();
        editor.putString(settingsAppisSMSOrders, input);
        editor.commit();
    }
    public static String getSettingsAppisSMSComments(Context context){
        return getAccountData(context).getString(settingsAppisSMSComments, "0");
    }
    public static void setSettingsAppisSMSComments(Context context, String input){
        SharedPreferences.Editor editor = getAccountData(context).edit();
        editor.putString(settingsAppisSMSComments, input);
        editor.commit();
    }
    public static String getSettingsAppisNotifiOrders(Context context){
        return getAccountData(context).getString(settingsAppisNotifiOrders, "0");
    }
    public static void setSettingsAppisNotifiOrders(Context context, String input){
        SharedPreferences.Editor editor = getAccountData(context).edit();
        editor.putString(settingsAppisNotifiOrders, input);
        editor.commit();
    }
    public static String getSettingsAppisNotifiComments(Context context){
        return getAccountData(context).getString(settingsAppisNotifiComments, "0");
    }
    public static void setSettingsAppisNotifiComments(Context context, String input){
        SharedPreferences.Editor editor = getAccountData(context).edit();
        editor.putString(settingsAppisNotifiComments, input);
        editor.commit();
    }

    public static String getMarket_remove(Context context){
        return getAccountData(context).getString(market_remove, "");
    }
    public static void setMarket_remove(Context context, String input){
        SharedPreferences.Editor editor = getAccountData(context).edit();
        editor.putString(market_remove, input);
        editor.commit();
    }
    public static String getProduct_remove(Context context){
        return getAccountData(context).getString(product_remove, "");
    }
    public static void setProduct_remove(Context context, String input){
        SharedPreferences.Editor editor = getAccountData(context).edit();
        editor.putString(product_remove, input);
        editor.commit();
    }
    public static String getHaraj_main_cat(Context context){
        return getAccountData(context).getString(haraj_main_cat, "");
    }
    public static void setHaraj_main_cat(Context context, String input){
        SharedPreferences.Editor editor = getAccountData(context).edit();
        editor.putString(haraj_main_cat, input);
        editor.commit();
    }
    public static String getMarket_mainCat(Context context){
        return getAccountData(context).getString(market_mainCat, "");
    }
    public static void setMarket_mainCat(Context context, String input){
        SharedPreferences.Editor editor = getAccountData(context).edit();
        editor.putString(market_mainCat, input);
        editor.commit();
    }
    public static String getMarket_mainCat_tab(Context context){
        return getAccountData(context).getString(market_mainCat_tab, "");
    }
    public static void setMarket_mainCat_tab(Context context, String input){
        SharedPreferences.Editor editor = getAccountData(context).edit();
        editor.putString(market_mainCat_tab, input);
        editor.commit();
    }
    public static String getMarket_add_is_true(Context context){
        return getAccountData(context).getString(market_add_is_true, "");
    }
    public static void setMarket_add_is_true(Context context, String input){
        SharedPreferences.Editor editor = getAccountData(context).edit();
        editor.putString(market_add_is_true, input);
        editor.commit();
    }
    public static String getIdLocationForOrder(Context context){
        return getAccountData(context).getString(idLocationForOrder, "");
    }
    public static void setIdLocationForOrder(Context context, String input){
        SharedPreferences.Editor editor = getAccountData(context).edit();
        editor.putString(idLocationForOrder, input);
        editor.commit();
    }























}
