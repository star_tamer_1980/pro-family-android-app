package libes;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by mac on 3/18/17.
 */

public class globalData {
    private static String language = Locale.getDefault().getDisplayLanguage();
    private static String font;
    public String getLanguage() {
        return language;
    }

    public static String getFont() {
        setFont();
        return font;
    }

    private static void setFont() {
        if(language.equals("English")){
            font = "fonts/arabic.ttf";
        }else{
            font = "fonts/arabic.ttf";
        }
    }
    public static String URLhost(){
        return "http://pro-families.com/_views/api/";
    }
    public static String URLThumb(){
        return "http://pro-families.com/uploaded/thumb/";
    }
    public static String getKeyAccountLogin(){
        return "accountLogin";
    }

}
