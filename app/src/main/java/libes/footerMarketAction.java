package libes;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import profamily.designsite.in.profamilies.marketMain;
import profamily.designsite.in.profamilies.marketNotificationAlert;
import profamily.designsite.in.profamilies.menuMarket;
import profamily.designsite.in.profamilies.notification_alerts;
import profamily.designsite.in.profamilies.orders_switch;
import profamily.designsite.in.profamilies.serviceMain;
import profamily.designsite.in.profamilies.serviceMenu;
import profamily.designsite.in.profamilies.serviceNotificationAlert;
import profamily.designsite.in.profamilies.serviceSearchList;

/**
 * Created by mac on 3/19/18.
 */

public class footerMarketAction {
    public static void footerHome(Context context){
        Intent i = new Intent(context, marketMain.class);
        context.startActivity(i);
    }
    public static void footerOrders(Context context){
        Intent i = new Intent(context, orders_switch.class);
        context.startActivity(i);
    }
    public static void footerNoti(Context context){
        Intent i = new Intent(context, marketNotificationAlert.class);
        context.startActivity(i);
    }
    public static void footerMore(Context context){
        Intent i = new Intent(context, menuMarket.class);
        context.startActivity(i);
    }
}
