package libes;

import android.content.Context;
import android.content.Intent;

import profamily.designsite.in.profamilies.HarajSettings;
import profamily.designsite.in.profamilies.Main2Activity;
import profamily.designsite.in.profamilies.MainHaraj;
import profamily.designsite.in.profamilies.MapsActivity;
import profamily.designsite.in.profamilies.haraj_search_list;
import profamily.designsite.in.profamilies.notifi;
import profamily.designsite.in.profamilies.notification_alerts;

/**
 * Created by mac on 2/24/18.
 */

public class footerHarajAction {
    public static void footerHome(Context context){
        Intent i = new Intent(context, MainHaraj.class);
        context.startActivity(i);
    }
    public static void footerSearch(Context context){
        Intent i = new Intent(context, haraj_search_list.class);
        context.startActivity(i);
    }
    public static void footerNoti(Context context){
        Intent i = new Intent(context, notification_alerts.class);
        context.startActivity(i);
    }
    public static void footerComments(Context context){
        Intent i = new Intent(context, notifi.class);
        context.startActivity(i);
    }
    public static void footerMore(Context context){
        Intent i = new Intent(context, HarajSettings.class);
        context.startActivity(i);
    }
}
