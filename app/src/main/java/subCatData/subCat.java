package subCatData;

/**
 * Created by mac on 4/18/17.
 */

public class subCat {
    private String id, category, lat, lon, idMainCat, idSubCat;
    public subCat() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getIdMainCat() {
        return idMainCat;
    }

    public void setIdMainCat(String idMainCat) {
        this.idMainCat = idMainCat;
    }

    public String getIdSubCat() {
        return idSubCat;
    }

    public void setIdSubCat(String idSubCat) {
        this.idSubCat = idSubCat;
    }
}
