package subCatData;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import libes.globalData;
import profamily.designsite.in.profamilies.MakretListSearch;
import profamily.designsite.in.profamilies.R;
import profamily.designsite.in.profamilies.notidetails;

/**
 * Created by mac on 4/18/17.
 */

public class subCatAdapter extends RecyclerView.Adapter<subCatAdapter.ViewHolder> {
    private Context v;
    private List<subCat> my_data;
    public subCatAdapter(Context v, List<subCat> data_list) {
        this.v = v;
        this.my_data = data_list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_noti, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final String id = my_data.get(position).getId();
        final String lat = my_data.get(position).getLat();
        final String lon = my_data.get(position).getLon();
        final String idMainCat = my_data.get(position).getIdMainCat();
        holder.title.setText(my_data.get(position).getCategory());

        holder.rowData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(v.getContext(), MakretListSearch.class);
                i.putExtra("idSubCat", id);
                i.putExtra("cat", idMainCat);
                i.putExtra("mapLat", lat);
                i.putExtra("mapLon", lon);
                i.putExtra("searchType", "catId");
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                v.getContext().startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return my_data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView title;
        public RelativeLayout rowData;

        public ViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            title.setTypeface(Typeface.createFromAsset(itemView.getContext().getAssets(), globalData.getFont()));
            rowData = (RelativeLayout) itemView.findViewById(R.id.rowData);
        }
    }

}
