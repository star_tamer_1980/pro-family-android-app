package commentsData;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import libes.globalData;
import profamily.designsite.in.profamilies.HarajImageGalleryAlbum;
import profamily.designsite.in.profamilies.HarajMenuProduct;
import profamily.designsite.in.profamilies.R;

/**
 * Created by mac on 4/18/17.
 */

public class commentsAdapter extends RecyclerView.Adapter<commentsAdapter.ViewHolder> {
    private Context v;
    private List<Comments> my_data;
    public commentsAdapter(Context v, List<Comments> data_list) {
        this.v = v;
        this.my_data = data_list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_comments, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final String id = my_data.get(position).getId();
        final String hasImage = my_data.get(position).getHasImage();
        holder.username.setText(my_data.get(position).getNameUser());
        holder.dateAdd.setText(my_data.get(position).getDateAdd());
        holder.comment.setText(Html.fromHtml(my_data.get(position).getComment()));
        if(hasImage.equals("1")) {
            holder.showImage.setVisibility(View.VISIBLE);
        }

        if(position % 2 == 1) {
            holder.rowData.setBackgroundColor(Color.parseColor("#fbefe7"));
        }
        holder.rowData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(hasImage.equals("1")) {
                    Intent i = new Intent(v.getContext(), HarajImageGalleryAlbum.class);
                    i.putExtra("idComment", id);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    v.getContext().startActivity(i);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return my_data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView username;
        public TextView dateAdd;
        public TextView comment;
        public TextView showImage;
        public RelativeLayout rowData;
        public ViewHolder(View itemView) {
            super(itemView);
            username = (TextView)itemView.findViewById(R.id.tv_username);
            username.setTypeface(Typeface.createFromAsset(itemView.getContext().getAssets(), globalData.getFont()));
            dateAdd = (TextView)itemView.findViewById(R.id.tv_date_add);
            dateAdd.setTypeface(Typeface.createFromAsset(itemView.getContext().getAssets(), globalData.getFont()));
            comment = (TextView)itemView.findViewById(R.id.tv_comment);
            comment.setTypeface(Typeface.createFromAsset(itemView.getContext().getAssets(), globalData.getFont()));
            showImage = (TextView)itemView.findViewById(R.id.tv_show_image);
            showImage.setTypeface(Typeface.createFromAsset(itemView.getContext().getAssets(), globalData.getFont()));
            rowData = (RelativeLayout) itemView.findViewById(R.id.rowData);

        }
    }

}
