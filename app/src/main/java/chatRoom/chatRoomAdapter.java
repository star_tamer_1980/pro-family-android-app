package chatRoom;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import libes.globalData;
import members.memberAdapter;
import profamily.designsite.in.profamilies.R;
import profamily.designsite.in.profamilies.chatUsers;
import profamily.designsite.in.profamilies.notidetails;

/**
 * Created by mac on 4/18/17.
 */

public class chatRoomAdapter extends RecyclerView.Adapter<chatRoomAdapter.ViewHolder> {
    private Context v;
    private List<chatRoom> my_data;
    public chatRoomAdapter(Context v, List<chatRoom> data_list) {
        this.v = v;
        this.my_data = data_list;
    }
    @Override
    public chatRoomAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_chat_room, parent, false);
        return new chatRoomAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(chatRoomAdapter.ViewHolder holder, final int position) {

        final String userId = my_data.get(position).getId();
        holder.title.setText(my_data.get(position).getName());
        holder.content.setText(my_data.get(position).getContent());
        holder.dateAdd.setText(my_data.get(position).getLastChatDate());
        holder.rowData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(v.getContext(), chatUsers.class);
                i.putExtra("otherUser", userId);
                i.putExtra("username", my_data.get(position).getName());
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                v.getContext().startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return my_data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView title;
        public TextView content;
        public TextView dateAdd;
        public RelativeLayout rowData;

        public ViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            title.setTypeface(Typeface.createFromAsset(itemView.getContext().getAssets(), globalData.getFont()));
            content = (TextView) itemView.findViewById(R.id.content);
            content.setTypeface(Typeface.createFromAsset(itemView.getContext().getAssets(), globalData.getFont()));
            dateAdd = (TextView) itemView.findViewById(R.id.timeData);
            dateAdd.setTypeface(Typeface.createFromAsset(itemView.getContext().getAssets(), globalData.getFont()));
            rowData = (RelativeLayout) itemView.findViewById(R.id.rowData);
        }
    }

}
