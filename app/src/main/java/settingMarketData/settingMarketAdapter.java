package settingMarketData;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.Resource;

import java.util.List;

import libes.KeyValueShared;
import profamily.designsite.in.profamilies.MarketMemberList;
import profamily.designsite.in.profamilies.R;
import profamily.designsite.in.profamilies.chatOnline;
import profamily.designsite.in.profamilies.marektPayment;
import profamily.designsite.in.profamilies.marketSupport;
import profamily.designsite.in.profamilies.marketSupportList;
import profamily.designsite.in.profamilies.menuMarket;
import profamily.designsite.in.profamilies.orders_switch;
import profamily.designsite.in.profamilies.pageDetails;
import profamily.designsite.in.profamilies.personalData;
import profamily.designsite.in.profamilies.settingsMarket;
import profamily.designsite.in.profamilies.userLogin;
import profamily.designsite.in.profamilies.userRegister;

/**
 * Created by mac on 11/28/17.
 */

public class settingMarketAdapter extends RecyclerView.Adapter<settingMarketAdapter.ViewHolder> {
    private Context v;
    private List<settingMarketModule> my_data;
    public settingMarketAdapter(Context context, List<settingMarketModule> settingsData){
        this.v = context;
        my_data = settingsData;
    }
    @Override
    public settingMarketAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_settings, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(settingMarketAdapter.ViewHolder holder, int position) {
        holder.title.setText(my_data.get(position).getCatName());
        //Glide.with(v).load(my_data.get(position).getCatImage()).into(holder.img);
        int resID = v.getResources().getIdentifier(my_data.get(position).getCatImage(), "drawable", "profamily.designsite.in.profamilies");
        holder.img.setImageResource(resID);
        final String id = my_data.get(position).getCatid();
        holder.rowData.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                    Toast.makeText(v.getContext(), id, Toast.LENGTH_SHORT).show();

                    if(id == "1"){
                        Intent i = new Intent(v.getContext(), personalData.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        v.getContext().startActivity(i);
                    }else if (id == "2"){
                        Intent i = new Intent(v.getContext(), orders_switch.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        v.getContext().startActivity(i);
                    }else if (id == "3"){
                        Intent i = new Intent(v.getContext(), MarketMemberList.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        v.getContext().startActivity(i);
                    }else if (id == "4"){
                        KeyValueShared.setId(v.getContext(), "");
                        KeyValueShared.setName(v.getContext(), "");
                        KeyValueShared.setEmail(v.getContext(), "");
                        KeyValueShared.setStatue(v.getContext(), "0");
                        KeyValueShared.setTel(v.getContext(), "");
                        KeyValueShared.setSettingsAppisNotifiComments(v.getContext(), "0");
                        KeyValueShared.setSettingsAppisNotifiOrders(v.getContext(), "0");
                        KeyValueShared.setSettingsAppisSMSComments(v.getContext(), "0");
                        KeyValueShared.setSettingsAppisSMSOrders(v.getContext(), "0");
                    }else if (id == "5"){
                        Intent i = new Intent(v.getContext(), marektPayment.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        v.getContext().startActivity(i);
                        ((menuMarket)v.getContext()).finish();
                    }else if (id == "6"){
                        Intent i = new Intent(v.getContext(), marketSupportList.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        v.getContext().startActivity(i);
                        ((menuMarket)v.getContext()).finish();
                    }else if (id == "7"){
                        Intent i = new Intent(v.getContext(), chatOnline.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        v.getContext().startActivity(i);
                    }else if (id == "8"){
                        Intent i = new Intent(v.getContext(), settingsMarket.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        v.getContext().startActivity(i);
                    }else if (id == "9"){
                        Intent i = new Intent(v.getContext(), userRegister.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        v.getContext().startActivity(i);
                    }else{
                        Intent i = new Intent(v.getContext(), userLogin.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        v.getContext().startActivity(i);
                    }

            }
        });
    }

    @Override
    public int getItemCount() {
        return my_data.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        TextView title;
        ImageView img;
        LinearLayout rowData;
        public ViewHolder(View itemView) {
            super(itemView);
            title = (TextView)itemView.findViewById(R.id.txt_title);
            img = (ImageView)itemView.findViewById(R.id.im_imotion);
            rowData = (LinearLayout)itemView.findViewById(R.id.lnData);
        }
    }
}
