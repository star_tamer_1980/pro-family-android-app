package settingMarketData;

/**
 * Created by mac on 11/28/17.
 */

public class settingMarketModule {
    private String catid, catImage, catName, activityName;
    public settingMarketModule(){}

    public String getCatid() {
        return catid;
    }

    public void setCatid(String catid) {
        this.catid = catid;
    }

    public String getCatImage() {
        return catImage;
    }

    public void setCatImage(String catImage) {
        this.catImage = catImage;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

}
