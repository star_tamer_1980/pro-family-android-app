package imageData;

/**
 * Created by mac on 4/18/17.
 */

public class ImageAlbum {
    private String img;
    private String imgURLSmall;
    private String imgURLMedium;
    private String imgURL;
    private String imgID;
    private String displayType;

    public ImageAlbum() {
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getImgURL() {
        return imgURL;
    }

    public void setImgURL(String imgURL) {
        this.imgURL = imgURL;
    }

    public String getImgURLSmall() {
        return imgURLSmall;
    }

    public void setImgURLSmall(String imgURLSmall) {
        this.imgURLSmall = imgURLSmall;
    }

    public String getImgURLMedium() {
        return imgURLMedium;
    }

    public void setImgURLMedium(String imgURLMedium) {
        this.imgURLMedium = imgURLMedium;
    }

    public String getImgID() {
        return imgID;
    }

    public void setImgID(String imgID) {
        this.imgID = imgID;
    }

    public String getDisplayType() {
        return displayType;
    }

    public void setDisplayType(String displayType) {
        this.displayType = displayType;
    }
}
