package imageData;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.List;

import profamily.designsite.in.profamilies.HarajImageGallery;
import profamily.designsite.in.profamilies.HarajImageRemove;
import profamily.designsite.in.profamilies.HarajMenuProduct;
import profamily.designsite.in.profamilies.HarajProductDetails;
import profamily.designsite.in.profamilies.R;

/**
 * Created by mac on 4/18/17.
 */

public class ImageAlbumAdapter extends RecyclerView.Adapter<ImageAlbumAdapter.ViewHolder> {
    private Context v;
    private List<ImageAlbum> my_data;
    public ImageAlbumAdapter(Context v, List<ImageAlbum> data_list) {
        this.v = v;
        this.my_data = data_list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_album_image, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final String imgName = my_data.get(position).getImgURL();
        final String imgId = my_data.get(position).getImgID();
        Glide.with(v).load(imgName).into(holder.imageView);
        holder.name.setText(imgName);
        Log.e("url is", imgName);
        //Log.e("name is", my_data.get(position).getImg());
        if(my_data.get(position).getDisplayType().toString().equals(v.getString(R.string.albumImageTypeEdit))){
            holder.imageRemove.setVisibility(View.VISIBLE);
        }
        holder.imageRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), HarajImageRemove.class);
                i.putExtra("imgId", imgId);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                v.getContext().startActivity(i);
            }
        });
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), HarajImageGallery.class);
                i.putExtra("imgName", imgName);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                v.getContext().startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return my_data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public ImageView imageView;
        public ImageView imageRemove;
        public TextView name;
        public RelativeLayout rowData;
        public ViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.image);
            imageRemove = (ImageView) itemView.findViewById(R.id.image_remove);
            name = (TextView)itemView.findViewById(R.id.tv_name);
            rowData = (RelativeLayout) itemView.findViewById(R.id.rowData);

        }
    }

}
