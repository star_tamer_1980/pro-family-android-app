package uploadFiles;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mac on 5/25/17.
 */

public class ServerResponse {
    @SerializedName("success")
    boolean success;
    @SerializedName("message")
    String message;
    @SerializedName("name")
    String imgName;

    public boolean getSuccess() {
        return success;
    }
    public String getMessage() {
        return message;
    }

    public String getImgName() {
        return imgName;
    }
}
