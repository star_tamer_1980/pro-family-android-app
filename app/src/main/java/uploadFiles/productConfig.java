package uploadFiles;

import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;

/**
 * Created by mac on 5/25/17.
 */

public interface productConfig {

    @POST("productAddImg.php")
    @Multipart
    Call<ServerResponse> upload(
            @Header("Authorization") String authorization,
            @PartMap Map<String, RequestBody> map,
            @Part("idProduct") RequestBody idUser
    );
}
