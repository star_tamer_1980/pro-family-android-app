package uploadFiles;

import java.util.Map;

import libes.globalData;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.PartMap;

/**
 * Created by mac on 5/25/17.
 */

public interface ApiConfig {

    @POST("imageAddReturnName.php")
    @Multipart
    Call<ServerResponse> upload(
            @Header("Authorization") String authorization,
            @PartMap Map<String, RequestBody> map,
            @Part("idUser") RequestBody idUser,
            @Part("isMain") RequestBody isMain,
            @Part("isVideo") RequestBody isVideo
    );
}
