package profamily.designsite.in.profamilies;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import libes.KeyValueShared;
import libes.globalData;

public class MarketAddData extends AppCompatActivity {

    TextView tvName, tvPhone, tvDesc, tvDelivery, tvMinPrice, tvWork, tvCat;
    EditText etName, etPhone, etDesc, etDelivery, etMinPrice;
    Spinner spFrom, spTo, spCat;
    Button btnSave;
    String ReqSuccess , ReqMessage, ReqId, cat;
    private ProgressDialog pDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_market_add_data);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        tvName = (TextView)findViewById(R.id.market_add_name_text);
        tvName.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        tvCat = (TextView)findViewById(R.id.market_add_cat_text);
        tvCat.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        tvPhone = (TextView)findViewById(R.id.market_add_phone_text);
        tvPhone.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        tvDesc = (TextView)findViewById(R.id.market_add_desc_text);
        tvDesc.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        tvDelivery = (TextView)findViewById(R.id.market_add_delivery_text);
        tvDelivery.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        tvMinPrice = (TextView)findViewById(R.id.market_add_min_price_text);
        tvMinPrice.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        tvWork = (TextView)findViewById(R.id.market_add_work_text);
        tvWork.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        spFrom = (Spinner)findViewById(R.id.work_from);
        spTo = (Spinner)findViewById(R.id.work_to);
        spCat = (Spinner)findViewById(R.id.market_add_cat);
        etName = (EditText) findViewById(R.id.market_add_name);
        etName.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        etPhone = (EditText) findViewById(R.id.market_add_phone);
        etPhone.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        etDesc = (EditText) findViewById(R.id.market_add_desc);
        etDesc.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        etDelivery = (EditText) findViewById(R.id.market_add_delivery);
        etDelivery.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        etMinPrice = (EditText) findViewById(R.id.market_add_min_price);
        etMinPrice.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        btnSave = (Button)findViewById(R.id.btn_save);
        btnSave.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        etName.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == 66){
                    etPhone.requestFocus();
                }
                return false;
            }
        });
        etPhone.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == 66){
                    etDesc.requestFocus();
                }
                return false;
            }
        });
        etDesc.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == 66){
                    etDelivery.requestFocus();
                }
                return false;
            }
        });
        etDelivery.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == 66){
                    etMinPrice.requestFocus();
                }
                return false;
            }
        });
        etMinPrice.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == 66){
                    spFrom.requestFocus();
                }
                return false;
            }
        });
        spFrom.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == 66){
                    spTo.requestFocus();
                }
                return false;
            }
        });
        spTo.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == 66){
                    btnSave.requestFocus();
                }
                return false;
            }
        });
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                marketProcess();
            }
        });
    }
    private void marketProcess(){
        String marketName = etName.getText().toString();
        String markePhone = etPhone.getText().toString();
        String markeDesc = etDesc.getText().toString();
        String markeDelivery = etDelivery.getText().toString();
        String markeMinPrice = etMinPrice.getText().toString();
        String workFrom = spFrom.getSelectedItem().toString();
        String workTo = spTo.getSelectedItem().toString();
        int catPosition = spCat.getSelectedItemPosition();
        if(catPosition > 3){
            catPosition = catPosition + 2;
        }
         cat = String.valueOf(catPosition);
        if(TextUtils.isEmpty(marketName)){
            etName.setError(getString(R.string.mustWriteMarketName));
        }else if (TextUtils.isEmpty(markePhone)){
            etPhone.setError(getString(R.string.mustWriteMarketPhone));
        }else if (TextUtils.isEmpty(markeDesc)){
            etDesc.setError(getString(R.string.mustWriteMarketDesc));
        }else if (TextUtils.isEmpty(markeDelivery)){
            etDelivery.setError(getString(R.string.mustWriteMarketDelivery));
        }else if (TextUtils.isEmpty(markeMinPrice)){
            etMinPrice.setError(getString(R.string.mustWriteMarketMinPrice));
        }else if (cat.equals("0")){
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.haraj_add_must_choose_cat), Toast.LENGTH_SHORT).show();
        }else{

            String data[] = new String[10];
            data[0] = globalData.URLhost() + "marketAddData.php";
            data[1] = KeyValueShared.getId(getApplicationContext());
            data[2] = marketName;
            data[3] = markePhone;
            data[4] = markeDesc;
            data[5] = markeDelivery;
            data[6] = markeMinPrice;
            data[7] = workFrom;
            data[8] = workTo;
            data[9] = cat;

            new marketAdd().execute(data);

            pDialog = new ProgressDialog(MarketAddData.this);
            pDialog.setMessage("جاري اضافة المتجر");
            pDialog.setTitle("برجاء الانتظار ثواني");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();

        }
    }
    public class marketAdd extends AsyncTask<String, Void, Boolean> {
        JSONObject jObj;
        @SuppressWarnings("deprecation")
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(9);
                nameValuePairs.add(new BasicNameValuePair("idUser", params[1]));
                nameValuePairs.add(new BasicNameValuePair("title", params[2]));
                nameValuePairs.add(new BasicNameValuePair("mobile", params[3]));
                nameValuePairs.add(new BasicNameValuePair("desc", params[4]));
                nameValuePairs.add(new BasicNameValuePair("delivery", params[5]));
                nameValuePairs.add(new BasicNameValuePair("minPrice", params[6]));
                nameValuePairs.add(new BasicNameValuePair("timeFrom", params[7]));
                nameValuePairs.add(new BasicNameValuePair("timeTo", params[8]));
                nameValuePairs.add(new BasicNameValuePair("catId", params[9]));
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if(status == 200){
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    Log.e("result is: ", data);
                    JSONObject jObj = new JSONObject(data.substring(data.indexOf("{"), data.lastIndexOf("}") + 1));
                    ReqSuccess = jObj.getString("success");
                    ReqMessage = jObj.getString("message");
                    ReqId = jObj.getString("id");

                    return true;
                }else{
                    Log.e("error client string", "here");
                }
            } catch (ClientProtocolException e) {
                Log.e("error client string", e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("error exp", e.getMessage());
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("error json", e.getMessage());
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            pDialog.dismiss();
            super.onPostExecute(result);
            if(result == false){
                Toast.makeText(getApplicationContext(), "لا يوجد بيانات " + ReqMessage, Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(getApplicationContext(), ReqMessage, Toast.LENGTH_LONG).show();
                Intent i = new Intent(MarketAddData.this, MarketAddImage.class);
                i.putExtra("idMarket", ReqId);
                i.putExtra("idCat", cat);
                startActivity(i);
                finish();
            }
        }
    }
}
