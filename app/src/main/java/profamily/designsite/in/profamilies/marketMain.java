package profamily.designsite.in.profamilies;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import libes.KeyValueShared;
import libes.footerMarketAction;
import libes.footerServiceAction;
import libes.globalData;
import marketsData.markets;
import marketsData.marketsAdapter;
import productData.Product;
import productData.ServiceAdapter;
import tab.tabs;
import tab.tabsMarketAdapter;
import tab.tabsServiceAdapter;

public class marketMain extends AppCompatActivity {
    private RecyclerView mRecyclerView1, rvTabMain;
    private tabsMarketAdapter tabMainAdapter;
    private List<tabs> data_list_tab_mainArray;
    private marketsAdapter adapter1;
    public List<markets> data_list1;
    public String dataTest, idSubCat = "0";
    private ProgressBar pBar1;
    String ReqSuccess;


    private int m_previouse_Total_Count1 = 0;
    private boolean loading = true;
    private int visibleThreshold = 15;
    int firstVisibleItem, visibleItemCount, totalItemCount;
    private TextView txtNotFound;
    LinearLayout lnHome, lnOrders, lnNoti, lnMore;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_market_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        lnHome = (LinearLayout)findViewById(R.id.btnHome);
        lnOrders = (LinearLayout)findViewById(R.id.btnOrders);
        lnNoti = (LinearLayout)findViewById(R.id.btnNoti);
        lnMore = (LinearLayout)findViewById(R.id.btnMore);
        lnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerMarketAction.footerHome(marketMain.this);
                finish();
            }
        });
        lnOrders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerMarketAction.footerOrders(marketMain.this);
                finish();
            }
        });
        lnNoti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerMarketAction.footerNoti(marketMain.this);
                finish();
            }
        });
        lnMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerMarketAction.footerMore(marketMain.this);
                finish();
            }
        });
        m_previouse_Total_Count1 = 0;
        txtNotFound = (TextView)findViewById(R.id.txtNotFound);
        mRecyclerView1 = (RecyclerView)findViewById(R.id.recycleView1);
        mRecyclerView1.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        pBar1 = (ProgressBar)findViewById(R.id.progressBar1);
        data_list1 = new ArrayList<>();
        mRecyclerView1.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager mLayoutManager = (LinearLayoutManager) mRecyclerView1
                        .getLayoutManager();
                mRecyclerView1.setLayoutManager(mLayoutManager);
                visibleItemCount = mRecyclerView1.getChildCount();
                totalItemCount = mLayoutManager.getItemCount();
                firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();
                if(totalItemCount == 0){
                    return ;
                }
                if(m_previouse_Total_Count1==totalItemCount){
                    return;
                }
                boolean loadMore = firstVisibleItem + visibleItemCount >= totalItemCount;
                if(loadMore){
                    m_previouse_Total_Count1 = totalItemCount;
                    pBar1.setVisibility(View.VISIBLE);
                    getLoadData(totalItemCount);
                }

            }
        });

        getLoadData(0);

        rvTabMain = (RecyclerView)findViewById(R.id.rvMainTab);
        rvTabMain.setHasFixedSize(true);
        rvTabMain.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        if(!KeyValueShared.getMarket_mainCat_tab(marketMain.this).equals("0")) {
            data_list_tab_mainArray = new ArrayList<>();

            String[] tabMainName = new String[0];
            String[] tabMainid = new String[0];

            if(KeyValueShared.getMarket_mainCat_tab(marketMain.this).equals("1")){
                tabMainName = getApplicationContext().getResources().getStringArray(R.array.marketEatName);
                tabMainid = getApplicationContext().getResources().getStringArray(R.array.marketEatId);
            }else if(KeyValueShared.getMarket_mainCat_tab(marketMain.this).equals("2")){
                tabMainName = getApplicationContext().getResources().getStringArray(R.array.marketSouqName);
                tabMainid = getApplicationContext().getResources().getStringArray(R.array.marketSouqId);
            }else if(KeyValueShared.getMarket_mainCat_tab(marketMain.this).equals("3")){
                tabMainName = getApplicationContext().getResources().getStringArray(R.array.marketHandmadeName);
                tabMainid = getApplicationContext().getResources().getStringArray(R.array.marketHandmadeId);
            }else if(KeyValueShared.getMarket_mainCat_tab(marketMain.this).equals("6")){
                tabMainName = getApplicationContext().getResources().getStringArray(R.array.marketAnimalName);
                tabMainid = getApplicationContext().getResources().getStringArray(R.array.marketAnimalId);
            }else if(KeyValueShared.getMarket_mainCat_tab(marketMain.this).equals("7")){
                tabMainName = getApplicationContext().getResources().getStringArray(R.array.marketVegtablesName);
                tabMainid = getApplicationContext().getResources().getStringArray(R.array.marketVegtablesId);
            }

            tabs subTabFirst = new tabs();
            subTabFirst.setId("0");
            subTabFirst.setTitle(getResources().getString(R.string.allData));
            subTabFirst.setIsTabMain("1");
            subTabFirst.setIsSelected("1");
            data_list_tab_mainArray.add(subTabFirst);
            for (int i = 0; i < tabMainName.length; i++) {
                tabs mainTab = new tabs();
                Log.e("cat id", tabMainid[i].toString());
                Log.e("cat name", tabMainName[i].toString());
                mainTab.setId(tabMainid[i].toString());
                mainTab.setTitle(tabMainName[i].toString());
                mainTab.setIsTabMain("1");
                data_list_tab_mainArray.add(mainTab);
            }
            tabMainAdapter = new tabsMarketAdapter(getApplication(), data_list_tab_mainArray);
            rvTabMain.setAdapter(tabMainAdapter);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_haraj, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id= item.getItemId();
        if(id == R.id.add){
            Intent i = new Intent(this, MarketMemberList.class);
            startActivity(i);
        } else if(id == R.id.back){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void getLoadData(Integer limit) {
        Log.e("sub cat", idSubCat);
        Log.e("main key cat", KeyValueShared.getMarket_mainCat_tab(marketMain.this));
        Log.e("main strings cat", KeyValueShared.getMarket_mainCat_tab(marketMain.this));
        pBar1.setVisibility(View.VISIBLE);
        String data[] = new String[5];
        data[0] = globalData.URLhost() + "marketFragList.php";
        data[1] = String.valueOf(limit);
        data[2] = KeyValueShared.getMarket_mainCat_tab(marketMain.this);
        data[3] = idSubCat;
        loadData1 bts = new loadData1();
        bts.execute(data);
    }


    public class loadData1 extends AsyncTask<String, Void, Boolean>{

        private String title;
        private String content;
        private String img;
        @SuppressWarnings("deprecation")
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(7);
                nameValuePairs.add(new BasicNameValuePair("limit", params[1]));
                nameValuePairs.add(new BasicNameValuePair("mapLat", KeyValueShared.getMapLat(getApplicationContext())));
                nameValuePairs.add(new BasicNameValuePair("mapLon", KeyValueShared.getMapLon(getApplicationContext())));
                nameValuePairs.add(new BasicNameValuePair("idMainCat", params[2]));
                nameValuePairs.add(new BasicNameValuePair("idSubCat", params[3]));
                nameValuePairs.add(new BasicNameValuePair("idUser", KeyValueShared.getId(getApplicationContext())));
                nameValuePairs.add(new BasicNameValuePair("token", KeyValueShared.getSettingsAppFirebaseToken(getApplicationContext())));
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if(status == 200){
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    Log.e("data is: ", data);
                    JSONObject jObj = new JSONObject(data.substring(data.indexOf("{"), data.lastIndexOf("}") + 1));
                    ReqSuccess = jObj.getString("success");
                    if(ReqSuccess.equals("1")){
                        dataTest = data;
                        if(jObj.has("data")) {
                            Log.e("url", params[0]);
                            Log.e("data", jObj.getString("data"));
                            JSONArray wee = jObj.getJSONArray("data");
                            for (int i = 0; i < wee.length(); i++) {
                                JSONObject c = wee.getJSONObject(i);
                                markets dataMarkets = new markets();
                                dataMarkets.setId(c.getString("id"));
                                dataMarkets.setName(c.getString("name"));
                                dataMarkets.setMobile(c.getString("mobile"));
                                dataMarkets.setMiniMumCharge(c.getString("miniMumCharge"));
                                dataMarkets.setMapLat(c.getString("mapLat"));
                                dataMarkets.setMapLong(c.getString("mapLon"));
                                dataMarkets.setDescribtions(c.getString("describtions"));
                                dataMarkets.setRating(c.getString("rating"));
                                dataMarkets.setNoRate(c.getString("noRating"));
                                dataMarkets.setMainCat(c.getString("cat"));
                                dataMarkets.setIdMainCat(c.getString("idMainCat"));
                                dataMarkets.setImg(c.getString("img"));
                                dataMarkets.setDelivery(c.getString("delivery"));
                                dataMarkets.setIsWork(c.getString("isWork"));
                                dataMarkets.setIdOwner(c.getString("idOwner"));
                                dataMarkets.setDistance(c.getString("distance"));
                                dataMarkets.setIdLastOrder(c.getString("idLastOrder"));
                                dataMarkets.setCountVideo(c.getString("countVedio"));
                                dataMarkets.setNumChat(c.getString("numChat"));
                                data_list1.add(dataMarkets);
                            }
                        }

                    }


                    return true;
                }else{
                    Log.e("error client string", "here");
                }
            } catch (ClientProtocolException e) {
                Log.e("error client string", e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("error exp", e.getMessage());
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("error json", e.getMessage());
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if(result == false){
                Toast.makeText(getApplicationContext(), "حدث خطأ، نرجو المحاوله في وقت لاحق", Toast.LENGTH_LONG).show();
            }else{
                if(ReqSuccess.equals("1")){
                    txtNotFound.setVisibility(View.INVISIBLE);
                    if(m_previouse_Total_Count1>0) {
                        adapter1.notifyDataSetChanged();
                        pBar1.setVisibility(View.INVISIBLE);
                    }else{
                        adapter1 = new marketsAdapter(getApplicationContext(), data_list1);
                        mRecyclerView1.setAdapter(adapter1);
                        pBar1.setVisibility(View.INVISIBLE);
                    }
                }else{
                    pBar1.setVisibility(View.INVISIBLE);
                }
            }
        }
    }
}
