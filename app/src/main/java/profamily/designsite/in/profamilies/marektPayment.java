package profamily.designsite.in.profamilies;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import libes.footerMarketAction;

public class marektPayment extends AppCompatActivity {

    LinearLayout lnHome, lnOrders, lnNoti, lnMore;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marekt_payment);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        lnHome = (LinearLayout)findViewById(R.id.btnHome);
        lnOrders = (LinearLayout)findViewById(R.id.btnOrders);
        lnNoti = (LinearLayout)findViewById(R.id.btnNoti);
        lnMore = (LinearLayout)findViewById(R.id.btnMore);
        lnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerMarketAction.footerHome(marektPayment.this);
                finish();
            }
        });
        lnOrders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerMarketAction.footerOrders(marektPayment.this);
                finish();
            }
        });
        lnNoti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerMarketAction.footerNoti(marektPayment.this);
                finish();
            }
        });
        lnMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerMarketAction.footerMore(marektPayment.this);
                finish();
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_haraj, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id= item.getItemId();
        if(id == R.id.add){
            Intent i = new Intent(this, MarketMemberList.class);
            startActivity(i);
        } else if(id == R.id.back){
            Intent i = new Intent(this, menuMarket.class);
            startActivity(i);
        }
        return super.onOptionsItemSelected(item);
    }
}
