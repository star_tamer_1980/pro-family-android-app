package profamily.designsite.in.profamilies;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cartSQLiteData.CartSQLite;
import cartSQLiteData.CartSQLiteAdapter;
import libes.KeyValueShared;
import libes.SQLiteConnection;
import libes.globalData;
import productData.Product;
import productData.ProductAdapter;

public class cart extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private CartSQLiteAdapter adapter;
    private List<CartSQLite> data_list;
    private TextView txtNotFound, tvCartDelivery, tvCartPrice;
    LinearLayout lnDelivery, lnAllPrice;
    SQLiteConnection db;
    Button buyOnLine;
    String ReqSuccess, ReqMessage;
    private ProgressDialog pDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        pDialog = new ProgressDialog(cart.this);
        pDialog.setMessage("جاري ارسال الطلب");
        pDialog.setTitle("برجاء الانتظار ثواني");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
        lnDelivery = (LinearLayout)findViewById(R.id.lnDelivery);
        lnAllPrice = (LinearLayout)findViewById(R.id.lnAllPrice);
        buyOnLine = (Button)findViewById(R.id.btn_buy);
        buyOnLine.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        buyOnLine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(cart.this, cartLocation.class);
                startActivity(i);
                /*
                if(KeyValueShared.getStatue(getApplicationContext()) != "0"){
                    List<CartSQLite> dataCart = db.getCart();
                    String dataJson = new Gson().toJson(dataCart);
                    Toast.makeText(getApplicationContext(), dataJson, Toast.LENGTH_SHORT).show();
                    Log.e("json is: ", dataJson);
                    String data[] = new String[4];
                    data[0] = globalData.URLhost() + "ordersAdd.php";
                    data[1] = KeyValueShared.getId(getApplicationContext());
                    data[2] = KeyValueShared.getMarkeId(getApplicationContext());
                    data[3] = dataJson;

                    new ordersAddAsynTask().execute(data);

                    pDialog.show();

                }else{
                    Intent i = new Intent(cart.this, userLogin.class);
                    startActivity(i);
                }
                */
            }
        });
        tvCartDelivery = (TextView)findViewById(R.id.tv_cart_delivery);
        tvCartDelivery.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        tvCartPrice = (TextView)findViewById(R.id.tv_cart_price_finish);
        tvCartPrice.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        txtNotFound = (TextView)findViewById(R.id.txtNotFound);
        txtNotFound.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        mRecyclerView = (RecyclerView)findViewById(R.id.recycleView1);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        db = new SQLiteConnection(getApplicationContext());
        data_list = db.getAllRecord();
        if(data_list.size() > 0) {
            adapter = new CartSQLiteAdapter(getApplicationContext(), data_list);
            mRecyclerView.setAdapter(adapter);
            txtNotFound.setVisibility(View.GONE);
            buyOnLine.setVisibility(View.VISIBLE);
            lnDelivery.setVisibility(View.VISIBLE);
            lnAllPrice.setVisibility(View.VISIBLE);
            tvCartDelivery.setText(KeyValueShared.getMarketDeliveryPrice(getApplicationContext()));
            int finalPrice = Integer.valueOf(db.getAllPrice()) + Integer.valueOf(KeyValueShared.getMarketDeliveryPrice(getApplicationContext()));
            tvCartPrice.setText(String.valueOf(finalPrice));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menubar_haraj_back, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id= item.getItemId();
        if(id == R.id.back){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    protected void onRestart() {
        refresh();
        super.onRestart();
    }

    public void refresh(){
        finish();
        startActivity(getIntent());
    }

}
