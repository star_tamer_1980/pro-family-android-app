package profamily.designsite.in.profamilies;

import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import libes.KeyValueShared;
import libes.globalData;

public class OrdersRate extends AppCompatActivity {

    RatingBar ratingBar;
    TextView isRate,rateIs,rateFrom;
    EditText rateComment;
    Button save;
    int rate = 0;
    private String ReqSuccess , ReqMessage;
    private ProgressDialog pDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders_rate);
        ratingBar = (RatingBar)findViewById(R.id.ratingBar);
        isRate = (TextView)findViewById(R.id.isRate);
        isRate.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        rateIs = (TextView)findViewById(R.id.rateIs);
        rateIs.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        rateFrom = (TextView)findViewById(R.id.rateFrom);
        rateFrom.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        rateComment = (EditText)findViewById(R.id.et_rate_comment);
        rateComment.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        save = (Button)findViewById(R.id.btn_save);
        save.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        Toast.makeText(getApplicationContext(), "idOrder is: " + getIntent().getStringExtra("idOrder"), Toast.LENGTH_SHORT).show();
        Toast.makeText(getApplicationContext(), "typeFinish is: " + getIntent().getStringExtra("typeFinish"), Toast.LENGTH_SHORT).show();
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                rateIs.setText(String.valueOf(Math.round(rating)));
                rate = Math.round(rating);
            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String data[] = new String[5];
                data[0] = globalData.URLhost() + "orderRate.php";
                data[1] = KeyValueShared.getId(getApplicationContext());
                data[2] = getIntent().getStringExtra("idOrder");
                data[3] = String.valueOf(rateComment.getText());
                data[4] = String.valueOf(rate);
                new rateAsynTask().execute(data);
                pDialog = new ProgressDialog(OrdersRate.this);
                pDialog.setMessage("جاري التقييم");
                pDialog.setTitle("برجاء الانتظار ثواني");
                pDialog.setIndeterminate(false);
                pDialog.setCancelable(true);
                pDialog.show();
            }
        });
        String data[] = new String[3];
        data[0] = globalData.URLhost() + "orderFinish.php";
        data[1] = getIntent().getStringExtra("typeFinish");
        data[2] = getIntent().getStringExtra("idOrder");

        new orderFinishAsynTask().execute(data);

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("جاري انهاء الطلب");
        pDialog.setTitle("برجاء الانتظار ثواني");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
        pDialog.show();
    }
    public class orderFinishAsynTask extends AsyncTask<String, Void, Boolean> {
        JSONObject jObj;
        @SuppressWarnings("deprecation")
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                nameValuePairs.add(new BasicNameValuePair("typeFinish", params[1]));
                nameValuePairs.add(new BasicNameValuePair("idOrder", params[2]));
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if(status == 200){
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    Log.e("data is: ", data);
                    JSONObject jObj = new JSONObject(data.substring(data.indexOf("{"), data.lastIndexOf("}") + 1));
                    ReqSuccess = jObj.getString("success");
                    ReqMessage = jObj.getString("message");
                    return true;
                }else{
                    Log.e("error client string", "here");
                }
            } catch (ClientProtocolException e) {
                Log.e("error client string", e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("error exp", e.getMessage());
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("error json", e.getMessage());
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            pDialog.dismiss();
            super.onPostExecute(result);
            if(result == false){
                Toast.makeText(getApplicationContext(), "لا يوجد بيانات " + ReqMessage, Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(getApplicationContext(), ReqMessage, Toast.LENGTH_LONG).show();
            }
        }
    }

    public class rateAsynTask extends AsyncTask<String, Void, Boolean> {
        JSONObject jObj;
        @SuppressWarnings("deprecation")
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(4);
                nameValuePairs.add(new BasicNameValuePair("idUser", params[1]));
                nameValuePairs.add(new BasicNameValuePair("idOrder", params[2]));
                nameValuePairs.add(new BasicNameValuePair("comment", params[3]));
                nameValuePairs.add(new BasicNameValuePair("rate", params[4]));
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if(status == 200){
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    Log.e("data is: ", data);
                    JSONObject jObj = new JSONObject(data.substring(data.indexOf("{"), data.lastIndexOf("}") + 1));
                    ReqSuccess = jObj.getString("success");
                    ReqMessage = jObj.getString("message");
                    return true;
                }else{
                    Log.e("error client string", "here");
                }
            } catch (ClientProtocolException e) {
                Log.e("error client string", e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("error exp", e.getMessage());
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("error json", e.getMessage());
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            pDialog.dismiss();
            super.onPostExecute(result);
            if(result == false){
                Toast.makeText(getApplicationContext(), "لا يوجد بيانات " + ReqMessage, Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(getApplicationContext(), ReqMessage, Toast.LENGTH_LONG).show();
                finish();
            }
        }
    }
}
