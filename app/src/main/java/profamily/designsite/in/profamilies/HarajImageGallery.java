package profamily.designsite.in.profamilies;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

public class HarajImageGallery extends AppCompatActivity {

    private ImageView imgView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_haraj_image_gallery);
        imgView = (ImageView)findViewById(R.id.im_image);
        Glide.with(this).load(getIntent().getStringExtra("imgName").toString()).into(imgView);
    }
}
