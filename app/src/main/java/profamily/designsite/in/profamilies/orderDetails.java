package profamily.designsite.in.profamilies;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.constraint.solver.SolverVariable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import libes.globalData;

public class orderDetails extends AppCompatActivity {

    TextView tvTitle, tvTimeAdd, tvPriceFinish, tvPriceDelivery, priceAll, tvStringPrice, tvStringPriceFinish, tvStringDelivery, tvStringPriceAll, tvStatue;
    Button btnCancel, btnFinish;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);
        tvTitle = (TextView)findViewById(R.id.tv_title);
        tvTitle.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        tvStringPrice = (TextView)findViewById(R.id.tv_string_price);
        tvStringPrice.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        tvStringPriceFinish = (TextView)findViewById(R.id.tv_price_finish_string);
        tvStringPriceFinish.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        tvStringDelivery = (TextView)findViewById(R.id.tv_price_delivery_string);
        tvStringDelivery.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        tvStringPriceAll = (TextView)findViewById(R.id.tv_price_all_string);
        tvStringPriceAll.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        tvPriceFinish = (TextView)findViewById(R.id.tv_price_finish);
        tvPriceFinish.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        tvPriceDelivery = (TextView)findViewById(R.id.tv_price_delivery);
        tvPriceDelivery.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        tvStatue = (TextView)findViewById(R.id.tv_statue);
        tvStatue.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        tvStatue.setText(getIntent().getStringExtra("statueTxt"));
        priceAll = (TextView)findViewById(R.id.tv_price_all);
        priceAll.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        tvTimeAdd = (TextView)findViewById(R.id.tv_time_add);
        tvTimeAdd.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        tvTitle.setText(getIntent().getStringExtra("title"));
        tvTimeAdd.setText(getIntent().getStringExtra("timeAdd"));
        tvPriceFinish.setText(String.valueOf(Integer.valueOf(getIntent().getStringExtra("price")) - Integer.valueOf(getIntent().getStringExtra("deliveryPrice"))));
        priceAll.setText(getIntent().getStringExtra("price"));
        tvPriceDelivery.setText(getIntent().getStringExtra("deliveryPrice"));
        btnCancel = (Button)findViewById(R.id.btn_order_cancel);
        btnCancel.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        btnFinish = (Button)findViewById(R.id.btn_order_finish);
        btnFinish.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        if(getIntent().getStringExtra("statue").equals("0")){
            btnCancel.setVisibility(View.VISIBLE);
        }
        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(orderDetails.this, OrdersRate.class);
                i.putExtra("typeFinish", "4");
                i.putExtra("idOrder", getIntent().getStringExtra("id"));
                startActivity(i);
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(orderDetails.this, OrdersRate.class);
                i.putExtra("typeFinish", "5");
                i.putExtra("idOrder", getIntent().getStringExtra("id"));
                startActivity(i);
            }
        });
    }
}
