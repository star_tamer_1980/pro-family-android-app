package profamily.designsite.in.profamilies;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import categoryData.catAdapter;
import categoryData.category;
import libes.KeyValueShared;
import libes.globalData;
import marketsData.markets;
import marketsData.marketsAdapter;

public class MarketAddSubCat extends AppCompatActivity {

    String idMarket, idMainCat;
    private RecyclerView mRecyclerView1;
    private catAdapter adapter1;
    private List<category> data_list1;
    List<String> listCountry = new ArrayList<String>();
    List<String> listCatId = new ArrayList<String>();
    ArrayList<String> TypeCountryArray = new ArrayList<String>();
    ArrayAdapter<String> adapterCountry;
    Spinner subCat;
    private String dataTest;
    private ProgressBar pBar1;
    String ReqSuccess;
    String fileApi = "marketCatData.php";
    int firstVisibleItem, visibleItemCount, totalItemCount;
    private TextView txtNotFound, titleCat;
    Button save, goToMap;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_market_add_sub_cat);
        idMarket = getIntent().getStringExtra("idMarket");
        idMainCat = getIntent().getStringExtra("idCat");
        txtNotFound = (TextView)findViewById(R.id.txtNotFound);
        txtNotFound.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        titleCat = (TextView)findViewById(R.id.market_add_cat_text);
        titleCat.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        mRecyclerView1 = (RecyclerView)findViewById(R.id.recycleView1);
        mRecyclerView1.setLayoutManager(new LinearLayoutManager(this));
        pBar1 = (ProgressBar)findViewById(R.id.progressBar1);
        data_list1 = new ArrayList<>();
        subCat = (Spinner)findViewById(R.id.market_add_cat);
        adapterCountry = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, TypeCountryArray);
        adapterCountry.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        subCat.setAdapter(adapterCountry);
        goToMap = (Button)findViewById(R.id.btn_go_map);
        goToMap.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        goToMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MarketAddSubCat.this, MarketAddMap.class);
                i.putExtra("idMarket", idMarket);
                startActivity(i);
            }
        });
        save = (Button)findViewById(R.id.btn_save);
        save.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title = subCat.getSelectedItem().toString();
                String titleId = listCatId.get(subCat.getSelectedItemPosition()).toString();
                Toast.makeText(getApplicationContext(), titleId, Toast.LENGTH_SHORT).show();
                String data[] = new String[4];
                data[0] = globalData.URLhost() + fileApi;
                data[1] = idMarket;
                data[2] = idMainCat;
                data[3] = listCatId.get(subCat.getSelectedItemPosition()).toString();
                loadData1 bts = new loadData1();
                bts.execute(data);
            }
        });
        String data[] = new String[4];
        data[0] = globalData.URLhost() + fileApi;
        data[1] = idMarket;
        data[2] = idMainCat;
        data[3] = "0";
        loadData1 bts = new loadData1();
        bts.execute(data);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        finish();
        startActivity(getIntent());
    }

    public class loadData1 extends AsyncTask<String, Void, Boolean> {

        @SuppressWarnings("deprecation")
        @Override
        protected Boolean doInBackground(String... params) {
            data_list1.clear();
            listCountry.clear();
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(3);
                nameValuePairs.add(new BasicNameValuePair("idMarket", params[1]));
                nameValuePairs.add(new BasicNameValuePair("idMainCat", params[2]));
                nameValuePairs.add(new BasicNameValuePair("idSubCat", params[3]));
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if(status == 200){
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    Log.e("error", data);
                    JSONObject jObj = new JSONObject(data.substring(data.indexOf("{"), data.lastIndexOf("}") + 1));
                    ReqSuccess = jObj.getString("success");
                    if(ReqSuccess.equals("1")){
                        dataTest = data;
                        if(jObj.has("cat")) {
                            JSONArray category = jObj.getJSONArray("cat");
                            Log.e("countries is: ", category.toString());
                            for (int i = 0; i < category.length(); i++) {
                                JSONObject c = category.getJSONObject(i);
                                listCountry.add(c.getString("title"));
                                listCatId.add(c.getString("id"));
                            }
                            JSONArray wee = jObj.getJSONArray("data");
                            Log.e("cat is: ", wee.toString());
                            for (int i = 0; i < wee.length(); i++) {
                                JSONObject c = wee.getJSONObject(i);
                                category dataCategory = new category();
                                dataCategory.setId(c.getString("id"));
                                dataCategory.setTitle(c.getString("title"));
                                data_list1.add(dataCategory);
                            }
                        }

                    }


                    return true;
                }else{
                    Log.e("error client string", "here");
                }
            } catch (ClientProtocolException e) {
                Log.e("error client string", e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("error exp", e.getMessage());
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("error json", e.getMessage());
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if(result == false){
                Toast.makeText(getApplicationContext(), "حدث خطأ، نرجو المحاوله في وقت لاحق", Toast.LENGTH_LONG).show();
            }else{
                if(ReqSuccess.equals("1")){
                    txtNotFound.setVisibility(View.INVISIBLE);
                    Toast.makeText(getApplicationContext(), "data set", Toast.LENGTH_SHORT).show();
                    TypeCountryArray.clear();
                    TypeCountryArray.addAll(listCountry);
                    adapterCountry.notifyDataSetChanged();

                        adapter1 = new catAdapter(getApplicationContext(), data_list1);
                        mRecyclerView1.setAdapter(adapter1);
                        pBar1.setVisibility(View.INVISIBLE);

                }else{
                    pBar1.setVisibility(View.INVISIBLE);
                }
            }
        }
    }
}
