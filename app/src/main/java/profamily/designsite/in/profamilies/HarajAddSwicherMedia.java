package profamily.designsite.in.profamilies;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

import libes.KeyValueShared;
import menuSettings.menuSettings;
import menuSettings.menuAddAdapter;

public class HarajAddSwicherMedia extends AppCompatActivity {


    private RecyclerView mvMenu;

    private menuAddAdapter menuAdapter;
    private List<menuSettings> menuArray;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_haraj_add_swicher_media);
        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        mvMenu = (RecyclerView)findViewById(R.id.menuHarajAdd);
        mvMenu.setHasFixedSize(true);
        mvMenu.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        setMenu();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menubar_haraj_back, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.back){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
    public void setMenu(){
        String[] menuName;
        String[] menuId;
        menuArray = new ArrayList<>();
        menuName = getApplicationContext().getResources().getStringArray(R.array.tabMain_categoryName);
        menuId = getApplicationContext().getResources().getStringArray(R.array.tabMain_categoryId);
        for(int i=0; i < menuName.length; i++){
            menuSettings menuData = new menuSettings();
            menuData.setMenuId(menuId[i].toString());
            menuData.setMenuName(menuName[i].toString());
            menuArray.add(menuData);
        }
        menuAdapter = new menuAddAdapter(getApplication(), menuArray);
        mvMenu.setAdapter(menuAdapter);
    }
}
