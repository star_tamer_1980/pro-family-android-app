package profamily.designsite.in.profamilies;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import categoryData.category;
import categoryData.categoryAdapter;
import libes.globalData;

public class MarketEditSwitch extends Activity {

    String id, idCat, img, ReqSuccess, ReqMessage;
    Button productAdd, marketEdit, marketEditImage, marketEditCat, marketEditMap, marketSnap, marketStop, marketRun, marketDelete;
    private ProgressDialog pDialog;
    String stName, stCatName, stImg, stDesc, stMobil, stTimeFrom, stTimeTo, stIsActive, stIsWork, stMapLat, stMapLon, stMinimumCharge, stDelivery, stPhone;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_market_edit_switch);
        id = getIntent().getStringExtra("idMarket");
        idCat = getIntent().getStringExtra("idCat");
        img = getIntent().getStringExtra("img");
        String data[] = new String[2];
        data[0] = globalData.URLhost() + "marketDetailsData.php";
        data[1] = id;

        new ProgressTask().execute(data);
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("جاري قراءة البيانات");
        pDialog.setTitle("الرجاء الانتظار");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
        pDialog.show();
        productAdd = (Button)findViewById(R.id.btn_market_add_product);
        productAdd.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        marketEdit = (Button)findViewById(R.id.btn_market_edit);
        marketEdit.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        marketEditImage = (Button)findViewById(R.id.btn_market_image);
        marketEditImage.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        marketEditCat = (Button)findViewById(R.id.btn_market_cat_edit);
        marketEditCat.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        marketEditMap = (Button)findViewById(R.id.btn_market_map_edit);
        marketEditMap.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        marketSnap = (Button)findViewById(R.id.btn_market_snap);
        marketSnap.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        marketStop = (Button)findViewById(R.id.btn_market_stop);
        marketStop.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        marketRun = (Button)findViewById(R.id.btn_market_run);
        marketRun.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        marketDelete = (Button)findViewById(R.id.btn_market_delete);
        marketDelete.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        productAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MarketEditSwitch.this, ProductEditList.class);
                i.putExtra("idMarket", id);
                i.putExtra("idMainCat", idCat);
                startActivity(i);
            }
        });
        marketEditImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MarketEditSwitch.this, MarketAddImage.class);
                i.putExtra("idMarket", id);
                i.putExtra("idCat", idCat);
                i.putExtra("img", img);
                startActivity(i);
            }
        });
        marketEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MarketEditSwitch.this, MarketDetailsEdit.class);
                i.putExtra("idMarket", id);
                i.putExtra("marketName", stName);
                i.putExtra("marketPhone", stPhone);
                i.putExtra("marketDesc", stDesc);
                i.putExtra("marketDelivery", stDelivery);
                i.putExtra("marketMinPrice", stMinimumCharge);
                i.putExtra("marketWorkFrom", stTimeFrom);
                i.putExtra("marketWorkTo", stTimeTo);
                i.putExtra("marketCatName", stCatName);
                startActivity(i);
            }
        });
        marketStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String data[] = new String[3];
                data[0] = globalData.URLhost() + "marketEditIsWork.php";
                data[1] = id;
                data[2] = "1";
                changeIsWork bts = new changeIsWork();
                bts.execute(data);
                pDialog = new ProgressDialog(MarketEditSwitch.this);
                pDialog.setMessage("جاري ايقاف المتجر");
                pDialog.setTitle("برو فاميليز");
                pDialog.setIndeterminate(false);
                pDialog.setCancelable(true);
                pDialog.show();
                marketStop.setVisibility(View.GONE);
                marketRun.setVisibility(View.VISIBLE);
            }
        });
        marketRun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String data[] = new String[3];
                data[0] = globalData.URLhost() + "marketEditIsWork.php";
                data[1] = id;
                data[2] = "0";
                changeIsWork bts = new changeIsWork();
                bts.execute(data);
                pDialog = new ProgressDialog(MarketEditSwitch.this);
                pDialog.setMessage("جاري تشغيل المتجر");
                pDialog.setTitle("برو فاميليز");
                pDialog.setIndeterminate(false);
                pDialog.setCancelable(true);
                pDialog.show();
                marketStop.setVisibility(View.VISIBLE);
                marketRun.setVisibility(View.GONE);
            }
        });
        marketEditCat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MarketEditSwitch.this, MarketAddSubCat.class);
                i.putExtra("idMarket", id);
                i.putExtra("idCat", idCat);
                startActivity(i);
            }
        });
        marketEditMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), stMapLat, Toast.LENGTH_SHORT).show();
                Toast.makeText(getApplicationContext(), stMapLon, Toast.LENGTH_SHORT).show();
                Intent i = new Intent(MarketEditSwitch.this, MarketAddMap.class);
                i.putExtra("idMarket", id);
                i.putExtra("mapLat", stMapLat);
                i.putExtra("mapLon", stMapLon);
                startActivity(i);
            }
        });
        marketDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    builder = new AlertDialog.Builder(MarketEditSwitch.this);
                } else {
                    builder = new AlertDialog.Builder(MarketEditSwitch.this);
                }
                builder.setTitle(getResources().getString(R.string.alert_title_order_delete))
                        .setMessage(getResources().getString(R.string.alert_message_order_delete))
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                String data[] = new String[2];
                                data[0] = globalData.URLhost() + "marketDelete.php";
                                data[1] = id;
                                deleteMarket bts = new deleteMarket();
                                bts.execute(data);
                                pDialog = new ProgressDialog(MarketEditSwitch.this);
                                pDialog.setMessage("جاري حذف المتجر");
                                pDialog.setTitle("برو فاميليز");
                                pDialog.setIndeterminate(false);
                                pDialog.setCancelable(true);
                                pDialog.show();
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Toast.makeText(getApplicationContext(), "delete no", Toast.LENGTH_LONG).show();
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        finish();
        startActivity(getIntent());
    }

    public class ProgressTask extends AsyncTask<String, Void, Boolean> {

        @SuppressWarnings("deprecation")
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
                nameValuePairs.add(new BasicNameValuePair("id", params[1]));
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));

                HttpResponse response = client.execute(post);

                int status = response.getStatusLine().getStatusCode();
                if(status == 200){
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    Log.e("error client string", data.toString());
                    JSONObject jObj = new JSONObject(data.substring(data.indexOf("{"), data.lastIndexOf("}") + 1));
                    ReqSuccess = jObj.getString("success");
                    if(ReqSuccess.equals("1")){
                        stName  = jObj.getString("name");
                        stImg  = jObj.getString("img");
                        stDesc  = jObj.getString("describtions");
                        stMobil  = jObj.getString("mobile");
                        stTimeFrom  = jObj.getString("timeFrom");
                        stTimeTo  = jObj.getString("timeTo");
                        stIsActive  = jObj.getString("isActive");
                        stIsWork  = jObj.getString("isWork");
                        stMapLat  = jObj.getString("mapLat");
                        stMapLon  = jObj.getString("mapLon");
                        stMinimumCharge  = jObj.getString("miniCharge");
                        stDelivery  = jObj.getString("delivery");
                        stPhone = jObj.getString("phone");
                        stCatName = jObj.getString("cat");
                    }


                    return true;
                }else{
                    Log.e("error client string", "here");
                }
            } catch (ClientProtocolException e) {
                Log.e("error client string", e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("error exp", e.getMessage());
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("error json", e.getMessage());
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            pDialog.dismiss();
            super.onPostExecute(result);
            if(result == false){
                Toast.makeText(getApplicationContext(), "حدث خطأ، نرجو المحاوله في وقت لاحق", Toast.LENGTH_LONG).show();
            }else{
                if(stIsWork.equals("0")){
                    marketRun.setVisibility(View.GONE);
                    marketStop.setVisibility(View.VISIBLE);
                }else{
                    marketRun.setVisibility(View.VISIBLE);
                    marketStop.setVisibility(View.GONE);
                }
            }
        }
    }
    public class deleteMarket extends AsyncTask<String, Void, Boolean> {


        @SuppressWarnings("deprecation")
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
                nameValuePairs.add(new BasicNameValuePair("idMarket", params[1]));
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if(status == 200){
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    Log.e("markets", data);
                    JSONObject jObj = new JSONObject(data.substring(data.indexOf("{"), data.lastIndexOf("}") + 1));
                    ReqSuccess = jObj.getString("success");
                    ReqMessage = jObj.getString("message");
                    return true;
                }else{
                    Log.e("error client string", "here");
                }
            } catch (ClientProtocolException e) {
                Log.e("error client string", e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("error exp", e.getMessage());
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("error json", e.getMessage());
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            pDialog.dismiss();
            super.onPostExecute(result);
            if(result == false){
                Toast.makeText(getApplicationContext(), "حدث خطأ، نرجو المحاوله في وقت لاحق", Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(getApplicationContext(), ReqMessage, Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }
    public class changeIsWork extends AsyncTask<String, Void, Boolean> {


        @SuppressWarnings("deprecation")
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                nameValuePairs.add(new BasicNameValuePair("id", params[1]));
                nameValuePairs.add(new BasicNameValuePair("isWork", params[2]));
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if(status == 200){
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    Log.e("markets", data);
                    JSONObject jObj = new JSONObject(data.substring(data.indexOf("{"), data.lastIndexOf("}") + 1));
                    ReqSuccess = jObj.getString("success");
                    ReqMessage = jObj.getString("message");
                    return true;
                }else{
                    Log.e("error client string", "here");
                }
            } catch (ClientProtocolException e) {
                Log.e("error client string", e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("error exp", e.getMessage());
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("error json", e.getMessage());
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            pDialog.dismiss();
            super.onPostExecute(result);
            if(result == false){
                Toast.makeText(getApplicationContext(), "حدث خطأ، نرجو المحاوله في وقت لاحق", Toast.LENGTH_LONG).show();
            }
        }
    }
}
