package profamily.designsite.in.profamilies;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Window;
import android.widget.MediaController;
import android.widget.VideoView;

public class videoPlay extends AppCompatActivity {
    VideoView videoplay;
    String url;
    MediaController mediac;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_video_play);
        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        url = getIntent().getExtras().getString("urlVideo");
        videoplay = (VideoView)findViewById(R.id.video);
        mediac = new MediaController(this);
        Uri uri = Uri.parse(url);
        videoplay.setVideoURI(uri);
        videoplay.start();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_haraj, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.add){
            Intent i = new Intent(videoPlay.this, HarajAddSwicherMedia.class);
            startActivity(i);
        } else if(id == R.id.back){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
