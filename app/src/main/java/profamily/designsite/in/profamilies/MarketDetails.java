package profamily.designsite.in.profamilies;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import categoryData.category;
import categoryData.categoryAdapter;
import libes.KeyValueShared;
import libes.SQLiteConnection;
import libes.footerMarketAction;
import libes.footerServiceAction;
import libes.globalData;
import marketsData.markets;
import subCatData.subCat;
import subCatData.subCatAdapter;

public class MarketDetails extends AppCompatActivity {

    String marketId, marketName, marketMobile, marketMiniMumCharge, marketMapLat, marketMapLong, marketDescribtion, marketRating, marketNoRate, marketIdMainCat, marketMainCatName, marketImg, marketDelivery, marketDistance, marketIdLastOrder, marketCountVideo, marketNumChat, marketIsWork, marketIdOwner, ReqSuccess;

    TextView tvTitle, tvMarketMainCat, tvMarketRoad, tvMarketMinPrice, tvMarketDelivery;
    ImageView imgImage;
    RatingBar rBar;
    Button btnMobileCall, btnMySnap, btnMarketRating, btn_cart, btn_edit;
    private RecyclerView mRecyclerView1;
    private categoryAdapter adapter1;
    private List<category> data_list1;
    private String dataTest;
    private ProgressDialog pDialog;
    SQLiteConnection db;
    LinearLayout lnHome, lnOrders, lnNoti, lnMore, ln_mobile_data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_market_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        btnMobileCall = (Button) findViewById(R.id.btnMobileCall);
        tvTitle = (TextView) findViewById(R.id.tv_market_title);
        tvMarketMainCat = (TextView) findViewById(R.id.tv_market_main_cat);
        tvMarketRoad = (TextView) findViewById(R.id.tv_market_data_road);
        tvMarketMinPrice = (TextView) findViewById(R.id.tv_market_price_mini);
        tvMarketDelivery = (TextView) findViewById(R.id.tv_market_delivery);
        rBar = (RatingBar) findViewById(R.id.ratingBar);
        imgImage = (ImageView)findViewById(R.id.im_market_image);
        ln_mobile_data = (LinearLayout) findViewById(R.id.ln_mobile_data);
        btn_edit = (Button) findViewById(R.id.btn_edit);
        btnMySnap = (Button) findViewById(R.id.btn_market_my_snap);
        btnMarketRating = (Button) findViewById(R.id.btn_rating);
        btn_cart = (Button) findViewById(R.id.btn_cart);
        mRecyclerView1 = (RecyclerView)findViewById(R.id.recycleView);
        mRecyclerView1.setLayoutManager(new LinearLayoutManager(this));
        data_list1 = new ArrayList<>();
        marketId = getIntent().getStringExtra("idMarket");
        if(getIntent().hasExtra("title")){
            setDetails();
            String data[] = new String[2];
            data[0] = globalData.URLhost() + "marketDetails.php";
            data[1] = marketId;
            loadData1 bts = new loadData1();
            bts.execute(data);
            pDialog = new ProgressDialog(this);
            pDialog.setMessage("جاري تحميل البيانات");
            pDialog.setTitle("برو فاميليز");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }else{
            String dataDetails[] = new String[2];
            dataDetails[0] = globalData.URLhost() + "marketDetailsData.php";
            dataDetails[1] = marketId;
            loadDetails btsDetailes = new loadDetails();
            btsDetailes.execute(dataDetails);
            pDialog = new ProgressDialog(this);
            pDialog.setMessage("جاري تحميل البيانات");
            pDialog.setTitle("برو فاميليز");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();

        }
        if (KeyValueShared.getId(getApplicationContext()).equals(marketIdOwner)) {
            btn_edit.setVisibility(View.VISIBLE);
        }
        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MarketDetails.this, MarketEditSwitch.class);
                i.putExtra("idMarket", marketId);
                i.putExtra("idCat", marketIdMainCat);
                i.putExtra("img", marketImg);
                startActivity(i);
            }
        });
        btnMySnap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MarketDetails.this, marketMySnap.class);
                startActivity(i);
            }
        });
        btnMarketRating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MarketDetails.this, marketListRating.class);
                i.putExtra("idMarket", marketId);
                startActivity(i);
            }
        });
        btn_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MarketDetails.this, cart.class);
                startActivity(i);
            }
        });
        db = new SQLiteConnection(getApplicationContext());
        if (db.getAllPrice() != "0" && db.getAllPrice() != null) {
            changeCart();
        }
        btnMobileCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(MarketDetails.this);
                builder1.setMessage(getResources().getString(R.string.alertMessageCall));
                builder1.setTitle(getResources().getString(R.string.alertTitleCall));
                builder1.setCancelable(true);

                builder1.setNeutralButton(
                        getResources().getString(R.string.alertCall),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                Intent callIntent = new Intent(Intent.ACTION_CALL);
                                callIntent.setData(Uri.parse("tel:" + marketMobile));
                                if (ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                    Toast.makeText(getApplicationContext(), "يجب السماح للتطبيق بالاتصال", Toast.LENGTH_SHORT).show();
                                    return;
                                }
                                startActivity(callIntent);
                            }
                        });

                builder1.setCancelable(false).setPositiveButton(getResources().getString(R.string.alertButtonCancel), new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });

        lnHome = (LinearLayout)findViewById(R.id.btnHome);
        lnOrders = (LinearLayout)findViewById(R.id.btnOrders);
        lnNoti = (LinearLayout)findViewById(R.id.btnNoti);
        lnMore = (LinearLayout)findViewById(R.id.btnMore);
        lnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerMarketAction.footerHome(MarketDetails.this);
                finish();
            }
        });
        lnOrders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerMarketAction.footerOrders(MarketDetails.this);
                finish();
            }
        });
        lnNoti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerMarketAction.footerNoti(MarketDetails.this);
                finish();
            }
        });
        lnMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerMarketAction.footerMore(MarketDetails.this);
                finish();
            }
        });
    }

    private void populateForm(){

        tvMarketMainCat.setText(marketMainCatName);
        tvMarketRoad.setText(marketDistance);
        tvMarketMinPrice.setText(marketMiniMumCharge);
        tvTitle.setText(marketName);
        tvMarketDelivery.setText(marketDelivery);
        btnMySnap.setText("سناباتي (" + marketCountVideo + ")");
        btnMarketRating.setText("سناباتي (" + marketNoRate + ")");
        Log.e("rate", marketRating);
        rBar.setRating(Integer.parseInt(marketRating));
        if (!marketIdMainCat.equals("1")) {
            btnMobileCall.setText(marketMobile);
            ln_mobile_data.setVisibility(View.VISIBLE);
        }
        Glide.with(getApplicationContext()).load(marketImg).into(imgImage);
        KeyValueShared.setMarketId(getApplicationContext(), marketId);
        KeyValueShared.setMarketDeliveryPrice(getApplicationContext(), marketDelivery);
        KeyValueShared.setMarketMinPrice(getApplicationContext(), marketMiniMumCharge);
    }
    private void setDetails(){
        marketId = getIntent().getStringExtra("idMarket");
        marketName = getIntent().getStringExtra("title");
        marketMobile = getIntent().getStringExtra("mobile");
        marketMiniMumCharge = getIntent().getStringExtra("minPrice");
        marketMapLat = getIntent().getStringExtra("mapLat");
        marketMapLong = getIntent().getStringExtra("mapLon");
        marketDescribtion = getIntent().getStringExtra("describtions");
        marketRating = getIntent().getStringExtra("rating");
        marketNoRate = getIntent().getStringExtra("noRating");
        marketMainCatName = getIntent().getStringExtra("cat");
        marketIdMainCat = getIntent().getStringExtra("idMainCat");
        marketImg = getIntent().getStringExtra("img");
        marketDelivery = getIntent().getStringExtra("delivery");
        marketIsWork = getIntent().getStringExtra("isWork");
        marketIdOwner = getIntent().getStringExtra("idOwner");
        marketDistance = getIntent().getStringExtra("distance");
        marketIdLastOrder = getIntent().getStringExtra("idLastOrder");
        marketCountVideo = getIntent().getStringExtra("countVedio");
        marketNumChat = getIntent().getStringExtra("numChat");
        populateForm();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_haraj, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id= item.getItemId();
        if(id == R.id.add){
            Intent i = new Intent(this, serviceAddAgreement.class);
            startActivity(i);
        } else if(id == R.id.back){
            Intent i = new Intent(this, marketMain.class);
            startActivity(i);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    protected void onRestart() {
        if (db.getAllPrice() != "0" && db.getAllPrice() != null){
            changeCart();
        }
        super.onRestart();
    }

    private void changeCart(){
        String newTitle = getResources().getString(R.string.myCart) + " ( " + db.getAllPrice() + " ) " + getResources().getString(R.string.min_price) + " ( " + KeyValueShared.getMarketMinPrice(getApplicationContext()) + " ) ";
        btn_cart.setText(newTitle);
    }
    public class loadData1 extends AsyncTask<String, Void, Boolean> {


        @SuppressWarnings("deprecation")
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
                nameValuePairs.add(new BasicNameValuePair("idMarket", params[1]));
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if(status == 200){
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    Log.e("markets", data);
                    JSONObject jObj = new JSONObject(data.substring(data.indexOf("{"), data.lastIndexOf("}") + 1));
                    ReqSuccess = jObj.getString("success");
                    if(ReqSuccess.equals("1")){
                        dataTest = data;
                        if(jObj.has("data")) {
                            JSONArray wee = jObj.getJSONArray("data");
                            for (int i = 0; i < wee.length(); i++) {
                                JSONObject c = wee.getJSONObject(i);
                                category dataCategory = new category();
                                dataCategory.setIdMarket(c.getString("idMarket"));
                                dataCategory.setId(c.getString("idSubCat"));
                                dataCategory.setTitle(c.getString("title"));
                                dataCategory.setMarketImage(c.getString("marketImage"));
                                data_list1.add(dataCategory);
                            }
                        }

                    }


                    return true;
                }else{
                    Log.e("error client string", "here");
                }
            } catch (ClientProtocolException e) {
                Log.e("error client string", e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("error exp", e.getMessage());
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("error json", e.getMessage());
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            pDialog.dismiss();
            super.onPostExecute(result);
            if(result == false){
                Toast.makeText(getApplicationContext(), "حدث خطأ، نرجو المحاوله في وقت لاحق", Toast.LENGTH_LONG).show();
            }else{
                if(ReqSuccess.equals("1")){

                    adapter1 = new categoryAdapter(getApplicationContext(), data_list1);
                    mRecyclerView1.setAdapter(adapter1);
                }else{
                }
            }
        }
    }
    public class loadDetails extends AsyncTask<String, Void, Boolean> {


        @SuppressWarnings("deprecation")
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
                nameValuePairs.add(new BasicNameValuePair("id", params[1]));
                nameValuePairs.add(new BasicNameValuePair("idUser", KeyValueShared.getId(getApplicationContext())));
                nameValuePairs.add(new BasicNameValuePair("mapLat", KeyValueShared.getMapLat(getApplicationContext())));
                nameValuePairs.add(new BasicNameValuePair("mapLon", KeyValueShared.getMapLon(getApplicationContext())));
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if(status == 200){
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    Log.e("markets", data);
                    JSONObject jObj = new JSONObject(data.substring(data.indexOf("{"), data.lastIndexOf("}") + 1));
                    ReqSuccess = jObj.getString("success");
                    if(ReqSuccess.equals("1")){
                        dataTest = data;
                        marketName = jObj.getString("name");
                        marketMobile = jObj.getString("mobile");
                        marketMiniMumCharge = jObj.getString("miniMumCharge");
                        marketMapLat = jObj.getString("mapLat");
                        marketMapLong = jObj.getString("mapLon");
                        marketDescribtion = jObj.getString("describtions");
                        marketRating = jObj.getString("rating");
                        marketNoRate = jObj.getString("noRating");
                        marketIdMainCat = jObj.getString("idMainCat");
                        marketMainCatName = jObj.getString("cat");
                        marketImg = jObj.getString("img");
                        marketDelivery = jObj.getString("delivery");
                        marketDistance = jObj.getString("distance");
                        marketIdLastOrder = jObj.getString("idLastOrder");
                        marketCountVideo = jObj.getString("countVedio");
                        marketNumChat = jObj.getString("numChat");
                        marketIsWork = jObj.getString("isWork");
                        marketIdOwner = jObj.getString("idOwner");

                    }


                    return true;
                }else{
                    Log.e("error client string", "here");
                }
            } catch (ClientProtocolException e) {
                Log.e("error client string", e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("error exp", e.getMessage());
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("error json", e.getMessage());
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if(result == false){
                Toast.makeText(getApplicationContext(), "حدث خطأ، نرجو المحاوله في وقت لاحق", Toast.LENGTH_LONG).show();
            }else{
                if(ReqSuccess.equals("1")){

                    populateForm();
                    String data[] = new String[2];
                    data[0] = globalData.URLhost() + "marketDetails.php";
                    data[1] = marketId;
                    loadData1 bts = new loadData1();
                    bts.execute(data);
                }else{
                }
            }
        }
    }
}
