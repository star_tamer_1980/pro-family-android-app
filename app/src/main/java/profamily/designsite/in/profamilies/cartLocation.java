package profamily.designsite.in.profamilies;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.Gson;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cartSQLiteData.CartSQLite;
import libes.KeyValueShared;
import libes.SQLiteConnection;
import libes.globalData;
import locationMap.LocationMap;
import locationMap.LocationsCartAdapter;

public class cartLocation extends AppCompatActivity {

    private RecyclerView mRecyclerView1;
    private LocationsCartAdapter adapter1;
    private Button btnAddOnline;
    private List<LocationMap> data_list1;
    private String dataTest;
    private ProgressBar pBar1;
    String ReqSuccess, ReqMessage;
    private ProgressDialog pDialog;
    SQLiteConnection db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart_location);
        db = new SQLiteConnection(getApplicationContext());
        KeyValueShared.setIdLocationForOrder(this, "");
        mRecyclerView1 = (RecyclerView)findViewById(R.id.recycleView1);
        mRecyclerView1.setLayoutManager(new LinearLayoutManager(this));
        pBar1 = (ProgressBar)findViewById(R.id.progressBar1);
        pDialog = new ProgressDialog(cartLocation.this);
        pDialog.setMessage("جاري ارسال الطلب");
        pDialog.setTitle("برجاء الانتظار ثواني");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
        data_list1 = new ArrayList<>();
        String data[] = new String[2];
        data[0] = globalData.URLhost() + "locationMapUserId.php";
        data[1] = KeyValueShared.getId(this);
        loadData1 bts = new loadData1();
        bts.execute(data);
        btnAddOnline = (Button)findViewById(R.id.btn_online_order);
        btnAddOnline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(KeyValueShared.getId(getApplicationContext()) == "0"){
                    final AlertDialog.Builder altdial = new AlertDialog.Builder(cartLocation.this);
                    altdial.setMessage(getResources().getString(R.string.message_must_login))
                            .setCancelable(false).setNeutralButton("الغاء", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                    return;
                                }
                            }).setNegativeButton("تسجيل عضويه", new DialogInterface.OnClickListener(){
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent i = new Intent(cartLocation.this, userRegister.class);
                            startActivity(i);
                            return;
                        }
                    }).setPositiveButton("دخول عضو", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent i = new Intent(cartLocation.this, userLogin.class);
                            startActivity(i);
                            return;
                        }
                    });
                    AlertDialog alert = altdial.create();
                    alert.show();
                }
                if(KeyValueShared.getIdLocationForOrder(getApplicationContext()) == "") {
                    final AlertDialog.Builder altdial = new AlertDialog.Builder(cartLocation.this);
                    altdial.setMessage(getResources().getString(R.string.message_choose_location))
                            .setCancelable(false).setPositiveButton("موافق", new DialogInterface.OnClickListener(){
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Toast.makeText(getApplicationContext(), KeyValueShared.getIdLocationForOrder(getApplicationContext()), Toast.LENGTH_SHORT).show();
                            dialog.cancel();
                            return;
                        }
                    });
                    AlertDialog alert = altdial.create();
                    alert.show();
                }else{
                    List<CartSQLite> dataCart = db.getCart();
                    String dataJson = new Gson().toJson(dataCart);
                    Toast.makeText(getApplicationContext(), dataJson, Toast.LENGTH_SHORT).show();
                    Log.e("json is: ", dataJson);
                    String data[] = new String[7];
                    data[0] = globalData.URLhost() + "ordersAdd.php";
                    data[1] = KeyValueShared.getId(getApplicationContext());
                    data[2] = KeyValueShared.getMarkeId(getApplicationContext());
                    data[3] = dataJson;
                    data[4] = KeyValueShared.getIdLocationForOrder(getApplicationContext());
                    data[5] = KeyValueShared.getMapLat(getApplicationContext());
                    data[6] = KeyValueShared.getMapLon(getApplicationContext());
                    pDialog.show();
                    new ordersAddAsynTask().execute(data);


                }
            }
        });
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        finish();
        startActivity(getIntent());
    }
    public class loadData1 extends AsyncTask<String, Void, Boolean> {

        private String title;
        private String content;
        @SuppressWarnings("deprecation")
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
                nameValuePairs.add(new BasicNameValuePair("userId", params[1]));
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if(status == 200){
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    JSONObject jObj = new JSONObject(data.substring(data.indexOf("{"), data.lastIndexOf("}") + 1));
                    ReqSuccess = jObj.getString("success");
                    LocationMap defaultLocationMap = new LocationMap();
                    defaultLocationMap.setId("0");
                    defaultLocationMap.setTitle("الموقع الحالي");
                    defaultLocationMap.setMapLat(KeyValueShared.getMapLat(getApplicationContext()));
                    defaultLocationMap.setMapLong(KeyValueShared.getMapLon(getApplicationContext()));
                    data_list1.add(defaultLocationMap);
                    if(ReqSuccess.equals("1")){
                        dataTest = data;
                        if(jObj.has("data")) {
                            JSONArray wee = jObj.getJSONArray("data");

                            for (int i = 0; i < wee.length(); i++) {
                                JSONObject c = wee.getJSONObject(i);
                                LocationMap dataLocationMap = new LocationMap();
                                dataLocationMap.setId(c.getString("id"));
                                dataLocationMap.setTitle(c.getString("title"));
                                dataLocationMap.setMapLat(c.getString("mapLat"));
                                dataLocationMap.setMapLong(c.getString("mapLon"));
                                data_list1.add(dataLocationMap);
                            }
                        }

                    }


                    return true;
                }else{
                    Log.e("error client string", "here");
                }
            } catch (ClientProtocolException e) {
                Log.e("error client string", e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("error exp", e.getMessage());
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("error json", e.getMessage());
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if(result == false){
                Toast.makeText(getApplicationContext(), "حدث خطأ، نرجو المحاوله في وقت لاحق", Toast.LENGTH_LONG).show();
            }else{
                    adapter1 = new LocationsCartAdapter(getApplicationContext(), data_list1);
                    mRecyclerView1.setAdapter(adapter1);
                    pBar1.setVisibility(View.INVISIBLE);

            }
        }
    }

    public class ordersAddAsynTask extends AsyncTask<String, Void, Boolean> {
        JSONObject jObj;
        @SuppressWarnings("deprecation")
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(6);
                nameValuePairs.add(new BasicNameValuePair("idUser", params[1]));
                nameValuePairs.add(new BasicNameValuePair("idMarket", params[2]));
                nameValuePairs.add(new BasicNameValuePair("productArray", params[3]));
                nameValuePairs.add(new BasicNameValuePair("idUserLocation", params[4]));
                nameValuePairs.add(new BasicNameValuePair("latitude", params[5]));
                nameValuePairs.add(new BasicNameValuePair("longitude", params[6]));
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if(status == 200){
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    Log.e("result data", data);
                    JSONObject jObj = new JSONObject(data.substring(data.indexOf("{"), data.lastIndexOf("}") + 1));
                    ReqSuccess = jObj.getString("success");
                    if(ReqSuccess.equals("1")){
                        ReqMessage = jObj.getString("message");
                    }else{
                        ReqMessage = jObj.getString("message");
                    }


                    return true;
                }else{
                    Log.e("error client string", "here");
                }
            } catch (ClientProtocolException e) {
                Log.e("error client string", e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("error exp", e.getMessage());
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("error json", e.getMessage());
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            pDialog.dismiss();
            super.onPostExecute(result);
            if(result == false){
                Toast.makeText(getApplicationContext(), "حدث خطأ، حاول مره أخرى " + ReqMessage, Toast.LENGTH_LONG).show();
            }else{
                if(ReqSuccess.equals("1")){
                    Toast.makeText(getApplicationContext(), ReqMessage, Toast.LENGTH_SHORT).show();
                    db.deleteAllRecord();

                }else{
                    Toast.makeText(getApplicationContext(), ReqMessage, Toast.LENGTH_LONG).show();
                }
                finish();
            }
        }
    }


}
