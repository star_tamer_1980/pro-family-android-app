package profamily.designsite.in.profamilies;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import libes.globalData;

public class OrdersDataForMarketDetails extends AppCompatActivity {

    TextView tvTitle, tvTimeAdd, tvPriceFinish, tvPriceDelivery, priceAll, tvStringPrice, tvStringPriceFinish, tvStringDelivery, tvStringPriceAll, tvStatue;
    Button btnProductView, btnOrderNowDelivery, btnIsAgree, btnOrderReciveFromMarket, btnOrderReciveFromMember, btnOrderCancel;
    String statue, id, ReqSuccess, ReqMessage;
    private ProgressDialog pDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders_data_for_market_details);
        statue = getIntent().getStringExtra("statue");
        id = getIntent().getStringExtra("id");
        tvTitle = (TextView)findViewById(R.id.tv_title);
        tvTitle.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        tvStringPrice = (TextView)findViewById(R.id.tv_string_price);
        tvStringPrice.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        tvStringPriceFinish = (TextView)findViewById(R.id.tv_price_finish_string);
        tvStringPriceFinish.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        tvStringDelivery = (TextView)findViewById(R.id.tv_price_delivery_string);
        tvStringDelivery.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        tvStringPriceAll = (TextView)findViewById(R.id.tv_price_all_string);
        tvStringPriceAll.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        tvPriceFinish = (TextView)findViewById(R.id.tv_price_finish);
        tvPriceFinish.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        tvPriceDelivery = (TextView)findViewById(R.id.tv_price_delivery);
        tvPriceDelivery.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        tvStatue = (TextView)findViewById(R.id.tv_statue);
        tvStatue.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        tvStatue.setText(getIntent().getStringExtra("statueTxt"));
        priceAll = (TextView)findViewById(R.id.tv_price_all);
        priceAll.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        tvTimeAdd = (TextView)findViewById(R.id.tv_time_add);
        tvTimeAdd.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        tvTitle.setText(getIntent().getStringExtra("title"));
        tvTimeAdd.setText(getIntent().getStringExtra("timeAdd"));
        tvPriceFinish.setText(String.valueOf(Integer.valueOf(getIntent().getStringExtra("price")) - Integer.valueOf(getIntent().getStringExtra("deliveryPrice"))));
        priceAll.setText(getIntent().getStringExtra("price"));
        tvPriceDelivery.setText(getIntent().getStringExtra("deliveryPrice"));
        btnProductView = (Button)findViewById(R.id.btn_order_view);
        btnProductView.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        btnOrderNowDelivery = (Button)findViewById(R.id.btn_order_now_delivery);
        btnOrderNowDelivery.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        btnIsAgree = (Button)findViewById(R.id.btn_order_is_agree);
        btnIsAgree.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        btnOrderReciveFromMarket = (Button)findViewById(R.id.btn_order_receive_from_market);
        btnOrderReciveFromMarket.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        btnOrderReciveFromMember = (Button)findViewById(R.id.btn_order_receive_from_member);
        btnOrderReciveFromMember.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        btnOrderCancel = (Button)findViewById(R.id.btn_order_cancel);
        btnOrderCancel.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        if(Integer.valueOf(statue) > 0){
            btnIsAgree.setEnabled(false);
        }
        if(Integer.valueOf(statue) > 1){
            btnOrderNowDelivery.setEnabled(false);
        }
        if(Integer.valueOf(statue) > 2){
            btnOrderReciveFromMarket.setEnabled(false);
        }
        if(Integer.valueOf(statue) > 3){
            btnOrderReciveFromMember.setEnabled(false);
        }
        if(Integer.valueOf(statue) > 4){
            btnOrderCancel.setEnabled(false);
        }
        btnIsAgree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeStatueFun("1");
            }
        });
        btnOrderNowDelivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeStatueFun("2");
            }
        });
        btnOrderReciveFromMarket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeStatueFun("3");
            }
        });
        btnOrderReciveFromMember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        btnOrderCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeStatueFun("5");
            }
        });
        btnProductView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(OrdersDataForMarketDetails.this, OrderProducts.class);
                i.putExtra("idOrder", id);
                startActivity(i);
            }
        });
    }
    public void changeStatueFun(final String statues){
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(OrdersDataForMarketDetails.this);
        } else {
            builder = new AlertDialog.Builder(OrdersDataForMarketDetails.this);
        }
        builder.setTitle(getResources().getString(R.string.alert_title_change_statue))
                .setMessage(getResources().getString(R.string.alert_message_change_statue))
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        String data[] = new String[3];
                        data[0] = globalData.URLhost() + "orderChangeStatue.php";
                        data[1] = statues;
                        data[2] = id;
                        changeStatue bts = new changeStatue();
                        bts.execute(data);
                        pDialog = new ProgressDialog(OrdersDataForMarketDetails.this);
                        pDialog.setMessage("جاري حذف المتجر");
                        pDialog.setTitle("برو فاميليز");
                        pDialog.setIndeterminate(false);
                        pDialog.setCancelable(true);
                        pDialog.show();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(getApplicationContext(), "delete no", Toast.LENGTH_LONG).show();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
    public class changeStatue extends AsyncTask<String, Void, Boolean> {


        @SuppressWarnings("deprecation")
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                nameValuePairs.add(new BasicNameValuePair("orderStatue", params[1]));
                nameValuePairs.add(new BasicNameValuePair("orderId", params[2]));
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if(status == 200){
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    Log.e("markets", data);
                    JSONObject jObj = new JSONObject(data.substring(data.indexOf("{"), data.lastIndexOf("}") + 1));
                    ReqSuccess = jObj.getString("success");
                    ReqMessage = jObj.getString("message");
                    return true;
                }else{
                    Log.e("error client string", "here");
                }
            } catch (ClientProtocolException e) {
                Log.e("error client string", e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("error exp", e.getMessage());
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("error json", e.getMessage());
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            pDialog.dismiss();
            super.onPostExecute(result);
            if(result == false){
                Toast.makeText(getApplicationContext(), "حدث خطأ، نرجو المحاوله في وقت لاحق", Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(getApplicationContext(), ReqMessage, Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }
}
