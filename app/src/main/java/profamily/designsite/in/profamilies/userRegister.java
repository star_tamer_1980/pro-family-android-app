package profamily.designsite.in.profamilies;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import libes.KeyValueShared;
import libes.globalData;

public class userRegister extends AppCompatActivity {


    private EditText etTel;
    private EditText etName;
    private EditText etEmail;
    private Button btnRegister;
    String ReqSuccess , ReqMessage;
    private ProgressDialog pDialog;
    String regName, regTel, regEmail, regId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_register);
        etTel = (EditText)findViewById(R.id.etTel);
        etName = (EditText)findViewById(R.id.etName);
        etEmail = (EditText)findViewById(R.id.etEmail);
        btnRegister = (Button)findViewById(R.id.btnRegister);
        etTel.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_NEXT || actionId == EditorInfo.IME_ACTION_DONE){
                    loginProccess();
                    return true;
                }
                return false;
            }
        });
        etName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_NEXT || actionId == EditorInfo.IME_ACTION_DONE){
                    loginProccess();
                    return true;
                }
                return false;
            }
        });
        etEmail.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_NEXT || actionId == EditorInfo.IME_ACTION_DONE){
                    loginProccess();
                    return true;
                }
                return false;
            }
        });
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginProccess();
            }
        });
    }
    private void loginProccess(){
        String Tel = etTel.getText().toString();
        String name = etName.getText().toString();
        String email = etEmail.getText().toString();
        if(TextUtils.isEmpty(Tel)){
            etTel.setError((getString(R.string.mustWriteTel)));
        }else if(TextUtils.isEmpty(name)){
            etName.setError(getString(R.string.mustWriteName));
        }else{
            String data[] = new String[4];
            data[0] = globalData.URLhost() + "memberAdd.php";
            data[1] = Tel;
            data[2] = name;
            data[3] = email;

            new MembersAsynTask().execute(data);

            pDialog = new ProgressDialog(userRegister.this);
            pDialog.setMessage("جاري تسجيل العضو");
            pDialog.setTitle("برجاء الانتظار ثواني");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }
    }

    public class MembersAsynTask extends AsyncTask<String, Void, Boolean> {
        JSONObject jObj;
        @SuppressWarnings("deprecation")
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(3);
                nameValuePairs.add(new BasicNameValuePair("tel", params[1]));
                nameValuePairs.add(new BasicNameValuePair("name", params[2]));
                nameValuePairs.add(new BasicNameValuePair("email", params[3]));
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if(status == 200){
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    JSONObject jObj = new JSONObject(data.substring(data.indexOf("{"), data.lastIndexOf("}") + 1));
                    ReqSuccess = jObj.getString("success");
                    if(ReqSuccess.equals("1")){
                        if(jObj.has("data")) {
                            JSONArray wee = jObj.getJSONArray("data");
                            for (int i = 0; i < wee.length(); i++) {
                                JSONObject c = wee.getJSONObject(i);
                                regTel = c.getString("tel");
                                regName = c.getString("name");
                                regEmail = c.getString("email");
                                regId = c.getString("id");
                            }
                        }

                    }else{
                        ReqMessage = jObj.getString("message");
                    }


                    return true;
                }else{
                    Log.e("error client string", "here");
                }
            } catch (ClientProtocolException e) {
                Log.e("error client string", e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("error exp", e.getMessage());
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("error json", e.getMessage());
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            pDialog.dismiss();
            super.onPostExecute(result);
            if(result == false){
                Toast.makeText(getApplicationContext(), "لا يوجد بيانات " + ReqMessage, Toast.LENGTH_LONG).show();
            }else{
                if(ReqSuccess.equals("1")){
                    Toast.makeText(getApplicationContext(), "تم تسجيل الدخول", Toast.LENGTH_LONG).show();
                    Toast.makeText(getApplicationContext(), "أهلاً بك: " + regName, Toast.LENGTH_LONG).show();

                    KeyValueShared.setTel(getApplicationContext(), regTel);
                    KeyValueShared.setName(getApplicationContext(), regName);
                    KeyValueShared.setEmail(getApplicationContext(), regEmail);
                    KeyValueShared.setStatue(getApplicationContext(), "1");
                    Intent i = new Intent(userRegister.this, MainHaraj.class);
                    startActivity(i);
                    finish();

                }else{
                    Toast.makeText(getApplicationContext(), ReqMessage, Toast.LENGTH_LONG).show();
                }
            }
        }
    }

}
