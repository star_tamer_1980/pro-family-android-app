package profamily.designsite.in.profamilies;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import libes.KeyValueShared;

public class Activity_user_SMS_Auth extends AppCompatActivity {

    EditText currentCode;
    Button btnAuth, btnReSend, btnCancel;
    private ProgressDialog pDialog;
    String LastCode, code;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user__sms__auth);
        currentCode = (EditText)findViewById(R.id.etCode);
        btnAuth = (Button)findViewById(R.id.btnAuth);
        btnReSend = (Button)findViewById(R.id.btnResend);
        btnCancel = (Button)findViewById(R.id.btnCancel);
        final Intent i = getIntent();
        LastCode = i.getStringExtra("code");
        btnAuth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentCode.getText().toString().equals(LastCode)){
                    Toast.makeText(getApplicationContext(), "تم تسجيل الدخول", Toast.LENGTH_LONG).show();
                    Toast.makeText(getApplicationContext(), "أهلاً بك: " + i.getStringExtra("name"), Toast.LENGTH_LONG).show();
                    KeyValueShared.setTel(getApplicationContext(), i.getStringExtra("tel"));
                    KeyValueShared.setName(getApplicationContext(), i.getStringExtra("name"));
                    KeyValueShared.setEmail(getApplicationContext(), i.getStringExtra("email"));
                    KeyValueShared.setId(getApplicationContext(), i.getStringExtra("id"));
                    KeyValueShared.setSettingsAppisSMSOrders(getApplicationContext(), i.getStringExtra("isSMSOrder"));
                    KeyValueShared.setSettingsAppisSMSComments(getApplicationContext(), i.getStringExtra("isSMSComments"));
                    KeyValueShared.setSettingsAppisNotifiOrders(getApplicationContext(), i.getStringExtra("isNotifiOrders"));
                    KeyValueShared.setSettingsAppisNotifiComments(getApplicationContext(), i.getStringExtra("isNotifiComments"));
                    KeyValueShared.setStatue(getApplicationContext(), "1");
                    finish();
                }
            }
        });
        btnReSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                pDialog = new ProgressDialog(Activity_user_SMS_Auth.this);
                pDialog.setMessage("جاري دخول العضو");
                pDialog.setTitle("برجاء الانتظار ثواني");
                pDialog.setIndeterminate(false);
                pDialog.setCancelable(true);
                pDialog.show();
                Random rand = new Random();

                code = String.valueOf(rand.nextInt(9999) + 1);
                String datass[] = new String[2];
                datass[0] = "http://www.mobily.ws/api/msgSend.php?mobile=966505402332&password=Suad4321&sender=Families&applicationType=68&lang=3&timeSend=0&dateSend=0" + "&numbers=966" + i.getStringExtra("tel") + "&msg=%D9%83%D9%88%D8%AF%20%D8%A7%D9%84%D8%AA%D8%AD%D9%82%D9%82%20%D9%87%D9%88%3A%20" + code;

                new SMSAsynTask().execute(datass);
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public class SMSAsynTask extends AsyncTask<String, Void, Boolean> {
        JSONObject jObj;
        String data;
        @SuppressWarnings("deprecation")
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if(status == 200){
                    HttpEntity entity = response.getEntity();
                    data = EntityUtils.toString(entity);
                    Log.e("data error", data);



                    return true;
                }else{
                    Log.e("error client string", "here");
                }
            } catch (ClientProtocolException e) {
                Log.e("error client string", e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("error exp", e.getMessage());
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            pDialog.dismiss();
            super.onPostExecute(result);
            if(result == false){
                Toast.makeText(getApplicationContext(), "حدث خطأ، نرجو المحاوله في وقت لاحق", Toast.LENGTH_LONG).show();
            }else{
                if (data.equals("1")){
                    LastCode = code;
                }
            }
        }
    }
}
