package profamily.designsite.in.profamilies;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MotionEvent;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import adapters.MyFragmentPagerAdapterProduct;
import fragments.haraj.fragment1;
import fragments.haraj.fragment2;
import fragments.haraj.fragment3;
import fragments.haraj.fragment4;
import fragments.haraj.fragment5;
import libes.KeyValueShared;
import libes.footerHarajAction;
import libes.globalData;
import productData.Product;
import productData.ProductAdapter;
import tab.tabs;
import tab.tabsAdapter;

public class MainHaraj extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    private RecyclerView mRecyclerView1, rvTabMain, rvTabSub, rvTabStatue;
    private tabsAdapter tabMainAdapter;
    private List<tabs> data_list_tab_mainArray;
    private tabsAdapter tabSubAdapter;
    private List<tabs> data_list_tab_subArray;
    private tabsAdapter tabStatueAdapter;
    private List<tabs> data_list_statueArray;
    private ProductAdapter adapter1;
    private List<Product> data_list1;
    private String dataTest;
    private ProgressDialog pDialog;
    private ProgressBar pBar1;
    String ReqSuccess;
    public String filename = "getHaraj.php";
    public String fileLastPro = "getHarajLastPro.php";
    public String changeStatue = "0";
    public String idMainCat, idSubCat;
    private String cityId = "0";
    String lastPro = "0";
    lastProCounter countTimer;

    private int m_previouse_Total_Count1 = 0;
    private boolean loading = true;
    private int visibleThreshold = 15;
    int firstVisibleItem, visibleItemCount, totalItemCount;
    private TextView txtNotFound;



    LinearLayout lnHome, lnSearch, lnNoti, lnComments, lnMore;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_haraj);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        idSubCat = "0";
        idMainCat = "0";
        lnHome = (LinearLayout)findViewById(R.id.btnHaraj);
        lnSearch = (LinearLayout)findViewById(R.id.btnSearch);
        lnNoti = (LinearLayout)findViewById(R.id.btnNoti);
        lnComments = (LinearLayout)findViewById(R.id.btnComments);
        lnMore = (LinearLayout)findViewById(R.id.btnMore);
        lnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerHarajAction.footerHome(MainHaraj.this);
                finish();
            }
        });
        lnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerHarajAction.footerSearch(MainHaraj.this);
            }
        });
        lnNoti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerHarajAction.footerNoti(MainHaraj.this);
                finish();
            }
        });
        lnComments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerHarajAction.footerComments(MainHaraj.this);
                finish();
            }
        });
        lnMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerHarajAction.footerMore(MainHaraj.this);
                finish();
            }
        });
        m_previouse_Total_Count1 = 0;
        txtNotFound = (TextView)findViewById(R.id.txtNotFound);
        mRecyclerView1 = (RecyclerView)findViewById(R.id.recycleView1);
        mRecyclerView1.setHasFixedSize(true);
        mRecyclerView1.setLayoutManager(new LinearLayoutManager(this));
        pBar1 = (ProgressBar)findViewById(R.id.progressBar1);
        pBar1.setVisibility(View.VISIBLE);
        data_list1 = new ArrayList<>();


        rvTabMain = (RecyclerView)findViewById(R.id.rvMainTab);
        rvTabMain.setHasFixedSize(true);
        rvTabMain.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        data_list_tab_mainArray = new ArrayList<>();
        String[] tabMainName = getApplicationContext().getResources().getStringArray(R.array.tabMain_name);
        String[] tabMainid = getApplicationContext().getResources().getStringArray(R.array.tabMain_id);
        String[] tabMainFileName = getApplicationContext().getResources().getStringArray(R.array.tabMain_fileName);
        String[] tabMainFileLastPro = getApplicationContext().getResources().getStringArray(R.array.tabMain_fileLastPro);
        for(int i = 0; i < tabMainName.length; i++){
            tabs mainTab = new tabs();
            Log.e("cat name", tabMainName[i].toString());
            mainTab.setId(tabMainid[i].toString());
            mainTab.setTitle(tabMainName[i].toString());
            mainTab.setFileName(tabMainFileName[i].toString());
            mainTab.setFileLastPro(tabMainFileLastPro[i].toString());
            mainTab.setIsTabMain("1");
            if (i == 0){
                mainTab.setIsSelected("1");
            }
            data_list_tab_mainArray.add(mainTab);
        }
        tabMainAdapter = new tabsAdapter(getApplication(), data_list_tab_mainArray);
        rvTabMain.setAdapter(tabMainAdapter);



        rvTabSub = (RecyclerView)findViewById(R.id.rvSubTab);
        rvTabSub.setHasFixedSize(true);
        rvTabSub.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        String[] tabSubName = getApplicationContext().getResources().getStringArray(R.array.tabSub_name_all);
        String[] tabSubId = getApplicationContext().getResources().getStringArray(R.array.tabSub_id_all);
        data_list_tab_subArray = new ArrayList<>();
        tabs subTabFirst = new tabs();
        subTabFirst.setId("0");
        subTabFirst.setTitle(getResources().getString(R.string.allData));
        subTabFirst.setIsTabMain("0");
        subTabFirst.setIsSelected("1");
        data_list_tab_subArray.add(subTabFirst);
        for(int i = 0; i < tabSubName.length; i++){
            tabs subTab = new tabs();
            subTab.setId(tabSubId[i].toString());
            subTab.setTitle(tabSubName[i].toString());
            subTab.setIsTabMain("0");
            data_list_tab_subArray.add(subTab);
        }
        tabSubAdapter = new tabsAdapter(getApplication(), data_list_tab_subArray);
        rvTabSub.setAdapter(tabSubAdapter);



        rvTabStatue = (RecyclerView)findViewById(R.id.rvStatueTab);
        rvTabStatue.setHasFixedSize(true);
        rvTabStatue.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        String[] tabStatueName = getApplicationContext().getResources().getStringArray(R.array.tabStatue_name);
        data_list_statueArray = new ArrayList<>();
        for(int i = 0; i < tabStatueName.length; i++){
            tabs statueTab = new tabs();
            statueTab.setId(String.valueOf(i));
            statueTab.setTitle(tabStatueName[i].toString());
            statueTab.setIsTabMain("2");
            if (i == 0){
                statueTab.setIsSelected("1");
            }
            data_list_statueArray.add(statueTab);
        }
        tabStatueAdapter = new tabsAdapter(getApplication(), data_list_statueArray);
        rvTabStatue.setAdapter(tabStatueAdapter);


        mRecyclerView1.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager mLayoutManager = (LinearLayoutManager) mRecyclerView1
                        .getLayoutManager();
                mRecyclerView1.setLayoutManager(mLayoutManager);
                visibleItemCount = mRecyclerView1.getChildCount();
                totalItemCount = mLayoutManager.getItemCount();
                firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();
                if(totalItemCount == 0){
                    return ;
                }
                if(m_previouse_Total_Count1==totalItemCount){
                    return;
                }
                boolean loadMore = firstVisibleItem + visibleItemCount >= totalItemCount;
                if(loadMore){
                    m_previouse_Total_Count1 = totalItemCount;
                    pBar1.setVisibility(View.VISIBLE);
                    getLoadData(totalItemCount);
                }

            }
        });

        resetData();
        startLoadData();
    }

    @Override
    protected void onStop() {
        counterStop();
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        finish();
        startActivity(getIntent());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_haraj, menu);
        return true;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.menu_car) {
            this.idSubCat = "26";
        } else if (id == R.id.menu_moto) {
            this. idSubCat = "27";
        } else if (id == R.id.menu_cycle) {
            this.idSubCat = "28";
        } else if (id == R.id.menu_moto_other) {
            this.idSubCat = "107";
        } else if (id == R.id.menu_earth_buy) {
            this.idSubCat = "110";
        } else if (id == R.id.menu_earth_rent) {
            this.idSubCat = "111";
        } else if (id == R.id.menu_apart_buy) {
            this.idSubCat = "112";
        } else if (id == R.id.menu_apart_rent) {
            this.idSubCat = "113";
        } else if (id == R.id.menu_villa_buy) {
            this.idSubCat = "114";
        } else if (id == R.id.menu_villa_rent) {
            this.idSubCat = "115";
        } else if (id == R.id.menu_home_buy) {
            this.idSubCat = "116";
        } else if (id == R.id.menu_home_rent) {
            this.idSubCat = "117";
        } else if (id == R.id.menu_rest_buy) {
            this.idSubCat = "118";
        } else if (id == R.id.menu_rest_rent) {
            this.idSubCat = "119";
        } else if (id == R.id.menu_market_buy) {
            this.idSubCat = "120";
        } else if (id == R.id.menu_market_rent) {
            this.idSubCat = "121";
        } else if (id == R.id.menu_tour_buy) {
            this.idSubCat = "122";
        } else if (id == R.id.menu_tour_rent) {
            this.idSubCat = "123";
        } else if (id == R.id.menu_farm_buy) {
            this.idSubCat = "124";
        } else if (id == R.id.menu_livel_rent) {
            this.idSubCat = "125";
        } else if (id == R.id.menu_building_other) {
            this.idSubCat = "108";
        } else if (id == R.id.nav_iphone) {
            this.idSubCat = "30";
        } else if (id == R.id.nav_nokia) {
            this.idSubCat = "31";
        } else if (id == R.id.nav_samsung) {
            this.idSubCat = "32";
        } else if (id == R.id.nav_huwawi) {
            this.idSubCat = "33";
        } else if (id == R.id.nav_mobile) {
            this.idSubCat = "34";
        } else if (id == R.id.nav_play) {
            this.idSubCat = "35";
        } else if (id == R.id.nav_xbox) {
            this.idSubCat = "36";
        } else if (id == R.id.nav_laptop) {
            this.idSubCat = "37";
        } else if (id == R.id.nav_comp) {
            this.idSubCat = "38";
        } else if (id == R.id.nav_ghanam) {
            this.idSubCat = "39";
        } else if (id == R.id.nav_ebel) {
            this.idSubCat = "40";
        } else if (id == R.id.nav_birds) {
            this.idSubCat = "42";
        } else if (id == R.id.nav_animals) {
            this.idSubCat = "41";
        } else if (id == R.id.menu_watch) {
            this.idSubCat = "44";
        } else if (id == R.id.menu_glasses) {
            this.idSubCat = "45";
        } else if (id == R.id.menu_perfume) {
            this.idSubCat = "46";
        } else if (id == R.id.menu_clothes_child) {
            this.idSubCat = "47";
        } else if (id == R.id.menu_clothes_girl) {
            this.idSubCat = "48";
        } else if (id == R.id.menu_clothes_men) {
            this.idSubCat = "49";
        } else if (id == R.id.menu_clothes_sports) {
            this.idSubCat = "50";
        } else if (id == R.id.menu_clothes_Other) {
            this.idSubCat = "106";
        }
        if(!this.idSubCat.equals("0")) {
            changeStatue = "0";
            startLoadData();
            tabSubAdapter.notifyDataSetChanged();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id= item.getItemId();
        counterStop();
        if(id == R.id.my_adv){
            Intent i = new Intent(MainHaraj.this, MyAdv.class);
            startActivity(i);
        } else if(id == R.id.add){
            Intent i = new Intent(MainHaraj.this, HarajAddSwicherMedia.class);
            startActivity(i);
        } else if(id == R.id.back){
            Intent i = new Intent(MainHaraj.this, Main2Activity.class);
            startActivity(i);
            finish();
        } else if(id == R.id.notification){
            Intent i = new Intent(MainHaraj.this, notification_alerts.class);
            startActivity(i);
        } else if(id == R.id.haraj_contact){
            Intent i = new Intent(MainHaraj.this, pageDetails.class);
            i.putExtra("id", "8");
            startActivity(i);
        } else if(id == R.id.haraj_commision){
            Intent i = new Intent(MainHaraj.this, pageDetails.class);
            i.putExtra("id", "11");
            startActivity(i);
        } else if(id == R.id.haraj_privacy){
            Intent i = new Intent(MainHaraj.this, pageDetails.class);
            i.putExtra("id", "12");
            startActivity(i);
        } else if(id == R.id.haraj_login){
            Intent i = new Intent(MainHaraj.this, userLogin.class);
            startActivity(i);
        } else if(id == R.id.haraj_register){
            Intent i = new Intent(MainHaraj.this, userRegister.class);
            startActivity(i);
            finish();
        } else if(id == R.id.haraj_logout){
            KeyValueShared.setStatue(getApplicationContext(), "0");
            KeyValueShared.setName(getApplicationContext(), "");
            KeyValueShared.setTel(getApplicationContext(), "");
            KeyValueShared.setEmail(getApplicationContext(), "");
            KeyValueShared.setId(getApplicationContext(), "0");
            Intent starterIntent = getIntent();
            finish();
            startActivity(starterIntent);
        } else if(id == R.id.my_personal_data){
            Intent i = new Intent(MainHaraj.this, personalData.class);
            startActivity(i);
            finish();
        } else if(id == R.id.admin_notification){
            Intent i = new Intent(MainHaraj.this, notifi.class);
            startActivity(i);
        }
        return super.onOptionsItemSelected(item);
    }

    public void setSubCat(){
        String[] tabSubNames = getApplicationContext().getResources().getStringArray(R.array.tabSub_name_all);
        String[] tabSubIds = getApplicationContext().getResources().getStringArray(R.array.tabSub_id_all);
        if(idMainCat.equals("0")){
            tabSubNames = getApplicationContext().getResources().getStringArray(R.array.tabSub_name_all);
            tabSubIds = getApplicationContext().getResources().getStringArray(R.array.tabSub_id_all);
        }else if(idMainCat.equals("1")){
            tabSubNames = getApplicationContext().getResources().getStringArray(R.array.tabSub_categoryCarName);
            tabSubIds = getApplicationContext().getResources().getStringArray(R.array.tabSub_categoryCarId);
        }else if(idMainCat.equals("2")){
            tabSubNames = getApplicationContext().getResources().getStringArray(R.array.tabSub_categoryBuildingName);
            tabSubIds = getApplicationContext().getResources().getStringArray(R.array.tabSub_categoryBuildingId);
        }else if(idMainCat.equals("3")){
            tabSubNames = getApplicationContext().getResources().getStringArray(R.array.tabSub_categoryElectricName);
            tabSubIds = getApplicationContext().getResources().getStringArray(R.array.tabSub_categoryElectricId);
        }else if(idMainCat.equals("4")){
            tabSubNames = getApplicationContext().getResources().getStringArray(R.array.tabSub_categoryAnimalsName);
            tabSubIds = getApplicationContext().getResources().getStringArray(R.array.tabSub_categoryAnimalsId);
        }else if(idMainCat.equals("5")){
            tabSubNames = getApplicationContext().getResources().getStringArray(R.array.tabSub_categoryPersonalName);
            tabSubIds = getApplicationContext().getResources().getStringArray(R.array.tabSub_categoryPersonalId);
        }
        data_list_tab_subArray.clear();
        tabs subTabFirst = new tabs();
        subTabFirst.setId("0");
        subTabFirst.setTitle(getResources().getString(R.string.allData));
        subTabFirst.setIsTabMain("0");
        subTabFirst.setIsSelected("1");
        data_list_tab_subArray.add(subTabFirst);
        for(int i = 0; i < tabSubNames.length; i++){
            tabs subTab = new tabs();
            subTab.setId(tabSubIds[i].toString());
            subTab.setTitle(tabSubNames[i].toString());
            subTab.setIsTabMain("0");
            Log.e("subcatname", tabSubNames[i].toString());
            data_list_tab_subArray.add(subTab);
        }

        tabSubAdapter = new tabsAdapter(getApplication(), data_list_tab_subArray);
        rvTabSub.setAdapter(tabSubAdapter);
    }
    public void resetData(){
        changeStatue = "0";
        idSubCat = "0";
        cityId = KeyValueShared.getSettingsAppMyCityId(this);

    }
    public void startLoadData(){
        m_previouse_Total_Count1 = 0;
        data_list1.clear();
        Log.e("size is", String.valueOf(data_list1.size()));
        Log.e("changeStatue is", String.valueOf(changeStatue));
        Log.e("idSubCat is", String.valueOf(idSubCat));
        Log.e("cityId is", String.valueOf(cityId));
        Log.e("filename is", filename);
        adapter1 = new ProductAdapter(getApplicationContext(), data_list1);
        mRecyclerView1.setAdapter(adapter1);
        String dataStart[] = new String[7];
        dataStart[0] = globalData.URLhost() + filename;
        dataStart[1] = "0";
        dataStart[2] = changeStatue;
        dataStart[3] = idSubCat;
        dataStart[4] = cityId;
        dataStart[5] = KeyValueShared.getMapLat(this);
        dataStart[6] = KeyValueShared.getMapLon(this);
        loadData1 bts = new loadData1();
        bts.execute(dataStart);
        tabStatueAdapter.notifyDataSetChanged();
    }
    public void getLoadData(Integer limit) {

        pBar1.setVisibility(View.VISIBLE);
        String data[] = new String[7];
        data[0] = globalData.URLhost() + filename;
        data[1] = String.valueOf(limit);
        data[2] = changeStatue;
        data[3] = idSubCat;
        data[4] = cityId;
        data[5] = KeyValueShared.getMapLat(this);
        data[6] = KeyValueShared.getMapLon(this);
        loadData1 bts = new loadData1();
        bts.execute(data);
    }
    public void counterStart(){
        countTimer = new lastProCounter(300000, 30000);
        countTimer.start();
    }
    public void counterStop(){
        if(countTimer != null) {
            countTimer.cancel();
        }
    }
    public class loadData1 extends AsyncTask<String, Void, Boolean> {

        private String title;
        private String content;
        private String img;
        @SuppressWarnings("deprecation")
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(6);
                nameValuePairs.add(new BasicNameValuePair("limit", params[1]));
                nameValuePairs.add(new BasicNameValuePair("changeStatue", params[2]));
                nameValuePairs.add(new BasicNameValuePair("idSubCat", params[3]));
                nameValuePairs.add(new BasicNameValuePair("cityId", params[4]));
                nameValuePairs.add(new BasicNameValuePair("latitude", params[5]));
                nameValuePairs.add(new BasicNameValuePair("longitude", params[6]));
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if(status == 200){
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    JSONObject jObj = new JSONObject(data.substring(data.indexOf("{"), data.lastIndexOf("}") + 1));
                    ReqSuccess = jObj.getString("success");
                    Log.e("data haraj", data);
                    Log.e("file is", params[0]);
                    lastPro = jObj.getString("lastPro");
                    if(ReqSuccess.equals("1")){
                        dataTest = data;
                        if(jObj.has("data")) {
                            JSONArray wee = jObj.getJSONArray("data");
                            for (int i = 0; i < wee.length(); i++) {
                                JSONObject c = wee.getJSONObject(i);
                                Product dataProduct = new Product();
                                dataProduct.setId(c.getString("id"));
                                dataProduct.setName(c.getString("name"));
                                dataProduct.setIdMainCat(c.getString("idMainCat"));
                                dataProduct.setIdSubCat(c.getString("idSubCat"));
                                dataProduct.setCatName(c.getString("catName"));
                                dataProduct.setPlace(c.getString("country"));
                                dataProduct.setCity(c.getString("city"));
                                dataProduct.setIsSum(c.getString("isSum"));
                                dataProduct.setImg(c.getString("img"));
                                dataProduct.setAlbumImg(c.getString("albumImg"));
                                dataProduct.setAlbumFullImg(c.getString("albumFullImg"));
                                dataProduct.setAllImages(c.getString("allImages"));
                                dataProduct.setTimeAdd(c.getString("timeAdd"));
                                dataProduct.setIdUser(c.getString("idUser"));
                                dataProduct.setUserNameProduct(c.getString("userName"));
                                dataProduct.setIsActive(c.getString("isActive"));
                                dataProduct.setPrice(c.getString("price"));
                                dataProduct.setMyPlaceAndCity(c.getString("location"));
                                dataProduct.setContact(c.getString("contact"));
                                dataProduct.setDescription(c.getString("description"));
                                dataProduct.setRating(c.getString("rating"));
                                dataProduct.setVideoImageName(c.getString("videoImageName"));
                                dataProduct.setVideoName(c.getString("videoName"));
                                dataProduct.setActionType("details");
                                data_list1.add(dataProduct);
                            }
                        }

                    }


                    return true;
                }else{
                    Log.e("error client string", "here");
                }
            } catch (ClientProtocolException e) {
                Log.e("error client string", e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("error exp", e.getMessage());
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("error json", e.getMessage());
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if(result == false){
                Toast.makeText(getApplicationContext(), "لا يوجد بيانات", Toast.LENGTH_LONG).show();
            }else{
                if(ReqSuccess.equals("1")){
                    Log.e("last pro is: ", lastPro);
                    counterStart();
                    txtNotFound.setVisibility(View.INVISIBLE);
                    if(m_previouse_Total_Count1>0) {
                        Log.e("second data", String.valueOf(m_previouse_Total_Count1));
                        pBar1.setVisibility(View.INVISIBLE);
                        /*
                        Toast.makeText(getApplicationContext(), "second " + m_previouse_Total_Count1, Toast.LENGTH_SHORT).show();
                        adapter1.notifyDataSetChanged();
                        pBar1.setVisibility(View.INVISIBLE);
                        */
                    }else{
                        adapter1 = new ProductAdapter(getApplicationContext(), data_list1);
                        mRecyclerView1.setAdapter(adapter1);
                        Log.e("first data: ", String.valueOf(m_previouse_Total_Count1));
                        pBar1.setVisibility(View.INVISIBLE);
                    }
                }else{
                    adapter1.notifyDataSetChanged();
                    pBar1.setVisibility(View.INVISIBLE);
                }
            }
        }
    }
    public class lastProCounter extends CountDownTimer{
        lastProCounter(long millisInFuture, long countDownInterval){
            super(millisInFuture, countDownInterval);
        }
        @Override
        public void onTick(long millisUntilFinished) {
            Log.e("Timer", "still work");
        }

        @Override
        public void onFinish() {
            Log.e("timer", "finish");
            counterStart();
        }
    }
}
