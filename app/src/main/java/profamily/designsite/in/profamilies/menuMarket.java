package profamily.designsite.in.profamilies;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import libes.KeyValueShared;
import libes.footerMarketAction;
import settingMarketData.settingMarketAdapter;
import settingMarketData.settingMarketModule;


public class menuMarket extends AppCompatActivity {

    private RecyclerView recyclerView;
    private settingMarketAdapter settingsMarketAdapter;
    private ArrayList<settingMarketModule> settingModule;

    private String[] catId;
    private String[] catImage;
    private String[] catName;

    private String[] catIdUnsigned = {"0", "9", "5", "6", "7", "8"};
    private String[] catImageUnSigned = {"user_icon", "add_icon", "price_icon", "support_icon", "chat_icon", "settings_icon"};
    private String[] catNameUnSigned = {"دخول عضو", "تسجيل عضو", "طرق الدفع", "الدعم", "المحادثه", "الإعدادات"};

    private String[] catIdSigned = {"1", "2", "3", "4", "5", "6", "7", "8"};
    private String[] catImageSigned = {"user_icon", "my_orders_icon", "my_markets_icon", "logout_icon", "price_icon", "support_icon", "chat_icon", "settings_icon"};
    private String[] catNameSigned = {"بياناتي الشخصيه", "طلباتي", "متاجري", "خروج", "طرق الدفع", "الدعم", "المحادثه", "الإعدادات"};

    LinearLayout lnHome, lnOrders, lnNoti, lnMore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_market);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(KeyValueShared.getId(this) == "") {
            catId = catIdUnsigned;
            catImage = catImageUnSigned;
            catName = catNameUnSigned;
        }else{
            catId = catIdSigned;
            catImage = catImageSigned;
            catName = catNameSigned;
        }
        recyclerView = (RecyclerView)findViewById(R.id.recycleView);
        settingModule = new ArrayList<>();
        for (int i = 0; i < catId.length; i++) {
            Log.e("catid is:", String.valueOf(catId[i]));
            Log.e("cat index is:", String.valueOf(i));
            Log.e("cat length is:", String.valueOf(catId.length));

            settingMarketModule localsetting = new settingMarketModule();
            localsetting.setCatid(catId[i]);
            localsetting.setCatImage(catImage[i]);
            localsetting.setCatName(catName[i]);
            settingModule.add(localsetting);
        }
        settingsMarketAdapter = new settingMarketAdapter(this, settingModule);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(settingsMarketAdapter);
        lnHome = (LinearLayout)findViewById(R.id.btnHome);
        lnOrders = (LinearLayout)findViewById(R.id.btnOrders);
        lnNoti = (LinearLayout)findViewById(R.id.btnNoti);
        lnMore = (LinearLayout)findViewById(R.id.btnMore);
        lnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerMarketAction.footerHome(menuMarket.this);
                finish();
            }
        });
        lnOrders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerMarketAction.footerOrders(menuMarket.this);
                finish();
            }
        });
        lnNoti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerMarketAction.footerNoti(menuMarket.this);
                finish();
            }
        });
        lnMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerMarketAction.footerMore(menuMarket.this);
                finish();
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_haraj, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id= item.getItemId();
        if(id == R.id.add){
            Intent i = new Intent(this, MarketMemberList.class);
            startActivity(i);
        } else if(id == R.id.back){
            Intent i = new Intent(this, marketMain.class);
            startActivity(i);
        }
        return super.onOptionsItemSelected(item);
    }

}
