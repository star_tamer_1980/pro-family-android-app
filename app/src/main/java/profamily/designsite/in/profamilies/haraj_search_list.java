package profamily.designsite.in.profamilies;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import historyData.historyAdapter;
import libes.globalData;
import libes.location;

public class haraj_search_list extends AppCompatActivity {

    private Spinner spCountry;
    private Spinner spCity;
    private ProgressDialog pDialog;
    private Button btnSearch;
    private EditText searchData;
    String ReqSuccess;
    List<String> listCountry = new ArrayList<String>();
    List<String> listcity = new ArrayList<String>();
    ArrayList<String> TypeCountryArray = new ArrayList<String>();
    ArrayAdapter<String> adapterCountry;
    ArrayList<String> subListItem = new ArrayList<String>();
    ArrayAdapter<String> subCountriesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_haraj_search_list);
        location getLocation = new location(this);
        final String lat = getLocation.Langitude;
        final String lon = getLocation.Longitude;
        searchData = (EditText)findViewById(R.id.txtSearch);

        spCountry = (Spinner)findViewById(R.id.sp_country);
        spCity = (Spinner)findViewById(R.id.sp_city);
        spCity.setVisibility(View.INVISIBLE);
        startAct();
        spCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String value = spCountry.getSelectedItem().toString();

                if(value.equals("في جميع المناطق")){
                    spCity.setVisibility(View.INVISIBLE);
                }else{
                    String data[] = new String[2];
                    data[0] = globalData.URLhost() + "getSubCountry.php";
                    data[1] = value;
                    SubCountriesAsynTask bts = new SubCountriesAsynTask();
                    bts.execute(data);
                    spCity.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        String data[] = new String[3];
        data[0] = globalData.URLhost() + "getCountry.php";
        data[1] = "0";
        ProgressTask bt = new ProgressTask();
        bt.execute(data);
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("جاري تحميل البيانات");
        pDialog.setTitle("برو فاميليز");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
        pDialog.show();
        btnSearch = (Button)findViewById(R.id.btnSearch);
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToSearch();
            }
        });
    }
    private void goToSearch(){
        if(searchData.getText().toString().matches("")){
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.mustWriteSearch), Toast.LENGTH_SHORT).show();
            searchData.setError(getString(R.string.mustWriteSearch));
            ((TextView)spCountry.getSelectedView()).setError(null);
            ((TextView)spCountry.getSelectedView()).clearFocus();

        }else{
            if(spCountry.getSelectedItemId() == 0){
                searchData.setError(null);
                searchData.clearFocus();
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.mustChooseCountry), Toast.LENGTH_SHORT).show();
                ((TextView)spCountry.getSelectedView()).setError("");
            }else{

                Intent i = new Intent(haraj_search_list.this, haraj_search.class);
                i.putExtra("txtSearch", searchData.getText().toString());
                i.putExtra("CountryName", spCountry.getSelectedItem().toString());
                i.putExtra("CityName", spCity.getSelectedItem().toString());
                startActivity(i);
            }
        }
    }
    private void startAct() {

        //TypeweightArray = getResources().getStringArray(R.array.list_weight);
        adapterCountry = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, TypeCountryArray);
        adapterCountry.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCountry.setAdapter(adapterCountry);
        subCountriesAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, subListItem);
        subCountriesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCity.setAdapter(subCountriesAdapter);
    }
    public class ProgressTask extends AsyncTask<String, Void, Boolean> {

        private String title;
        private String content;
        private String img;
        @SuppressWarnings("deprecation")
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
                nameValuePairs.add(new BasicNameValuePair("id", params[1]));
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));

                HttpResponse response = client.execute(post);

                int status = response.getStatusLine().getStatusCode();
                if(status == 200){
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    JSONObject jObj = new JSONObject(data.substring(data.indexOf("{"), data.lastIndexOf("}") + 1));
                    ReqSuccess = jObj.getString("success");
                    if(ReqSuccess.equals("1")){
                        listCountry.add("في جميع الدول");
                        if(jObj.has("data")) {
                            JSONArray wee = jObj.getJSONArray("data");
                            for (int i = 0; i < wee.length(); i++) {
                                JSONObject c = wee.getJSONObject(i);
                                listCountry.add(c.getString("title"));
                            }
                        }else{
                            listCountry.add("لا يوجد دول");
                        }

                    }
                    return true;
                }else{
                    Log.e("error client string", "here");
                }
            } catch (ClientProtocolException e) {
                Log.e("error client string", e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("error exp", e.getMessage());
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("error json", e.getMessage());
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            pDialog.dismiss();
            super.onPostExecute(result);
            if(result == false){
                Toast.makeText(getApplicationContext(), "حدث خطأ، نرجو المحاوله في وقت لاحق", Toast.LENGTH_LONG).show();
            }else{
                if(ReqSuccess.equals("1")){

                    TypeCountryArray.clear();
                    TypeCountryArray.addAll(listCountry);
                    adapterCountry.notifyDataSetChanged();

                }else{
                    Toast.makeText(getApplicationContext(), "لم يتم حفظ البيانات", Toast.LENGTH_LONG).show();
                }
            }
        }
    }
    public class SubCountriesAsynTask extends AsyncTask<String, Void, Boolean>{

        private String title;
        private String content;
        private String img;
        @SuppressWarnings("deprecation")
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
                nameValuePairs.add(new BasicNameValuePair("value", params[1]));
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));

                HttpResponse response = client.execute(post);

                int status = response.getStatusLine().getStatusCode();
                if(status == 200){
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    JSONObject jObj = new JSONObject(data.substring(data.indexOf("{"), data.lastIndexOf("}") + 1));
                    ReqSuccess = jObj.getString("success");
                    if(ReqSuccess.equals("1")){
                        listcity.clear();
                        listcity.add("في جميع المدن");
                        if(jObj.has("data")) {
                            JSONArray wee = jObj.getJSONArray("data");
                            for (int i = 0; i < wee.length(); i++) {
                                JSONObject c = wee.getJSONObject(i);
                                listcity.add(c.getString("title"));
                            }
                        }else{
                            listcity.add("لا يوجد دول");
                        }

                    }else{
                        listcity.add("لا يوجد دول");
                    }


                    return true;
                }else{
                    Log.e("error client string", "here");
                }
            } catch (ClientProtocolException e) {
                Log.e("error client string", e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("error exp", e.getMessage());
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("error json", e.getMessage());
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            pDialog.dismiss();
            super.onPostExecute(result);
            if(result == false){
                Toast.makeText(getApplicationContext(), "حدث خطأ، نرجو المحاوله في وقت لاحق", Toast.LENGTH_LONG).show();
            }else{
                if(ReqSuccess.equals("1")){

                    subListItem.clear();
                    subListItem.addAll(listcity);
                    subCountriesAdapter.notifyDataSetChanged();

                }else{
                    Toast.makeText(getApplicationContext(), "لم يتم حفظ البيانات", Toast.LENGTH_LONG).show();
                }
            }
        }

    }

}
