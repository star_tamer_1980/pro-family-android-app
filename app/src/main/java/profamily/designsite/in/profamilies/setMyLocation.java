package profamily.designsite.in.profamilies;

import android.graphics.Typeface;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import libes.KeyValueShared;
import libes.globalData;
import libes.location;

public class setMyLocation extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    String lat = "24.7255566", lon = "47.1020276", ReqSuccess;

    EditText nameLocation, etLocation;
    Button btnSaveMyLocation;
    ImageView locationList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_my_location);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        btnSaveMyLocation = (Button)findViewById(R.id.saveMyLocation);
        btnSaveMyLocation.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        btnSaveMyLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                KeyValueShared.setMapLat(getApplicationContext(), lat);
                KeyValueShared.setMapLon(getApplicationContext(), lon);
                finish();
            }
        });
        location getLocation = new location(this);
        if(getLocation.Langitude != "0" && getLocation.Longitude != "0") {
            lat = getLocation.Langitude;
            lon = getLocation.Longitude;
        }
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(Float.parseFloat(lat), Float.parseFloat(lon));
        mMap.addMarker(new MarkerOptions().position(sydney).title("My location"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 9));
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                mMap.clear();
                mMap.addMarker(new MarkerOptions().position(latLng));
                lat = String.valueOf(latLng.latitude);
                lon = String.valueOf(latLng.longitude);
            }
        });
    }
}
