package profamily.designsite.in.profamilies;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import libes.footerMarketAction;

public class marketSupportList extends AppCompatActivity {

    LinearLayout ln1, ln2, ln3, ln4, ln5, ln6, ln7, ln8, ln9, ln10;
    LinearLayout lnHome, lnOrders, lnNoti, lnMore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_market_support_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ln1 = (LinearLayout)findViewById(R.id.ln_1);
        ln2 = (LinearLayout)findViewById(R.id.ln_2);
        ln3 = (LinearLayout)findViewById(R.id.ln_3);
        ln4 = (LinearLayout)findViewById(R.id.ln_4);
        ln5 = (LinearLayout)findViewById(R.id.ln_5);
        ln6 = (LinearLayout)findViewById(R.id.ln_6);
        ln7 = (LinearLayout)findViewById(R.id.ln_7);
        ln8 = (LinearLayout)findViewById(R.id.ln_8);
        ln9 = (LinearLayout)findViewById(R.id.ln_9);
        ln10 = (LinearLayout)findViewById(R.id.ln_10);
        ln1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(marketSupportList.this, pageDetails.class);
                i.putExtra("id", "13");
                startActivity(i);
            }
        });
        ln2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(marketSupportList.this, pageDetails.class);
                i.putExtra("id", "14");
                startActivity(i);
            }
        });
        ln3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(marketSupportList.this, pageDetails.class);
                i.putExtra("id", "15");
                startActivity(i);
            }
        });
        ln4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(marketSupportList.this, pageDetails.class);
                i.putExtra("id", "12");
                startActivity(i);
            }
        });
        ln5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(marketSupportList.this, pageDetails.class);
                i.putExtra("id", "7");
                startActivity(i);
            }
        });
        ln6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(marketSupportList.this, pageDetails.class);
                i.putExtra("id", "6");
                startActivity(i);
            }
        });
        ln7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(marketSupportList.this, pageDetails.class);
                i.putExtra("id", "9");
                startActivity(i);
            }
        });
        ln8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(marketSupportList.this, pageDetails.class);
                i.putExtra("id", "10");
                startActivity(i);
            }
        });
        ln9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(marketSupportList.this, pageDetails.class);
                i.putExtra("id", "11");
                startActivity(i);
            }
        });
        ln10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(marketSupportList.this, pageDetails.class);
                i.putExtra("id", "18");
                startActivity(i);
            }
        });
        lnHome = (LinearLayout)findViewById(R.id.btnHome);
        lnOrders = (LinearLayout)findViewById(R.id.btnOrders);
        lnNoti = (LinearLayout)findViewById(R.id.btnNoti);
        lnMore = (LinearLayout)findViewById(R.id.btnMore);
        lnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerMarketAction.footerHome(marketSupportList.this);
                finish();
            }
        });
        lnOrders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerMarketAction.footerOrders(marketSupportList.this);
                finish();
            }
        });
        lnNoti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerMarketAction.footerNoti(marketSupportList.this);
                finish();
            }
        });
        lnMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerMarketAction.footerMore(marketSupportList.this);
                finish();
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_haraj, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id= item.getItemId();
        if(id == R.id.add){
            Intent i = new Intent(this, MarketMemberList.class);
            startActivity(i);
        } else if(id == R.id.back){
            Intent i = new Intent(this, menuMarket.class);
            startActivity(i);
        }
        return super.onOptionsItemSelected(item);
    }
    public void goToIntent(String id){
        Toast.makeText(getApplicationContext(), id, Toast.LENGTH_SHORT).show();
    }
}
