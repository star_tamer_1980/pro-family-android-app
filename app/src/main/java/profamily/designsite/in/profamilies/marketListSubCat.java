package profamily.designsite.in.profamilies;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import libes.KeyValueShared;
import libes.globalData;
import noti.notificationAdmin;
import noti.notificationAdminAdapter;
import subCatData.subCat;
import subCatData.subCatAdapter;

public class marketListSubCat extends AppCompatActivity {

    private RecyclerView mRecyclerView1;
    private GridLayoutManager gridLayoutManager;
    private subCatAdapter adapter1;
    private List<subCat> data_list1;
    private String dataTest;
    private ProgressDialog pDialog;
    private ProgressBar pBar1;
    List<String> listNoti = new ArrayList<String>();
    String ReqSuccess;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_market_list_sub_cat);
        mRecyclerView1 = (RecyclerView)findViewById(R.id.recycleView1);
        mRecyclerView1.setLayoutManager(new LinearLayoutManager(this));
        pBar1 = (ProgressBar)findViewById(R.id.progressBar1);
        data_list1 = new ArrayList<>();

        String data[] = new String[4];
        data[0] = globalData.URLhost() + "marketListSubCat.php";
        data[1] = getIntent().getStringExtra("cat");
        data[2] = KeyValueShared.getMapLat(getApplicationContext());
        data[3] = KeyValueShared.getMapLon(getApplicationContext());
        loadData1 bts = new loadData1();
        bts.execute(data);
    }
    public class loadData1 extends AsyncTask<String, Void, Boolean> {

        private String title;
        private String content;
        @SuppressWarnings("deprecation")
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
                nameValuePairs.add(new BasicNameValuePair("cat", params[1]));
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if(status == 200){
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    JSONObject jObj = new JSONObject(data.substring(data.indexOf("{"), data.lastIndexOf("}") + 1));
                    ReqSuccess = jObj.getString("success");
                    String cat = jObj.getString("cat");
                    if(ReqSuccess.equals("1")){
                        dataTest = data;
                        if(jObj.has("data")) {
                            JSONArray wee = jObj.getJSONArray("data");
                            subCat firstSubCat = new subCat();
                            firstSubCat.setId("0");
                            firstSubCat.setCategory(getResources().getString(R.string.cat_all_category));
                            firstSubCat.setLat(params[2]);
                            firstSubCat.setLon(params[3]);
                            firstSubCat.setIdMainCat(cat);
                            data_list1.add(firstSubCat);
                            for (int i = 0; i < wee.length(); i++) {
                                JSONObject c = wee.getJSONObject(i);

                                subCat dataSubCat = new subCat();
                                dataSubCat.setId(c.getString("id"));
                                dataSubCat.setCategory(c.getString("title"));
                                dataSubCat.setLat(params[2]);
                                dataSubCat.setLon(params[3]);
                                dataSubCat.setIdMainCat(cat);
                                data_list1.add(dataSubCat);
                            }
                        }

                    }


                    return true;
                }else{
                    Log.e("error client string", "here");
                }
            } catch (ClientProtocolException e) {
                Log.e("error client string", e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("error exp", e.getMessage());
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("error json", e.getMessage());
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if(result == false){
                Toast.makeText(getApplicationContext(), "حدث خطأ، نرجو المحاوله في وقت لاحق", Toast.LENGTH_LONG).show();
            }else{
                if(ReqSuccess.equals("1")){
                        adapter1 = new subCatAdapter(getApplicationContext(), data_list1);
                        mRecyclerView1.setAdapter(adapter1);
                        pBar1.setVisibility(View.INVISIBLE);
                }else{
                    pBar1.setVisibility(View.INVISIBLE);
                }
            }
        }
    }
}
