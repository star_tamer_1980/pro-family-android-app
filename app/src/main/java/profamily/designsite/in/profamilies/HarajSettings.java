package profamily.designsite.in.profamilies;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import libes.KeyValueShared;
import libes.footerHarajAction;
import menuSettings.menuSettings;
import menuSettings.menuAdapter;

public class HarajSettings extends AppCompatActivity {

    private RecyclerView mvMenu;

    private menuAdapter menuAdapter;
    private List<menuSettings> menuArray;
    LinearLayout lnHome, lnSearch, lnNoti, lnComments, lnMore;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_haraj_settings);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        lnHome = (LinearLayout)findViewById(R.id.btnHaraj);
        lnSearch = (LinearLayout)findViewById(R.id.btnSearch);
        lnNoti = (LinearLayout)findViewById(R.id.btnNoti);
        lnComments = (LinearLayout)findViewById(R.id.btnComments);
        lnMore = (LinearLayout)findViewById(R.id.btnMore);
        lnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerHarajAction.footerHome(HarajSettings.this);
                finish();
            }
        });
        lnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerHarajAction.footerSearch(HarajSettings.this);
            }
        });
        lnNoti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerHarajAction.footerNoti(HarajSettings.this);
                finish();
            }
        });
        lnComments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerHarajAction.footerComments(HarajSettings.this);
                finish();
            }
        });
        lnMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerHarajAction.footerMore(HarajSettings.this);
                finish();
            }
        });
        mvMenu = (RecyclerView)findViewById(R.id.menuHarajSettings);
        mvMenu.setHasFixedSize(true);
        mvMenu.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        setMenu();
    }
    @Override
    protected void onRestart() {
        setMenu();
        super.onRestart();
    }
    public void setMenu(){
        String[] menuName;
        String[] menuId;
        String[] menuIcon;
        menuArray = new ArrayList<>();
        if(KeyValueShared.getId(this).equals("0")) {
            menuName = getApplicationContext().getResources().getStringArray(R.array.harajMenuForUsersName);
            menuId = getApplicationContext().getResources().getStringArray(R.array.harajMenuForUsersId);
            menuIcon = getApplicationContext().getResources().getStringArray(R.array.harajMenuForUsersIcon);
        }else{
            menuName = getApplicationContext().getResources().getStringArray(R.array.harajMenuForMembersName);
            menuId = getApplicationContext().getResources().getStringArray(R.array.harajMenuForMembersId);
            menuIcon = getApplicationContext().getResources().getStringArray(R.array.harajMenuForMembersIcon);
        }
        for(int i=0; i < menuName.length; i++){
            menuSettings menuData = new menuSettings();
            menuData.setMenuId(menuId[i].toString());
            if(menuId[i].toString().equals("3")){
                String name = menuName[i].toString() + " (" + KeyValueShared.getSettingsAppMyCityName(getApplicationContext()) + ")";
                menuData.setMenuName(name);
            }else if(menuId[i].toString().equals("7")){
                String name = menuName[i].toString() + " (" + KeyValueShared.getName(getApplicationContext()) + ")";
                menuData.setMenuName(name);
            }else{
                menuData.setMenuName(menuName[i].toString());
            }

            menuData.setMenuIcon(menuIcon[i].toString());
            Log.e("name", menuName[i].toString());
            menuArray.add(menuData);
        }
        menuAdapter = new menuAdapter(getApplication(), menuArray);
        mvMenu.setAdapter(menuAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_haraj, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.add){
            Intent i = new Intent(HarajSettings.this, HarajAddSwicherMedia.class);
            startActivity(i);
        } else if(id == R.id.back){
            Intent i = new Intent(HarajSettings.this, MainHaraj.class);
            startActivity(i);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
