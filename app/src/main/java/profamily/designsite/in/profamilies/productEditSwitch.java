package profamily.designsite.in.profamilies;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import libes.globalData;

public class productEditSwitch extends AppCompatActivity {

    Button editData, editImage, editRun, editStop, editDelete;
    String idProduct, idMarket, isAllow, img, ReqSuccess, ReqMessage;
    private ProgressDialog pDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_edit_switch);
        idProduct = getIntent().getStringExtra("idProduct");
        idMarket = getIntent().getStringExtra("idMarket");
        isAllow = getIntent().getStringExtra("isAllow");
        img = getIntent().getStringExtra("img");
        Toast.makeText(this, isAllow, Toast.LENGTH_SHORT).show();
        editData = (Button)findViewById(R.id.btn_Product_Edit);
        editData.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        editImage = (Button)findViewById(R.id.btn_Product_Image);
        editImage.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        editRun = (Button)findViewById(R.id.btn_Product_Run);
        editRun.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        editStop = (Button)findViewById(R.id.btn_Product_Stop);
        editStop.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        editDelete = (Button)findViewById(R.id.btn_Product_Delete);
        editDelete.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        editImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(productEditSwitch.this, ProductAddImage.class);
                i.putExtra("idProduct", idProduct);
                i.putExtra("img", img);
                startActivity(i);
            }
        });
        editRun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String data[] = new String[3];
                data[0] = globalData.URLhost() + "productEditIsAllow.php";
                data[1] = idProduct;
                data[2] = "0";
                changeIsWork bts = new changeIsWork();
                bts.execute(data);
                pDialog = new ProgressDialog(productEditSwitch.this);
                pDialog.setMessage("جاري تشغيل المنتج");
                pDialog.setTitle("برو فاميليز");
                pDialog.setIndeterminate(false);
                pDialog.setCancelable(true);
                pDialog.show();
                editStop.setVisibility(View.VISIBLE);
                editRun.setVisibility(View.GONE);
            }
        });
        editStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String data[] = new String[3];
                data[0] = globalData.URLhost() + "productEditIsAllow.php";
                data[1] = idProduct;
                data[2] = "1";
                changeIsWork bts = new changeIsWork();
                bts.execute(data);
                pDialog = new ProgressDialog(productEditSwitch.this);
                pDialog.setMessage("جاري ايقاف المنتج");
                pDialog.setTitle("برو فاميليز");
                pDialog.setIndeterminate(false);
                pDialog.setCancelable(true);
                pDialog.show();
                editStop.setVisibility(View.GONE);
                editRun.setVisibility(View.VISIBLE);
            }
        });
        editData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(productEditSwitch.this, ProductEditData.class);
                i.putExtra("idProduct", idProduct);
                i.putExtra("idMarket", idMarket);
                startActivity(i);
            }
        });
        editDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    builder = new AlertDialog.Builder(productEditSwitch.this);
                } else {
                    builder = new AlertDialog.Builder(productEditSwitch.this);
                }
                builder.setTitle(getResources().getString(R.string.alert_title_order_delete))
                        .setMessage(getResources().getString(R.string.alert_message_order_delete))
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                String data[] = new String[2];
                                data[0] = globalData.URLhost() + "productDelete.php";
                                data[1] = idProduct;
                                deleteProduct bts = new deleteProduct();
                                bts.execute(data);
                                pDialog = new ProgressDialog(productEditSwitch.this);
                                pDialog.setMessage("جاري حذف المنتج");
                                pDialog.setTitle("برو فاميليز");
                                pDialog.setIndeterminate(false);
                                pDialog.setCancelable(true);
                                pDialog.show();
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Toast.makeText(getApplicationContext(), "delete no", Toast.LENGTH_LONG).show();
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });
        if(isAllow.equals("0")){
            editStop.setVisibility(View.VISIBLE);
            editRun.setVisibility(View.GONE);
        }else{
            editStop.setVisibility(View.GONE);
            editRun.setVisibility(View.VISIBLE);
        }
    }
public class deleteProduct extends AsyncTask<String, Void, Boolean> {


    @SuppressWarnings("deprecation")
    @Override
    protected Boolean doInBackground(String... params) {
        try {
            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(params[0]);
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
            nameValuePairs.add(new BasicNameValuePair("idProduct", params[1]));
            post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));
            HttpResponse response = client.execute(post);
            int status = response.getStatusLine().getStatusCode();
            if(status == 200){
                HttpEntity entity = response.getEntity();
                String data = EntityUtils.toString(entity);
                Log.e("markets", data);
                JSONObject jObj = new JSONObject(data.substring(data.indexOf("{"), data.lastIndexOf("}") + 1));
                ReqSuccess = jObj.getString("success");
                ReqMessage = jObj.getString("message");
                return true;
            }else{
                Log.e("error client string", "here");
            }
        } catch (ClientProtocolException e) {
            Log.e("error client string", e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            Log.e("error exp", e.getMessage());
            e.printStackTrace();
        } catch (JSONException e) {
            Log.e("error json", e.getMessage());
            e.printStackTrace();
        }

        return false;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        pDialog.dismiss();
        super.onPostExecute(result);
        if(result == false){
            Toast.makeText(getApplicationContext(), "حدث خطأ، نرجو المحاوله في وقت لاحق", Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(getApplicationContext(), ReqMessage, Toast.LENGTH_SHORT).show();
            finish();
        }
    }
}
public class changeIsWork extends AsyncTask<String, Void, Boolean> {


    @SuppressWarnings("deprecation")
    @Override
    protected Boolean doInBackground(String... params) {
        try {
            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(params[0]);
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
            nameValuePairs.add(new BasicNameValuePair("id", params[1]));
            nameValuePairs.add(new BasicNameValuePair("isAllow", params[2]));
            post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));
            HttpResponse response = client.execute(post);
            int status = response.getStatusLine().getStatusCode();
            if(status == 200){
                HttpEntity entity = response.getEntity();
                String data = EntityUtils.toString(entity);
                Log.e("markets", data);
                JSONObject jObj = new JSONObject(data.substring(data.indexOf("{"), data.lastIndexOf("}") + 1));
                ReqSuccess = jObj.getString("success");
                ReqMessage = jObj.getString("message");
                return true;
            }else{
                Log.e("error client string", "here");
            }
        } catch (ClientProtocolException e) {
            Log.e("error client string", e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            Log.e("error exp", e.getMessage());
            e.printStackTrace();
        } catch (JSONException e) {
            Log.e("error json", e.getMessage());
            e.printStackTrace();
        }

        return false;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        pDialog.dismiss();
        super.onPostExecute(result);
        if(result == false){
            Toast.makeText(getApplicationContext(), "حدث خطأ، نرجو المحاوله في وقت لاحق", Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(getApplicationContext(), ReqMessage, Toast.LENGTH_LONG).show();
        }
    }
}
}
