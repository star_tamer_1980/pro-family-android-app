package profamily.designsite.in.profamilies;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.Toolbar;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

import libes.KeyValueShared;
import libes.globalData;

public class settingsApp_myCity extends AppCompatActivity {

    private Spinner spCity;
    private ProgressDialog pDialog;
    String ReqSuccess;
    List<String> listcity = new ArrayList<String>();
    List<String> listcityId = new ArrayList<String>();
    ArrayList<String> subListItem = new ArrayList<String>();
    ArrayAdapter<String> subCountriesAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_settings_app_my_city);

        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        spCity = (Spinner)findViewById(R.id.sp_city);
        spCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.e("selected is", spCity.getSelectedItem().toString());
                Log.e("selected id is", String.valueOf(spCity.getSelectedItemId()));
                //String id = String.valueOf(listcity.indexOf(String.valueOf(spCity.getSelectedItemId())));
                Log.e("selected ids is", String.valueOf(listcityId.get((int) spCity.getSelectedItemId())));
                KeyValueShared.setSettingsAppMyCityName(getApplicationContext(),spCity.getSelectedItem().toString());
                KeyValueShared.setSettingsAppMyCityId(getApplicationContext(),String.valueOf(listcityId.get((int) spCity.getSelectedItemId())));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Log.e("selected is", "no selected");
            }
        });
        subCountriesAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, subListItem);
        subCountriesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCity.setAdapter(subCountriesAdapter);
        String data[] = new String[2];
        data[0] = globalData.URLhost() + "getSubCountry.php";
        data[1] = KeyValueShared.getSettingsAppCountryId(this);
        SubCountriesAsynTask bt = new SubCountriesAsynTask();
        bt.execute(data);
        pDialog = new ProgressDialog(settingsApp_myCity.this);
        pDialog.setMessage("جاري تحميل البيانات");
        pDialog.setTitle("برو فاميليز");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
        pDialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_haraj, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.add){
            Intent i = new Intent(settingsApp_myCity.this, HarajAddSwicherMedia.class);
            startActivity(i);
        } else if(id == R.id.back){
            Intent i = new Intent(settingsApp_myCity.this, MainHaraj.class);
            startActivity(i);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
    public class SubCountriesAsynTask extends AsyncTask<String, Void, Boolean>{

        private String title;
        private String content;
        private String img;
        @SuppressWarnings("deprecation")
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
                nameValuePairs.add(new BasicNameValuePair("value", params[1]));
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));

                HttpResponse response = client.execute(post);

                int status = response.getStatusLine().getStatusCode();
                if(status == 200){
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    JSONObject jObj = new JSONObject(data.substring(data.indexOf("{"), data.lastIndexOf("}") + 1));
                    ReqSuccess = jObj.getString("success");
                    Log.e("city is: ", data);
                    if(ReqSuccess.equals("1")){
                        listcity.clear();
                        listcity.add(getResources().getString(R.string.haraj_add_choose_city));
                        listcityId.clear();
                        listcityId.add("0");
                        if(jObj.has("data")) {
                            JSONArray wee = jObj.getJSONArray("subCountry");
                            for (int i = 0; i < wee.length(); i++) {
                                JSONObject c = wee.getJSONObject(i);
                                listcity.add(c.getString("title"));
                                listcityId.add(c.getString("id"));
                                Log.e("title is", c.getString("title"));
                                Log.e("id is", c.getString("id"));
                            }
                        }else{
                            listcity.add("لا يوجد مدن");
                        }

                    }else{
                        listcity.add("لا يوجد مدن");
                    }


                    return true;
                }else{
                    Log.e("error client string", "here");
                }
            } catch (ClientProtocolException e) {
                Log.e("error client string", e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("error exp", e.getMessage());
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("error json", e.getMessage());
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            pDialog.dismiss();
            super.onPostExecute(result);
            if(result == false){
                Toast.makeText(getApplicationContext(), "حدث خطأ، نرجو المحاوله في وقت لاحق", Toast.LENGTH_LONG).show();
            }else{
                if(ReqSuccess.equals("1")){

                    subListItem.clear();
                    subListItem.addAll(listcity);
                    int indexItem = listcityId.indexOf(KeyValueShared.getSettingsAppMyCityId(getApplicationContext()));
                    spCity.setSelection(indexItem);
                    subCountriesAdapter.notifyDataSetChanged();
                    Log.e("city id", KeyValueShared.getSettingsAppMyCityId(getApplicationContext()));
                }else{
                    Toast.makeText(getApplicationContext(), "لم يتم حفظ البيانات", Toast.LENGTH_LONG).show();
                }
            }
        }

    }
}
