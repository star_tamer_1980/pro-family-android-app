package profamily.designsite.in.profamilies;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.ToggleButton;

public class serviceAddAgreement extends AppCompatActivity {

    private ToggleButton btn1, btn2;
    private boolean btnStatue1 = false, btnStatue2 = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_add_agreement);
        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        btn1 = (ToggleButton)findViewById(R.id.tog1);
        btn2 = (ToggleButton)findViewById(R.id.tog2);
        btn1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    btnStatue1 = true;
                }else{
                    btnStatue1 = false;
                }
                if(btnStatue1 == true && btnStatue2 == true){
                    Intent i = new Intent(serviceAddAgreement.this, serviceAdd.class);
                    startActivity(i);
                }
            }
        });
        btn2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    btnStatue2 = true;
                }else{
                    btnStatue2 = false;
                }
                if(btnStatue1 == true && btnStatue2 == true){
                    Intent i = new Intent(serviceAddAgreement.this, serviceAdd.class);
                    startActivity(i);
                    finish();
                }
            }
        });
    }
}
