package profamily.designsite.in.profamilies;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.TabHost;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import adapters.MyFragmentPagerAdapterProduct;
import fragments.service.service1;
import fragments.service.service2;
import fragments.service.service3;
import fragments.service.service4;
import fragments.service.service5;
import fragments.service.service6;
import fragments.service.service7;
import fragments.vegtables.vegtables1;
import fragments.vegtables.vegtables2;
import fragments.vegtables.vegtables3;
import fragments.vegtables.vegtables4;
import libes.KeyValueShared;
import libes.SQLiteConnection;
import libes.globalData;

public class marketFragServicesList extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, ViewPager.OnPageChangeListener, TabHost.OnTabChangeListener {

    ViewPager viewPager;
    TabHost tabHost;
    NavigationView navigationView;
    SQLiteConnection db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_market_frag_eat);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        db = new SQLiteConnection(getApplicationContext());
        db.deleteAllRecord();

        initViewPager();
        initTabHost();
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        setMenu();

    }

    public void setMenu(){
        if(KeyValueShared.getStatue(getApplicationContext()).equals("1")){
            navigationView.getMenu().setGroupVisible(R.id.groupLogin, true);
        }else{
            navigationView.getMenu().setGroupVisible(R.id.groupLogOut, true);
        }
        navigationView.setNavigationItemSelectedListener(this);
    }
    private void initTabHost() {
        tabHost = (TabHost) findViewById(R.id.tabhost);
        tabHost.setup();
        String[] tabNames = {getResources().getString(R.string.allData),
                getResources().getString(R.string.fragSebaka),
                getResources().getString(R.string.fragNegara),
                getResources().getString(R.string.fragCarsMecanic),
                getResources().getString(R.string.fragCarsElectric),
                getResources().getString(R.string.fragCreateBuilding),
                getResources().getString(R.string.fragServiceDelivery),
        };
        for (int i=0; i<tabNames.length; i++){
            TabHost.TabSpec tabSpec;
            tabSpec = tabHost.newTabSpec(tabNames[i]);
            tabSpec.setIndicator(tabNames[i]);
            tabSpec.setContent(new marketFragServicesList.FakeContent(getApplicationContext()));
            tabHost.addTab(tabSpec);
        }
        for(int i=0;i<tabHost.getTabWidget().getChildCount();i++) {
            tabHost.getTabWidget().getChildAt(i).setBackgroundColor(getResources().getColor(R.color.nav_bg_press)); //unselected
            TextView tv=(TextView) tabHost.getTabWidget().getChildAt(i).findViewById(android.R.id.title);
            tv.setTextColor(this.getResources().getColorStateList(R.color.tabTxtColor));
            tv.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        }
        tabHost.getTabWidget().getChildAt(0).setBackgroundColor(getResources().getColor(R.color.navigationBarColor));
        tabHost.setOnTabChangedListener(this);
    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        db.deleteAllRecord();
        finish();
        startActivity(getIntent());
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_haraj_add) {
            Intent i = new Intent(marketFragServicesList.this, HarajAddSwicherMedia.class);
            i.putExtra("id", "7");
            startActivity(i);
        } else if (id == R.id.nav_personal_data) {
            Intent i = new Intent(marketFragServicesList.this, personalData.class);
            startActivity(i);
        } else if (id == R.id.nav_orders_display) {
            Intent i = new Intent(marketFragServicesList.this, orders_switch.class);
            startActivity(i);
        } else if (id == R.id.nav_markets_display) {
            Intent i = new Intent(marketFragServicesList.this, MarketMemberList.class);
            startActivity(i);
        } else if (id == R.id.nav_notification_admin) {
            Intent i = new Intent(marketFragServicesList.this, notifi.class);
            startActivity(i);
        } else if (id == R.id.nav_logout) {
            KeyValueShared.setStatue(getApplicationContext(), "0");
            KeyValueShared.setName(getApplicationContext(), "");
            KeyValueShared.setTel(getApplicationContext(), "");
            KeyValueShared.setEmail(getApplicationContext(), "");
            KeyValueShared.setId(getApplicationContext(), "0");
            finish();
            startActivity(getIntent());
        } else if (id == R.id.haraj_register) {
            Intent i = new Intent(marketFragServicesList.this, userRegister.class);
            startActivity(i);
        } else if (id == R.id.haraj_login) {
            Intent i = new Intent(marketFragServicesList.this, userLogin.class);
            startActivity(i);
        } else if (id == R.id.nav_about) {
            Intent i = new Intent(marketFragServicesList.this, pageDetails.class);
            i.putExtra("id", "7");
            startActivity(i);
        } else if (id == R.id.nav_privacy) {
            Intent i = new Intent(marketFragServicesList.this, pageDetails.class);
            i.putExtra("id", "12");
            startActivity(i);
        } else if (id == R.id.nav_contact) {
            Intent i = new Intent(marketFragServicesList.this, contactUs.class);
            startActivity(i);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int selectedItem) {
        tabHost.setCurrentTab(selectedItem);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onTabChanged(String tabId) {
        int selectedItem = tabHost.getCurrentTab();
        viewPager.setCurrentItem(selectedItem);
        HorizontalScrollView hScrollView = (HorizontalScrollView)findViewById(R.id.h_scroll_view);
        View tabView = tabHost.getCurrentTabView();

        for(int i=0;i<tabHost.getTabWidget().getChildCount();i++) {
            tabHost.getTabWidget().getChildAt(i).setBackgroundColor(getResources().getColor(R.color.nav_bg_press)); //unselected
            TextView tv=(TextView) tabHost.getTabWidget().getChildAt(i).findViewById(android.R.id.title);
            tv.setTextColor(this.getResources().getColorStateList(R.color.tabTxtColor));
            tv.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        }
        tabHost.getTabWidget().getChildAt(tabHost.getCurrentTab()).setBackgroundColor(getResources().getColor(R.color.navigationBarColor)); // selected
        int scrollPos = tabView.getLeft()
                - (hScrollView.getWidth() - tabView.getWidth()) / 2;
        hScrollView.smoothScrollTo(scrollPos, 0);
    }

    public class FakeContent implements TabHost.TabContentFactory{
        Context context;
        public FakeContent(Context mcontext){
            context = mcontext;
        }
        @Override
        public View createTabContent(String tag) {
            View fakeView = new View(context);
            fakeView.setMinimumHeight(0);
            fakeView.setMinimumWidth(0);
            return fakeView;
        }
    }

    private void initViewPager() {

        viewPager = (ViewPager)findViewById(R.id.view_pager);

        List<Fragment> listFragments = new ArrayList<Fragment>();

        listFragments.add(new service1());
        listFragments.add(new service2());
        listFragments.add(new service3());
        listFragments.add(new service4());
        listFragments.add(new service5());
        listFragments.add(new service6());
        listFragments.add(new service7());

        MyFragmentPagerAdapterProduct myFragmentPagerAdapterProduct = new MyFragmentPagerAdapterProduct(
                getSupportFragmentManager(), listFragments);
        viewPager.setAdapter(myFragmentPagerAdapterProduct);
        viewPager.setOnPageChangeListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_market_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id= item.getItemId();
        if(id == R.id.add_market){
            Intent i = new Intent(marketFragServicesList.this, MarketMemberList.class);
            startActivity(i);
        } else if(id == R.id.notification){
            Intent i = new Intent(marketFragServicesList.this, notification_alerts.class);
            startActivity(i);
        }
        return super.onOptionsItemSelected(item);
    }
}
