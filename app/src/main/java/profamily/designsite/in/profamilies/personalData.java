package profamily.designsite.in.profamilies;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import libes.KeyValueShared;
import libes.globalData;

public class personalData extends AppCompatActivity {

    private EditText etTel;
    private EditText etName;
    private EditText etEmail;
    private Button btnSave;
    String ReqSuccess , ReqMessage;
    private ProgressDialog pDialog;
    String regName, regTel, regEmail, regId;
    private TextView tvTel;
    private TextView tvName;
    private TextView tvEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_data);
        etTel = (EditText)findViewById(R.id.etTel);
        etTel.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        tvTel = (TextView)findViewById(R.id.tv_tel);
        tvTel.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        etName = (EditText)findViewById(R.id.etName);
        etName.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        tvName = (TextView)findViewById(R.id.tv_name);
        tvName.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        etEmail = (EditText)findViewById(R.id.etEmail);
        etEmail.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        tvEmail = (TextView)findViewById(R.id.tv_email);
        tvEmail.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        btnSave = (Button)findViewById(R.id.btnSave);
        btnSave.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        etTel.setText(KeyValueShared.getTel(getApplicationContext()));
        etName.setText(KeyValueShared.getName(getApplicationContext()));
        etEmail.setText(KeyValueShared.getEmail(getApplicationContext()));
        etTel.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_NEXT || actionId == EditorInfo.IME_ACTION_DONE){
                    loginProccess();
                    return true;
                }
                return false;
            }
        });
        etName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_NEXT || actionId == EditorInfo.IME_ACTION_DONE){
                    loginProccess();
                    return true;
                }
                return false;
            }
        });
        etEmail.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_NEXT || actionId == EditorInfo.IME_ACTION_DONE){
                    loginProccess();
                    return true;
                }
                return false;
            }
        });
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginProccess();
            }
        });
    }

    private void loginProccess(){
        String Tel = etTel.getText().toString();
        String name = etName.getText().toString();
        String email = etEmail.getText().toString();
        if(TextUtils.isEmpty(Tel)){
            etTel.setError((getString(R.string.mustWriteTel)));
        }else if(TextUtils.isEmpty(name)){
            etName.setError(getString(R.string.mustWriteName));
        }else{
            String data[] = new String[5];
            data[0] = globalData.URLhost() + "memberEdit.php";
            data[1] = Tel;
            data[2] = name;
            data[3] = email;
            data[4] = KeyValueShared.getId(getApplicationContext());
            Toast.makeText(getApplicationContext(), "tel is: " + Tel, Toast.LENGTH_SHORT).show();
            Toast.makeText(getApplicationContext(), "name is: " + name, Toast.LENGTH_SHORT).show();
            Toast.makeText(getApplicationContext(), "email is: " + email, Toast.LENGTH_SHORT).show();
            Toast.makeText(getApplicationContext(), "id is: " + KeyValueShared.getId(getApplicationContext()), Toast.LENGTH_SHORT).show();
            new MembersAsynTask().execute(data);

            pDialog = new ProgressDialog(personalData.this);
            pDialog.setMessage("جاري حفظ بيانات العضو");
            pDialog.setTitle("برجاء الانتظار ثواني");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }
    }
    public class MembersAsynTask extends AsyncTask<String, Void, Boolean> {
        JSONObject jObj;
        @SuppressWarnings("deprecation")
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(4);
                nameValuePairs.add(new BasicNameValuePair("tel", params[1]));
                nameValuePairs.add(new BasicNameValuePair("name", params[2]));
                nameValuePairs.add(new BasicNameValuePair("email", params[3]));
                nameValuePairs.add(new BasicNameValuePair("id", params[4]));
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if(status == 200){
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);





                    Log.e("error client string", data);
                    JSONObject jObj = new JSONObject(data.substring(data.indexOf("{"), data.lastIndexOf("}") + 1));
                    ReqSuccess = jObj.getString("success");
                    ReqMessage = jObj.getString("message");
                    return true;
                }else{
                    Log.e("error client string", "here");
                }
            } catch (ClientProtocolException e) {
                Log.e("error client string", e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("error exp", e.getMessage());
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("error json", e.getMessage());
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            pDialog.dismiss();
            super.onPostExecute(result);
            if(result == false){
                Toast.makeText(getApplicationContext(), "لا يوجد بيانات " + ReqMessage, Toast.LENGTH_LONG).show();
            }else{
                if(ReqSuccess.equals("1")){
                    Toast.makeText(getApplicationContext(), "تم تعديل البيانات", Toast.LENGTH_LONG).show();
                    KeyValueShared.setTel(getApplicationContext(), etTel.getText().toString());
                    KeyValueShared.setName(getApplicationContext(), etName.getText().toString());
                    KeyValueShared.setEmail(getApplicationContext(), etEmail.getText().toString());
                    Intent i = new Intent(personalData.this, MainHaraj.class);
                    startActivity(i);
                    finish();

                }else{
                    Toast.makeText(getApplicationContext(), ReqMessage, Toast.LENGTH_LONG).show();
                }
            }
        }
    }
}
