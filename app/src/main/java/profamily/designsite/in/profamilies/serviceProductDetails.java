package profamily.designsite.in.profamilies;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import commentsData.Comments;
import commentsData.commentsAdapter;
import imageData.ImageAlbum;
import imageData.ImageAlbumAdapter;
import libes.KeyValueShared;
import libes.alertsApp;
import libes.footerServiceAction;
import libes.globalData;
import productData.Product;

public class serviceProductDetails extends AppCompatActivity {


    private String ReqSuccess, idProduct = "0", idUser = "0", isRating = "0", rating = "0", usernameProduct;
    private ProgressDialog pDialog;
    private LinearLayout lnRate, lnEdit, lnDelete, ln_chat;
    private ScrollView svScroll;
    private ImageView imgMain, img_rate;
    private TextView tvTitle, tvContent, tvTel, tv_rateNo;
    Product dataProduct = new Product();
    private boolean isDelete = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_product_details);
        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        imgMain = (ImageView)findViewById(R.id.img_main);
        svScroll = (ScrollView)findViewById(R.id.svScroll);
        tvTitle = (TextView)findViewById(R.id.tv_title);
        tvTitle.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        tvContent = (TextView)findViewById(R.id.tv_content);
        tvContent.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        tvTel = (TextView)findViewById(R.id.tv_tel);
        tvTel.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        tv_rateNo = (TextView)findViewById(R.id.tv_rateNo);
        img_rate = (ImageView)findViewById(R.id.img_rate);
        lnRate = (LinearLayout)findViewById(R.id.lnRate);
        lnDelete = (LinearLayout)findViewById(R.id.lnDelete);
        ln_chat = (LinearLayout)findViewById(R.id.lnChat);
        lnEdit = (LinearLayout)findViewById(R.id.lnEdit);

        lnRate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(KeyValueShared.getId(getApplicationContext()).equals("0")){
                    alertsApp.mustLogin(svScroll);
                }else{
                    String data[] = new String[3];
                    data[0] = globalData.URLhost() + "harajAddRemoveRateProduct.php";
                    data[1] = idProduct;
                    data[2] = KeyValueShared.getId(getApplicationContext());
                    new sendId().execute(data);
                    pDialog = new ProgressDialog(serviceProductDetails.this);
                    pDialog.setMessage("جاري تنفيذ الأمر");
                    pDialog.setTitle("الرجاء الانتظار");
                    pDialog.setIndeterminate(false);
                    pDialog.setCancelable(true);
                    pDialog.show();
                }
            }
        });
        lnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (KeyValueShared.getId(getApplicationContext()).equals("0")){
                    alertsApp.mustLogin(svScroll);
                }else {
                    String data[] = new String[3];
                    data[0] = globalData.URLhost() + "productDelete.php";
                    data[1] = idProduct;
                    data[2] = KeyValueShared.getId(getApplicationContext());
                    new sendId().execute(data);
                    pDialog = new ProgressDialog(serviceProductDetails.this);
                    pDialog.setMessage("جاري حذف الإعلان");
                    pDialog.setTitle("الرجاء الانتظار");
                    pDialog.setIndeterminate(false);
                    pDialog.setCancelable(true);
                    pDialog.show();
                    isDelete = true;
                }
            }
        });
        ln_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (KeyValueShared.getId(getApplicationContext()).equals("0")){
                    alertsApp.mustLogin(svScroll);
                }else {
                    Log.e("to - from", idUser + " - " + KeyValueShared.getId(getApplicationContext()));
                    Intent i = new Intent(serviceProductDetails.this, chatUsers.class);
                    i.putExtra("otherUser", idUser);
                    i.putExtra("username", usernameProduct);
                    startActivity(i);
                }
            }
        });
        lnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (KeyValueShared.getId(getApplicationContext()).equals("0")){
                    alertsApp.mustLogin(svScroll);
                }else {
                    Intent i = new Intent(v.getContext(), serviceAdd.class);
                    i.putExtra("idProduct", idProduct);
                    i.putExtra("title", getIntent().getStringExtra("title"));
                    i.putExtra("content", getIntent().getStringExtra("content"));
                    i.putExtra("tel", getIntent().getStringExtra("tel"));
                    i.putExtra("idUser", getIntent().getStringExtra("idUser"));
                    i.putExtra("imgMain", getIntent().getStringExtra("img"));
                    i.putExtra("imgName", getIntent().getStringExtra("imgName"));
                    i.putExtra("isEdit", "1");
                    v.getContext().startActivity(i);
                }
            }
        });
        Log.e("idUserProduct", idUser);
        Log.e("idUser", KeyValueShared.getId(getApplicationContext()));
        if(idUser.equals(getIntent().getStringExtra("idUser"))){
            lnDelete.setVisibility(View.VISIBLE);
            lnEdit.setVisibility(View.VISIBLE);
        }
        if(getIntent().hasExtra("id")) {
            idProduct = String.valueOf(getIntent().getStringExtra("id"));
            Toast.makeText(getApplicationContext(), "new id is: " + idProduct, Toast.LENGTH_LONG).show();
            tvTitle.setText(String.valueOf(getIntent().getStringExtra("title")));
            tvContent.setText(Html.fromHtml(String.valueOf(getIntent().getStringExtra("content"))));
            tvTel.setText(String.valueOf(getIntent().getStringExtra("tel")));
            tv_rateNo.setText(String.valueOf(getIntent().getStringExtra("rating")));
            idUser = String.valueOf(getIntent().getStringExtra("idUser"));
            usernameProduct = String.valueOf(getIntent().getStringExtra("usernameProduct"));
            if(idUser.equals(KeyValueShared.getId(getApplicationContext()))){
                lnEdit.setVisibility(View.VISIBLE);
                lnDelete.setVisibility(View.VISIBLE);
            }else {
                lnEdit.setVisibility(View.GONE);
                lnDelete.setVisibility(View.GONE);
            }
            Glide.with(getApplicationContext()).load(String.valueOf(getIntent().getStringExtra("img"))).into(imgMain);
            String data[] = new String[3];
            data[0] = globalData.URLhost() + "commentGetData.php";
            data[1] = idProduct;
            data[2] = KeyValueShared.getId(getApplicationContext());
            new ratingProduct().execute(data);
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_haraj, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.add){
            Intent i = new Intent(this, serviceAddAgreement.class);
            startActivity(i);
            finish();
        } else if(id == R.id.back){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }


    public class sendId extends AsyncTask<String, Void, Boolean> {
        String message;
        @SuppressWarnings("deprecation")
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                nameValuePairs.add(new BasicNameValuePair("idProduct", params[1]));
                nameValuePairs.add(new BasicNameValuePair("idUser", params[2]));
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));

                HttpResponse response = client.execute(post);

                int status = response.getStatusLine().getStatusCode();
                if(status == 200){
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    JSONObject jObj = new JSONObject(data.substring(data.indexOf("{"), data.lastIndexOf("}") + 1));
                    ReqSuccess = jObj.getString("success");
                    message = jObj.getString("message");

                    return true;
                }else{
                    Log.e("error client string", "here");
                }
            } catch (ClientProtocolException e) {
                Log.e("error client string", e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("error exp", e.getMessage());
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("error json", e.getMessage());
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            pDialog.dismiss();
            super.onPostExecute(result);
            if(result == false){
                Toast.makeText(getApplicationContext(), "حدث خطأ، نرجو المحاوله في وقت لاحق", Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();

                if(isDelete == true){
                    finish();
                }
                String data[] = new String[3];
                data[0] = globalData.URLhost() + "commentGetData.php";
                data[1] = idProduct;
                data[2] = KeyValueShared.getId(getApplicationContext());
                new ratingProduct().execute(data);
            }
        }
    }


    public class ratingProduct extends AsyncTask<String, Void, Boolean> {
        @SuppressWarnings("deprecation")
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                nameValuePairs.add(new BasicNameValuePair("id", params[1]));
                nameValuePairs.add(new BasicNameValuePair("idUser", params[2]));
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));

                HttpResponse response = client.execute(post);

                int status = response.getStatusLine().getStatusCode();
                if(status == 200){
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    Log.e("data", data);
                    JSONObject jObj = new JSONObject(data.substring(data.indexOf("{"), data.lastIndexOf("}") + 1));
                    ReqSuccess = jObj.getString("success");
                    isRating = jObj.getString("isRating");
                    rating = jObj.getString("rating");
                    return true;
                }else{
                    Log.e("error client string", "here");
                }
            } catch (ClientProtocolException e) {
                Log.e("error client string", e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("error exp", e.getMessage());
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("error json", e.getMessage());
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if(result == false){
                Toast.makeText(getApplicationContext(), "حدث خطأ، نرجو المحاوله في وقت لاحق", Toast.LENGTH_LONG).show();
            }else{
                    tv_rateNo.setText(rating);
                    if(isRating.equals("0")){
                        img_rate.setImageResource(R.drawable.heart_icon);
                    }else{
                        img_rate.setImageResource(R.drawable.heart_green);
                    }
            }
        }
    }
}
