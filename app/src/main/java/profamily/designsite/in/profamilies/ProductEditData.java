package profamily.designsite.in.profamilies;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import categoryData.category;
import libes.KeyValueShared;
import libes.globalData;

public class ProductEditData extends AppCompatActivity {

    String idProduct, idMainCat, ReqSuccess, ReqMessage, dataTest, stName, stPrice, stDesc, stIdSubCat;
    TextView tvProductName, tvProductPrice, tvProductDesc, tvCat;
    EditText etProductName, etProductPrice, etProductDesc;
    Spinner spCat;
    Button btnSave;
    ProgressDialog pDialog;
    List<String> listCategory = new ArrayList<String>();
    List<String> listCatId = new ArrayList<String>();
    ArrayList<String> TypeCategoryArray = new ArrayList<String>();
    ArrayAdapter<String> adapterCategory;
    private List<category> data_list1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_edit_data);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        idProduct = getIntent().getStringExtra("idProduct");
        Toast.makeText(this, idProduct, Toast.LENGTH_SHORT).show();
        idMainCat = getIntent().getStringExtra("idMainCat");
        tvProductName = (TextView)findViewById(R.id.product_add_name_text);
        tvProductName.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        tvProductPrice = (TextView)findViewById(R.id.product_add_price_text);
        tvProductPrice.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        tvProductDesc = (TextView)findViewById(R.id.product_add_desc_text);
        tvProductDesc.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        etProductName = (EditText) findViewById(R.id.product_add_name);
        etProductName.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        etProductName.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == 66){
                    etProductPrice.requestFocus();
                }
                return false;
            }
        });
        etProductPrice = (EditText)findViewById(R.id.product_add_price);
        etProductPrice.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        etProductPrice.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == 66){
                    etProductDesc.requestFocus();
                }
                return false;
            }
        });
        etProductDesc = (EditText)findViewById(R.id.product_add_desc);
        etProductDesc.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        etProductDesc.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == 66){
                    spCat.requestFocus();
                }
                return false;
            }
        });
        tvCat = (TextView)findViewById(R.id.product_add_cat_text);
        tvCat.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        data_list1 = new ArrayList<>();
        listCategory.add(getResources().getString(R.string.haraj_add_must_choose_cat));
        listCatId.add("0");
        spCat = (Spinner)findViewById(R.id.product_add_cat);
        adapterCategory = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, TypeCategoryArray);
        adapterCategory.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCat.setAdapter(adapterCategory);
        spCat.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == 66){
                    btnSave.requestFocus();
                }
                return false;
            }
        });
        String data[] = new String[2];
        data[0] = globalData.URLhost() + "productDetailsData.php";
        data[1] = idProduct;
        loadData1 bts = new loadData1();
        bts.execute(data);
        btnSave = (Button)findViewById(R.id.btn_save);
        btnSave.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                marketProcess();
            }
        });
        btnSave.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == 66){
                    marketProcess();
                }
                return false;
            }
        });
    }
    private void marketProcess(){
        String productName = etProductName.getText().toString();
        String productPrice = etProductPrice.getText().toString();
        String productDesc = etProductDesc.getText().toString();
        String idSubCat = String.valueOf(spCat.getSelectedItemPosition());

        if(TextUtils.isEmpty(productName)){
            etProductName.setError(getString(R.string.mustWriteProductName));
        }else if (TextUtils.isEmpty(productPrice)){
            etProductPrice.setError(getString(R.string.mustWriteProductPrice));
        }else if (TextUtils.isEmpty(productDesc)){
            etProductDesc.setError(getString(R.string.mustWriteProductDesc));
        }else if (idSubCat.equals("0")){
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.haraj_add_must_choose_cat), Toast.LENGTH_SHORT).show();
        }else{

            String data[] = new String[6];
            data[0] = globalData.URLhost() + "productEditData.php";
            data[1] = productName;
            data[2] = productPrice;
            data[3] = productDesc;
            data[4] = listCatId.get(Integer.valueOf(idSubCat)).toString();
            data[5] = idProduct;

            new productEdit().execute(data);

            pDialog = new ProgressDialog(ProductEditData.this);
            pDialog.setMessage("جاري اضافة المنتج");
            pDialog.setTitle("برجاء الانتظار ثواني");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();

        }
    }
    public class productEdit extends AsyncTask<String, Void, Boolean> {
        JSONObject jObj;
        @SuppressWarnings("deprecation")
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(5);
                nameValuePairs.add(new BasicNameValuePair("productName", params[1]));
                nameValuePairs.add(new BasicNameValuePair("productPrice", params[2]));
                nameValuePairs.add(new BasicNameValuePair("productDesc", params[3]));
                nameValuePairs.add(new BasicNameValuePair("idSubCat", params[4]));
                nameValuePairs.add(new BasicNameValuePair("idProduct", params[5]));
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if(status == 200){
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    Log.e("result is: ", data);
                    JSONObject jObj = new JSONObject(data.substring(data.indexOf("{"), data.lastIndexOf("}") + 1));
                    ReqSuccess = jObj.getString("success");
                    ReqMessage = jObj.getString("message");

                    return true;
                }else{
                    Log.e("error client string", "here");
                }
            } catch (ClientProtocolException e) {
                Log.e("error client string", e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("error exp", e.getMessage());
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("error json", e.getMessage());
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            pDialog.dismiss();
            super.onPostExecute(result);
            if(result == false){
                Toast.makeText(getApplicationContext(), "لا يوجد بيانات " + ReqMessage, Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(getApplicationContext(), ReqMessage, Toast.LENGTH_LONG).show();
                finish();
            }
        }
    }
    public class loadData1 extends AsyncTask<String, Void, Boolean> {

        @SuppressWarnings("deprecation")
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
                nameValuePairs.add(new BasicNameValuePair("id", params[1]));
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if(status == 200){
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    Log.e("errors", data);
                    JSONObject jObj = new JSONObject(data.substring(data.indexOf("{"), data.lastIndexOf("}") + 1));
                    ReqSuccess = jObj.getString("success");
                    if(ReqSuccess.equals("1")){
                        dataTest = data;
                        if(jObj.has("data")) {

                            JSONArray details = jObj.getJSONArray("data");
                            Log.e("cat is: ", details.toString());
                            for (int i = 0; i < details.length(); i++) {
                                JSONObject c = details.getJSONObject(i);
                                stName = c.getString("title");
                                stDesc = c.getString("desc");
                                stPrice = c.getString("price");
                                stIdSubCat = c.getString("idSubCat");
                            }
                        }
                        if(jObj.has("cat")) {

                            JSONArray wee = jObj.getJSONArray("cat");
                            Log.e("cat is: ", wee.toString());
                            for (int i = 0; i < wee.length(); i++) {
                                JSONObject c = wee.getJSONObject(i);
                                category dataCategory = new category();
                                listCategory.add(c.getString("title"));
                                listCatId.add(c.getString("id"));
                                data_list1.add(dataCategory);
                            }
                        }

                    }


                    return true;
                }else{
                    Log.e("error client strings", "here");
                }
            } catch (ClientProtocolException e) {
                Log.e("error client stringd", e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("error exp", e.getMessage());
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("error json", e.getMessage());
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if(result == false){
                Toast.makeText(getApplicationContext(), "حدث خطأ، نرجو المحاوله في وقت لاحق", Toast.LENGTH_LONG).show();
            }else{
                if(ReqSuccess.equals("1")){
                    etProductName.setText(stName);
                    etProductPrice.setText(stPrice);
                    etProductDesc.setText(stDesc);
                    TypeCategoryArray.clear();
                    TypeCategoryArray.addAll(listCategory);
                    adapterCategory.notifyDataSetChanged();
                    spCat.setSelection(listCatId.indexOf(stIdSubCat));
                }
            }
        }
    }
}
