package profamily.designsite.in.profamilies;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import libes.KeyValueShared;
import libes.footerServiceAction;
import libes.globalData;
import libes.location;
import productData.Product;
import productData.ServiceAdapter;
import search.searchHistory;
import search.searchHistoryAdapter;

public class serviceSearchList extends AppCompatActivity {

    private RecyclerView mRecyclerView1;
    private ProgressDialog pDialog;
    private ProgressBar pBar1;
    private EditText searchData;
    private Button btnSearch;
    String ReqSuccess, lat, lon;
    LinearLayout lnHome, lnSearch, lnNoti, lnMore;
    private searchHistoryAdapter adapter1;
    private List<searchHistory> data_list1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_search_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE|WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        location getLocation = new location(this);
        lat = getLocation.Langitude;
        lon = getLocation.Longitude;
        btnSearch = (Button)findViewById(R.id.btnSearchData);
        searchData = (EditText)findViewById(R.id.txtSearch);
        lnHome = (LinearLayout)findViewById(R.id.btnHome);
        lnSearch = (LinearLayout)findViewById(R.id.btnSearch);
        lnNoti = (LinearLayout)findViewById(R.id.btnNoti);
        lnMore = (LinearLayout)findViewById(R.id.btnMore);
        lnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerServiceAction.footerHome(serviceSearchList.this);
                finish();
            }
        });
        lnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerServiceAction.footerSearch(serviceSearchList.this);
                finish();
            }
        });
        lnNoti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerServiceAction.footerNoti(serviceSearchList.this);
                finish();
            }
        });
        lnMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerServiceAction.footerMore(serviceSearchList.this);
                finish();
            }
        });
        searchData.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(searchData.getText().toString().length() > 2) {
                    if(data_list1.size() > 0) {
                        data_list1.clear();
                        adapter1.notifyDataSetChanged();
                    }
                    pBar1.setVisibility(View.VISIBLE);
                    String dataStart[] = new String[7];
                    dataStart[0] = globalData.URLhost() + "serviceSearchGet.php";
                    dataStart[1] = searchData.getText().toString();
                    dataStart[2] = KeyValueShared.getSettingsAppCountryId(serviceSearchList.this);
                    loadData1 bts = new loadData1();
                    bts.execute(dataStart);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mRecyclerView1 = (RecyclerView)findViewById(R.id.recycleView1);
        mRecyclerView1.setHasFixedSize(true);
        mRecyclerView1.setLayoutManager(new LinearLayoutManager(this));
        pBar1 = (ProgressBar)findViewById(R.id.progressBar1);
        //pBar1.setVisibility(View.VISIBLE);
        data_list1 = new ArrayList<>();
        String dataStart[] = new String[7];
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(serviceSearchList.this, serviceSearchResult.class);
                i.putExtra("txtSearch", searchData.getText().toString());
                startActivity(i);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menubar_haraj_back, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.add){
            Intent i = new Intent(serviceSearchList.this, serviceAddAgreement.class);
            startActivity(i);
        } else if(id == R.id.back){
            Intent i = new Intent(serviceSearchList.this, serviceMain.class);
            startActivity(i);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public class loadData1 extends AsyncTask<String, Void, Boolean> {

        private String title;
        private String content;
        private String img;
        @SuppressWarnings("deprecation")
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(6);
                nameValuePairs.add(new BasicNameValuePair("title", params[1]));
                nameValuePairs.add(new BasicNameValuePair("countryId", params[2]));
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if(status == 200){
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    Log.e("history", data);
                    JSONObject jObj = new JSONObject(data.substring(data.indexOf("{"), data.lastIndexOf("}") + 1));
                    ReqSuccess = jObj.getString("success");
                    if(ReqSuccess.equals("1")){
                        if(jObj.has("data")) {
                            JSONArray wee = jObj.getJSONArray("data");
                            for (int i = 0; i < wee.length(); i++) {
                                JSONObject c = wee.getJSONObject(i);
                                searchHistory dataHistory = new searchHistory();
                                dataHistory.setTitle(c.getString("title"));
                                dataHistory.setCount(c.getString("count"));
                                Log.e("title", c.getString("title"));
                                data_list1.add(dataHistory);
                            }
                        }

                    }


                    return true;
                }else{
                    Log.e("error client string", "here");
                }
            } catch (ClientProtocolException e) {
                Log.e("error client string", e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("error exp", e.getMessage());
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("error json", e.getMessage());
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);

            if(result == false){
                Toast.makeText(getApplicationContext(), "لا يوجد بيانات", Toast.LENGTH_LONG).show();
            }else{
                if(ReqSuccess.equals("1")){
                    adapter1 = new searchHistoryAdapter(getApplicationContext(), data_list1);
                    mRecyclerView1.setAdapter(adapter1);
                        pBar1.setVisibility(View.INVISIBLE);
                }else{
                    pBar1.setVisibility(View.INVISIBLE);
                }
            }
        }
    }
}
