package profamily.designsite.in.profamilies;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import libes.KeyValueShared;
import libes.globalData;

public class commentAdd extends AppCompatActivity {

    private Button btnAddComment;
    private EditText etName, etComment;
    private String ReqSuccess , ReqMessage;
    private ProgressDialog pDialog;
    private TextView tvName;
    private TextView tvComments;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment_add);
        etName = (EditText)findViewById(R.id.et_comment_name);
        etName.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        tvName = (TextView)findViewById(R.id.tv_name);
        tvName.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        etComment = (EditText)findViewById(R.id.et_comment_comment);
        etComment.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        tvComments = (TextView)findViewById(R.id.tv_comments);
        tvComments.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        btnAddComment = (Button)findViewById(R.id.btn_comment_add);
        btnAddComment.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        btnAddComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginProccess();
            }
        });
    }
    private void loginProccess(){
        String stName = etName.getText().toString();
        String stComment = etComment.getText().toString();
        if(TextUtils.isEmpty(stName)){
            etName.setError((getString(R.string.mustWriteName)));
        }else if(TextUtils.isEmpty(stComment)){
            etComment.setError((getString(R.string.mustWriteComment)));
        }else{
            String data[] = new String[4];
            data[0] = globalData.URLhost() + "commentAdd.php";
            data[1] = stName;
            data[2] = stComment;
            data[3] = String.valueOf(getIntent().getStringExtra("idProduct"));

            new MembersAsynTask().execute(data);

            pDialog = new ProgressDialog(this);
            pDialog.setMessage("جاري إضافة التعليق");
            pDialog.setTitle("برجاء الانتظار ثواني");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }
    }
    public class MembersAsynTask extends AsyncTask<String, Void, Boolean> {
        JSONObject jObj;
        @SuppressWarnings("deprecation")
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(3);
                nameValuePairs.add(new BasicNameValuePair("name", params[1]));
                nameValuePairs.add(new BasicNameValuePair("comment", params[2]));
                nameValuePairs.add(new BasicNameValuePair("idProduct", params[3]));
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if(status == 200){
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    JSONObject jObj = new JSONObject(data.substring(data.indexOf("{"), data.lastIndexOf("}") + 1));
                    ReqSuccess = jObj.getString("success");
                    if(ReqSuccess.equals("1")){

                    }else{
                        ReqMessage = jObj.getString("message");
                    }


                    return true;
                }else{
                    Log.e("error client string", "here");
                }
            } catch (ClientProtocolException e) {
                Log.e("error client string", e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("error exp", e.getMessage());
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("error json", e.getMessage());
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            pDialog.dismiss();
            super.onPostExecute(result);
            if(result == false){
                Toast.makeText(getApplicationContext(), "لا يوجد بيانات " + ReqMessage, Toast.LENGTH_LONG).show();
            }else{
                if(ReqSuccess.equals("1")){

                    finish();

                }else{
                    Toast.makeText(getApplicationContext(), ReqMessage, Toast.LENGTH_LONG).show();
                }
            }
        }
    }
}
