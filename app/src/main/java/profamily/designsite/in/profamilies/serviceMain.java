package profamily.designsite.in.profamilies;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import libes.KeyValueShared;
import libes.footerHarajAction;
import libes.footerServiceAction;
import libes.globalData;
import productData.Product;
import productData.ProductAdapter;
import productData.ServiceAdapter;
import tab.tabs;
import tab.tabsAdapter;
import tab.tabsServiceAdapter;

public class serviceMain extends AppCompatActivity {

    private RecyclerView mRecyclerView1, rvTabMain, rvTabStatue;
    private tabsServiceAdapter tabMainAdapter;
    private List<tabs> data_list_tab_mainArray;
    private tabsServiceAdapter tabStatueAdapter;
    private List<tabs> data_list_statueArray;
    private ProgressBar pBar1;
    private ServiceAdapter adapter1;
    private List<Product> data_list1;
    String ReqSuccess;
    public String filename = "getService.php", idSubCat = "0", idChangeStatue = "0", cityId = "0";
    LinearLayout lnHome, lnSearch, lnNoti, lnMore;
    private int m_previouse_Total_Count1 = 0;
    int firstVisibleItem, visibleItemCount, totalItemCount;
    private TextView txtNotFound;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        lnHome = (LinearLayout)findViewById(R.id.btnHome);
        lnSearch = (LinearLayout)findViewById(R.id.btnSearch);
        lnNoti = (LinearLayout)findViewById(R.id.btnNoti);
        lnMore = (LinearLayout)findViewById(R.id.btnMore);
        lnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerServiceAction.footerHome(serviceMain.this);
                finish();
            }
        });
        lnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerServiceAction.footerSearch(serviceMain.this);
                finish();
            }
        });
        lnNoti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerServiceAction.footerNoti(serviceMain.this);
                finish();
            }
        });
        lnMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerServiceAction.footerMore(serviceMain.this);
                finish();
            }
        });
        txtNotFound = (TextView)findViewById(R.id.txtNotFound);
        mRecyclerView1 = (RecyclerView)findViewById(R.id.recycleView1);
        mRecyclerView1.setHasFixedSize(true);
        mRecyclerView1.setLayoutManager(new LinearLayoutManager(this));
        pBar1 = (ProgressBar)findViewById(R.id.progressBar1);
        pBar1.setVisibility(View.VISIBLE);
        data_list1 = new ArrayList<>();


        rvTabMain = (RecyclerView)findViewById(R.id.rvMainTab);
        rvTabMain.setHasFixedSize(true);
        rvTabMain.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        data_list_tab_mainArray = new ArrayList<>();
        String[] tabMainName = getApplicationContext().getResources().getStringArray(R.array.serviceCatName);
        String[] tabMainid = getApplicationContext().getResources().getStringArray(R.array.serviceCatId);
        tabs subTabFirst = new tabs();
        subTabFirst.setId("0");
        subTabFirst.setTitle(getResources().getString(R.string.allData));
        subTabFirst.setIsTabMain("1");
        subTabFirst.setIsSelected("1");
        data_list_tab_mainArray.add(subTabFirst);
        for(int i = 0; i < tabMainName.length; i++){
            tabs mainTab = new tabs();
            Log.e("cat id", tabMainid[i].toString());
            Log.e("cat name", tabMainName[i].toString());
            mainTab.setId(tabMainid[i].toString());
            mainTab.setTitle(tabMainName[i].toString());
            mainTab.setIsTabMain("1");
            data_list_tab_mainArray.add(mainTab);
        }
        tabMainAdapter = new tabsServiceAdapter(getApplication(), data_list_tab_mainArray);
        rvTabMain.setAdapter(tabMainAdapter);



        rvTabStatue = (RecyclerView)findViewById(R.id.rvStatueTab);
        rvTabStatue.setHasFixedSize(true);
        rvTabStatue.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        String[] tabStatueName = getApplicationContext().getResources().getStringArray(R.array.tabStatue_name);
        data_list_statueArray = new ArrayList<>();
        for(int i = 0; i < tabStatueName.length; i++){
            tabs statueTab = new tabs();
            statueTab.setId(String.valueOf(i));
            statueTab.setTitle(tabStatueName[i].toString());
            statueTab.setIsTabMain("2");
            if (i == 0){
                statueTab.setIsSelected("1");
            }
            data_list_statueArray.add(statueTab);
        }
        tabStatueAdapter = new tabsServiceAdapter(getApplication(), data_list_statueArray);
        rvTabStatue.setAdapter(tabStatueAdapter);




        mRecyclerView1.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager mLayoutManager = (LinearLayoutManager) mRecyclerView1
                        .getLayoutManager();
                mRecyclerView1.setLayoutManager(mLayoutManager);
                visibleItemCount = mRecyclerView1.getChildCount();
                totalItemCount = mLayoutManager.getItemCount();
                firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();
                if(totalItemCount == 0){
                    return ;
                }
                if(m_previouse_Total_Count1==totalItemCount){
                    return;
                }
                boolean loadMore = firstVisibleItem + visibleItemCount >= totalItemCount;
                if(loadMore){
                    m_previouse_Total_Count1 = totalItemCount;
                    pBar1.setVisibility(View.VISIBLE);
                    getLoadData(totalItemCount);
                }

            }
        });
        resetData();
        startLoadData();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        getLoadData(0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_haraj, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id= item.getItemId();
        if(id == R.id.add){
            Intent i = new Intent(this, serviceAddAgreement.class);
            startActivity(i);
        } else if(id == R.id.back){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
    public void resetData(){
        idChangeStatue = "0";
        idSubCat = "0";
        cityId = KeyValueShared.getSettingsAppMyCityId(this);

    }
    public void startLoadData(){
        m_previouse_Total_Count1 = 0;
        data_list1.clear();
        Log.e("size is", String.valueOf(data_list1.size()));
        Log.e("changeStatue is", String.valueOf(idChangeStatue));
        Log.e("idSubCat is", String.valueOf(idSubCat));
        Log.e("cityId is", String.valueOf(cityId));
        Log.e("filename is", filename);
        adapter1 = new ServiceAdapter(getApplicationContext(), data_list1);
        mRecyclerView1.setAdapter(adapter1);
        String dataStart[] = new String[7];
        dataStart[0] = globalData.URLhost() + filename;
        dataStart[1] = "0";
        dataStart[2] = idChangeStatue;
        dataStart[3] = idSubCat;
        dataStart[4] = cityId;
        dataStart[5] = KeyValueShared.getMapLat(this);
        dataStart[6] = KeyValueShared.getMapLon(this);
        loadData1 bts = new loadData1();
        bts.execute(dataStart);
        tabStatueAdapter.notifyDataSetChanged();
    }
    public void getLoadData(Integer limit) {
Log.e("sub cat id is:", idSubCat);
        pBar1.setVisibility(View.VISIBLE);
        String data[] = new String[7];
        data[0] = globalData.URLhost() + filename;
        data[1] = String.valueOf(limit);
        data[2] = idChangeStatue;
        data[3] = idSubCat;
        data[4] = cityId;
        data[5] = KeyValueShared.getMapLat(this);
        data[6] = KeyValueShared.getMapLon(this);
        loadData1 bts = new loadData1();
        bts.execute(data);
    }
    public class loadData1 extends AsyncTask<String, Void, Boolean> {

        private String title;
        private String content;
        private String img;
        @SuppressWarnings("deprecation")
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(6);
                nameValuePairs.add(new BasicNameValuePair("limit", params[1]));
                nameValuePairs.add(new BasicNameValuePair("changeStatue", params[2]));
                nameValuePairs.add(new BasicNameValuePair("idSubCat", params[3]));
                nameValuePairs.add(new BasicNameValuePair("cityId", params[4]));
                nameValuePairs.add(new BasicNameValuePair("latitude", params[5]));
                nameValuePairs.add(new BasicNameValuePair("longitude", params[6]));
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if(status == 200){
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    JSONObject jObj = new JSONObject(data.substring(data.indexOf("{"), data.lastIndexOf("}") + 1));
                    ReqSuccess = jObj.getString("success");
                    if(ReqSuccess.equals("1")){
                        if(jObj.has("data")) {
                            JSONArray wee = jObj.getJSONArray("data");
                            for (int i = 0; i < wee.length(); i++) {
                                JSONObject c = wee.getJSONObject(i);
                                Product dataProduct = new Product();
                                dataProduct.setId(c.getString("id"));
                                dataProduct.setName(c.getString("name"));
                                dataProduct.setIdMainCat(c.getString("idMainCat"));
                                dataProduct.setIdSubCat(c.getString("idSubCat"));
                                dataProduct.setCatName(c.getString("catName"));
                                dataProduct.setPlace(c.getString("country"));
                                dataProduct.setCity(c.getString("city"));
                                dataProduct.setImg(c.getString("img"));
                                dataProduct.setVideoImageName(c.getString("imgName"));
                                dataProduct.setIdUser(c.getString("idUser"));
                                dataProduct.setUserNameProduct(c.getString("userName"));
                                dataProduct.setIsActive(c.getString("isActive"));
                                dataProduct.setMyPlaceAndCity(c.getString("location"));
                                dataProduct.setContact(c.getString("contact"));
                                dataProduct.setDescription(c.getString("description"));
                                dataProduct.setRating(c.getString("rating"));
                                data_list1.add(dataProduct);
                            }
                        }

                    }


                    return true;
                }else{
                    Log.e("error client string", "here");
                }
            } catch (ClientProtocolException e) {
                Log.e("error client string", e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("error exp", e.getMessage());
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("error json", e.getMessage());
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if(result == false){
                Toast.makeText(getApplicationContext(), "لا يوجد بيانات", Toast.LENGTH_LONG).show();
            }else{
                if(ReqSuccess.equals("1")){
                    txtNotFound.setVisibility(View.INVISIBLE);
                    if(m_previouse_Total_Count1>0) {
                        Log.e("second data", String.valueOf(m_previouse_Total_Count1));
                        pBar1.setVisibility(View.INVISIBLE);
                        /*
                        Toast.makeText(getApplicationContext(), "second " + m_previouse_Total_Count1, Toast.LENGTH_SHORT).show();
                        adapter1.notifyDataSetChanged();
                        pBar1.setVisibility(View.INVISIBLE);
                        */
                    }else{
                        mRecyclerView1.setAdapter(adapter1);
                        Log.e("first data: ", String.valueOf(m_previouse_Total_Count1));
                        pBar1.setVisibility(View.INVISIBLE);
                    }
                }else{
                    adapter1.notifyDataSetChanged();
                    pBar1.setVisibility(View.INVISIBLE);
                }
            }
        }
    }
}
