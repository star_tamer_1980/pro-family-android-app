package profamily.designsite.in.profamilies;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import commentsData.Comments;
import commentsData.commentsAdapter;
import imageData.ImageAlbum;
import imageData.ImageAlbumAdapter;
import libes.KeyValueShared;
import libes.alertsApp;
import libes.globalData;
import productData.Product;
import productData.ProductAdapter;
import productData.ProductSimilarAdapter;

public class HarajProductDetails extends AppCompatActivity {

    private String ReqSuccess, idProduct, statue = "0";
    private Button btnAddComment;
    private TextView tvTitle, tvTime, tvUser, tvLocation, tvContent, tvTel, tvPrice;
    private ProgressDialog pDialog;
    private RecyclerView mRecyclerView, rvComments, rvSimilar;
    private ImageAlbumAdapter adapter;
    private List<ImageAlbum> Image_list;
    private ProductSimilarAdapter adapterSimilar;
    private List<Product> similar_list;
    private commentsAdapter commentAdapter;
    private List<Comments> comment_list;
    private LinearLayout lnPreviouseProduct, lnForwardProduct, lnBlock, lnRate, lnEdit, lnRefresh, lnDelete, lnUser, ln_chat;
    private RelativeLayout rlVideo;
    private ScrollView svScroll;
    private ImageView imgVideo;

    Product dataProduct = new Product();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_haraj_product_details);
        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        idProduct = String.valueOf(getIntent().getStringExtra("id"));
        svScroll = (ScrollView)findViewById(R.id.svScroll);
        btnAddComment = (Button)findViewById(R.id.btn_add_comment);
        btnAddComment.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        tvTitle = (TextView)findViewById(R.id.tv_title);
        tvTitle.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        tvPrice = (TextView)findViewById(R.id.tv_price);
        tvPrice.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        tvTime = (TextView)findViewById(R.id.tv_time);
        tvTime.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        tvUser = (TextView)findViewById(R.id.tv_user);
        tvUser.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        tvLocation = (TextView)findViewById(R.id.tv_location);
        tvLocation.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        tvContent = (TextView)findViewById(R.id.tv_content);
        tvContent.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        tvTel = (TextView)findViewById(R.id.tv_tel);
        tvTel.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        mRecyclerView = (RecyclerView)findViewById(R.id.mRecyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        rvSimilar = (RecyclerView)findViewById(R.id.mrSimilar);
        rvSimilar.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        rvSimilar.setLayoutManager(new GridLayoutManager(this,3));
        similar_list = new ArrayList<>();
        Image_list = new ArrayList<>();
        rvComments = (RecyclerView)findViewById(R.id.rv_comments);
        rvComments.setLayoutManager(new LinearLayoutManager(this));
        comment_list = new ArrayList<>();
        btnAddComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HarajProductDetails.this, commentAdd.class);
                i.putExtra("idProduct", idProduct);
                startActivity(i);
            }
        });
        String data[] = new String[3];
        data[0] = globalData.URLhost() + "harajProductDetailsNextId.php";
        data[1] = idProduct;
        data[2] = statue;
        new ProgressTask().execute(data);
        pDialog = new ProgressDialog(HarajProductDetails.this);
        pDialog.setMessage("جاري قراءة البيانات");
        pDialog.setTitle("الرجاء الانتظار");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
        pDialog.show();
        ln_chat = (LinearLayout)findViewById(R.id.ln_chat);
        rlVideo = (RelativeLayout)findViewById(R.id.rlVideo);
        imgVideo = (ImageView)findViewById(R.id.imgVideo);
        lnPreviouseProduct = (LinearLayout)findViewById(R.id.lnPreviouseProduct);
        lnForwardProduct = (LinearLayout)findViewById(R.id.lnForwardProduct);
        lnRefresh = (LinearLayout)findViewById(R.id.lnRefresh);
        lnDelete = (LinearLayout)findViewById(R.id.lnDelete);
        lnRate = (LinearLayout)findViewById(R.id.lnRate);
        lnBlock = (LinearLayout)findViewById(R.id.lnBlock);
        lnEdit = (LinearLayout)findViewById(R.id.lnEdit);
        lnUser = (LinearLayout)findViewById(R.id.lnUser);
        lnPreviouseProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "next, id is: " + idProduct, Toast.LENGTH_SHORT).show();
                svScroll.fullScroll(ScrollView.FOCUS_UP);
                statue = "1";
                String data[] = new String[3];
                data[0] = globalData.URLhost() + "harajProductDetailsNextId.php";
                data[1] = idProduct;
                data[2] = statue;
                new ProgressTask().execute(data);
                pDialog = new ProgressDialog(HarajProductDetails.this);
                pDialog.setMessage("جاري قراءة البيانات");
                pDialog.setTitle("الرجاء الانتظار");
                pDialog.setIndeterminate(false);
                pDialog.setCancelable(true);
                pDialog.show();
            }
        });
        lnForwardProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "previouse, id is: " + idProduct, Toast.LENGTH_SHORT).show();
                svScroll.fullScroll(ScrollView.FOCUS_UP);
                statue = "2";
                String data[] = new String[3];
                data[0] = globalData.URLhost() + "harajProductDetailsNextId.php";
                data[1] = idProduct;
                data[2] = statue;
                new ProgressTask().execute(data);
                pDialog = new ProgressDialog(HarajProductDetails.this);
                pDialog.setMessage("جاري قراءة البيانات");
                pDialog.setTitle("الرجاء الانتظار");
                pDialog.setIndeterminate(false);
                pDialog.setCancelable(true);
                pDialog.show();
            }
        });
        lnRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (KeyValueShared.getId(getApplicationContext()).equals("0")){
                    alertsApp.mustLogin(svScroll);
                }else {
                    String data[] = new String[3];
                    data[0] = globalData.URLhost() + "harajRefreshProduct.php";
                    data[1] = idProduct;
                    data[2] = KeyValueShared.getId(getApplicationContext());
                    new sendId().execute(data);
                    pDialog = new ProgressDialog(HarajProductDetails.this);
                    pDialog.setMessage("جاري تحديث البيانات");
                    pDialog.setTitle("الرجاء الانتظار");
                    pDialog.setIndeterminate(false);
                    pDialog.setCancelable(true);
                    pDialog.show();
                }
            }
        });
        lnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (KeyValueShared.getId(getApplicationContext()).equals("0")){
                    alertsApp.mustLogin(svScroll);
                }else {
                    Intent i = new Intent(v.getContext(), HarajAdd.class);
                    i.putExtra("idProduct", dataProduct.getId());
                    i.putExtra("isEdit", "1");
                    v.getContext().startActivity(i);
                }
            }
        });
        lnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (KeyValueShared.getId(getApplicationContext()).equals("0")){
                    alertsApp.mustLogin(svScroll);
                }else {
                    String data[] = new String[3];
                    data[0] = globalData.URLhost() + "productDelete.php";
                    data[1] = idProduct;
                    data[2] = KeyValueShared.getId(getApplicationContext());
                    new sendId().execute(data);
                    pDialog = new ProgressDialog(HarajProductDetails.this);
                    pDialog.setMessage("جاري حذف الإعلان");
                    pDialog.setTitle("الرجاء الانتظار");
                    pDialog.setIndeterminate(false);
                    pDialog.setCancelable(true);
                    pDialog.show();
                    finish();
                }
            }
        });
        lnRate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (KeyValueShared.getId(getApplicationContext()).equals("0")){
                    alertsApp.mustLogin(svScroll);
                }else {
                    String data[] = new String[3];
                    data[0] = globalData.URLhost() + "harajAddRemoveRateProduct.php";
                    data[1] = idProduct;
                    data[2] = KeyValueShared.getId(getApplicationContext());
                    new sendId().execute(data);
                    pDialog = new ProgressDialog(HarajProductDetails.this);
                    pDialog.setMessage("جاري تنفيذ الأمر");
                    pDialog.setTitle("الرجاء الانتظار");
                    pDialog.setIndeterminate(false);
                    pDialog.setCancelable(true);
                    pDialog.show();
                }
            }
        });
        lnBlock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HarajProductDetails.this, harajViolentAdd.class);
                i.putExtra("idProduct", idProduct);
                startActivity(i);

            }
        });
        lnUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(getApplicationContext(), MyAdv.class);
                i.putExtra("idUser", String.valueOf(dataProduct.getIdUser()));
                v.getContext().startActivity(i);
            }
        });
        rlVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HarajProductDetails.this, videoPlay.class);
                i.putExtra("urlVideo", dataProduct.getVideoName());
                startActivity(i);
            }
        });
        ln_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (KeyValueShared.getId(getApplicationContext()).equals("0")){
                    alertsApp.mustLogin(svScroll);
                }else {
                    Intent i = new Intent(HarajProductDetails.this, chatUsers.class);
                    i.putExtra("otherUser", dataProduct.getIdUser());
                    i.putExtra("username", dataProduct.getUserNameProduct());
                    startActivity(i);
                }
            }
        });
    }
    private void resetall(){
        tvTitle.setText("");
        tvContent.setText("");
        tvPrice.setText("");
        tvTime.setText("");
        tvLocation.setText("");
        tvUser.setText("");
        tvTel.setText("");
        similar_list.clear();
        Image_list.clear();
        comment_list.clear();

        adapter = new ImageAlbumAdapter(getApplicationContext(), Image_list);
        mRecyclerView.setAdapter(adapter);
        commentAdapter = new commentsAdapter(getApplicationContext(), comment_list);
        rvComments.setAdapter(commentAdapter);
        adapterSimilar = new ProductSimilarAdapter(getApplicationContext(), similar_list);
        rvSimilar.setAdapter(adapterSimilar);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_haraj, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.add){
            Intent i = new Intent(HarajProductDetails.this, HarajAddSwicherMedia.class);
            startActivity(i);
        } else if(id == R.id.back){
            Intent i = new Intent(HarajProductDetails.this, MainHaraj.class);
            startActivity(i);
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    protected void onRestart() {
        super.onRestart();
        finish();
        startActivity(getIntent());
    }
    public class ProgressTask extends AsyncTask<String, Void, Boolean> {
        String stTitle, stTime, stUser, stLocation, stContent, stTel, stPrice;
        @SuppressWarnings("deprecation")
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                nameValuePairs.add(new BasicNameValuePair("id", params[1]));
                nameValuePairs.add(new BasicNameValuePair("statue", params[2]));
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));

                HttpResponse response = client.execute(post);

                int status = response.getStatusLine().getStatusCode();
                if(status == 200){
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    JSONObject jObj = new JSONObject(data.substring(data.indexOf("{"), data.lastIndexOf("}") + 1));
                    ReqSuccess = jObj.getString("success");
                    if(ReqSuccess.equals("1")){
                        dataProduct.setId(jObj.getString("id"));
                        dataProduct.setName(jObj.getString("name"));
                        dataProduct.setIdMainCat(jObj.getString("idMainCat"));
                        dataProduct.setIdSubCat(jObj.getString("idSubCat"));
                        dataProduct.setCatName(jObj.getString("catName"));
                        dataProduct.setPlace(jObj.getString("country"));
                        dataProduct.setCity(jObj.getString("city"));
                        dataProduct.setIsSum(jObj.getString("isSum"));
                        dataProduct.setImg(jObj.getString("img"));
                        dataProduct.setAlbumImg(jObj.getString("albumImg"));
                        dataProduct.setAlbumFullImg(jObj.getString("albumFullImg"));
                        dataProduct.setAllImages(jObj.getString("allImages"));
                        dataProduct.setTimeAdd(jObj.getString("timeAdd"));
                        dataProduct.setIdUser(jObj.getString("idUser"));
                        dataProduct.setUserNameProduct(jObj.getString("userName"));
                        dataProduct.setIsActive(jObj.getString("isActive"));
                        dataProduct.setPrice(jObj.getString("price"));
                        dataProduct.setMyPlaceAndCity(jObj.getString("location"));
                        dataProduct.setContact(jObj.getString("contact"));
                        dataProduct.setDescription(jObj.getString("description"));
                        dataProduct.setRating(jObj.getString("rating"));
                        dataProduct.setVideoImageName(jObj.getString("videoImageName"));
                        dataProduct.setVideoName(jObj.getString("videoName"));

                    }


                    return true;
                }else{
                    Log.e("error client string", "here");
                }
            } catch (ClientProtocolException e) {
                Log.e("error client string", e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("error exp", e.getMessage());
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("error json", e.getMessage());
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            pDialog.dismiss();
            super.onPostExecute(result);
            if(result == false){
                Toast.makeText(getApplicationContext(), "حدث خطأ، نرجو المحاوله في وقت لاحق", Toast.LENGTH_LONG).show();
            }else{
                if(ReqSuccess.equals("1")){
                    resetall();
                    idProduct = dataProduct.getId();
                    Toast.makeText(getApplicationContext(), "currnet id is: " + dataProduct.getId() + "new id is: " + idProduct, Toast.LENGTH_LONG).show();
                    tvTitle.setText(dataProduct.getName());
                    tvContent.setText(Html.fromHtml(dataProduct.getDescription()));
                    tvPrice.setText(dataProduct.getPrice());
                    tvTime.setText(dataProduct.getTimeAdd());
                    tvLocation.setText(dataProduct.getMyPlaceAndCity());
                    tvUser.setText(dataProduct.getUserNameProduct());
                    tvTel.setText(dataProduct.getContact());
                    if(!dataProduct.getAlbumFullImg().equals("")){
                        String data = dataProduct.getAlbumFullImg();
                        Log.e("image name", data);
                        if(data.contains("|||")) {
                            String[] fullImage = data.split("\\ \\|\\|\\|\\ ");
                            String[] smallImage = dataProduct.getAllImages().split("\\ \\|\\|\\|\\ ");
                            String[] nameImage = dataProduct.getAllImages().split("\\ \\|\\|\\|\\ ");
                            Log.e("print array", Arrays.toString(fullImage));
                            for (int i = 0; i < fullImage.length; i++) {
                                if(!fullImage[i].equals("")) {
                                    ImageAlbum dataImage = new ImageAlbum();
                                    dataImage.setImgURL(fullImage[i]);
                                    //dataImage.setImg(nameImage[i]);
                                    //dataImage.setImgURLSmall(smallImage[i]);
                                    dataImage.setDisplayType(getResources().getString(R.string.albumImageTypeDisplay));
                                    Image_list.add(dataImage);
                                    Log.e("image data", dataImage.getImgURL());
                                }
                            }
                        }
                        adapter = new ImageAlbumAdapter(getApplicationContext(), Image_list);
                        mRecyclerView.setAdapter(adapter);
                    }

                    if(!dataProduct.getVideoImageName().equals("")){
                        Log.e("found image", dataProduct.getVideoImageName());
                        rlVideo.setVisibility(View.VISIBLE);
                        Glide.with(getApplicationContext()).load(dataProduct.getVideoImageName()).into(imgVideo);
                    }
                    Log.e("idUser Local is", KeyValueShared.getId(getApplicationContext()));
                    Log.e("idUser product is", dataProduct.getIdUser());
                    if(String.valueOf(dataProduct.getIdUser()).equals(KeyValueShared.getId(getApplicationContext()))){
                        Toast.makeText(HarajProductDetails.this, "local " + KeyValueShared.getId(getApplicationContext()) + " = online " + dataProduct.getIdUser(), Toast.LENGTH_SHORT).show();
                        lnRefresh.setVisibility(View.VISIBLE);
                        lnEdit.setVisibility(View.VISIBLE);
                        lnDelete.setVisibility(View.VISIBLE);
                    }else{
                        Toast.makeText(HarajProductDetails.this, "local " + KeyValueShared.getId(getApplicationContext()) + " != online " + dataProduct.getIdUser(), Toast.LENGTH_SHORT).show();
                        lnRefresh.setVisibility(View.GONE);
                        lnEdit.setVisibility(View.GONE);
                        lnDelete.setVisibility(View.GONE);
                    }
                    String data[] = new String[2];
                    data[0] = globalData.URLhost() + "commentGetData.php";
                    data[1] = idProduct;
                    new comments().execute(data);

                }else{
                    Toast.makeText(getApplicationContext(), "لا يوجد مستخدم", Toast.LENGTH_LONG).show();
                }
            }
        }
    }
    public class comments extends AsyncTask<String, Void, Boolean> {
        @SuppressWarnings("deprecation")
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
                nameValuePairs.add(new BasicNameValuePair("id", params[1]));
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));

                HttpResponse response = client.execute(post);

                int status = response.getStatusLine().getStatusCode();
                if(status == 200){
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    JSONObject jObj = new JSONObject(data.substring(data.indexOf("{"), data.lastIndexOf("}") + 1));
                    ReqSuccess = jObj.getString("success");
                    if(ReqSuccess.equals("1")){
                        if(jObj.has("data")) {
                            JSONArray wee = jObj.getJSONArray("data");
                            for (int i = 0; i < wee.length(); i++) {
                                JSONObject c = wee.getJSONObject(i);
                                Comments dataComment = new Comments();
                                dataComment.setNameUser(c.getString("username"));
                                dataComment.setDateAdd(c.getString("dateAdd"));
                                dataComment.setComment(c.getString("comment"));
                                dataComment.setHasImage(c.getString("hasImage"));
                                dataComment.setId(c.getString("id"));
                                comment_list.add(dataComment);
                            }
                        }
                    }


                    return true;
                }else{
                    Log.e("error client string", "here");
                }
            } catch (ClientProtocolException e) {
                Log.e("error client string", e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("error exp", e.getMessage());
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("error json", e.getMessage());
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            pDialog.dismiss();
            super.onPostExecute(result);
            if(result == false){
                Toast.makeText(getApplicationContext(), "حدث خطأ، نرجو المحاوله في وقت لاحق", Toast.LENGTH_LONG).show();
            }else{
                if(ReqSuccess.equals("1")){
                    commentAdapter = new commentsAdapter(getApplicationContext(), comment_list);
                    rvComments.setAdapter(commentAdapter);
                }else{
                    Log.e("comment", "not found");
                }
                String data[] = new String[2];
                data[0] = globalData.URLhost() + "harajSimilarProduct.php";
                data[1] = dataProduct.getIdSubCat();
                new similar().execute(data);
            }
        }
    }
    public class similar extends AsyncTask<String, Void, Boolean> {

        @SuppressWarnings("deprecation")
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
                nameValuePairs.add(new BasicNameValuePair("idCat", params[1]));
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if(status == 200){
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    JSONObject jObj = new JSONObject(data.substring(data.indexOf("{"), data.lastIndexOf("}") + 1));
                    ReqSuccess = jObj.getString("success");
                    Log.e("data similar", data);
                    if(ReqSuccess.equals("1")){
                        if(jObj.has("data")) {
                            JSONArray wee = jObj.getJSONArray("data");
                            for (int i = 0; i < wee.length(); i++) {
                                JSONObject c = wee.getJSONObject(i);
                                Product dataProduct = new Product();
                                dataProduct.setId(c.getString("id"));
                                dataProduct.setName(c.getString("name"));
                                dataProduct.setIdMainCat(c.getString("idMainCat"));
                                dataProduct.setIdSubCat(c.getString("idSubCat"));
                                dataProduct.setCatName(c.getString("catName"));
                                dataProduct.setPlace(c.getString("country"));
                                dataProduct.setCity(c.getString("city"));
                                dataProduct.setIsSum(c.getString("isSum"));
                                dataProduct.setImg(c.getString("img"));
                                dataProduct.setAlbumImg(c.getString("albumImg"));
                                dataProduct.setAlbumFullImg(c.getString("albumFullImg"));
                                dataProduct.setAllImages(c.getString("allImages"));
                                dataProduct.setTimeAdd(c.getString("timeAdd"));
                                dataProduct.setIdUser(c.getString("idUser"));
                                dataProduct.setUserNameProduct(c.getString("userName"));
                                dataProduct.setIsActive(c.getString("isActive"));
                                dataProduct.setPrice(c.getString("price"));
                                dataProduct.setMyPlaceAndCity(c.getString("location"));
                                dataProduct.setContact(c.getString("contact"));
                                dataProduct.setDescription(c.getString("description"));
                                dataProduct.setVideoImageName(c.getString("videoImageName"));
                                dataProduct.setVideoName(c.getString("videoName"));
                                dataProduct.setActionType("details");
                                similar_list.add(dataProduct);
                            }
                        }

                    }


                    return true;
                }else{
                    Log.e("error client string", "here");
                }
            } catch (ClientProtocolException e) {
                Log.e("error client string", e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("error exp", e.getMessage());
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("error json", e.getMessage());
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if(result == false){
                Toast.makeText(getApplicationContext(), "حدث خطأ، نرجو المحاوله في وقت لاحق", Toast.LENGTH_LONG).show();
            }else{
                if(ReqSuccess.equals("1")){

                        adapterSimilar = new ProductSimilarAdapter(getApplicationContext(), similar_list);
                        rvSimilar.setAdapter(adapterSimilar);

                }
            }
        }
    }
    public class sendId extends AsyncTask<String, Void, Boolean> {
        String message;
        @SuppressWarnings("deprecation")
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                nameValuePairs.add(new BasicNameValuePair("idProduct", params[1]));
                nameValuePairs.add(new BasicNameValuePair("idUser", params[2]));
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));

                HttpResponse response = client.execute(post);

                int status = response.getStatusLine().getStatusCode();
                if(status == 200){
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    JSONObject jObj = new JSONObject(data.substring(data.indexOf("{"), data.lastIndexOf("}") + 1));
                    ReqSuccess = jObj.getString("success");
                    message = jObj.getString("message");

                    return true;
                }else{
                    Log.e("error client string", "here");
                }
            } catch (ClientProtocolException e) {
                Log.e("error client string", e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("error exp", e.getMessage());
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("error json", e.getMessage());
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            pDialog.dismiss();
            super.onPostExecute(result);
            if(result == false){
                Toast.makeText(getApplicationContext(), "حدث خطأ، نرجو المحاوله في وقت لاحق", Toast.LENGTH_LONG).show();
            }else{
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();

            }
        }
    }
}
