package profamily.designsite.in.profamilies;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import imageData.ImageAlbum;
import imageData.ImageAlbumAdapter;
import libes.KeyValueShared;
import libes.globalData;

public class HarajAddSwicher extends AppCompatActivity {

    String id;
    Button btnAddMainImage, btnAddAlbumImage, btnFinish;
    ImageView ivImageMain;
    String ReqSuccess;
    String ReqMessage;
    private RecyclerView mRecyclerView;
    private ImageAlbumAdapter adapter;
    private List<ImageAlbum> Image_list;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_haraj_add_swicher);
        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mRecyclerView = (RecyclerView)findViewById(R.id.mRecyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        Image_list = new ArrayList<>();
        Intent intent = getIntent();
        id = intent.getStringExtra("id");
        btnFinish = (Button)findViewById(R.id.btnFinish);
        btnFinish.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        btnAddMainImage = (Button)findViewById(R.id.btnAddMainImage);
        btnAddMainImage.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        btnAddAlbumImage = (Button)findViewById(R.id.btnAddAlbumImage);
        btnAddAlbumImage.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        ivImageMain = (ImageView)findViewById(R.id.ivImageMain);
        btnFinish.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btnAddMainImage.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent(HarajAddSwicher.this, AddPhotoProduct.class);
                i.putExtra("isMain", "0");
                i.putExtra("isVideo", "0");
                startActivity(i);
            }
        });
        btnAddAlbumImage.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent(HarajAddSwicher.this, AddPhotoProduct.class);
                i.putExtra("isMain", "1");
                i.putExtra("isVideo", "0");
                startActivity(i);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menubar_haraj_back, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.back){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    protected void onResume() {
        Image_list.clear();
        String data[] = new String[2];
        data[0] = globalData.URLhost() + "tempFilesGetData.php";
        data[1] = KeyValueShared.getIdForMember(getApplicationContext());
        productGetDataAsynTask bts = new productGetDataAsynTask();
        bts.execute(data);
        super.onResume();
    }

    public class productGetDataAsynTask extends AsyncTask<String, Void, Boolean> {

        String img = "";
        @SuppressWarnings("deprecation")
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
                nameValuePairs.add(new BasicNameValuePair("idUser", params[1]));
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));
                HttpResponse response = client.execute(post);

                int status = response.getStatusLine().getStatusCode();
                if(status == 200){
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    JSONObject jObj = new JSONObject(data.substring(data.indexOf("{"), data.lastIndexOf("}") + 1));
                    ReqSuccess = jObj.getString("success");
                    if(ReqSuccess.equals("1")){
                        try{
                            img = jObj.getString("mainImg");
                            if(jObj.has("album")) {
                                JSONArray wee = jObj.getJSONArray("album");
                                for (int i = 0; i < wee.length(); i++) {
                                    JSONObject c = wee.getJSONObject(i);
                                    ImageAlbum dataImage = new ImageAlbum();
                                    dataImage.setImgID(c.getString("id"));
                                    dataImage.setImgURLSmall(c.getString("imgUrlSmall"));
                                    dataImage.setImgURLMedium(c.getString("imgUrlMedium"));
                                    dataImage.setImgURL(c.getString("imgUrl"));
                                    dataImage.setDisplayType(getResources().getString(R.string.albumImageTypeEdit));
                                    Image_list.add(dataImage);
                                }
                            }
                        }catch(JSONException e){
                            Log.e("error jsons", e.getMessage());
                            e.printStackTrace();
                        }
                    }


                    return true;
                }else{
                    Log.e("error client string", "here");
                }
            } catch (ClientProtocolException e) {
                Log.e("error client string", e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("error exp", e.getMessage());
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("error json", e.getMessage());
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            //pDialog.dismiss();
            super.onPostExecute(result);
            if(result == false){
                Toast.makeText(getApplicationContext(), "حدث خطأ، نرجو المحاوله في وقت لاحق", Toast.LENGTH_LONG).show();
            }else{
                if(ReqSuccess.equals("1")){
                    Glide.with(getApplicationContext()).load(img).into(ivImageMain);
                    adapter = new ImageAlbumAdapter(getApplicationContext(), Image_list);
                    mRecyclerView.setAdapter(adapter);
                }else{
                    Toast.makeText(getApplicationContext(), "لا يوجد صوره", Toast.LENGTH_LONG).show();
                }
            }
        }
    }
}
