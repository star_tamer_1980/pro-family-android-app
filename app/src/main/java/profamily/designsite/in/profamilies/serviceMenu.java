package profamily.designsite.in.profamilies;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import libes.KeyValueShared;
import libes.footerHarajAction;
import libes.footerServiceAction;
import menuSettings.menuServiceAdapter;
import menuSettings.menuSettings;

public class serviceMenu extends AppCompatActivity {

    private RecyclerView mvMenu;

    private menuServiceAdapter menuAdapter;
    private List<menuSettings> menuArray;
    LinearLayout lnHome, lnSearch, lnNoti, lnMore;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_service_menu);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        lnHome = (LinearLayout)findViewById(R.id.btnHome);
        lnSearch = (LinearLayout)findViewById(R.id.btnSearch);
        lnNoti = (LinearLayout)findViewById(R.id.btnNoti);
        lnMore = (LinearLayout)findViewById(R.id.btnMore);
        lnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerServiceAction.footerHome(serviceMenu.this);
                finish();
            }
        });
        lnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerServiceAction.footerSearch(serviceMenu.this);
                finish();
            }
        });
        lnNoti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerServiceAction.footerNoti(serviceMenu.this);
                finish();
            }
        });
        lnMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerServiceAction.footerMore(serviceMenu.this);
                finish();
            }
        });
        mvMenu = (RecyclerView)findViewById(R.id.menuServiceSettings);
        mvMenu.setHasFixedSize(true);
        mvMenu.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        setMenu();
    }
    @Override
    protected void onRestart() {
        setMenu();
        super.onRestart();
    }
    public void setMenu(){
        String[] menuName;
        String[] menuId;
        String[] menuIcon;
        menuArray = new ArrayList<>();
        if(KeyValueShared.getId(this).equals("0")) {
            menuName = getApplicationContext().getResources().getStringArray(R.array.serviceMenuForUsersName);
            menuId = getApplicationContext().getResources().getStringArray(R.array.serviceMenuForUsersId);
            menuIcon = getApplicationContext().getResources().getStringArray(R.array.harajMenuForUsersIcon);
        }else{
            menuName = getApplicationContext().getResources().getStringArray(R.array.serviceMenuForMembersName);
            menuId = getApplicationContext().getResources().getStringArray(R.array.serviceMenuForMembersId);
            menuIcon = getApplicationContext().getResources().getStringArray(R.array.serviceMenuForMembersIcon);
        }
        for(int i=0; i < menuName.length; i++){
            menuSettings menuData = new menuSettings();
            menuData.setMenuId(menuId[i].toString());
            if(menuId[i].toString().equals("3")){
                String name = menuName[i].toString() + " (" + KeyValueShared.getSettingsAppMyCityName(getApplicationContext()) + ")";
                menuData.setMenuName(name);
            }else if(menuId[i].toString().equals("7")){
                String name = menuName[i].toString() + " (" + KeyValueShared.getName(getApplicationContext()) + ")";
                menuData.setMenuName(name);
            }else{
                menuData.setMenuName(menuName[i].toString());
            }

            menuData.setMenuIcon(menuIcon[i].toString());
            Log.e("name", menuName[i].toString());
            menuArray.add(menuData);
        }
        menuAdapter = new menuServiceAdapter(getApplication(), menuArray);
        mvMenu.setAdapter(menuAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_haraj, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.add){
            Intent i = new Intent(serviceMenu.this, serviceAddAgreement.class);
            startActivity(i);
        } else if(id == R.id.back){
            Intent i = new Intent(serviceMenu.this, serviceMain.class);
            startActivity(i);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
