package profamily.designsite.in.profamilies;

import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.Key;
import java.util.ArrayList;
import java.util.List;

import libes.KeyValueShared;
import libes.SQLiteConnection;
import libes.globalData;
import marketsData.markets;
import marketsData.marketsAdapter;
import noti.notificationAdmin;
import noti.notificationAdminAdapter;

public class MakretListSearch extends AppCompatActivity {
    private RecyclerView mRecyclerView1;
    private GridLayoutManager gridLayoutManager;
    private marketsAdapter adapter1;
    private List<markets> data_list1;
    private String dataTest;
    private ProgressDialog pDialog;
    private ProgressBar pBar1;
    List<String> listNoti = new ArrayList<String>();
    String ReqSuccess;
    String fileApi = "marketFragList.php";


    private int m_previouse_Total_Count1 = 0;
    private boolean loading = true;
    private int visibleThreshold = 15;
    int firstVisibleItem, visibleItemCount, totalItemCount;
    private TextView txtNotFound;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_makret_list_search);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if(KeyValueShared.getMapLat(getApplicationContext()).equals("0") || KeyValueShared.getMapLon(getApplicationContext()).equals("0")){
            fileApi = "marketListSearchWithOutLocation.php";
        }
        m_previouse_Total_Count1 = 0;
        txtNotFound = (TextView)findViewById(R.id.txtNotFound);
        txtNotFound.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        mRecyclerView1 = (RecyclerView)findViewById(R.id.recycleView1);
        mRecyclerView1.setLayoutManager(new LinearLayoutManager(this));
        pBar1 = (ProgressBar)findViewById(R.id.progressBar1);
        data_list1 = new ArrayList<>();
        mRecyclerView1.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager mLayoutManager = (LinearLayoutManager) mRecyclerView1
                        .getLayoutManager();
                mRecyclerView1.setLayoutManager(mLayoutManager);
                visibleItemCount = mRecyclerView1.getChildCount();
                totalItemCount = mLayoutManager.getItemCount();
                firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();
                if(totalItemCount == 0){
                    return ;
                }
                if(m_previouse_Total_Count1==totalItemCount){
                    return;
                }
                boolean loadMore = firstVisibleItem + visibleItemCount >= totalItemCount;
                if(loadMore){
                    m_previouse_Total_Count1 = totalItemCount;
                    pBar1.setVisibility(View.VISIBLE);
                    getLoadData(totalItemCount);
                }

            }
        });

        String data[] = new String[8];
        data[0] = globalData.URLhost() + fileApi;
        data[1] = "0";
        data[2] = KeyValueShared.getMapLat(getApplicationContext());
        data[3] = KeyValueShared.getMapLon(getApplicationContext());
        data[4] = getIntent().getStringExtra("idMainCat");
        data[5] = getIntent().getStringExtra("idSubCat");
        data[6] = getIntent().getStringExtra("idUser");
        data[7] = getIntent().getStringExtra("token");
        loadData1 bts = new loadData1();
        bts.execute(data);
        KeyValueShared.setMarketId(getApplicationContext(), "");
        KeyValueShared.setMarketDeliveryPrice(getApplicationContext(), "");
        KeyValueShared.setMarketMinPrice(getApplicationContext(), "");
        SQLiteConnection db = new SQLiteConnection(getApplicationContext());
        db.deleteAllRecord();
    }

    @Override
    protected void onRestart() {
        KeyValueShared.setMarketId(getApplicationContext(), "");
        KeyValueShared.setMarketDeliveryPrice(getApplicationContext(), "");
        KeyValueShared.setMarketMinPrice(getApplicationContext(), "");
        SQLiteConnection db = new SQLiteConnection(getApplicationContext());
        db.deleteAllRecord();
        super.onRestart();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_haraj, menu);
        return super.onCreateOptionsMenu(menu);
    }
    public void getLoadData(Integer limit) {

        pBar1.setVisibility(View.VISIBLE);
        String data[] = new String[8];
        data[0] = globalData.URLhost() + fileApi;
        data[1] = String.valueOf(limit);
        data[2] = KeyValueShared.getMapLat(getApplicationContext());
        data[3] = KeyValueShared.getMapLon(getApplicationContext());
        data[4] = getIntent().getStringExtra("idMainCat");
        data[5] = getIntent().getStringExtra("idSubCat");
        data[6] = getIntent().getStringExtra("idUser");
        data[7] = getIntent().getStringExtra("token");

        loadData1 bts = new loadData1();
        bts.execute(data);
    }
    public class loadData1 extends AsyncTask<String, Void, Boolean> {

        @SuppressWarnings("deprecation")
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(7);
                nameValuePairs.add(new BasicNameValuePair("limit", params[1]));
                nameValuePairs.add(new BasicNameValuePair("mapLat", params[2]));
                nameValuePairs.add(new BasicNameValuePair("mapLon", params[3]));
                nameValuePairs.add(new BasicNameValuePair("idMainCat", params[4]));
                nameValuePairs.add(new BasicNameValuePair("idSubCat", params[5]));
                nameValuePairs.add(new BasicNameValuePair("idUser", params[6]));
                nameValuePairs.add(new BasicNameValuePair("token", params[7]));
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if(status == 200){
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    JSONObject jObj = new JSONObject(data.substring(data.indexOf("{"), data.lastIndexOf("}") + 1));
                    ReqSuccess = jObj.getString("success");
                    if(ReqSuccess.equals("1")){
                        dataTest = data;
                        if(jObj.has("data")) {
                            JSONArray wee = jObj.getJSONArray("data");
                            Log.e("error", wee.toString());
                            for (int i = 0; i < wee.length(); i++) {
                                JSONObject c = wee.getJSONObject(i);
                                markets dataMarkets = new markets();
                                dataMarkets.setId(c.getString("id"));
                                dataMarkets.setName(c.getString("name"));
                                dataMarkets.setImg(c.getString("img"));
                                dataMarkets.setIdOwner(c.getString("idOwner"));
                                dataMarkets.setIdMainCat(c.getString("idMainCat"));
                                dataMarkets.setMainCat(c.getString("cat"));
                                dataMarkets.setDelivery(c.getString("delivery"));
                                dataMarkets.setMiniMumCharge(c.getString("miniMumCharge"));
                                dataMarkets.setDescribtions(c.getString("describtions"));
                                dataMarkets.setRating(c.getString("rating"));
                                dataMarkets.setDistance(c.getString("distance"));
                                data_list1.add(dataMarkets);
                            }
                        }

                    }


                    return true;
                }else{
                    Log.e("error client string", "here");
                }
            } catch (ClientProtocolException e) {
                Log.e("error client string", e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("error exp", e.getMessage());
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("error json", e.getMessage());
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if(result == false){
                Toast.makeText(getApplicationContext(), "حدث خطأ، نرجو المحاوله في وقت لاحق", Toast.LENGTH_LONG).show();
            }else{
                if(ReqSuccess.equals("1")){
                    txtNotFound.setVisibility(View.INVISIBLE);
                    if(m_previouse_Total_Count1>0) {
                        adapter1.notifyDataSetChanged();
                        pBar1.setVisibility(View.INVISIBLE);
                    }else{
                        adapter1 = new marketsAdapter(getApplicationContext(), data_list1);
                        mRecyclerView1.setAdapter(adapter1);
                        pBar1.setVisibility(View.INVISIBLE);
                    }
                }else{
                    pBar1.setVisibility(View.INVISIBLE);
                }
            }
        }
    }
}
