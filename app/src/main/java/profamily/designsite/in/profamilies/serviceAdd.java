package profamily.designsite.in.profamilies;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import imageData.ImageAlbum;
import imageData.ImageAlbumAdapter;
import libes.KeyValueShared;
import libes.globalData;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import productData.Product;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uploadFiles.ApiConfig;
import uploadFiles.AppConfig;
import uploadFiles.ServerResponse;

public class serviceAdd extends AppCompatActivity {


    private String isEdit = "0" /* 0 = add, 1 = edit*/, idProduct = "0";
    private EditText etTitle, etContent, etTel;
    private Button haraj_add_media, btnSave;
    private TextView tvTitle, tvTel, tvContent;
    private ScrollView vScroll;
    private ImageView imgMain;
    private Bitmap thumb;

    static final int REQUEST_IMAGE_CAPTURE = 1, REQUEST_GALLERY_CAPTURE = 2;
    private ProgressDialog progressDialog;
    private String mediaPath;
    private Uri imageUri;
    private TextView tvResult;
    private String ImagesName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_add);
        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        imgMain = (ImageView)findViewById(R.id.imgMain);
        vScroll = (ScrollView)findViewById(R.id.vScroll);
        etTitle = (EditText)findViewById(R.id.haraj_add_title);
        etTitle.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        tvTitle = (TextView)findViewById(R.id.haraj_add_title);
        tvTitle.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        etTel = (EditText)findViewById(R.id.haraj_add_tel);
        etTel.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        tvTel = (TextView)findViewById(R.id.haraj_add_tel);
        tvTel.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        etContent = (EditText)findViewById(R.id.haraj_add_content);
        etContent.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        tvContent = (TextView)findViewById(R.id.haraj_add_content);
        tvContent.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        haraj_add_media = (Button)findViewById(R.id.haraj_add_media);
        btnSave = (Button)findViewById(R.id.haraj_add_save);
        btnSave.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));

        if(getIntent().hasExtra("isEdit")) {

            isEdit = getIntent().getExtras().getString("isEdit");
            idProduct = getIntent().getExtras().getString("idProduct");
            etTitle.setText(getIntent().getExtras().getString("title"));
            etContent.setText(getIntent().getExtras().getString("content"));
            etTel.setText(getIntent().getExtras().getString("tel"));
            if(getIntent().hasExtra("imgMain")) {
                String fullUrlImage = getIntent().getExtras().getString("imgMain");
                imgMain.setVisibility(View.VISIBLE);
                Glide.with(getApplicationContext()).load(fullUrlImage).into(imgMain);
                ImagesName = getIntent().getExtras().getString("imgName");
            }
        }
        etTitle.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == 66){
                    etTel.requestFocus();
                }
                return false;
            }
        });
        etTel.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == 66){
                    etContent.requestFocus();
                }
                return false;
            }
        });
        haraj_add_media.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseMediaMainImage(vScroll);
            }
        });
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getData();
            }
        });
    }
    public void chooseMediaMainImage(final View view){
        AlertDialog.Builder builder1 = new AlertDialog.Builder(view.getContext());
        builder1.setMessage(view.getResources().getString(R.string.alertMediaMessage));
        builder1.setTitle(view.getResources().getString(R.string.alertMediaTitle));
        builder1.setCancelable(true);

        builder1.setNeutralButton(
                view.getResources().getString(R.string.alertMediaBtn1),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ContentValues values = new ContentValues();
                        values.put(MediaStore.Images.Media.TITLE, "New Picture");
                        values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
                        imageUri = getContentResolver().insert(
                                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                        startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
                    }
                });

        builder1.setNegativeButton(
                view.getResources().getString(R.string.alertMediaBtn2),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Create intent to Open Image applications like Gallery, Google Photos
                        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
// Start the Intent
                        startActivityForResult(galleryIntent, REQUEST_GALLERY_CAPTURE);
                    }
                });
        builder1.setCancelable(false).setPositiveButton(view.getResources().getString(R.string.alertButtonCancel), new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }
    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK){
            if (resultCode == Activity.RESULT_OK) {
                try {
                    String imageurl = getRealPathFromURI(imageUri);
                    mediaPath = imageurl;
                    //aImageView.setImageBitmap(BitmapFactory.decodeFile(mediaPath));
                    Toast.makeText(getApplicationContext(), imageurl.toString(), Toast.LENGTH_SHORT).show();
                    progressDialog = new ProgressDialog(this);
                    progressDialog.setMessage("Uploading...");
                    uploadFile();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        }
        if(requestCode == REQUEST_GALLERY_CAPTURE && resultCode == RESULT_OK){
            try {
                // When an Image is picked
                if (requestCode == REQUEST_GALLERY_CAPTURE && resultCode == RESULT_OK
                        && null != data) {
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    assert cursor != null;
                    cursor.moveToFirst();
                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    mediaPath = cursor.getString(columnIndex);

                    //aImageView.setImageBitmap(BitmapFactory.decodeFile(mediaPath));
                    cursor.close();
                    progressDialog = new ProgressDialog(this);
                    progressDialog.setMessage("Uploading...");
                    uploadFile();

                } else {
                    Toast.makeText(this, "You haven't picked Image",
                            Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                        .show();
            }
        }

    }
    private void uploadFile() {
        progressDialog.show();
        Map<String, RequestBody> map = new HashMap<>();
        File file = new File(String.valueOf(mediaPath));

        RequestBody idUser = RequestBody.create(MediaType.parse("text/plain"), KeyValueShared.getIdForMember(this));
        RequestBody isMain = RequestBody.create(MediaType.parse("text/plain"), "0");
        RequestBody isVideo = RequestBody.create(MediaType.parse("text/plain"), "0");
        RequestBody requestBody = RequestBody.create(MediaType.parse("*/*"), file);
        map.put("file\"; filename=\"" + file.getName() + "\"", requestBody);
        ApiConfig getResponse = AppConfig.getRetrofit().create(ApiConfig.class);
        Call<ServerResponse> call = getResponse.upload("token", map, idUser, isMain, isVideo);

        call.enqueue(new Callback<ServerResponse>() {
            @Override
            public void onResponse(Call<ServerResponse> call, Response<ServerResponse> response) {
                progressDialog.dismiss();
                ServerResponse serverResponse = response.body();
                if(serverResponse != null){
                    if(serverResponse.getSuccess()){
                        ImagesName = serverResponse.getImgName();
                        String fullUrlImage = globalData.URLThumb() + ImagesName;
                        Log.e("url is:", fullUrlImage);
                        imgMain.setVisibility(View.VISIBLE);
                        Glide.with(getApplicationContext()).load(fullUrlImage).into(imgMain);
                    }else{
                        Toast.makeText(getApplicationContext(), serverResponse.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }else{
                    Log.e("Response", serverResponse.toString());
                }
            }

            @Override
            public void onFailure(Call<ServerResponse> call, Throwable t) {
                Log.e("error", t.getMessage().toString());
                Toast.makeText(getApplicationContext(), "message is " + t.getMessage().toString(), Toast.LENGTH_LONG).show();
            }
        });
    }
    public class getProductData extends AsyncTask<String, Void, Boolean> {
        String ReqSuccess;
        Product dataProduct = new Product();
        @SuppressWarnings("deprecation")
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                nameValuePairs.add(new BasicNameValuePair("id", params[1]));
                nameValuePairs.add(new BasicNameValuePair("statue", params[2]));
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));

                HttpResponse response = client.execute(post);

                int status = response.getStatusLine().getStatusCode();
                if(status == 200){
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    JSONObject jObj = new JSONObject(data.substring(data.indexOf("{"), data.lastIndexOf("}") + 1));
                    ReqSuccess = jObj.getString("success");
                    if(ReqSuccess.equals("1")){
                        dataProduct.setId(jObj.getString("id"));
                        dataProduct.setName(jObj.getString("name"));
                        dataProduct.setIdMainCat(jObj.getString("idMainCat"));
                        dataProduct.setIdSubCat(jObj.getString("idSubCat"));
                        dataProduct.setCatName(jObj.getString("catName"));
                        dataProduct.setPlace(jObj.getString("country"));
                        dataProduct.setCity(jObj.getString("city"));
                        dataProduct.setIsSum(jObj.getString("isSum"));
                        dataProduct.setImg(jObj.getString("img"));
                        dataProduct.setAlbumImg(jObj.getString("albumImg"));
                        dataProduct.setAlbumFullImg(jObj.getString("albumFullImg"));
                        dataProduct.setAllImages(jObj.getString("allImages"));
                        dataProduct.setTimeAdd(jObj.getString("timeAdd"));
                        dataProduct.setIdUser(jObj.getString("idUser"));
                        dataProduct.setUserNameProduct(jObj.getString("userName"));
                        dataProduct.setIsActive(jObj.getString("isActive"));
                        dataProduct.setPrice(jObj.getString("price"));
                        dataProduct.setMyPlaceAndCity(jObj.getString("location"));
                        dataProduct.setContact(jObj.getString("contact"));
                        dataProduct.setDescription(jObj.getString("description"));
                        dataProduct.setRating(jObj.getString("rating"));
                        dataProduct.setVideoImageName(jObj.getString("videoImageName"));
                        dataProduct.setVideoName(jObj.getString("videoName"));

                    }


                    return true;
                }else{
                    Log.e("error client string", "here");
                }
            } catch (ClientProtocolException e) {
                Log.e("error client string", e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("error exp", e.getMessage());
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("error json", e.getMessage());
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            progressDialog.dismiss();
            super.onPostExecute(result);
            if(result == false){
                Toast.makeText(getApplicationContext(), "حدث خطأ، نرجو المحاوله في وقت لاحق", Toast.LENGTH_LONG).show();
            }else{
                if(ReqSuccess.equals("1")){
                    idProduct = dataProduct.getId();
                    etTitle.setText(dataProduct.getName());
                    etContent.setText(dataProduct.getDescription());
                    etTel.setText(dataProduct.getContact());
                    ImagesName = dataProduct.getAllImages();
                    btnSave.setText(getResources().getString(R.string.save_data));
                }else{
                    Toast.makeText(getApplicationContext(), "لا يوجد بيانات", Toast.LENGTH_LONG).show();
                }
            }
        }
    }
    private void getData(){
        if(TextUtils.isEmpty(etTitle.getText().toString())){
            etTitle.setError(getResources().getString(R.string.mustWriteTitle));
        }else if(TextUtils.isEmpty(etTel.getText().toString())){
            etTel.setError(getResources().getString(R.string.mustWriteTel));
        }else {
            if (isEdit.equals("0")){
                Intent i = new Intent(serviceAdd.this, serviceAddCat.class);
                i.putExtra("title", etTitle.getText().toString());
                i.putExtra("tel", etTel.getText().toString());
                i.putExtra("content", etContent.getText().toString());
                i.putExtra("imgMain", ImagesName);
                startActivity(i);
                finish();
            }else{
                String dataCat[] = new String[8];
                dataCat[0] = globalData.URLhost() + "serviceEdit.php";
                dataCat[1] = etTitle.getText().toString();
                dataCat[2] = etTel.getText().toString();
                dataCat[4] = etContent.getText().toString();
                dataCat[6] = ImagesName;
                dataCat[7] = idProduct;
                saveAdvertise saveProduct = new saveAdvertise();
                saveProduct.execute(dataCat);
            }
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menubar_haraj_back, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.back){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
    public class saveAdvertise extends AsyncTask<String, Void, Boolean>{
        String ReqSuccess;
        @SuppressWarnings("deprecation")
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(7);
                nameValuePairs.add(new BasicNameValuePair("title", params[1]));
                nameValuePairs.add(new BasicNameValuePair("tel", params[2]));
                nameValuePairs.add(new BasicNameValuePair("content", params[4]));
                nameValuePairs.add(new BasicNameValuePair("mainImage", params[6]));
                nameValuePairs.add(new BasicNameValuePair("idProduct", params[7]));
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));

                HttpResponse response = client.execute(post);

                int status = response.getStatusLine().getStatusCode();
                if(status == 200){
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    Log.e("data", data.toString());
                    JSONObject jObj = new JSONObject(data.substring(data.indexOf("{"), data.lastIndexOf("}") + 1));
                    ReqSuccess = jObj.getString("success");
                    return true;
                }else{
                    Log.e("error client string", "here");
                }
            } catch (ClientProtocolException e) {
                Log.e("error client string", e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("error exp", e.getMessage());
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("error json", e.getMessage());
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if(result == false){
                Toast.makeText(getApplicationContext(), "حدث خطأ، نرجو المحاوله في وقت لاحق", Toast.LENGTH_LONG).show();
            }else{
                if(ReqSuccess.equals("1")){
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.haraj_add_finish), Toast.LENGTH_LONG).show();
                    finish();
                }else{
                    Toast.makeText(getApplicationContext(), "لم يتم حفظ البيانات", Toast.LENGTH_LONG).show();
                }
            }
        }

    }
}
