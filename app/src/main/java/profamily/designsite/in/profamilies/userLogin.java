package profamily.designsite.in.profamilies;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import libes.KeyValueShared;
import libes.globalData;
import java.util.Random;

public class userLogin extends AppCompatActivity {

    private EditText etTel;
    private Button btnLogin, btnGoToRegister;
    String ReqSuccess , ReqMessage;
    private ProgressDialog pDialog;
    String code;
    String regName, regTel, regEmail, regId, regIsSMSOrder, regIsSMSComments, regIsNotifiOrders, regIsNotifiComments, isSMSStatue;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_login);
        etTel = (EditText)findViewById(R.id.etTel);
        btnGoToRegister = (Button)findViewById(R.id.btnGoToRegister);
        btnGoToRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(userLogin.this, userRegister.class);
                startActivity(i);
                finish();
            }
        });
        btnLogin = (Button)findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginProccess();
            }
        });
        etTel.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_NEXT || actionId == EditorInfo.IME_ACTION_DONE){
                    loginProccess();
                    return true;
                }
                return false;
            }
        });
    }
    private void loginProccess(){
        String Tel = etTel.getText().toString();
        if(TextUtils.isEmpty(Tel)){
            etTel.setError((getString(R.string.mustWriteTel)));
        }else{
            String data[] = new String[2];
            data[0] = globalData.URLhost() + "memberLogin.php";
            data[1] = Tel;

            new MembersAsynTask().execute(data);

            pDialog = new ProgressDialog(this);
            pDialog.setMessage("جاري دخول العضو");
            pDialog.setTitle("برجاء الانتظار ثواني");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }
    }
    public class MembersAsynTask extends AsyncTask<String, Void, Boolean> {
        JSONObject jObj;
        @SuppressWarnings("deprecation")
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
                nameValuePairs.add(new BasicNameValuePair("tel", params[1]));
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if(status == 200){
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    Log.e("data error", data);
                    JSONObject jObj = new JSONObject(data.substring(data.indexOf("{"), data.lastIndexOf("}") + 1));
                    ReqSuccess = jObj.getString("success");
                    isSMSStatue = jObj.getString("isSMSLogin");
                    if(ReqSuccess.equals("1")){
                        if(jObj.has("data")) {
                            JSONArray wee = jObj.getJSONArray("data");
                            for (int i = 0; i < wee.length(); i++) {
                                JSONObject c = wee.getJSONObject(i);
                                regTel = c.getString("tel");
                                regName = c.getString("name");
                                regEmail = c.getString("email");
                                regId = c.getString("id");
                                regIsSMSOrder = String.valueOf(c.get("isSMSOrders"));
                                regIsSMSComments = String.valueOf(c.get("isSMSComments"));
                                regIsNotifiOrders = String.valueOf(c.get("isNotifiOrders"));
                                regIsSMSComments = String.valueOf(c.get("isNotifiComments"));

                            }
                        }

                    }else{
                        ReqMessage = jObj.getString("message");
                    }


                    return true;
                }else{
                    Log.e("error client string", "here");
                }
            } catch (ClientProtocolException e) {
                Log.e("error client string", e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("error exp", e.getMessage());
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("error json", e.getMessage());
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            pDialog.dismiss();
            super.onPostExecute(result);
            if(result == false){
                Toast.makeText(getApplicationContext(), "لا يوجد بيانات " + ReqMessage, Toast.LENGTH_LONG).show();
            }else{
                if(ReqSuccess.equals("1")){
                    Log.e("sms statue", isSMSStatue);
                    if (isSMSStatue.equals("1")) {
                        Random rand = new Random();
                        code = String.valueOf(rand.nextInt(9999) + 1);
                        String datass[] = new String[2];
                        datass[0] = "http://www.mobily.ws/api/msgSend.php?mobile=966505402332&password=Suad4321&sender=Families&applicationType=68&lang=3&timeSend=0&dateSend=0" + "&numbers=966" + regTel + "&msg=%D9%83%D9%88%D8%AF%20%D8%A7%D9%84%D8%AA%D8%AD%D9%82%D9%82%20%D9%87%D9%88%3A%20" + code;

                        new SMSAsynTask().execute(datass);
                    }else{

                        Toast.makeText(getApplicationContext(), "تم تسجيل الدخول", Toast.LENGTH_LONG).show();
                        Toast.makeText(getApplicationContext(), "أهلاً بك: " + regName, Toast.LENGTH_LONG).show();
                        KeyValueShared.setTel(getApplicationContext(), regTel);
                        KeyValueShared.setName(getApplicationContext(), regName);
                        KeyValueShared.setEmail(getApplicationContext(), regEmail);
                        KeyValueShared.setId(getApplicationContext(), regId);
                        KeyValueShared.setSettingsAppisSMSOrders(getApplicationContext(), regIsSMSOrder);
                        KeyValueShared.setSettingsAppisSMSComments(getApplicationContext(), regIsSMSComments);
                        KeyValueShared.setSettingsAppisNotifiOrders(getApplicationContext(), regIsNotifiOrders);
                        KeyValueShared.setSettingsAppisNotifiComments(getApplicationContext(), regIsNotifiComments);
                        KeyValueShared.setStatue(getApplicationContext(), "1");
                        finish();
                    }

                }else{
                    Toast.makeText(getApplicationContext(), ReqMessage, Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    public class SMSAsynTask extends AsyncTask<String, Void, Boolean> {
        JSONObject jObj;
        String data;
        @SuppressWarnings("deprecation")
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if(status == 200){
                    HttpEntity entity = response.getEntity();
                    data = EntityUtils.toString(entity);
                    Log.e("data error", data);



                    return true;
                }else{
                    Log.e("error client string", "here");
                }
            } catch (ClientProtocolException e) {
                Log.e("error client string", e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("error exp", e.getMessage());
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            pDialog.dismiss();
            super.onPostExecute(result);
            if(result == false){
                Toast.makeText(getApplicationContext(), "حدث خطأ، نرجو المحاوله في وقت لاحق", Toast.LENGTH_LONG).show();
            }else{
                if (data.equals("1")){
                    Intent i = new Intent(userLogin.this, Activity_user_SMS_Auth.class);
                    i.putExtra("tel", regTel);
                    i.putExtra("name", regName);
                    i.putExtra("email", regEmail);
                    i.putExtra("id", regId);
                    i.putExtra("isSMSOrder", regIsSMSOrder);
                    i.putExtra("isSMSComments", regIsSMSComments);
                    i.putExtra("isNotifiOrders", regIsNotifiOrders);
                    i.putExtra("isNotifiComments", regIsNotifiComments);
                    i.putExtra("code", code);
                    startActivity(i);
                    finish();
                }else{
                    Toast.makeText(getApplicationContext(), "تم تسجيل الدخول", Toast.LENGTH_LONG).show();
                    Toast.makeText(getApplicationContext(), "أهلاً بك: " + regName, Toast.LENGTH_LONG).show();
                    KeyValueShared.setTel(getApplicationContext(), regTel);
                    KeyValueShared.setName(getApplicationContext(), regName);
                    KeyValueShared.setEmail(getApplicationContext(), regEmail);
                    KeyValueShared.setId(getApplicationContext(), regId);
                    KeyValueShared.setSettingsAppisSMSOrders(getApplicationContext(), regIsSMSOrder);
                    KeyValueShared.setSettingsAppisSMSComments(getApplicationContext(), regIsSMSComments);
                    KeyValueShared.setSettingsAppisNotifiOrders(getApplicationContext(), regIsNotifiOrders);
                    KeyValueShared.setSettingsAppisNotifiComments(getApplicationContext(), regIsNotifiComments);
                    KeyValueShared.setStatue(getApplicationContext(), "1");
                    finish();
                }
            }
        }
    }
}
