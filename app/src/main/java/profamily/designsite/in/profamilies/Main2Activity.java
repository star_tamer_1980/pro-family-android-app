package profamily.designsite.in.profamilies;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import libes.KeyValueShared;
import libes.SQLiteConnection;
import libes.globalData;
import libes.location;

public class Main2Activity extends AppCompatActivity {

    List<String> listcat = new ArrayList<String>();
    ArrayList<String> catListItem = new ArrayList<String>();
    ArrayAdapter<String> catAdapter;
    private Spinner spcat;
    private ProgressDialog pDialog;
    String ReqSuccess, address;
    private Button goToMap, btnSearch;
    String lat = "", lon = "";
    SQLiteConnection db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        db = new SQLiteConnection(getApplicationContext());
        db.deleteAllRecord();
        KeyValueShared.setWordSearch(this, "");
        location getLocation = new location(this);
        lat = getLocation.Langitude;
        lon = getLocation.Longitude;
        KeyValueShared.setMapLat(getApplicationContext(), lat);
        KeyValueShared.setMapLon(getApplicationContext(), lon);
        KeyValueShared.setMarket_mainCat_tab(Main2Activity.this, "0");
        String address = getResources().getString(R.string.myLocationDefault);
        Log.e("dd", "lat is: " + lat + "lon is: " + lon);
        if (!lat.isEmpty() && !lon.isEmpty()) {
            try {
                address = location.getCompleteAddress(getApplicationContext(), lat.toString(), lon.toString());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        goToMap = (Button)findViewById(R.id.goToMap);
        goToMap.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        goToMap.setText(address);
        if(lat.toString().equals("0") && lon.toString().equals("0")) {
            Intent i = new Intent(Main2Activity.this, setMyLocation.class);
            startActivity(i);
        }
        goToMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            Intent i = new Intent(Main2Activity.this, MapsActivity.class);
            startActivity(i);

            }
        });
        spcat = (Spinner)findViewById(R.id.sp_cat);
        spcat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(spcat.getSelectedItemPosition() == 0){
                    KeyValueShared.setMarket_mainCat_tab(Main2Activity.this, "0");
                }else if(spcat.getSelectedItemPosition() == 1){
                    KeyValueShared.setMarket_mainCat_tab(Main2Activity.this, "1");
                }else if(spcat.getSelectedItemPosition() == 2){
                    KeyValueShared.setMarket_mainCat_tab(Main2Activity.this, "2");
                }else if(spcat.getSelectedItemPosition() == 3){
                    KeyValueShared.setMarket_mainCat_tab(Main2Activity.this, "3");
                }else if(spcat.getSelectedItemPosition() == 4){

                }else if(spcat.getSelectedItemPosition() == 5){
                    KeyValueShared.setMarket_mainCat_tab(Main2Activity.this, "6");
                }else if(spcat.getSelectedItemPosition() == 6){
                    KeyValueShared.setMarket_mainCat_tab(Main2Activity.this, "7");
                }else if(spcat.getSelectedItemPosition() == 7){

                }else if(spcat.getSelectedItemPosition() == 8){
                    KeyValueShared.setMarket_mainCat_tab(Main2Activity.this, "9");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        btnSearch = (Button)findViewById(R.id.btn_search);
        btnSearch.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(Main2Activity.this, String.valueOf(spcat.getSelectedItemPosition()), Toast.LENGTH_SHORT).show();
                if(String.valueOf(spcat.getSelectedItemPosition()).equals("4")) {
                    Intent i = new Intent(Main2Activity.this, MainHaraj.class);
                    startActivity(i);
                }else if(String.valueOf(spcat.getSelectedItemPosition()).equals("7")) {
                    Intent i = new Intent(Main2Activity.this, serviceMain.class);
                    startActivity(i);

                }else {
                    Intent i = new Intent(Main2Activity.this, marketMain.class);
                    startActivity(i);

                }
            }
        });
        startAct();
        String[] catNameArr = getApplicationContext().getResources().getStringArray(R.array.catName_array);
        String[] catIdArr = getApplicationContext().getResources().getStringArray(R.array.catId_array);
        for(int i = 0; i < catNameArr.length; i++){
            Log.e("cat name", catNameArr[i].toString());
            listcat.add(catNameArr[i].toString());
        }
        catListItem.clear();
        catListItem.addAll(listcat);
        catAdapter.notifyDataSetChanged();


        getAppDataSettings();
        /*

        String[] catNameArr = getApplicationContext().getResources().getStringArray(R.array.catName_array);
        String[] catIdArr = getApplicationContext().getResources().getStringArray(R.array.catId_array);
        for(int i = 0; i < catNameArr.length; i++){
            Log.e("cat name", catNameArr[i].toString());
        }
        String data[] = new String[3];
        data[0] = globalData.URLhost() + "getCategory.php";
        data[1] = "0";
        ProgressTask bt = new ProgressTask();
        bt.execute(data);
        pDialog = new ProgressDialog(Main2Activity.this);
        pDialog.setMessage("جاري تحميل البيانات");
        pDialog.setTitle("برو فاميليز");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
        pDialog.show();
        */
    }
    @Override
    protected void onRestart() {
        super.onRestart();
        getAppDataSettings();
    }

    protected void getAppDataSettings(){
        KeyValueShared.setMarket_mainCat_tab(Main2Activity.this, "0");
        KeyValueShared.setWordSearch(this, "");
        if(KeyValueShared.getMapLat(getApplicationContext()) != "0" && KeyValueShared.getMapLon(getApplicationContext()) != "0") {
            address = getResources().getString(R.string.myLocationDefault);
            if (!lat.isEmpty() && !lon.isEmpty()) {
                try {
                    address = location.getCompleteAddress(getApplicationContext(), KeyValueShared.getMapLat(getApplicationContext()), KeyValueShared.getMapLon(getApplicationContext()));
                    setCityData();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                goToMap.setText(address);
            }
        }
    }
    protected void setCityData(){
        String data[] = new String[4];
        data[0] = globalData.URLhost() + "cityGetData.php";
        data[1] = KeyValueShared.getMapLat(getApplicationContext());
        data[2] = KeyValueShared.getMapLon(getApplicationContext());
        data[3] = KeyValueShared.getId(this);

        getCitySetting bts = new getCitySetting();
        bts.execute(data);
    }
    private void startAct() {


        catAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, catListItem);
        catAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spcat.setAdapter(catAdapter);


    }


    public class ProgressTask extends AsyncTask<String, Void, Boolean> {


        @SuppressWarnings("deprecation")
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
                nameValuePairs.add(new BasicNameValuePair("id", params[1]));
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));

                HttpResponse response = client.execute(post);

                int status = response.getStatusLine().getStatusCode();
                if(status == 200){
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    JSONObject jObj = new JSONObject(data.substring(data.indexOf("{"), data.lastIndexOf("}") + 1));
                    ReqSuccess = jObj.getString("success");
                    if(ReqSuccess.equals("1")){
                        listcat.add(getResources().getString(R.string.cat_all_category));
                        if(jObj.has("data")) {
                            JSONArray wee = jObj.getJSONArray("data");
                            for (int i = 0; i < wee.length(); i++) {
                                JSONObject c = wee.getJSONObject(i);
                                listcat.add(c.getString("countries"));
                            }
                        }else{
                            listcat.add("لا يوجد دول");
                        }


                    }else{
                        listcat.add("لا يوجد أقسام");
                    }


                    return true;
                }else{
                    Log.e("error client string", "here");
                }
            } catch (ClientProtocolException e) {
                Log.e("error client string", e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("error exp", e.getMessage());
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("error json", e.getMessage());
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            pDialog.dismiss();
            super.onPostExecute(result);
            if(result == false){
                Toast.makeText(getApplicationContext(), "حدث خطأ، نرجو المحاوله في وقت لاحق", Toast.LENGTH_LONG).show();
            }else{
                if(ReqSuccess.equals("1")){

                    catListItem.clear();
                    catListItem.addAll(listcat);
                    catAdapter.notifyDataSetChanged();

                }else{
                    Toast.makeText(getApplicationContext(), "لم يتم حفظ البيانات", Toast.LENGTH_LONG).show();
                }
            }
        }



    }
    public class getCitySetting extends AsyncTask<String, Void, Boolean> {


        @SuppressWarnings("deprecation")
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(3);
                nameValuePairs.add(new BasicNameValuePair("late", params[1]));
                nameValuePairs.add(new BasicNameValuePair("longe", params[2]));
                nameValuePairs.add(new BasicNameValuePair("idUser", params[3]));
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));

                HttpResponse response = client.execute(post);

                int status = response.getStatusLine().getStatusCode();
                if(status == 200){
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    JSONObject jObj = new JSONObject(data.substring(data.indexOf("{"), data.lastIndexOf("}") + 1));
                    ReqSuccess = jObj.getString("success");
                    Log.e("json is: ", data);
                    if(ReqSuccess.equals("1")){
                        KeyValueShared.setSettingsAppNoNoti(getApplicationContext(), jObj.getString("notiNo"));
                        KeyValueShared.setSettingsAppCityId(getApplicationContext(), jObj.getString("id"));
                        KeyValueShared.setSettingsAppCityName(getApplicationContext(), jObj.getString("title"));
                        KeyValueShared.setSettingsAppCountryName(getApplicationContext(), jObj.getString("catName"));
                        KeyValueShared.setSettingsAppCountryId(getApplicationContext(), jObj.getString("catId"));
                        if (KeyValueShared.getSettingsAppMyCityName(getApplicationContext()).equals("")){
                            KeyValueShared.setSettingsAppMyCityId(getApplicationContext(),jObj.getString("id"));
                            KeyValueShared.setSettingsAppMyCityName(getApplicationContext(), jObj.getString("title"));
                        }
                    }else{
                        Toast.makeText(Main2Activity.this, "لم نستطع الحصول علي معلومات المدينه الخاصه بك", Toast.LENGTH_SHORT).show();
                    }


                    return true;
                }else{
                    Log.e("error client string", "here");
                }
            } catch (ClientProtocolException e) {
                Log.e("error client string", e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("error exp", e.getMessage());
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("error json", e.getMessage());
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if(result == false){
                Toast.makeText(getApplicationContext(), "حدث خطأ، نرجو المحاوله في وقت لاحق", Toast.LENGTH_LONG).show();
            }else{
                if(ReqSuccess.equals("1")){

                    Toast.makeText(Main2Activity.this, "city id is: " + KeyValueShared.getSettingsAppMyCityId(getApplicationContext()), Toast.LENGTH_SHORT).show();
                    Toast.makeText(Main2Activity.this, "city name is: " + KeyValueShared.getSettingsAppMyCityName(getApplicationContext()), Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(getApplicationContext(), "لم يتم حفظ البيانات", Toast.LENGTH_LONG).show();
                }
            }
        }



    }


}
