package profamily.designsite.in.profamilies;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import libes.KeyValueShared;
import libes.globalData;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uploadFiles.ApiConfig;
import uploadFiles.AppConfig;
import uploadFiles.ServerResponse;

public class addVideoProduct extends AppCompatActivity {

    static final int REQUEST_VIDEO_CAPTURE = 1;
    private Button btnCamera;
    private ProgressDialog progressDialog;
    private String mediaPath;
    private Uri VideoUri;
    private String userId;
    private TextView tvResult;
    private String isMainData, isVideoData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_video_product);
        userId = KeyValueShared.getIdForMember(getApplicationContext());
        Toast.makeText(getApplicationContext(), userId, Toast.LENGTH_LONG).show();
        tvResult = (TextView)findViewById(R.id.tvResult);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Uploading...");
        isMainData = "0";
        isVideoData = "1";
        btnCamera = (Button)findViewById(R.id.btnPhotoCamera);
        btnCamera.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        btnCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                if(takeVideoIntent.resolveActivity(getPackageManager()) != null){
                    startActivityForResult(takeVideoIntent, REQUEST_VIDEO_CAPTURE);
                }
            }
        });
    }
    public String getPath(Uri uri) {
        String[] projection = { MediaStore.Video.Media.DATA };
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        if (cursor != null) {
            // HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
            // THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else
            return null;
    }
    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_VIDEO_CAPTURE && resultCode == RESULT_OK){
            if (resultCode == Activity.RESULT_OK) {
                try {
                    VideoUri = data.getData();
                    mediaPath = getRealPathFromURI(VideoUri);
                    Toast.makeText(getApplicationContext(), VideoUri.toString(), Toast.LENGTH_SHORT).show();

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        }
        uploadFile();
    }

    private void uploadFile() {
        progressDialog.show();
        Map<String, RequestBody> map = new HashMap<>();
        File file = new File(String.valueOf(mediaPath));

        RequestBody idUser = RequestBody.create(MediaType.parse("text/plain"), userId);
        RequestBody isMain = RequestBody.create(MediaType.parse("text/plain"), isMainData);
        RequestBody isVideo = RequestBody.create(MediaType.parse("text/plain"), isVideoData);
        RequestBody requestBody = RequestBody.create(MediaType.parse("*/*"), file);
        map.put("file\"; filename=\"" + file.getName() + "\"", requestBody);
        ApiConfig getResponse = AppConfig.getRetrofit().create(ApiConfig.class);
        Call<ServerResponse> call = getResponse.upload("token", map, idUser, isMain, isVideo);

        call.enqueue(new Callback<ServerResponse>() {
            @Override
            public void onResponse(Call<ServerResponse> call, Response<ServerResponse> response) {
                progressDialog.dismiss();
                ServerResponse serverResponse = response.body();
                if(serverResponse != null){
                    if(serverResponse.getSuccess()){
                        Toast.makeText(getApplicationContext(), serverResponse.getMessage(), Toast.LENGTH_LONG).show();
                    }else{
                        Toast.makeText(getApplicationContext(), serverResponse.getMessage(), Toast.LENGTH_LONG).show();
                    }

                }else{
                    Log.e("Response", serverResponse.toString());
                }
                tvResult.setText(serverResponse.getMessage());
                finish();
            }

            @Override
            public void onFailure(Call<ServerResponse> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage().toString(), Toast.LENGTH_LONG).show();
                tvResult.setText(t.getMessage());
                progressDialog.dismiss();
            }
        });
    }
}
