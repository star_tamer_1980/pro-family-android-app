package profamily.designsite.in.profamilies;

import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import libes.globalData;
import ordersData.Orders;
import ordersData.OrdersAdapter;
import ordersData.OrdersProductAdapter;
import productData.Product;
import productData.ProductAdapter;

public class OrderProducts extends AppCompatActivity {

    private String idProducct;
    private RecyclerView mRecyclerView;
    private OrdersProductAdapter adapter;
    private List<Orders> data_list;
    private String dataTest;
    private ProgressDialog pDialog;
    private ProgressBar pBar;
    List<String> listProduct = new ArrayList<String>();
    String ReqSuccess;


    private int m_previouse_Total_Count = 0;
    private boolean loading = true;
    private int visibleThreshold = 5;
    int firstVisibleItem, visibleItemCount, totalItemCount;
    private TextView txtNotFound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_products);
        idProducct = getIntent().getStringExtra("idOrder");
        Toast.makeText(getApplicationContext(), idProducct, Toast.LENGTH_SHORT).show();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        txtNotFound = (TextView)findViewById(R.id.txtNotFound);
        txtNotFound.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        mRecyclerView = (RecyclerView)findViewById(R.id.recycleView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        pBar = (ProgressBar)findViewById(R.id.progressBar1);
        data_list = new ArrayList<>();

        String data[] = new String[2];
        data[0] = globalData.URLhost() + "ordersProduct.php";
        data[1] = idProducct;
        loadData bts = new loadData();
        bts.execute(data);
    }
    public class loadData extends AsyncTask<String, Void, Boolean> {

        private String title;
        private String content;
        private String img;
        @SuppressWarnings("deprecation")
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
                nameValuePairs.add(new BasicNameValuePair("idOrderData", params[1]));
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if(status == 200){
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    Log.e("data is: ", data);
                    JSONObject jObj = new JSONObject(data.substring(data.indexOf("{"), data.lastIndexOf("}") + 1));
                    ReqSuccess = jObj.getString("success");
                    if(ReqSuccess.equals("1")){
                        dataTest = data;
                        if(jObj.has("data")) {
                            JSONArray wee = jObj.getJSONArray("data");
                            for (int i = 0; i < wee.length(); i++) {
                                JSONObject c = wee.getJSONObject(i);
                                Orders dataOrder = new Orders();
                                dataOrder.setId(c.getString("id"));
                                dataOrder.setMarketName(c.getString("productName"));
                                dataOrder.setPrice(c.getString("quantity"));
                                dataOrder.setImg(c.getString("img"));
                                data_list.add(dataOrder);
                            }
                        }

                    }


                    return true;
                }else{
                    Log.e("error client string", "here");
                }
            } catch (ClientProtocolException e) {
                Log.e("error client string", e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("error exp", e.getMessage());
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("error json", e.getMessage());
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if(result == false){
                Toast.makeText(getApplicationContext(), "حدث خطأ، نرجو المحاوله في وقت لاحق", Toast.LENGTH_LONG).show();
            }else{
                if(ReqSuccess.equals("1")){
                    txtNotFound.setVisibility(View.INVISIBLE);
                        adapter = new OrdersProductAdapter(getApplicationContext(), data_list);
                        mRecyclerView.setAdapter(adapter);
                        pBar.setVisibility(View.INVISIBLE);
                }else{
                    pBar.setVisibility(View.INVISIBLE);
                }
            }
            txtNotFound.setText(getResources().getString(R.string.notFoundData));
        }
    }
}
