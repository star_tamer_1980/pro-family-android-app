package profamily.designsite.in.profamilies;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import org.w3c.dom.Text;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import libes.KeyValueShared;
import libes.globalData;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uploadFiles.ApiConfig;
import uploadFiles.AppConfig;
import uploadFiles.ServerResponse;



public class AddPhotoProduct extends AppCompatActivity {

    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int REQUEST_GALLERY_CAPTURE = 2;
    private Button btnCamera, btnGallery, btnUpload;
    private ImageView aImageView;
    ProgressDialog progressDialog;
    private String mediaPath;
    Uri imageUri;
    TextView tvResult;
    String isMainData, isVideoData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_photo_product);
        tvResult = (TextView)findViewById(R.id.tvResult);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Uploading...");
        isMainData = getIntent().getStringExtra("isMain");
        isVideoData = getIntent().getStringExtra("isVideo");
        aImageView = (ImageView)findViewById(R.id.imageView1);
        btnUpload = (Button)findViewById(R.id.btnUpload);
        btnUpload.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        btnGallery = (Button)findViewById(R.id.btnPhotoGallery);
        btnGallery.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        btnCamera = (Button)findViewById(R.id.btnPhotoCamera);
        btnCamera.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        btnCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContentValues values = new ContentValues();
                values.put(MediaStore.Images.Media.TITLE, "New Picture");
                values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
                imageUri = getContentResolver().insert(
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
            }
        });
        btnGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Create intent to Open Image applications like Gallery, Google Photos
                Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
// Start the Intent
                startActivityForResult(galleryIntent, REQUEST_GALLERY_CAPTURE);
            }
        });
        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Log.e("startUpload", finalFile.toString());
                uploadFile();
                //uploadFileToServer(finalFile.toString(), globalData.URLhost() + "imgAdd.php");
            }
        });
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK){
            if (resultCode == Activity.RESULT_OK) {
                try {
                    String imageurl = getRealPathFromURI(imageUri);
                    mediaPath = imageurl;
                    aImageView.setImageBitmap(BitmapFactory.decodeFile(mediaPath));
                    Toast.makeText(getApplicationContext(), imageurl.toString(), Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        }
        if(requestCode == REQUEST_GALLERY_CAPTURE && resultCode == RESULT_OK){
            try {
                // When an Image is picked
                if (requestCode == REQUEST_GALLERY_CAPTURE && resultCode == RESULT_OK
                        && null != data) {
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    assert cursor != null;
                    cursor.moveToFirst();
                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    mediaPath = cursor.getString(columnIndex);

                    aImageView.setImageBitmap(BitmapFactory.decodeFile(mediaPath));
                    cursor.close();

                } else {
                    Toast.makeText(this, "You haven't picked Image",
                            Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                        .show();
            }
        }
        uploadFile();
    }

    private void uploadFile() {
        progressDialog.show();
        Map<String, RequestBody> map = new HashMap<>();
        File file = new File(String.valueOf(mediaPath));

        RequestBody idUser = RequestBody.create(MediaType.parse("text/plain"), KeyValueShared.getIdForMember(this));
        RequestBody isMain = RequestBody.create(MediaType.parse("text/plain"), isMainData);
        RequestBody isVideo = RequestBody.create(MediaType.parse("text/plain"), isVideoData);
        RequestBody requestBody = RequestBody.create(MediaType.parse("*/*"), file);
        map.put("file\"; filename=\"" + file.getName() + "\"", requestBody);
        ApiConfig getResponse = AppConfig.getRetrofit().create(ApiConfig.class);
        Call<ServerResponse> call = getResponse.upload("token", map, idUser, isMain, isVideo);

        call.enqueue(new Callback<ServerResponse>() {
            @Override
            public void onResponse(Call<ServerResponse> call, Response<ServerResponse> response) {
                progressDialog.dismiss();
                ServerResponse serverResponse = response.body();
                if(serverResponse != null){
                    if(serverResponse.getSuccess()){
                        Toast.makeText(getApplicationContext(), serverResponse.getMessage(), Toast.LENGTH_LONG).show();
                    }else{
                        Toast.makeText(getApplicationContext(), serverResponse.getMessage(), Toast.LENGTH_LONG).show();
                    }

                }else{
                    Log.e("Response", serverResponse.toString());
                }
                tvResult.setText(serverResponse.getMessage());
                finish();
            }

            @Override
            public void onFailure(Call<ServerResponse> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage().toString(), Toast.LENGTH_LONG).show();
                tvResult.setText(t.getMessage());
                progressDialog.dismiss();
            }
        });
    }
}
/*

<?php
header ( 'Content-Type: application/json; charset="utf-8"' );
include '../../settings.php';
ini_set('upload_max_filesize', '50M');
ini_set('post_max_size', '50M');
ini_set('memory_limit', '128M');
ini_set('max_input_vars', '1000');
$langPath = language::lang_path();
include ($langPath);
$target_dir = APP_PATH . "tempUpload/";
$target_file_name = $target_dir . basename($_FILES["file"]["name"]);
$response = array();
if(isset($_FILES["file"])){
    $pImage = "";
        $upload = new uploading();
        $upload->filesControl = $_FILES['file'];
        $upload->isReturnArray = false;
        $upload->filesSeparate = "";
        $upload->fileIsThumb = true;
        $pImage = $upload->doUploadOneFile();
    if($pImage!=""){
        $files_temp = new files_temp();
        $files_temp->imgName = $pImage;
        $files_temp->idUser = $_POST['idUser'];
        $files_temp->isMain = $_POST['isMain'];
        $files_temp->isVideo = "0";
        $success = true;
        $message = "Successfully Uploaded" . $pImage . $_POST['idUser'];
    }else{
        $success = false;
        $message = "Error while uplaoding" . $_POST['idUser'];
    }
}else{
    $success = false;
    $message = "Required Field Missing" . $_POST['idUser'];
}
$response['success'] = $success;
$response['message'] = $message;
echo json_encode($response);
 */