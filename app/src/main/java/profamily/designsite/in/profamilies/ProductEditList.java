package profamily.designsite.in.profamilies;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import libes.KeyValueShared;
import libes.SQLiteConnection;
import libes.globalData;
import productData.Product;
import productData.ProductMarketAdapter;

public class ProductEditList extends AppCompatActivity {
    String idMarket, idMainCat;
    private RecyclerView mRecyclerView;
    private ProductMarketAdapter adapter;
    private List<Product> data_list;
    private String dataTest;
    private ProgressDialog pDialog;
    private ProgressBar pBar;
    List<String> listProduct = new ArrayList<String>();
    String ReqSuccess;
    Button btn_add;

    private int m_previouse_Total_Count = 0;
    private boolean loading = true;
    private int visibleThreshold = 5;
    int firstVisibleItem, visibleItemCount, totalItemCount;
    private TextView txtNotFound;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_edit_list);
        idMarket = getIntent().getStringExtra("idMarket");
        idMainCat = getIntent().getStringExtra("idMainCat");
        txtNotFound = (TextView)findViewById(R.id.txtNotFound);
        txtNotFound.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        mRecyclerView = (RecyclerView)findViewById(R.id.recycleView1);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        pBar = (ProgressBar)findViewById(R.id.progressBar1);
        data_list = new ArrayList<>();
        btn_add = (Button)findViewById(R.id.btn_add);
        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ProductEditList.this, ProductAddData.class);
                i.putExtra("idMarket", idMarket);
                i.putExtra("idMainCat", idMainCat);
                startActivity(i);
            }
        });
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager mLayoutManager = (LinearLayoutManager) mRecyclerView
                        .getLayoutManager();
                mRecyclerView.setLayoutManager(mLayoutManager);
                visibleItemCount = mRecyclerView.getChildCount();
                totalItemCount = mLayoutManager.getItemCount();
                firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();
                if(totalItemCount == 0){
                    return ;
                }
                if(m_previouse_Total_Count==totalItemCount){
                    return;
                }
                boolean loadMore = firstVisibleItem + visibleItemCount >= totalItemCount;
                if(loadMore){
                    m_previouse_Total_Count = totalItemCount;
                    pBar.setVisibility(View.VISIBLE);
                    getLoadData(totalItemCount);
                }

            }
        });
        String data[] = new String[3];
        data[0] = globalData.URLhost() + "productListForMarket.php";
        data[1] = "0";
        data[2] = idMarket;
        loadData bts = new loadData();
        bts.execute(data);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        finish();
        startActivity(getIntent());
    }

    public void getLoadData(Integer limit) {

        pBar.setVisibility(View.VISIBLE);
        String data[] = new String[3];
        data[0] = globalData.URLhost() + "productListForMarket.php";
        data[1] = String.valueOf(limit);
        data[2] = idMarket;
        loadData bts = new loadData();
        bts.execute(data);
    }
    public class loadData extends AsyncTask<String, Void, Boolean> {

        private String title;
        private String content;
        private String img;
        @SuppressWarnings("deprecation")
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                nameValuePairs.add(new BasicNameValuePair("limit", params[1]));
                nameValuePairs.add(new BasicNameValuePair("idMarket", params[2]));
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if(status == 200){
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    Log.e("result", data);
                    JSONObject jObj = new JSONObject(data.substring(data.indexOf("{"), data.lastIndexOf("}") + 1));
                    ReqSuccess = jObj.getString("success");
                    if(ReqSuccess.equals("1")){
                        dataTest = data;
                        if(jObj.has("data")) {
                            JSONArray wee = jObj.getJSONArray("data");
                            for (int i = 0; i < wee.length(); i++) {
                                JSONObject c = wee.getJSONObject(i);
                                Product dataProduct = new Product();
                                dataProduct.setId(c.getString("id"));
                                dataProduct.setName(c.getString("name"));
                                dataProduct.setDescription(c.getString("description"));
                                dataProduct.setPrice(c.getString("price"));
                                dataProduct.setImg(c.getString("img"));
                                dataProduct.setRating(c.getString("rating"));
                                dataProduct.setIdMainCat(c.getString("idMainCat"));
                                dataProduct.setIsActive(c.getString("isActive"));
                                dataProduct.setIsAllow(c.getString("isAllow"));
                                dataProduct.setIdMarket(params[2]);
                                dataProduct.setActionType("details");
                                dataProduct.setIdUser(c.getString("idUser"));
                                data_list.add(dataProduct);
                            }
                        }

                    }


                    return true;
                }else{
                    Log.e("error client string", "here");
                }
            } catch (ClientProtocolException e) {
                Log.e("error client string", e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("error exp", e.getMessage());
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("error json", e.getMessage());
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if(result == false){
                Toast.makeText(getApplicationContext(), "حدث خطأ، نرجو المحاوله في وقت لاحق", Toast.LENGTH_LONG).show();
            }else{
                if(ReqSuccess.equals("1")){
                    txtNotFound.setVisibility(View.INVISIBLE);
                    if(m_previouse_Total_Count>0) {
                        adapter.notifyDataSetChanged();
                        pBar.setVisibility(View.INVISIBLE);
                    }else{
                        adapter = new ProductMarketAdapter(getApplicationContext(), data_list);
                        mRecyclerView.setAdapter(adapter);
                        pBar.setVisibility(View.INVISIBLE);
                    }
                }else{
                    pBar.setVisibility(View.INVISIBLE);
                }
            }
        }
    }
}
