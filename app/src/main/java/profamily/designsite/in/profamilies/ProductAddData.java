package profamily.designsite.in.profamilies;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import categoryData.catAdapter;
import categoryData.category;
import libes.KeyValueShared;
import libes.globalData;

public class ProductAddData extends AppCompatActivity {

    String id, idMarket, idMainCat, ReqSuccess, ReqMessage, dataTest;
    TextView tvProductName, tvProductPrice, tvProductDesc, tvCat;
    EditText etProductName, etProductPrice, etProductDesc;
    Spinner spCat;
    Button btnSave;
    ProgressDialog pDialog;
    String fileApi = "marketCatData.php";
    List<String> listCategory = new ArrayList<String>();
    List<String> listCatId = new ArrayList<String>();
    ArrayList<String> TypeCategoryArray = new ArrayList<String>();
    ArrayAdapter<String> adapterCategory;
    private List<category> data_list1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_add_data);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        idMarket = getIntent().getStringExtra("idMarket");
        idMainCat = getIntent().getStringExtra("idMainCat");
        tvProductName = (TextView)findViewById(R.id.product_add_name_text);
        tvProductName.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        tvProductPrice = (TextView)findViewById(R.id.product_add_price_text);
        tvProductPrice.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        tvProductDesc = (TextView)findViewById(R.id.product_add_desc_text);
        tvProductDesc.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        etProductName = (EditText) findViewById(R.id.product_add_name);
        etProductName.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        etProductName.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == 66){
                    etProductPrice.requestFocus();
                }
                return false;
            }
        });
        etProductPrice = (EditText)findViewById(R.id.product_add_price);
        etProductPrice.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        etProductPrice.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == 66){
                    etProductDesc.requestFocus();
                }
                return false;
            }
        });
        etProductDesc = (EditText)findViewById(R.id.product_add_desc);
        etProductDesc.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        etProductDesc.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == 66){
                    spCat.requestFocus();
                }
                return false;
            }
        });
        tvCat = (TextView)findViewById(R.id.product_add_cat_text);
        tvCat.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        data_list1 = new ArrayList<>();
        listCategory.add(getResources().getString(R.string.haraj_add_must_choose_cat));
        listCatId.add("0");
        spCat = (Spinner)findViewById(R.id.product_add_cat);
        adapterCategory = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, TypeCategoryArray);
        adapterCategory.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCat.setAdapter(adapterCategory);
        spCat.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == 66){
                    btnSave.requestFocus();
                }
                return false;
            }
        });
        String data[] = new String[4];
        data[0] = globalData.URLhost() + fileApi;
        data[1] = idMarket;
        data[2] = "0";
        data[3] = "0";
        loadData1 bts = new loadData1();
        bts.execute(data);
        btnSave = (Button)findViewById(R.id.btn_save);
        btnSave.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                marketProcess();
            }
        });
        btnSave.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == 66){
                    marketProcess();
                }
                return false;
            }
        });
    }
    private void marketProcess(){
        String productName = etProductName.getText().toString();
        String productPrice = etProductPrice.getText().toString();
        String productDesc = etProductDesc.getText().toString();
        String idSubCat = String.valueOf(spCat.getSelectedItemPosition());

        if(TextUtils.isEmpty(productName)){
            etProductName.setError(getString(R.string.mustWriteProductName));
        }else if (TextUtils.isEmpty(productPrice)){
            etProductPrice.setError(getString(R.string.mustWriteProductPrice));
        }else if (TextUtils.isEmpty(productDesc)){
            etProductDesc.setError(getString(R.string.mustWriteProductDesc));
        }else if (idSubCat.equals("0")){
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.haraj_add_must_choose_cat), Toast.LENGTH_SHORT).show();
        }else{

            String data[] = new String[8];
            data[0] = globalData.URLhost() + "productAddData.php";
            data[1] = KeyValueShared.getId(getApplicationContext());
            data[2] = idMarket;
            data[3] = productName;
            data[4] = productPrice;
            data[5] = productDesc;
            data[6] = idMainCat;
            data[7] = listCatId.get(Integer.valueOf(idSubCat)).toString();

            new productAdd().execute(data);

            pDialog = new ProgressDialog(ProductAddData.this);
            pDialog.setMessage("جاري اضافة المنتج");
            pDialog.setTitle("برجاء الانتظار ثواني");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();

        }
    }
    public class productAdd extends AsyncTask<String, Void, Boolean> {
        JSONObject jObj;
        @SuppressWarnings("deprecation")
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(7);
                nameValuePairs.add(new BasicNameValuePair("idUser", params[1]));
                nameValuePairs.add(new BasicNameValuePair("idMarket", params[2]));
                nameValuePairs.add(new BasicNameValuePair("productName", params[3]));
                nameValuePairs.add(new BasicNameValuePair("productPrice", params[4]));
                nameValuePairs.add(new BasicNameValuePair("productDesc", params[5]));
                nameValuePairs.add(new BasicNameValuePair("idMainCat", params[6]));
                nameValuePairs.add(new BasicNameValuePair("idSubCat", params[7]));
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if(status == 200){
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    Log.e("result is: ", data);
                    JSONObject jObj = new JSONObject(data.substring(data.indexOf("{"), data.lastIndexOf("}") + 1));
                    ReqSuccess = jObj.getString("success");
                    ReqMessage = jObj.getString("message");
                    id = jObj.getString("id");

                    return true;
                }else{
                    Log.e("error client string", "here");
                }
            } catch (ClientProtocolException e) {
                Log.e("error client string", e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("error exp", e.getMessage());
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("error json", e.getMessage());
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            pDialog.dismiss();
            super.onPostExecute(result);
            if(result == false){
                Toast.makeText(getApplicationContext(), "لا يوجد بيانات " + ReqMessage, Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(getApplicationContext(), ReqMessage, Toast.LENGTH_LONG).show();
                Toast.makeText(getApplicationContext(), id, Toast.LENGTH_LONG).show();
                Intent i = new Intent(ProductAddData.this, productEditSwitch.class);
                i.putExtra("idMarket", idMarket);
                i.putExtra("idProduct", id);
                i.putExtra("isAllow", "0");
                startActivity(i);

                finish();
            }
        }
    }
    public class loadData1 extends AsyncTask<String, Void, Boolean> {

        @SuppressWarnings("deprecation")
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(3);
                nameValuePairs.add(new BasicNameValuePair("idMarket", params[1]));
                nameValuePairs.add(new BasicNameValuePair("idMainCat", params[2]));
                nameValuePairs.add(new BasicNameValuePair("idSubCat", params[3]));
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if(status == 200){
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    Log.e("error", data);
                    JSONObject jObj = new JSONObject(data.substring(data.indexOf("{"), data.lastIndexOf("}") + 1));
                    ReqSuccess = jObj.getString("success");
                    if(ReqSuccess.equals("1")){
                        dataTest = data;
                        if(jObj.has("cat")) {

                            JSONArray wee = jObj.getJSONArray("data");
                            Log.e("cat is: ", wee.toString());
                            for (int i = 0; i < wee.length(); i++) {
                                JSONObject c = wee.getJSONObject(i);
                                category dataCategory = new category();
                                listCategory.add(c.getString("title"));
                                listCatId.add(c.getString("id"));
                                data_list1.add(dataCategory);
                            }
                        }

                    }


                    return true;
                }else{
                    Log.e("error client string", "here");
                }
            } catch (ClientProtocolException e) {
                Log.e("error client string", e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("error exp", e.getMessage());
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("error json", e.getMessage());
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if(result == false){
                Toast.makeText(getApplicationContext(), "حدث خطأ، نرجو المحاوله في وقت لاحق", Toast.LENGTH_LONG).show();
            }else{
                if(ReqSuccess.equals("1")){
                    Toast.makeText(getApplicationContext(), "data set", Toast.LENGTH_SHORT).show();
                    TypeCategoryArray.clear();
                    TypeCategoryArray.addAll(listCategory);
                    adapterCategory.notifyDataSetChanged();

                }
            }
        }
    }
}
