package profamily.designsite.in.profamilies;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import libes.KeyValueShared;
import libes.globalData;
import locationMap.LocationMap;
import locationMap.LocationMapAdapter;
import subCatData.subCat;
import subCatData.subCatAdapter;

public class LocationList extends AppCompatActivity {

    private RecyclerView mRecyclerView1;
    private Button addNewLocation;
    private GridLayoutManager gridLayoutManager;
    private LocationMapAdapter adapter1;
    private List<LocationMap> data_list1;
    private String dataTest;
    private ProgressDialog pDialog;
    private ProgressBar pBar1;
    List<String> listNoti = new ArrayList<String>();
    String ReqSuccess;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_list);
        mRecyclerView1 = (RecyclerView)findViewById(R.id.recycleView1);
        mRecyclerView1.setLayoutManager(new LinearLayoutManager(this));
        pBar1 = (ProgressBar)findViewById(R.id.progressBar1);
        data_list1 = new ArrayList<>();
        addNewLocation = (Button)findViewById(R.id.btnAddLocation);
        addNewLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LocationList.this, MapsActivity.class);
                startActivity(i);
            }
        });
        String data[] = new String[2];
        data[0] = globalData.URLhost() + "locationMapUserId.php";
        data[1] = KeyValueShared.getId(this);
        loadData1 bts = new loadData1();
        bts.execute(data);
        Toast.makeText(getApplicationContext(), KeyValueShared.getId(getApplicationContext()), Toast.LENGTH_SHORT).show();
    }
    @Override
    protected void onRestart() {
        super.onRestart();
        finish();
        startActivity(getIntent());
    }
    public class loadData1 extends AsyncTask<String, Void, Boolean> {

        private String title;
        private String content;
        @SuppressWarnings("deprecation")
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
                nameValuePairs.add(new BasicNameValuePair("userId", params[1]));
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if(status == 200){
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    JSONObject jObj = new JSONObject(data.substring(data.indexOf("{"), data.lastIndexOf("}") + 1));
                    ReqSuccess = jObj.getString("success");
                    if(ReqSuccess.equals("1")){
                        dataTest = data;
                        if(jObj.has("data")) {
                            JSONArray wee = jObj.getJSONArray("data");
                            for (int i = 0; i < wee.length(); i++) {
                                JSONObject c = wee.getJSONObject(i);
                                LocationMap dataLocationMap = new LocationMap();
                                dataLocationMap.setTitle(c.getString("title"));
                                dataLocationMap.setMapLat(c.getString("mapLat"));
                                dataLocationMap.setMapLong(c.getString("mapLon"));
                                data_list1.add(dataLocationMap);
                            }
                        }

                    }


                    return true;
                }else{
                    Log.e("error client string", "here");
                }
            } catch (ClientProtocolException e) {
                Log.e("error client string", e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("error exp", e.getMessage());
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("error json", e.getMessage());
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if(result == false){
                Toast.makeText(getApplicationContext(), "حدث خطأ، نرجو المحاوله في وقت لاحق", Toast.LENGTH_LONG).show();
            }else{
                if(ReqSuccess.equals("1")){
                    adapter1 = new LocationMapAdapter(getApplicationContext(), data_list1);
                    mRecyclerView1.setAdapter(adapter1);
                    pBar1.setVisibility(View.INVISIBLE);
                }else{
                    pBar1.setVisibility(View.INVISIBLE);
                }
            }
        }
    }
}
