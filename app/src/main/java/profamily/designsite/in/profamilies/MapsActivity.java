package profamily.designsite.in.profamilies;

import android.content.DialogInterface;
import android.content.Intent;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.identity.intents.Address;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import libes.KeyValueShared;
import libes.alertsApp;
import libes.globalData;
import libes.location;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    String lat = "24.7255566", lon = "47.1020276", nameLocation, ReqSuccess;

    EditText etLocation;
    Button addLocation, btnLocationSearch, goSearch;
    ImageView locationList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        final SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        location getLocation = new location(this);
        if(getLocation.Langitude != "0" && getLocation.Longitude != "0") {
            lat = getLocation.Langitude;
            lon = getLocation.Longitude;
        }
        addLocation = (Button)findViewById(R.id.add_name_to_location);
        locationList = (ImageView)findViewById(R.id.location_list);
        goSearch = (Button)findViewById(R.id.go_search);
        etLocation = (EditText)findViewById(R.id.et_location);
        btnLocationSearch = (Button)findViewById(R.id.btn_location_search);
        locationList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MapsActivity.this, LocationList.class);
                startActivity(i);
                finish();
            }
        });
        goSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setLocations();

            }
        });
        btnLocationSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String address = etLocation.getText().toString();
                List<android.location.Address> addressList = null;
                if(address != null || !address.equals("")){
                    Geocoder geocoder = new Geocoder(getApplicationContext());
                    try {
                        addressList = geocoder.getFromLocationName(address, 1);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    android.location.Address address1 = addressList.get(0);
                    lat = String.valueOf(address1.getLatitude());
                    lon = String.valueOf(address1.getLongitude());
                    mMap.clear();
                    onMapReady(mMap);
                }else{
                    etLocation.setError(getResources().getString(R.string.mustWriteAddress));
                }
            }
        });
        addLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertLocationMethod();
            }
        });

    }
    private void alertLocationMethod(){
        if(!KeyValueShared.getId(getApplicationContext()).toString().equals("")){

            AlertDialog.Builder builder1 = new AlertDialog.Builder(MapsActivity.this);
            builder1.setMessage(getResources().getString(R.string.alertMessageAddLocation));
            builder1.setTitle(getResources().getString(R.string.alertTitleAddLocation));
            builder1.setCancelable(true);

            final EditText input = new EditText(MapsActivity.this);
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT);
            input.setLayoutParams(lp);
            builder1.setView(input);

            builder1.setNeutralButton(
                    getResources().getString(R.string.alertButtonAdd),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            if(!input.getText().toString().equals("")){
                                String dataCat[] = new String[5];
                                dataCat[0] = globalData.URLhost() + "mapLocationAdd.php";
                                dataCat[1] = input.getText().toString();
                                dataCat[2] = lat;
                                dataCat[3] = lon;
                                dataCat[4] = KeyValueShared.getId(getApplicationContext());
                                saveAdvertise saveProduct = new saveAdvertise();
                                saveProduct.execute(dataCat);
                                setLocations();
                                Toast.makeText(getApplicationContext(), "تم الاضافه", Toast.LENGTH_LONG).show();
                            }else{
                                Toast.makeText(getApplicationContext(), "يجب كتابة اسم للموقع", Toast.LENGTH_LONG).show();
                                alertLocationMethod();
                            }
                        }
                    });

            builder1.setCancelable(false).setPositiveButton(getResources().getString(R.string.alertButtonCancel), new DialogInterface.OnClickListener(){
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            AlertDialog alert11 = builder1.create();
            alert11.show();
        }else{
            AlertDialog.Builder builder1 = new AlertDialog.Builder(MapsActivity.this);
            builder1.setMessage(getResources().getString(R.string.alertMessageMustLogin));
            builder1.setTitle(getResources().getString(R.string.alertTitleMustLogin));
            builder1.setCancelable(true);

            builder1.setNeutralButton(
                    getResources().getString(R.string.alertButtonMustRegister),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Intent i = new Intent(MapsActivity.this, userRegister.class);
                            startActivity(i);
                            dialog.cancel();
                        }
                    });

            builder1.setNegativeButton(
                    getResources().getString(R.string.alertButtonMustLogin),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Intent i = new Intent(MapsActivity.this, userLogin.class);
                            startActivity(i);
                            dialog.cancel();
                        }
                    });
            builder1.setCancelable(false).setPositiveButton(getResources().getString(R.string.alertButtonCancel), new DialogInterface.OnClickListener(){
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            AlertDialog alert11 = builder1.create();
            alert11.show();
        }



/*

*/
    }
    public void setLocations(){
        KeyValueShared.setMapLat(getApplicationContext(), lat);
        KeyValueShared.setMapLon(getApplicationContext(), lon);

        Intent i = new Intent(MapsActivity.this, marketMain.class);
        startActivity(i);
        finish();
    }
    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(Float.parseFloat(lat), Float.parseFloat(lon));
        mMap.addMarker(new MarkerOptions().position(sydney).title("My location"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 9));
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                mMap.clear();
                mMap.addMarker(new MarkerOptions().position(latLng));
                lat = String.valueOf(latLng.latitude);
                lon = String.valueOf(latLng.longitude);
            }
        });
    }

    public class saveAdvertise extends AsyncTask<String, Void, Boolean> {

        @SuppressWarnings("deprecation")
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(4);
                nameValuePairs.add(new BasicNameValuePair("nameLocation", params[1]));
                nameValuePairs.add(new BasicNameValuePair("mapLat", params[2]));
                nameValuePairs.add(new BasicNameValuePair("mapLon", params[3]));
                nameValuePairs.add(new BasicNameValuePair("idUser", params[4]));
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));

                HttpResponse response = client.execute(post);

                int status = response.getStatusLine().getStatusCode();
                if(status == 200){
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    Log.e("url", params[0]);
                    Log.e("data", data.toString());
                    JSONObject jObj = new JSONObject(data.substring(data.indexOf("{"), data.lastIndexOf("}") + 1));
                    ReqSuccess = jObj.getString("success");
                    return true;
                }else{
                    Log.e("error client string", "here");
                }
            } catch (ClientProtocolException e) {
                Log.e("error client string", e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("error exp", e.getMessage());
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("error json", e.getMessage());
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if(result == false){
                Toast.makeText(getApplicationContext(), "حدث خطأ، نرجو المحاوله في وقت لاحق", Toast.LENGTH_LONG).show();
            }else{
                if(ReqSuccess.equals("1")){
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.haraj_add_finish), Toast.LENGTH_LONG).show();
                    finish();
                }else{
                    Toast.makeText(getApplicationContext(), "لم يتم حفظ البيانات", Toast.LENGTH_LONG).show();
                }
            }
        }

    }
}
