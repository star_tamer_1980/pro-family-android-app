package profamily.designsite.in.profamilies;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import libes.footerMarketAction;

public class orders_switch extends AppCompatActivity {

    Button ordersNew, ordersFinish, ordersCancel;
    LinearLayout lnHome, lnOrders, lnNoti, lnMore;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders_switch);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ordersNew = (Button)findViewById(R.id.orders_new);
        ordersCancel = (Button)findViewById(R.id.orders_cancel);
        ordersFinish = (Button)findViewById(R.id.orders_finish);
        ordersFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(orders_switch.this, OrdersData.class);
                i.putExtra("TypeOrder", "4");
                startActivity(i);
            }
        });
        ordersCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(orders_switch.this, OrdersData.class);
                i.putExtra("TypeOrder", "5");
                startActivity(i);
            }
        });
        ordersNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(orders_switch.this, OrdersData.class);
                i.putExtra("TypeOrder", "0");
                startActivity(i);
            }
        });
        lnHome = (LinearLayout)findViewById(R.id.btnHome);
        lnOrders = (LinearLayout)findViewById(R.id.btnOrders);
        lnNoti = (LinearLayout)findViewById(R.id.btnNoti);
        lnMore = (LinearLayout)findViewById(R.id.btnMore);
        lnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerMarketAction.footerHome(orders_switch.this);
                finish();
            }
        });
        lnOrders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerMarketAction.footerOrders(orders_switch.this);
                finish();
            }
        });
        lnNoti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerMarketAction.footerNoti(orders_switch.this);
                finish();
            }
        });
        lnMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerMarketAction.footerMore(orders_switch.this);
                finish();
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_haraj, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id= item.getItemId();
        if(id == R.id.add){
            Intent i = new Intent(this, MarketMemberList.class);
            startActivity(i);
        } else if(id == R.id.back){
            Intent i = new Intent(this, marketMain.class);
            startActivity(i);
        }
        return super.onOptionsItemSelected(item);
    }
}
