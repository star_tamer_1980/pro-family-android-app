package profamily.designsite.in.profamilies;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import chat.chat;
import chat.chatAdapter;
import libes.KeyValueShared;
import libes.globalData;

public class chatUsers extends AppCompatActivity {

    private String userCurrent, userOther;
    private RecyclerView rvChat;
    private Button btnSend;
    private EditText etMessage;
    private TextView tvOtherUser;
    private ProgressDialog pDialog;
    private List<chat> chatList;
    private chatAdapter chatadapter;
    private Boolean isFirst = true;
    refreshChat countTimer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_chat_users);
        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        userOther = getIntent().getExtras().getString("otherUser");
        userCurrent = KeyValueShared.getId(this);
        rvChat = (RecyclerView)findViewById(R.id.rvchat);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setReverseLayout(true);
        rvChat.setLayoutManager(mLayoutManager);
        btnSend = (Button)findViewById(R.id.btnSave);
        etMessage = (EditText)findViewById(R.id.etMessage);
        tvOtherUser = (TextView)findViewById(R.id.tvOtherUser);
        tvOtherUser.setText(getIntent().getExtras().getString("username"));
        chatList = new ArrayList<>();
        String data[] = new String[4];
        data[0] = globalData.URLhost() + "chatHarajGet.php";
        data[1] = userCurrent;
        data[2] = userOther;
        data[3] = "0";
        new harajChat().execute(data);
        pDialog = new ProgressDialog(chatUsers.this);
        pDialog.setMessage("جاري قراءة البيانات");
        pDialog.setTitle("الرجاء الانتظار");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
        pDialog.show();
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = etMessage.getText().toString();
                if(!message.equals("")) {
                    String data[] = new String[4];
                    data[0] = globalData.URLhost() + "chatHarajAdd.php";
                    data[1] = userCurrent;
                    data[2] = userOther;
                    data[3] = message;
                    new harajChatAdd().execute(data);
                    etMessage.setText("");
                }else{
                    etMessage.setError("يجب كتابة رساله");
                    Toast.makeText(getApplicationContext(), "يجب كتابة رساله", Toast.LENGTH_SHORT).show();
                }
            }
        });

        counterStart();
    }
    private void clearChat(){
        final int size = chatList.size();
        if(size>0){
            for (int i = 0; i< size; i++){
                chatList.remove(0);
            }
            if(chatadapter != null) {
                chatadapter.notifyItemRangeRemoved(0, size);
            }
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_haraj, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.add){
            Intent i = new Intent(chatUsers.this, HarajAddSwicherMedia.class);
            startActivity(i);
        } else if(id == R.id.back){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
    public class harajChat extends AsyncTask<String, Void, Boolean> {
        String ReqSuccess;
        @SuppressWarnings("deprecation")
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(3);
                nameValuePairs.add(new BasicNameValuePair("fromUser", params[1]));
                nameValuePairs.add(new BasicNameValuePair("toUser", params[2]));
                nameValuePairs.add(new BasicNameValuePair("limit", params[3]));
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));

                HttpResponse response = client.execute(post);

                int status = response.getStatusLine().getStatusCode();
                if(status == 200){
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    Log.e("chat", data);
                    JSONObject jObj = new JSONObject(data.substring(data.indexOf("{"), data.lastIndexOf("}") + 1));
                    ReqSuccess = jObj.getString("success");
                    if(ReqSuccess.equals("1")){
                        if(jObj.has("data")) {
                            JSONArray wee = jObj.getJSONArray("data");
                            for (int i = 0; i < wee.length(); i++) {
                                JSONObject c = wee.getJSONObject(i);
                                chat dataChat = new chat();
                                dataChat.setFromUser(c.getString("fromUser"));
                                dataChat.setToUser(c.getString("toUser"));
                                dataChat.setDateAdd(c.getString("dateAdd"));
                                dataChat.setContent(c.getString("content"));
                                dataChat.setId(c.getString("id"));
                                chatList.add(dataChat);
                            }
                        }
                    }


                    return true;
                }else{
                    Log.e("error client string", "here");
                }
            } catch (ClientProtocolException e) {
                Log.e("error client string", e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("error exp", e.getMessage());
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("error json", e.getMessage());
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            pDialog.dismiss();
            super.onPostExecute(result);
            if(result == false){
                Toast.makeText(getApplicationContext(), "حدث خطأ، نرجو المحاوله في وقت لاحق", Toast.LENGTH_LONG).show();
            }else{
                if(ReqSuccess.equals("1")){
                    if(isFirst == true) {
                        chatadapter = new chatAdapter(getApplicationContext(), chatList);
                        rvChat.setAdapter(chatadapter);
                        isFirst = false;
                    }else{
                        chatadapter.notifyItemRangeChanged(0, chatList.size());
                    }
                }else{
                    Log.e("chat", "not found");
                }
            }
        }
    }
    public class harajChatAdd extends AsyncTask<String, Void, Boolean> {
        String ReqSuccess;
        @SuppressWarnings("deprecation")
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(3);
                nameValuePairs.add(new BasicNameValuePair("fromUser", params[1]));
                nameValuePairs.add(new BasicNameValuePair("toUser", params[2]));
                nameValuePairs.add(new BasicNameValuePair("content", params[3]));
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));

                HttpResponse response = client.execute(post);

                int status = response.getStatusLine().getStatusCode();
                if(status == 200){
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    Log.e("chat", data);
                    JSONObject jObj = new JSONObject(data.substring(data.indexOf("{"), data.lastIndexOf("}") + 1));
                    ReqSuccess = jObj.getString("success");
                    if(ReqSuccess.equals("1")){
                        if(jObj.has("data")) {
                            JSONArray wee = jObj.getJSONArray("data");
                            for (int i = 0; i < wee.length(); i++) {
                                JSONObject c = wee.getJSONObject(i);
                                chat dataChat = new chat();
                                dataChat.setFromUser(c.getString("fromUser"));
                                dataChat.setToUser(c.getString("toUser"));
                                dataChat.setDateAdd(c.getString("dateAdd"));
                                dataChat.setContent(c.getString("content"));
                                dataChat.setId(c.getString("id"));
                                chatList.add(dataChat);
                            }
                        }
                    }


                    return true;
                }else{
                    Log.e("error client string", "here");
                }
            } catch (ClientProtocolException e) {
                Log.e("error client string", e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("error exp", e.getMessage());
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("error json", e.getMessage());
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            pDialog.dismiss();
            super.onPostExecute(result);
            if(result == false){
                Toast.makeText(getApplicationContext(), "حدث خطأ، نرجو المحاوله في وقت لاحق", Toast.LENGTH_LONG).show();
            }else{
                if(ReqSuccess.equals("1")){
                    chatadapter = new chatAdapter(getApplicationContext(), chatList);
                    rvChat.setAdapter(chatadapter);
                }else{
                    Log.e("chat", "not found");
                }

                clearChat();
                String data[] = new String[4];
                data[0] = globalData.URLhost() + "chatHarajGet.php";
                data[1] = userCurrent;
                data[2] = userOther;
                data[3] = "0";
                new harajChat().execute(data);
            }
        }
    }
    public void counterStart(){
        countTimer = new refreshChat(50000, 30000);
        countTimer.start();
    }
    public class refreshChat extends CountDownTimer {
        refreshChat(long millisInFuture, long countDownInterval){
            super(millisInFuture, countDownInterval);
        }
        @Override
        public void onTick(long millisUntilFinished) {
            Log.e("Timer chat", "still work");
            clearChat();
            String data[] = new String[4];
            data[0] = globalData.URLhost() + "chatHarajGet.php";
            data[1] = userCurrent;
            data[2] = userOther;
            data[3] = "0";
            new harajChat().execute(data);
        }

        @Override
        public void onFinish() {
            Log.e("timer chat", "finish");
            counterStart();
        }
    }
}
