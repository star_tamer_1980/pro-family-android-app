package profamily.designsite.in.profamilies;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import libes.globalData;

public class ConnectionError extends AppCompatActivity {

    private TextView tvConnection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connection_error);
        tvConnection = (TextView)findViewById(R.id.tv_connection);
        tvConnection.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
    }
}
