package profamily.designsite.in.profamilies;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.Random;

import libes.ConnectionManager;
import libes.KeyValueShared;

public class splashScreen extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        Thread myThread = new Thread(){
            @Override
            public void run() {
                try {
                    Random rn = new Random();
                    String rnF = String.valueOf(rn.nextInt(50000) + 40000);
                    KeyValueShared.setMEMBER_RAND_id(getApplicationContext(), rnF);
                    sleep(3000);
                    if(ConnectionManager.isInternetConnected(splashScreen.this)) {
                        Intent intent = new Intent(getApplicationContext(), Main2Activity.class);
                        startActivity(intent);
                    }else{
                        Intent intent = new Intent(getApplicationContext(), ConnectionError.class);
                        startActivity(intent);
                    }
                    finish();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        myThread.start();
    }
}
