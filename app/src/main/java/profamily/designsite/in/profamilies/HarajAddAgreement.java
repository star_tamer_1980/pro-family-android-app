package profamily.designsite.in.profamilies;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Toast;
import android.widget.ToggleButton;

public class HarajAddAgreement extends AppCompatActivity {

    private String idCat;
    private ToggleButton btn1, btn2;
    private boolean btnStatue1 = false, btnStatue2 = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_haraj_add_agreement);
        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        idCat = getIntent().getExtras().getString("idCat");
        btn1 = (ToggleButton)findViewById(R.id.tog1);
        btn2 = (ToggleButton)findViewById(R.id.tog2);
        btn1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    btnStatue1 = true;
                }else{
                    btnStatue1 = false;
                }
                if(btnStatue1 == true && btnStatue2 == true){
                    Intent i = new Intent(HarajAddAgreement.this, HarajAdd.class);
                    i.putExtra("idCat", idCat);
                    startActivity(i);
                }
            }
        });
        btn2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    btnStatue2 = true;
                }else{
                    btnStatue2 = false;
                }
                if(btnStatue1 == true && btnStatue2 == true){
                    Intent i = new Intent(HarajAddAgreement.this, HarajAdd.class);
                    i.putExtra("idCat", idCat);
                    startActivity(i);
                    finish();
                }
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menubar_haraj_back, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.back){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
