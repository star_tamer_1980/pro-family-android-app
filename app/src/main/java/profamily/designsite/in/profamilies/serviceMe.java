package profamily.designsite.in.profamilies;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import libes.KeyValueShared;
import libes.alertsApp;
import libes.footerServiceAction;
import libes.globalData;
import productData.Product;
import productData.ProductAdapter;
import productData.ServiceAdapter;

public class serviceMe extends AppCompatActivity {

    private RecyclerView mRecyclerView1;
    private ServiceAdapter adapter1;
    private List<Product> data_list1;
    private String idUser;
    private ProgressBar pBar1;
    List<String> listNoti = new ArrayList<String>();
    String ReqSuccess;

    LinearLayout lnHome, lnSearch, lnNoti, lnMore;
    private int m_previouse_Total_Count1 = 0;
    private boolean loading = true;
    private int visibleThreshold = 15;
    int firstVisibleItem, visibleItemCount, totalItemCount;
    private TextView txtNotFound;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_me);
        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        idUser = getIntent().getExtras().getString("idUser");
        m_previouse_Total_Count1 = 0;
        txtNotFound = (TextView)findViewById(R.id.txtNotFound);
        txtNotFound.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));lnHome = (LinearLayout)findViewById(R.id.btnHome);
        lnSearch = (LinearLayout)findViewById(R.id.btnSearch);
        lnNoti = (LinearLayout)findViewById(R.id.btnNoti);
        lnMore = (LinearLayout)findViewById(R.id.btnMore);
        lnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerServiceAction.footerHome(serviceMe.this);
                finish();
            }
        });
        lnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerServiceAction.footerSearch(serviceMe.this);
                finish();
            }
        });
        lnNoti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerServiceAction.footerNoti(serviceMe.this);
                finish();
            }
        });
        lnMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerServiceAction.footerMore(serviceMe.this);
                finish();
            }
        });
        mRecyclerView1 = (RecyclerView)findViewById(R.id.recycleView1);
        mRecyclerView1.setLayoutManager(new LinearLayoutManager(this));
        pBar1 = (ProgressBar)findViewById(R.id.progressBar1);
        data_list1 = new ArrayList<>();
        mRecyclerView1.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager mLayoutManager = (LinearLayoutManager) mRecyclerView1
                        .getLayoutManager();
                mRecyclerView1.setLayoutManager(mLayoutManager);
                visibleItemCount = mRecyclerView1.getChildCount();
                totalItemCount = mLayoutManager.getItemCount();
                firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();
                if(totalItemCount == 0){
                    return ;
                }
                if(m_previouse_Total_Count1==totalItemCount){
                    return;
                }
                boolean loadMore = firstVisibleItem + visibleItemCount >= totalItemCount;
                if(loadMore){
                    m_previouse_Total_Count1 = totalItemCount;
                    pBar1.setVisibility(View.VISIBLE);
                    getLoadData(totalItemCount);
                }

            }
        });


    }
    @Override
    protected void onResume() {
        super.onResume();
        m_previouse_Total_Count1 = 0;
        data_list1.clear();
        String data[] = new String[3];
        data[0] = globalData.URLhost() + "serviceMyProduct.php";
        data[1] = "0";
        data[2] = KeyValueShared.getId(this);
        loadData1 bts = new loadData1();
        //bts.execute(data);
    }

    @Override
    protected void onStart() {
        super.onStart();
        String data[] = new String[3];
        data[0] = globalData.URLhost() + "serviceMyProduct.php";
        data[1] = "0";
        data[2] = idUser;
        loadData1 bts = new loadData1();
        bts.execute(data);
    }

    public void getLoadData(Integer limit) {

        pBar1.setVisibility(View.VISIBLE);
        String data[] = new String[3];
        data[0] = globalData.URLhost() + "serviceMyProduct.php";
        data[1] = String.valueOf(limit);
        data[2] = idUser;

        loadData1 bts = new loadData1();
        bts.execute(data);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menubar_haraj_back, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.add){
            Intent i = new Intent(serviceMe.this, serviceAddAgreement.class);
            startActivity(i);
        } else if(id == R.id.back){
            Intent i = new Intent(serviceMe.this, serviceMenu.class);
            startActivity(i);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
    public class loadData1 extends AsyncTask<String, Void, Boolean> {

        private String numberRating, username, userStatue, userDateRegister;
        @SuppressWarnings("deprecation")
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                nameValuePairs.add(new BasicNameValuePair("limit", params[1]));
                nameValuePairs.add(new BasicNameValuePair("idUser", params[2]));
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if(status == 200){
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    Log.e("error client string", data);
                    JSONObject jObj = new JSONObject(data.substring(data.indexOf("{"), data.lastIndexOf("}") + 1));
                    ReqSuccess = jObj.getString("success");
                    if(ReqSuccess.equals("1")){
                        if(jObj.has("data")) {
                            JSONArray wee = jObj.getJSONArray("data");
                            for (int i = 0; i < wee.length(); i++) {
                                JSONObject c = wee.getJSONObject(i);
                                Product dataProduct = new Product();
                                dataProduct.setId(c.getString("id"));
                                dataProduct.setName(c.getString("name"));
                                dataProduct.setIdMainCat(c.getString("idMainCat"));
                                dataProduct.setPlace(c.getString("country"));
                                dataProduct.setCity(c.getString("city"));
                                dataProduct.setImg(c.getString("img"));
                                dataProduct.setIdUser(c.getString("idUser"));
                                dataProduct.setUserNameProduct(c.getString("userName"));
                                dataProduct.setIsActive(c.getString("isActive"));
                                dataProduct.setMyPlaceAndCity(c.getString("location"));
                                dataProduct.setContact(c.getString("contact"));
                                dataProduct.setDescription(c.getString("description"));
                                dataProduct.setRating(c.getString("rating"));
                                data_list1.add(dataProduct);
                            }
                        }

                    }


                    return true;
                }else{
                    Log.e("error client string", "here");
                }
            } catch (ClientProtocolException e) {
                Log.e("error client string", e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("error exp", e.getMessage());
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("error json", e.getMessage());
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if(result == false){
                Toast.makeText(getApplicationContext(), "حدث خطأ، نرجو المحاوله في وقت لاحق", Toast.LENGTH_LONG).show();
            }else{
                if(ReqSuccess.equals("1")){
                    txtNotFound.setVisibility(View.INVISIBLE);
                    if(m_previouse_Total_Count1>0) {
                        adapter1.notifyDataSetChanged();
                        pBar1.setVisibility(View.INVISIBLE);
                    }else{
                        adapter1 = new ServiceAdapter(getApplicationContext(), data_list1);
                        mRecyclerView1.setAdapter(adapter1);
                        pBar1.setVisibility(View.INVISIBLE);
                    }
                }else{
                    pBar1.setVisibility(View.INVISIBLE);
                }
            }
        }
    }
}
