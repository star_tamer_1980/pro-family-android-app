package profamily.designsite.in.profamilies;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.annotation.IntegerRes;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import imageData.ImageAlbum;
import imageData.ImageAlbumAdapter;
import libes.KeyValueShared;
import libes.SQLiteConnection;
import libes.footerMarketAction;
import libes.globalData;
import productData.Product;

public class marketProductDetails extends AppCompatActivity {

    private String ReqSuccess, isAllow;
    private Button btnAddToCart, btnEditData, pluse, minuse;
    private TextView tvTitle, tvContent, number, tvPrice;
    String marketName, stTitle, img, idMarket, stPrice;
    private ImageView img_main;
    SQLiteConnection db;
    private Product productData;
    LinearLayout lnHome, lnOrders, lnNoti, lnMore;

    public marketProductDetails() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_market_product_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        db = new SQLiteConnection(getApplicationContext());
        Intent i = getIntent();
        productData = (Product)i.getSerializableExtra("product");
        idMarket = productData.getIdMarket();
        isAllow = productData.getIsAllow();
        stTitle = productData.getName();
        stPrice = productData.getPrice();
        marketName = productData.getMarketName();
        img = productData.getImg();
        btnEditData = (Button)findViewById(R.id.btn_product_edit);
        btnEditData.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        pluse = (Button)findViewById(R.id.btn_pluse);
        pluse.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        minuse = (Button)findViewById(R.id.btn_minuse);
        minuse.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        number = (TextView)findViewById(R.id.tv_number);
        number.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        tvTitle = (TextView)findViewById(R.id.tv_title);
        tvTitle.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        tvContent = (TextView)findViewById(R.id.tv_content);
        tvContent.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        tvPrice = (TextView)findViewById(R.id.tv_price);
        tvPrice.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        img_main = (ImageView)findViewById(R.id.img_main);
        btnAddToCart = (Button)findViewById(R.id.btn_product_cart);
        btnAddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int idProduct = Integer.valueOf(productData.getId());
                if(db.isProductFound(productData.getId())){
                    int quantity = Integer.valueOf(number.getText().toString()) + Integer.valueOf(db.getQuantity(String.valueOf(idProduct)));
                    String priceFinish = String.valueOf(Integer.valueOf(stPrice) * quantity);
                    if(db.UpdateRowCart(idProduct, String.valueOf(quantity), priceFinish)){
                        Toast.makeText(getApplicationContext(), "edit", Toast.LENGTH_SHORT).show();
                        AlertDialog.Builder altdial = new AlertDialog.Builder(marketProductDetails.this);
                        altdial.setMessage(getResources().getString(R.string.message_add_to_cart))
                        .setCancelable(false).setPositiveButton("إكمال التسوق", new DialogInterface.OnClickListener(){
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        })
                        .setNegativeButton("عرض سلة التسوق", new DialogInterface.OnClickListener(){
                              @Override
                              public void onClick(DialogInterface dialog, int which) {
                                  Intent i = new Intent(marketProductDetails.this, cart.class);
                                  startActivity(i);
                                  finish();
                              }
                        });
                        AlertDialog alert = altdial.create();
                        alert.show();
                    }else{
                        Toast.makeText(getApplicationContext(), "Not edit", Toast.LENGTH_SHORT).show();
                    }

                }else{
                    String priceFinish =String.valueOf(Integer.valueOf(number.getText().toString()) * Integer.valueOf(stPrice));
                    db.InsertRowCart(idProduct,
                            Integer.valueOf(idMarket),
                            marketName,
                            stTitle,
                            number.getText().toString(),
                            priceFinish,
                            img);
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.add_successfully), Toast.LENGTH_SHORT).show();
                    AlertDialog.Builder altdial = new AlertDialog.Builder(marketProductDetails.this);
                    altdial.setMessage(getResources().getString(R.string.message_add_to_cart))
                            .setCancelable(false).setPositiveButton("إكمال التسوق", new DialogInterface.OnClickListener(){

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                            .setNegativeButton("عرض سلة التسوق", new DialogInterface.OnClickListener(){
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent i = new Intent(marketProductDetails.this, cart.class);
                                    startActivity(i);
                                    finish();
                                }
                            });
                    AlertDialog alert = altdial.create();
                    alert.show();

                }
                db.close();

            }
        });
        if(KeyValueShared.getId(getApplicationContext()).equals(productData.getIdUser())){
            btnEditData.setVisibility(View.VISIBLE);
        }
        Toast.makeText(this, productData.getIdUser(), Toast.LENGTH_SHORT).show();
        btnEditData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(marketProductDetails.this, productEditSwitch.class);
                i.putExtra("idProduct", productData.getId());
                i.putExtra("idMarket", idMarket);
                i.putExtra("isAllow", isAllow);
                i.putExtra("img", img);
                startActivity(i);
            }
        });
        tvTitle.setText(stTitle);
        tvPrice.setText(stPrice + " " + getResources().getString(R.string.ryal) + " " + productData.getPackageData());
        tvContent.setText(Html.fromHtml(productData.getDescription()));
        Glide.with(getApplicationContext()).load(img).into(img_main);

        pluse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int num = Integer.valueOf((String) number.getText());
                num++;
                number.setText(String.valueOf(num));
            }
        });
        minuse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int num = Integer.valueOf((String) number.getText());
                if(num != 1) {
                    num--;
                    number.setText(String.valueOf(num));
                }
            }
        });
        lnHome = (LinearLayout)findViewById(R.id.btnHome);
        lnOrders = (LinearLayout)findViewById(R.id.btnOrders);
        lnNoti = (LinearLayout)findViewById(R.id.btnNoti);
        lnMore = (LinearLayout)findViewById(R.id.btnMore);
        lnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerMarketAction.footerHome(marketProductDetails.this);
                finish();
            }
        });
        lnOrders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerMarketAction.footerOrders(marketProductDetails.this);
                finish();
            }
        });
        lnNoti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerMarketAction.footerNoti(marketProductDetails.this);
                finish();
            }
        });
        lnMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerMarketAction.footerMore(marketProductDetails.this);
                finish();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_haraj, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id= item.getItemId();
        if(id == R.id.add){
            Intent i = new Intent(this, serviceAddAgreement.class);
            startActivity(i);
        } else if(id == R.id.back){

            Intent i = new Intent(this, marketListProduct.class);
            i.putExtra("idMarket", productData.getIdMarket());
            i.putExtra("idSubCat", productData.getIdSubCat());
            startActivity(i);
            finish();

        }
        return super.onOptionsItemSelected(item);
    }


}
