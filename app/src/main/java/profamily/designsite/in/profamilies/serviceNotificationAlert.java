package profamily.designsite.in.profamilies;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import Noti_alerts.Notifications_alert;
import Noti_alerts.Notifications_alert_adapter;
import libes.KeyValueShared;
import libes.footerHarajAction;
import libes.footerServiceAction;
import libes.globalData;

public class serviceNotificationAlert extends AppCompatActivity {


    private RecyclerView mRecyclerView1;
    private Notifications_alert_adapter adapter1;
    private List<Notifications_alert> data_list1;
    private String dataTest;
    private ProgressDialog pDialog;
    private ProgressBar pBar1;
    String ReqSuccess;
    private int m_previouse_Total_Count1 = 0;
    private boolean loading = true;
    private int visibleThreshold = 15;
    int firstVisibleItem, visibleItemCount, totalItemCount;
    private TextView txtNotFound;

    LinearLayout lnHome, lnSearch, lnNoti, lnMore;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_notification_alert);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        lnHome = (LinearLayout)findViewById(R.id.btnHome);
        lnSearch = (LinearLayout)findViewById(R.id.btnSearch);
        lnNoti = (LinearLayout)findViewById(R.id.btnNoti);
        lnMore = (LinearLayout)findViewById(R.id.btnMore);
        lnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerServiceAction.footerHome(serviceNotificationAlert.this);
                finish();
            }
        });
        lnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerServiceAction.footerSearch(serviceNotificationAlert.this);
                finish();
            }
        });
        lnNoti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerServiceAction.footerNoti(serviceNotificationAlert.this);
                finish();
            }
        });
        lnMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerServiceAction.footerMore(serviceNotificationAlert.this);
                finish();
            }
        });
        m_previouse_Total_Count1 = 0;
        txtNotFound = (TextView)findViewById(R.id.txtNotFound);
        txtNotFound.setTypeface(Typeface.createFromAsset(getAssets(), globalData.getFont()));
        mRecyclerView1 = (RecyclerView)findViewById(R.id.recycleView1);
        mRecyclerView1.setLayoutManager(new LinearLayoutManager(this));
        pBar1 = (ProgressBar)findViewById(R.id.progressBar1);
        data_list1 = new ArrayList<>();
        mRecyclerView1.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager mLayoutManager = (LinearLayoutManager) mRecyclerView1
                        .getLayoutManager();
                mRecyclerView1.setLayoutManager(mLayoutManager);
                visibleItemCount = mRecyclerView1.getChildCount();
                totalItemCount = mLayoutManager.getItemCount();
                firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();
                if(totalItemCount == 0){
                    return ;
                }
                if(m_previouse_Total_Count1==totalItemCount){
                    return;
                }
                boolean loadMore = firstVisibleItem + visibleItemCount >= totalItemCount;
                if(loadMore){
                    m_previouse_Total_Count1 = totalItemCount;
                    pBar1.setVisibility(View.VISIBLE);
                    getLoadData(totalItemCount);
                }

            }
        });

        String data[] = new String[3];
        data[0] = globalData.URLhost() + "notification_alert_get.php";
        data[1] = "0";
        data[2] = KeyValueShared.getId(getApplicationContext()).toString();
        loadData1 bts = new loadData1();
        bts.execute(data);
    }
    public void getLoadData(Integer limit) {

        pBar1.setVisibility(View.VISIBLE);
        String data[] = new String[3];
        data[0] = globalData.URLhost() + "notification_alert_get.php";
        data[1] = String.valueOf(limit);
        data[2] = KeyValueShared.getId(getApplicationContext()).toString();

        loadData1 bts = new loadData1();
        bts.execute(data);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menubar_haraj_back, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.back){
            Intent i = new Intent(serviceNotificationAlert.this, serviceMain.class);
            startActivity(i);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
    public class loadData1 extends AsyncTask<String, Void, Boolean> {

        private String title;
        private String content;
        @SuppressWarnings("deprecation")
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                nameValuePairs.add(new BasicNameValuePair("limit", params[1]));
                nameValuePairs.add(new BasicNameValuePair("idUser", params[2]));
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if(status == 200){
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    JSONObject jObj = new JSONObject(data.substring(data.indexOf("{"), data.lastIndexOf("}") + 1));
                    ReqSuccess = jObj.getString("success");
                    if(ReqSuccess.equals("1")){
                        dataTest = data;
                        Log.e("data is: ", dataTest);
                        if(jObj.has("data")) {
                            JSONArray wee = jObj.getJSONArray("data");
                            for (int i = 0; i < wee.length(); i++) {
                                JSONObject c = wee.getJSONObject(i);
                                Notifications_alert dataPro = new Notifications_alert();
                                dataPro.setId(c.getString("id"));
                                dataPro.setTitle(c.getString("title"));
                                dataPro.setContent(c.getString("content"));
                                dataPro.setDateAdd(c.getString("dateAdd"));
                                dataPro.setTypeAlert(c.getString("typeAlert"));
                                dataPro.setIdFirst(c.getString("idFirst"));
                                dataPro.setIdSecond(c.getString("idSecond"));
                                dataPro.setIdThird(c.getString("idThird"));
                                dataPro.setIsView(c.getString("isView"));
                                dataPro.setName(c.getString("name"));
                                data_list1.add(dataPro);
                            }
                        }

                    }


                    return true;
                }else{
                    Log.e("error client string", "here");
                }
            } catch (ClientProtocolException e) {
                Log.e("error client string", e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("error exp", e.getMessage());
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("error json", e.getMessage());
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if(result == false){
                Toast.makeText(getApplicationContext(), "حدث خطأ، نرجو المحاوله في وقت لاحق", Toast.LENGTH_LONG).show();
            }else{
                if(ReqSuccess.equals("1")){
                    txtNotFound.setVisibility(View.INVISIBLE);
                    if(m_previouse_Total_Count1>0) {
                        adapter1.notifyDataSetChanged();
                        pBar1.setVisibility(View.INVISIBLE);
                    }else{
                        adapter1 = new Notifications_alert_adapter(getApplicationContext(), data_list1);
                        mRecyclerView1.setAdapter(adapter1);
                        pBar1.setVisibility(View.INVISIBLE);
                    }
                }else{
                    pBar1.setVisibility(View.INVISIBLE);
                }
            }
        }
    }
}
