package profamily.designsite.in.profamilies;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import libes.KeyValueShared;
import libes.globalData;

public class HarajAddCountry extends AppCompatActivity {

    private Spinner spCountry;
    private Spinner spCity;
    private Spinner spCat;
    private Button btnSave;
    private ProgressDialog pDialog;
    String idUserReal, idUser, ReqSuccess;
    List<String> listCountry = new ArrayList<String>();
    List<String> listCountryId = new ArrayList<String>();
    ArrayList<String> TypeCountryArray = new ArrayList<String>();
    ArrayAdapter<String> adapterCountry;
    List<String> listcity = new ArrayList<String>();
    List<String> listcityId = new ArrayList<String>();
    ArrayList<String> subListItem = new ArrayList<String>();
    ArrayAdapter<String> subCountriesAdapter;

    List<String> listCat = new ArrayList<String>();
    List<String> listCatId = new ArrayList<String>();
    ArrayList<String> TypeCatArray = new ArrayList<String>();
    ArrayAdapter<String> adapterCat;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_haraj_add_country);
        idUser = KeyValueShared.getId(getApplicationContext());
        idUserReal = KeyValueShared.getId(getApplicationContext());
        spCountry = (Spinner)findViewById(R.id.sp_country);
        btnSave = (Button)findViewById(R.id.btnSave);
        spCity = (Spinner)findViewById(R.id.sp_city);
        spCity.setVisibility(View.INVISIBLE);
        spCat = (Spinner)findViewById(R.id.sp_cat);
        startAct();
        spCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String value = spCountry.getSelectedItem().toString();

                if(value.equals("في جميع المناطق")){
                    spCity.setVisibility(View.INVISIBLE);
                }else{
                    String data[] = new String[2];
                    data[0] = globalData.URLhost() + "getSubCountry.php";
                    data[1] = value;
                    SubCountriesAsynTask bts = new SubCountriesAsynTask();
                    bts.execute(data);
                    spCity.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        String data[] = new String[3];
        data[0] = globalData.URLhost() + "getCountry.php";
        data[1] = "0";
        ProgressTask bt = new ProgressTask();
        bt.execute(data);




        listCat = Arrays.asList(getResources().getStringArray(R.array.tabSub_name_all));
        listCatId = Arrays.asList(getResources().getStringArray(R.array.tabSub_id_all));
        if(getIntent().getExtras().getString("idCat").equals("1")){
            listCat = Arrays.asList(getResources().getStringArray(R.array.tabSub_categoryCarName));
            listCatId = Arrays.asList(getResources().getStringArray(R.array.tabSub_categoryCarId));
        }else if(getIntent().getExtras().getString("idCat").equals("2")){
            listCat = Arrays.asList(getResources().getStringArray(R.array.tabSub_categoryBuildingName));
            listCatId = Arrays.asList(getResources().getStringArray(R.array.tabSub_categoryBuildingId));
        }else if(getIntent().getExtras().getString("idCat").equals("3")){
            listCat = Arrays.asList(getResources().getStringArray(R.array.tabSub_categoryElectricName));
            listCatId = Arrays.asList(getResources().getStringArray(R.array.tabSub_categoryElectricId));
        }else if(getIntent().getExtras().getString("idCat").equals("4")){
            listCat = Arrays.asList(getResources().getStringArray(R.array.tabSub_categoryAnimalsName));
            listCatId = Arrays.asList(getResources().getStringArray(R.array.tabSub_categoryAnimalsId));
        }else if(getIntent().getExtras().getString("idCat").equals("5")){
            listCat = Arrays.asList(getResources().getStringArray(R.array.tabSub_categoryPersonalName));
            listCatId = Arrays.asList(getResources().getStringArray(R.array.tabSub_categoryPersonalId));
        }
        TypeCatArray.clear();
        TypeCatArray.addAll(listCat);
        adapterCat.notifyDataSetChanged();



        /*
        String dataCat[] = new String[3];
        dataCat[0] = globalData.URLhost() + "categoryHaraj.php";
        dataCat[1] = "0";
        getCat btCat = new getCat();
        btCat.execute(dataCat);
        */
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("جاري تحميل البيانات");
        pDialog.setTitle("برو فاميليز");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
        pDialog.show();
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String videoFileName = "";
                String  videoImageName= "";
                String albumImage = "";
                if(getIntent().getExtras().containsKey("videoFileName")){
                    videoFileName = getIntent().getStringExtra("videoFileName");
                }
                if(getIntent().getExtras().containsKey("videoImageName")){
                    videoImageName = getIntent().getStringExtra("videoImageName");
                }
                if(getIntent().getExtras().containsKey("albumImage")){
                    albumImage = getIntent().getStringExtra("albumImage");
                }
                if(spCountry.getSelectedItem().toString().equals(getResources().getString(R.string.haraj_add_choose_country))){
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.haraj_add_must_choose_country), Toast.LENGTH_SHORT).show();
                }else if(spCity.getSelectedItem().toString().equals(getResources().getString(R.string.haraj_add_choose_city))){
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.haraj_add_must_choose_city), Toast.LENGTH_SHORT).show();
                }else if(spCat.getSelectedItem().toString().equals(getResources().getString(R.string.haraj_add_choose_cat))){
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.haraj_add_must_choose_cat), Toast.LENGTH_SHORT).show();
                }else{
                    String dataCat[] = new String[15];
                    dataCat[0] = globalData.URLhost() + "harajAddIPhone.php";
                    dataCat[1] = getIntent().getStringExtra("title");
                    dataCat[2] = getIntent().getStringExtra("tel");
                    dataCat[3] = getIntent().getStringExtra("price");
                    dataCat[4] = getIntent().getStringExtra("content");
                    dataCat[5] = getIntent().getStringExtra("isSom");
                    dataCat[6] = listCountryId.get(spCountry.getSelectedItemPosition()).toString();
                    dataCat[7] = listcityId.get(spCity.getSelectedItemPosition()).toString();
                    dataCat[8] = listCatId.get(spCat.getSelectedItemPosition()).toString();
                    dataCat[9] = idUser;
                    dataCat[10] = albumImage;
                    dataCat[11] = videoFileName;
                    dataCat[12] = videoImageName;
                    dataCat[13] = KeyValueShared.getMapLat(getApplicationContext());
                    dataCat[14] = KeyValueShared.getMapLon(getApplicationContext());
                    saveAdvertise saveProduct = new saveAdvertise();
                    saveProduct.execute(dataCat);
                }

            }
        });
    }
    private void startAct() {

        //TypeweightArray = getResources().getStringArray(R.array.list_weight);
        adapterCountry = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, TypeCountryArray);
        adapterCountry.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCountry.setAdapter(adapterCountry);
        adapterCat = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, TypeCatArray);
        adapterCat.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCat.setAdapter(adapterCat);
        subCountriesAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, subListItem);
        subCountriesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCity.setAdapter(subCountriesAdapter);
    }
    public class ProgressTask extends AsyncTask<String, Void, Boolean> {


        @SuppressWarnings("deprecation")
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
                nameValuePairs.add(new BasicNameValuePair("id", params[1]));
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));

                HttpResponse response = client.execute(post);

                int status = response.getStatusLine().getStatusCode();
                if(status == 200){
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    JSONObject jObj = new JSONObject(data.substring(data.indexOf("{"), data.lastIndexOf("}") + 1));
                    ReqSuccess = jObj.getString("success");
                    if(ReqSuccess.equals("1")){
                        listCountry.add(getResources().getString(R.string.haraj_add_choose_country));
                        listCountryId.add("0");
                        if(jObj.has("data")) {
                            JSONArray wee = jObj.getJSONArray("data");
                            for (int i = 0; i < wee.length(); i++) {
                                JSONObject c = wee.getJSONObject(i);
                                listCountry.add(c.getString("title"));
                                listCountryId.add(c.getString("id"));
                                Log.e("title", c.getString("title"));
                                Log.e("id", c.getString("id"));
                            }
                        }else{
                            listCountry.add("لا يوجد دول");
                        }

                    }
                    return true;
                }else{
                    Log.e("error client string", "here");
                }
            } catch (ClientProtocolException e) {
                Log.e("error client string", e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("error exp", e.getMessage());
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("error json", e.getMessage());
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            pDialog.dismiss();
            super.onPostExecute(result);
            if(result == false){
                Toast.makeText(getApplicationContext(), "حدث خطأ، نرجو المحاوله في وقت لاحق", Toast.LENGTH_LONG).show();
            }else{
                if(ReqSuccess.equals("1")){

                    TypeCountryArray.clear();
                    TypeCountryArray.addAll(listCountry);
                    int indexItem = listCountryId.indexOf(KeyValueShared.getSettingsAppCountryId(getApplicationContext()));
                    spCountry.setSelection(indexItem);
                    adapterCountry.notifyDataSetChanged();

                    Log.e("id country", KeyValueShared.getSettingsAppCountryId(getApplicationContext()));
                    Log.e("index Item", String.valueOf(indexItem));
                }else{
                    Toast.makeText(getApplicationContext(), "لم يتم حفظ البيانات", Toast.LENGTH_LONG).show();
                }
            }
        }



    }
    public class SubCountriesAsynTask extends AsyncTask<String, Void, Boolean>{

        private String title;
        private String content;
        private String img;
        @SuppressWarnings("deprecation")
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
                nameValuePairs.add(new BasicNameValuePair("value", params[1]));
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));

                HttpResponse response = client.execute(post);

                int status = response.getStatusLine().getStatusCode();
                if(status == 200){
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    JSONObject jObj = new JSONObject(data.substring(data.indexOf("{"), data.lastIndexOf("}") + 1));
                    ReqSuccess = jObj.getString("success");
                    if(ReqSuccess.equals("1")){
                        listcity.clear();
                        listcity.add(getResources().getString(R.string.haraj_add_choose_city));
                        listcityId.clear();
                        listcityId.add("0");
                        if(jObj.has("data")) {
                            JSONArray wee = jObj.getJSONArray("data");
                            for (int i = 0; i < wee.length(); i++) {
                                JSONObject c = wee.getJSONObject(i);
                                listcity.add(c.getString("title"));
                                listcityId.add(c.getString("id"));
                                Log.e("title", c.getString("title"));
                                Log.e("id", c.getString("id"));
                            }
                        }else{
                            listcity.add("لا يوجد دول");
                        }

                    }else{
                        listcity.add("لا يوجد دول");
                    }


                    return true;
                }else{
                    Log.e("error client string", "here");
                }
            } catch (ClientProtocolException e) {
                Log.e("error client string", e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("error exp", e.getMessage());
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("error json", e.getMessage());
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            pDialog.dismiss();
            super.onPostExecute(result);
            if(result == false){
                Toast.makeText(getApplicationContext(), "حدث خطأ، نرجو المحاوله في وقت لاحق", Toast.LENGTH_LONG).show();
            }else{
                if(ReqSuccess.equals("1")){

                    subListItem.clear();
                    subListItem.addAll(listcity);
                    int indexItem = listcityId.indexOf(KeyValueShared.getSettingsAppMyCityId(getApplicationContext()));
                    spCity.setSelection(indexItem);
                    subCountriesAdapter.notifyDataSetChanged();

                    Log.e("id city", KeyValueShared.getSettingsAppMyCityId(getApplicationContext()));
                    Log.e("index Item", String.valueOf(indexItem));
                }else{
                    Toast.makeText(getApplicationContext(), "لم يتم حفظ البيانات", Toast.LENGTH_LONG).show();
                }
            }
        }

    }
    public class saveAdvertise extends AsyncTask<String, Void, Boolean>{

        @SuppressWarnings("deprecation")
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(14);
                nameValuePairs.add(new BasicNameValuePair("title", params[1]));
                nameValuePairs.add(new BasicNameValuePair("tel", params[2]));
                nameValuePairs.add(new BasicNameValuePair("price", params[3]));
                nameValuePairs.add(new BasicNameValuePair("content", params[4]));
                nameValuePairs.add(new BasicNameValuePair("isSom", params[5]));
                nameValuePairs.add(new BasicNameValuePair("country", params[6]));
                nameValuePairs.add(new BasicNameValuePair("city", params[7]));
                nameValuePairs.add(new BasicNameValuePair("cat", params[8]));
                nameValuePairs.add(new BasicNameValuePair("idUser", params[9]));
                nameValuePairs.add(new BasicNameValuePair("albumImage", params[10]));
                nameValuePairs.add(new BasicNameValuePair("videoName", params[11]));
                nameValuePairs.add(new BasicNameValuePair("videoImageName", params[12]));
                nameValuePairs.add(new BasicNameValuePair("latitude", params[13]));
                nameValuePairs.add(new BasicNameValuePair("longitude", params[14]));
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));

                HttpResponse response = client.execute(post);

                int status = response.getStatusLine().getStatusCode();
                if(status == 200){
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    Log.e("data", data.toString());
                    JSONObject jObj = new JSONObject(data.substring(data.indexOf("{"), data.lastIndexOf("}") + 1));
                    ReqSuccess = jObj.getString("success");
                    return true;
                }else{
                    Log.e("error client string", "here");
                }
            } catch (ClientProtocolException e) {
                Log.e("error client string", e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("error exp", e.getMessage());
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("error json", e.getMessage());
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if(result == false){
                Toast.makeText(getApplicationContext(), "حدث خطأ، نرجو المحاوله في وقت لاحق", Toast.LENGTH_LONG).show();
            }else{
                if(ReqSuccess.equals("1")){
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.haraj_add_finish), Toast.LENGTH_LONG).show();
                    finish();
                }else{
                    Toast.makeText(getApplicationContext(), "لم يتم حفظ البيانات", Toast.LENGTH_LONG).show();
                }
            }
        }

    }
}
