package profamily.designsite.in.profamilies;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class HarajAddSwicherVideo extends AppCompatActivity {

    Button btnVideo, btnFinish;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_haraj_add_swicher_video);
        btnVideo = (Button)findViewById(R.id.btnAddVideo);
        btnFinish = (Button)findViewById(R.id.btnFinish);
        btnVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HarajAddSwicherVideo.this, addVideoProduct.class);
                i.putExtra("isMain", "0");
                i.putExtra("isVideo", "1");
                startActivity(i);
            }
        });
        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
