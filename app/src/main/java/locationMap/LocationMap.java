package locationMap;

/**
 * Created by mac on 4/18/17.
 */

public class LocationMap {
    private String id, mapLat, mapLong, title;
    public LocationMap() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMapLat() {
        return mapLat;
    }

    public void setMapLat(String mapLat) {
        this.mapLat = mapLat;
    }

    public String getMapLong() {
        return mapLong;
    }

    public void setMapLong(String mapLong) {
        this.mapLong = mapLong;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
