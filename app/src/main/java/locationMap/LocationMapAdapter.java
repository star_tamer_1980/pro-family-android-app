package locationMap;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import libes.KeyValueShared;
import libes.globalData;
import profamily.designsite.in.profamilies.LocationList;
import profamily.designsite.in.profamilies.Main2Activity;
import profamily.designsite.in.profamilies.MakretListSearch;
import profamily.designsite.in.profamilies.MapsActivity;
import profamily.designsite.in.profamilies.R;
import profamily.designsite.in.profamilies.marketMain;

/**
 * Created by mac on 4/18/17.
 */

public class LocationMapAdapter extends RecyclerView.Adapter<LocationMapAdapter.ViewHolder> {
    private Context v;
    private List<LocationMap> my_data;
    public LocationMapAdapter(Context v, List<LocationMap> data_list) {
        this.v = v;
        this.my_data = data_list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_noti, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final String mapLat = my_data.get(position).getMapLat();
        final String mapLong = my_data.get(position).getMapLong();
        holder.title.setText(my_data.get(position).getTitle());
        holder.rowData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                KeyValueShared.setMapLat(v, mapLat);
                KeyValueShared.setMapLon(v, mapLong);
                Intent i = new Intent(view.getContext(), marketMain.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                view.getContext().startActivity(i);
                ((LocationList)view.getContext()).finish();
            }
        });
    }

    @Override
    public int getItemCount() {
        return my_data.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        public TextView title;
        public LinearLayout rowData;

        public ViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            title.setTypeface(Typeface.createFromAsset(itemView.getContext().getAssets(), globalData.getFont()));
            rowData = (LinearLayout) itemView.findViewById(R.id.rowData);
        }
    }

}
