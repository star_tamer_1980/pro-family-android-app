package chat;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import libes.KeyValueShared;
import libes.globalData;
import profamily.designsite.in.profamilies.R;
import profamily.designsite.in.profamilies.notidetails;
import chat.chat;

/**
 * Created by mac on 4/18/17.
 */

public class chatAdapter extends RecyclerView.Adapter<chatAdapter.ViewHolder> {
    private Context v;
    private List<chat> my_data;
    public chatAdapter(Context v, List<chat> data_list) {
        this.v = v;
        this.my_data = data_list;
    }
    @Override
    public chatAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_chat, parent, false);
        return new chatAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        final String userId = my_data.get(position).getId();
        holder.content.setText(my_data.get(position).getContent());
        if(my_data.get(position).getFromUser().equals(KeyValueShared.getId(v.getApplicationContext()))){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                holder.rowData.setBackgroundColor(Color.parseColor("#E9EBEE"));
            }
        }
    }

    @Override
    public int getItemCount() {
        return my_data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView content;
        public RelativeLayout rowData;

        public ViewHolder(View itemView) {
            super(itemView);
            content = (TextView) itemView.findViewById(R.id.content);
            content.setTypeface(Typeface.createFromAsset(itemView.getContext().getAssets(), globalData.getFont()));
            rowData = (RelativeLayout) itemView.findViewById(R.id.rowData);
        }
    }

}
