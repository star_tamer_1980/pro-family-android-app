package cartSQLiteData;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.List;

import libes.SQLiteConnection;
import libes.globalData;
import profamily.designsite.in.profamilies.R;
import profamily.designsite.in.profamilies.waitData;

/**
 * Created by mac on 4/18/17.
 */

public class CartSQLiteMarketAdapter extends RecyclerView.Adapter<CartSQLiteMarketAdapter.ViewHolder> {
    private Context v;
    private List<CartSQLiteMarket> my_data;
    public CartSQLiteMarketAdapter(Context v, List<CartSQLiteMarket> data_list) {
        this.v = v;
        this.my_data = data_list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_product_cart, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final String id = my_data.get(position).getId();
        final String idMarket = my_data.get(position).getIdMarket();
        holder.priceFinish.setText(my_data.get(position).getProductPriceFinish());
        holder.quantity.setText(my_data.get(position).getProductQuantity());
        Glide.with(v).load(my_data.get(position).getImg()).into(holder.imageView);
        holder.rowData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(v.getApplicationContext(), my_data.get(position).getProductPriceFinish(), Toast.LENGTH_SHORT).show();
                Toast.makeText(v.getApplicationContext(), my_data.get(position).getProductQuantity(), Toast.LENGTH_SHORT).show();
            }
        });
        holder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SQLiteConnection db = new SQLiteConnection(view.getContext());
                db.deleteOneRecord(id);
                Intent i = new Intent(view.getContext(), waitData.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                view.getContext().startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return my_data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView title;
        public ImageView imageView;
        public TextView quantity;
        public TextView priceFinish;
        public Button deleteButton;
        public RelativeLayout rowData;

        public ViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            title.setTypeface(Typeface.createFromAsset(itemView.getContext().getAssets(), globalData.getFont()));
            priceFinish = (TextView) itemView.findViewById(R.id.tv_cart_price_finish);
            priceFinish.setTypeface(Typeface.createFromAsset(itemView.getContext().getAssets(), globalData.getFont()));
            imageView = (ImageView) itemView.findViewById(R.id.image);
            quantity = (TextView) itemView.findViewById(R.id.tv_cart_quantity);
            quantity.setTypeface(Typeface.createFromAsset(itemView.getContext().getAssets(), globalData.getFont()));
            deleteButton = (Button) itemView.findViewById(R.id.btnDelete);
            rowData = (RelativeLayout) itemView.findViewById(R.id.rowData);
        }
    }

}
