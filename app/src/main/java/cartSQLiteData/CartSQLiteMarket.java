package cartSQLiteData;

/**
 * Created by mac on 4/18/17.
 */

public class CartSQLiteMarket {
    private String id, idMarket, productQuantity, productPriceFinish, img;
    public CartSQLiteMarket() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdMarket() {
        return idMarket;
    }

    public void setIdMarket(String idMarket) {
        this.idMarket = idMarket;
    }

    public String getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(String productQuantity) {
        this.productQuantity = productQuantity;
    }

    public String getProductPriceFinish() {
        return productPriceFinish;
    }

    public void setProductPriceFinish(String productPriceFinish) {
        this.productPriceFinish = productPriceFinish;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
}
