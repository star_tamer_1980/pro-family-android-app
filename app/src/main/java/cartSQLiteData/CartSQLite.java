package cartSQLiteData;

/**
 * Created by mac on 4/18/17.
 */

public class CartSQLite {
    private String id, idProduct, idMarket, productName, productQuantity, productPriceFinish, img;
    public CartSQLite() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(String idProduct) {
        this.idProduct = idProduct;
    }

    public String getIdMarket() {
        return idMarket;
    }

    public void setIdMarket(String idMarket) {
        this.idMarket = idMarket;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(String productQuantity) {
        this.productQuantity = productQuantity;
    }

    public String getProductPriceFinish() {
        return productPriceFinish;
    }

    public void setProductPriceFinish(String productPriceFinish) {
        this.productPriceFinish = productPriceFinish;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
}
