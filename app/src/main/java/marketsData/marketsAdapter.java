package marketsData;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.List;

import libes.globalData;
import profamily.designsite.in.profamilies.HarajImageGalleryAlbum;
import profamily.designsite.in.profamilies.HarajMenuProduct;
import profamily.designsite.in.profamilies.HarajProductDetails;
import profamily.designsite.in.profamilies.MarketDetails;
import profamily.designsite.in.profamilies.R;
import profamily.designsite.in.profamilies.marketMain;

/**
 * Created by mac on 4/18/17.
 */

public class marketsAdapter extends RecyclerView.Adapter<marketsAdapter.ViewHolder> {
    private Context v;
    private List<markets> my_data;
    public marketsAdapter(Context v, List<markets> data_list) {
        this.v = v;
        this.my_data = data_list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_market, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        if(my_data.get(position).getIsWork().equals("1")) {
            holder.tv_isWork.setTextColor(Color.parseColor("#FE6401"));
            holder.tv_isWork.setText("مغلق");
        }else{
            holder.tv_isWork.setTextColor(Color.parseColor("#508f3b"));
        }
        holder.title.setText(my_data.get(position).getName());
        holder.idMainCat.setText(my_data.get(position).getMainCat());
        holder.tvDataroad.setText(my_data.get(position).getDistance());
        holder.tvPriceMini.setText(my_data.get(position).getMiniMumCharge());
        holder.tvPriceDelivery.setText(my_data.get(position).getDelivery());
        holder.ratingBar.setRating(Integer.parseInt(my_data.get(position).getRating()));
        Glide.with(v).load(my_data.get(position).getImg()).into(holder.imageView);
        holder.rowData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("nuRating", my_data.get(position).getNoRate());
                Log.e("countVideo", my_data.get(position).getCountVideo());

                Intent i = new Intent(v.getContext(), MarketDetails.class);

                i.putExtra("idMarket", my_data.get(position).getId());
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                v.getContext().startActivity(i);
                ((marketMain)v.getContext()).finish();
            }
        });
    }

    @Override
    public int getItemCount() {
        return my_data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView title;
        public RatingBar ratingBar;
        public TextView idMainCat;
        public TextView tvPriceMini;
        public TextView tvPriceDelivery;
        public TextView tvDataroad;
        public TextView tv_isWork;
        public ImageView imageView;
        public RelativeLayout rowData;
        public ViewHolder(View itemView) {
            super(itemView);
            title = (TextView)itemView.findViewById(R.id.tv_title);
            title.setTypeface(Typeface.createFromAsset(itemView.getContext().getAssets(), globalData.getFont()));
            tv_isWork = (TextView)itemView.findViewById(R.id.tv_isWork);
            tv_isWork.setTypeface(Typeface.createFromAsset(itemView.getContext().getAssets(), globalData.getFont()));
            tvDataroad = (TextView)itemView.findViewById(R.id.tv_data_road);
            tvDataroad.setTypeface(Typeface.createFromAsset(itemView.getContext().getAssets(), globalData.getFont()));
            ratingBar = (RatingBar)itemView.findViewById(R.id.ratingBar);
            idMainCat = (TextView)itemView.findViewById(R.id.tv_main_cat);
            idMainCat.setTypeface(Typeface.createFromAsset(itemView.getContext().getAssets(), globalData.getFont()));
            tvPriceMini = (TextView)itemView.findViewById(R.id.tv_price_mini);
            tvPriceMini.setTypeface(Typeface.createFromAsset(itemView.getContext().getAssets(), globalData.getFont()));
            tvPriceDelivery = (TextView)itemView.findViewById(R.id.tv_price_delivery);
            tvPriceDelivery.setTypeface(Typeface.createFromAsset(itemView.getContext().getAssets(), globalData.getFont()));
            imageView = (ImageView) itemView.findViewById(R.id.image);
            rowData = (RelativeLayout) itemView.findViewById(R.id.rowData);

        }
    }

}
