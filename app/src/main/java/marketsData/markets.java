package marketsData;

/**
 * Created by mac on 4/18/17.
 */

public class markets {
    private String id;
    private String name;
    private String img;
    private String describtions;
    private String mobile;
    private String workTime;
    private String timeFrom;
    private String timeTo;
    private String rDate;
    private String isActive;
    private String isWork = "0";
    private String idOwner;
    private String place;
    private String city;
    private String district;
    private String mapLat;
    private String mapLong;
    private String miniMumCharge;
    private String delivery;
    private String minPrice;
    private String mainCat;
    private String idMainCat;
    private String distance;
    private String rating;
    private String noRate;
    private String idLastOrder;
    private String numChat;
    private String countVideo;

    public markets() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getDescribtions() {
        return describtions;
    }

    public void setDescribtions(String describtions) {
        this.describtions = describtions;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getWorkTime() {
        return workTime;
    }

    public void setWorkTime(String workTime) {
        this.workTime = workTime;
    }

    public String getTimeFrom() {
        return timeFrom;
    }

    public void setTimeFrom(String timeFrom) {
        this.timeFrom = timeFrom;
    }

    public String getTimeTo() {
        return timeTo;
    }

    public void setTimeTo(String timeTo) {
        this.timeTo = timeTo;
    }

    public String getrDate() {
        return rDate;
    }

    public void setrDate(String rDate) {
        this.rDate = rDate;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getIsWork() {
        return isWork;
    }

    public void setIsWork(String isWork) {
        this.isWork = isWork;
    }

    public String getIdOwner() {
        return idOwner;
    }

    public void setIdOwner(String idOwner) {
        this.idOwner = idOwner;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getMapLat() {
        return mapLat;
    }

    public void setMapLat(String mapLat) {
        this.mapLat = mapLat;
    }

    public String getMapLong() {
        return mapLong;
    }

    public void setMapLong(String mapLong) {
        this.mapLong = mapLong;
    }

    public String getMiniMumCharge() {
        return miniMumCharge;
    }

    public void setMiniMumCharge(String miniMumCharge) {
        this.miniMumCharge = miniMumCharge;
    }

    public String getDelivery() {
        return delivery;
    }

    public void setDelivery(String delivery) {
        this.delivery = delivery;
    }

    public String getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(String minPrice) {
        this.minPrice = minPrice;
    }

    public String getMainCat() {
        return mainCat;
    }

    public void setMainCat(String mainCat) {
        this.mainCat = mainCat;
    }

    public String getIdMainCat() {
        return idMainCat;
    }

    public void setIdMainCat(String idMainCat) {
        this.idMainCat = idMainCat;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getNoRate() {
        return noRate;
    }

    public void setNoRate(String noRate) {
        this.noRate = noRate;
    }

    public String getIdLastOrder() {
        return idLastOrder;
    }

    public void setIdLastOrder(String idLastOrder) {
        this.idLastOrder = idLastOrder;
    }

    public String getNumChat() {
        return numChat;
    }

    public void setNumChat(String numChat) {
        this.numChat = numChat;
    }

    public String getCountVideo() {
        return countVideo;
    }

    public void setCountVideo(String countVideo) {
        this.countVideo = countVideo;
    }
}
