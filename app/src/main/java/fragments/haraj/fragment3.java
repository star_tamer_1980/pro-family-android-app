package fragments.haraj;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import libes.globalData;
import productData.ProductAdapter;
import productData.Product;
import profamily.designsite.in.profamilies.R;


public class fragment3 extends Fragment {

    private RecyclerView mRecyclerView3;
    private GridLayoutManager gridLayoutManager;
    private ProductAdapter adapter3;
    private List<Product> data_list3;
    private String dataTest;
    private ProgressDialog pDialog;
    private ProgressBar pBar3;
    List<String> listProduct = new ArrayList<String>();
    String ReqSuccess;


    private int m_previouse_Total_Count3 = 0;
    private boolean loading = true;
    private int visibleThreshold = 15;
    int firstVisibleItem, visibleItemCount, totalItemCount;
    private TextView txtNotFound;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_fragment3, container, false);
        m_previouse_Total_Count3 = 0;
        txtNotFound = (TextView)v.findViewById(R.id.txtNotFound);
        mRecyclerView3 = (RecyclerView)v.findViewById(R.id.recycleView3);
        mRecyclerView3.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        pBar3 = (ProgressBar)v.findViewById(R.id.progressBar1);
        data_list3 = new ArrayList<>();
        mRecyclerView3.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager mLayoutManager = (LinearLayoutManager) mRecyclerView3
                        .getLayoutManager();
                mRecyclerView3.setLayoutManager(mLayoutManager);
                visibleItemCount = mRecyclerView3.getChildCount();
                totalItemCount = mLayoutManager.getItemCount();
                firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();
                if(totalItemCount == 0){
                    return ;
                }
                if(m_previouse_Total_Count3==totalItemCount){
                    return;
                }
                boolean loadMore = firstVisibleItem + visibleItemCount >= totalItemCount;
                if(loadMore){
                    m_previouse_Total_Count3 = totalItemCount;
                    pBar3.setVisibility(View.VISIBLE);
                    getLoadData(totalItemCount);
                }

            }
        });

        String data[] = new String[2];
        data[0] = globalData.URLhost() + "getHarajBuild.php";
        data[1] = "0";
        loadData3 bts = new loadData3();
        bts.execute(data);

        return v;
    }

    public void getLoadData(Integer limit) {

        pBar3.setVisibility(View.VISIBLE);
        String data[] = new String[2];
        data[0] = globalData.URLhost() + "getHarajBuild.php";
        data[1] = String.valueOf(limit);

        loadData3 bts = new loadData3();
        bts.execute(data);
    }
    public class loadData3 extends AsyncTask<String, Void, Boolean>{

        private String title;
        private String content;
        private String img;
        @SuppressWarnings("deprecation")
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
                nameValuePairs.add(new BasicNameValuePair("limit", params[1]));
                post.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));
                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                if(status == 200){
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    JSONObject jObj = new JSONObject(data.substring(data.indexOf("{"), data.lastIndexOf("}") + 1));
                    ReqSuccess = jObj.getString("success");
                    if(ReqSuccess.equals("1")){
                        dataTest = data;
                        if(jObj.has("data")) {
                            JSONArray wee = jObj.getJSONArray("data");
                            for (int i = 0; i < wee.length(); i++) {
                                JSONObject c = wee.getJSONObject(i);
                                Product dataProduct = new Product();
                                dataProduct.setId(c.getString("id"));
                                dataProduct.setName(c.getString("name"));
                                dataProduct.setImg(c.getString("img"));
                                dataProduct.setMyPlaceAndCity(c.getString("location"));
                                dataProduct.setUserNameProduct(c.getString("userName"));
                                dataProduct.setTimeAdd(c.getString("timeAdd"));
                                dataProduct.setIdMainCat(c.getString("idMainCat"));
                                dataProduct.setActionType("details");
                                data_list3.add(dataProduct);
                            }
                        }

                    }


                    return true;
                }else{
                    Log.e("error client string", "here");
                }
            } catch (ClientProtocolException e) {
                Log.e("error client string", e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("error exp", e.getMessage());
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e("error json", e.getMessage());
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if(result == false){
                Toast.makeText(getContext(), "حدث خطأ، نرجو المحاوله في وقت لاحق", Toast.LENGTH_LONG).show();
            }else{
                if(ReqSuccess.equals("1")){
                    txtNotFound.setVisibility(View.INVISIBLE);
                    if(m_previouse_Total_Count3>0) {
                        adapter3.notifyDataSetChanged();
                        pBar3.setVisibility(View.INVISIBLE);
                    }else{
                        adapter3 = new ProductAdapter(getActivity(), data_list3);
                        mRecyclerView3.setAdapter(adapter3);
                        pBar3.setVisibility(View.INVISIBLE);
                    }
                }else{
                    pBar3.setVisibility(View.INVISIBLE);
                }
            }
        }
    }

}
