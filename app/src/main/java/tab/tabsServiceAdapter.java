package tab;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import profamily.designsite.in.profamilies.MainHaraj;
import profamily.designsite.in.profamilies.R;
import profamily.designsite.in.profamilies.serviceMain;

/**
 * Created by mac on 4/18/17.
 */

public class tabsServiceAdapter extends RecyclerView.Adapter<tabsServiceAdapter.ViewHolder> {
    private Context v;
    private List<tabs> my_data;
    public tabsServiceAdapter(Context v, List<tabs> data_list) {
        this.v = v;
        this.my_data = data_list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_segment, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.title.setText(my_data.get(position).getTitle());
        holder.rowData.setBackgroundColor(Color.parseColor("#ededed"));
        holder.rowData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(my_data.get(position).getIsTabMain() == "1") {
                    Log.e("main idSubCat", my_data.get(position).getId());
                    ((serviceMain )v.getContext()).resetData();
                    ((serviceMain) v.getContext()).idSubCat = my_data.get(position).getId();
                }
                if(my_data.get(position).getIsTabMain() == "2") {
                    Log.e("segment idSubCat", ((serviceMain) v.getContext()).idSubCat);
                    ((serviceMain )v.getContext()).idChangeStatue = my_data.get(position).getId();
                }

                my_data.get(0).setIsSelected("0");
                ((serviceMain )v.getContext()).startLoadData();
                my_data.get(position).setIsSelected("1");
                notifyDataSetChanged();


            }




        });
        if (my_data.get(position).getIsSelected() == "1"){
            my_data.get(position).setIsSelected("0");
            my_data.get(0).setIsSelected("1");
            holder.rowData.setBackgroundColor(Color.parseColor("#FC5109"));
            holder.title.setTextColor(Color.WHITE);
        }else {
            holder.rowData.setBackgroundColor(Color.parseColor("#ededed"));
            holder.title.setTextColor(Color.BLACK);
        }
    }

    @Override
    public int getItemCount() {
        return my_data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView title;
        public LinearLayout rowData;
        public ViewHolder(View itemView) {
            super(itemView);
            title = (TextView)itemView.findViewById(R.id.title);
            rowData = (LinearLayout) itemView.findViewById(R.id.rowData);
        }
    }

}
