package tab;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import profamily.designsite.in.profamilies.HarajImageGalleryAlbum;
import profamily.designsite.in.profamilies.MainHaraj;
import profamily.designsite.in.profamilies.R;

/**
 * Created by mac on 4/18/17.
 */

public class tabsAdapter extends RecyclerView.Adapter<tabsAdapter.ViewHolder> {
    private Context v;
    private List<tabs> my_data;
    public tabsAdapter(Context v, List<tabs> data_list) {
        this.v = v;
        this.my_data = data_list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_segment, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.title.setText(my_data.get(position).getTitle());
        holder.rowData.setBackgroundColor(Color.parseColor("#ededed"));
        holder.rowData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(my_data.get(position).getIsTabMain() == "0"){
                    Log.e("sub idSubCat", ((MainHaraj) v.getContext()).idSubCat);
                    Log.e("id from sub tab", my_data.get(position).getId());
                    ((MainHaraj) v.getContext()).idSubCat = my_data.get(position).getId();
                }
                if(my_data.get(position).getIsTabMain() == "1") {
                    Log.e("main idSubCat", ((MainHaraj) v.getContext()).idSubCat);
                    ((MainHaraj) v.getContext()).idMainCat = my_data.get(position).getId();
                    //((MainHaraj) v.getContext()).idSubCat = my_data.get(position).getId();
                    ((MainHaraj) v.getContext()).filename = my_data.get(position).getFileName();
                    ((MainHaraj) v.getContext()).fileLastPro = my_data.get(position).getFileLastPro();
                    ((MainHaraj )v.getContext()).resetData();
                    ((MainHaraj )v.getContext()).setSubCat();
                    /*
                    ((MainHaraj) v.getContext()).filename = my_data.get(position).getFileName();
                    ((MainHaraj) v.getContext()).fileLastPro = my_data.get(position).getFileLastPro();
                    ((MainHaraj )v.getContext()).resetData();
                    */
                }if(my_data.get(position).getIsTabMain() == "2") {
                    Log.e("segment idSubCat", ((MainHaraj) v.getContext()).idSubCat);
                    ((MainHaraj )v.getContext()).changeStatue = my_data.get(position).getId();
                }

                my_data.get(0).setIsSelected("0");
                ((MainHaraj )v.getContext()).startLoadData();
                my_data.get(position).setIsSelected("1");
                notifyDataSetChanged();


                /*
                    Intent i = new Intent(v.getContext(), HarajImageGalleryAlbum.class);
                    i.putExtra("idComment", id);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    v.getContext().startActivity(i);
                */
            }




        });
        if (my_data.get(position).getIsSelected() == "1"){
            my_data.get(position).setIsSelected("0");
            my_data.get(0).setIsSelected("1");
            holder.rowData.setBackgroundColor(Color.parseColor("#FC5109"));
            holder.title.setTextColor(Color.WHITE);
        }else {
            holder.rowData.setBackgroundColor(Color.parseColor("#ededed"));
            holder.title.setTextColor(Color.BLACK);
        }
    }

    @Override
    public int getItemCount() {
        return my_data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView title;
        public LinearLayout rowData;
        public ViewHolder(View itemView) {
            super(itemView);
            title = (TextView)itemView.findViewById(R.id.title);
            rowData = (LinearLayout) itemView.findViewById(R.id.rowData);
        }
    }

}
