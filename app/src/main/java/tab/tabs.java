package tab;

/**
 * Created by mac on 4/18/17.
 */

public class tabs {
    private String id;
    private String title;
    private String fileName;
    private String fileLastPro;
    private String isTabMain;   // 1 = main, 0 = sub, 2 = statue
    private String isSelected = "0";    // 1 = selected, 0 = not selected

    public tabs() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileLastPro() {
        return fileLastPro;
    }

    public void setFileLastPro(String fileLastPro) {
        this.fileLastPro = fileLastPro;
    }

    public String getIsTabMain() {
        return isTabMain;
    }

    public void setIsTabMain(String isTabMain) {
        this.isTabMain = isTabMain;
    }

    public String getIsSelected() {
        return isSelected;
    }

    public void setIsSelected(String isSelected) {
        this.isSelected = isSelected;
    }
}
