package members;

/**
 * Created by mac on 3/3/18.
 */

public class members {
    public String id;
    public String name;
    public String email;
    public String MainImage;
    public String AlbumImage;
    public String active;
    public String SMS;
    public String registerDate;
    public String loginDate;
    public String isOwnerMarket;
    public String place;
    public String city;
    public String district;
    public String isSMSOrders;
    public String isSMSComments;
    public String isNotifiOrders;
    public String isNotifiComments;

    public void members(){

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMainImage() {
        return MainImage;
    }

    public void setMainImage(String mainImage) {
        MainImage = mainImage;
    }

    public String getAlbumImage() {
        return AlbumImage;
    }

    public void setAlbumImage(String albumImage) {
        AlbumImage = albumImage;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getSMS() {
        return SMS;
    }

    public void setSMS(String SMS) {
        this.SMS = SMS;
    }

    public String getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(String registerDate) {
        this.registerDate = registerDate;
    }

    public String getLoginDate() {
        return loginDate;
    }

    public void setLoginDate(String loginDate) {
        this.loginDate = loginDate;
    }

    public String getIsOwnerMarket() {
        return isOwnerMarket;
    }

    public void setIsOwnerMarket(String isOwnerMarket) {
        this.isOwnerMarket = isOwnerMarket;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getIsSMSOrders() {
        return isSMSOrders;
    }

    public void setIsSMSOrders(String isSMSOrders) {
        this.isSMSOrders = isSMSOrders;
    }

    public String getIsSMSComments() {
        return isSMSComments;
    }

    public void setIsSMSComments(String isSMSComments) {
        this.isSMSComments = isSMSComments;
    }

    public String getIsNotifiOrders() {
        return isNotifiOrders;
    }

    public void setIsNotifiOrders(String isNotifiOrders) {
        this.isNotifiOrders = isNotifiOrders;
    }

    public String getIsNotifiComments() {
        return isNotifiComments;
    }

    public void setIsNotifiComments(String isNotifiComments) {
        this.isNotifiComments = isNotifiComments;
    }
}
