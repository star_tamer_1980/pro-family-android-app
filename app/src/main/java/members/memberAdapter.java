package members;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.List;

import libes.globalData;
import noti.notificationAdminAdapter;
import productData.Product;
import profamily.designsite.in.profamilies.HarajMenuProduct;
import profamily.designsite.in.profamilies.HarajProductDetails;
import profamily.designsite.in.profamilies.R;
import profamily.designsite.in.profamilies.marketProductDetails;
import profamily.designsite.in.profamilies.notidetails;

/**
 * Created by mac on 4/18/17.
 */

public class memberAdapter extends RecyclerView.Adapter<memberAdapter.ViewHolder> {
    private Context v;
    private List<members> my_data;
    public memberAdapter(Context v, List<members> data_list) {
        this.v = v;
        this.my_data = data_list;
    }
    @Override
    public memberAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_noti, parent, false);
        return new memberAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(memberAdapter.ViewHolder holder, int position) {
        final String id = my_data.get(position).getId();

        holder.rowData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(v.getContext(), notidetails.class);
                i.putExtra("id", id);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                v.getContext().startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return my_data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView title;
        public TextView content;
        public TextView dateAdd;
        public RelativeLayout rowData;

        public ViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            title.setTypeface(Typeface.createFromAsset(itemView.getContext().getAssets(), globalData.getFont()));
            content = (TextView) itemView.findViewById(R.id.content);
            content.setTypeface(Typeface.createFromAsset(itemView.getContext().getAssets(), globalData.getFont()));
            dateAdd = (TextView) itemView.findViewById(R.id.timeData);
            dateAdd.setTypeface(Typeface.createFromAsset(itemView.getContext().getAssets(), globalData.getFont()));
            rowData = (RelativeLayout) itemView.findViewById(R.id.rowData);
        }
    }

}
