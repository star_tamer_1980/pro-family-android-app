package productData;

import java.io.Serializable;

/**
 * Created by mac on 4/18/17.
 */

public class Product implements Serializable {
    private String id, idMarket, marketName, marketImage, idUser, idMainCat, idSubCat, catName, place, city, district, isActive = "0", isAllow = "0", isMarketWork, isSum, actionType /*نوع الطلب تفاصيل او تعديل*/;
    private String name, description, img, albumImg, albumFullImg, allImages, price, contact, miniMumQuantity, MyPlaceAndCity, UserNameProduct, TimeAdd, rating, packageData, packageId, videoName, videoImageName;

    public Product() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdMarket() {
        return idMarket;
    }

    public void setIdMarket(String idMarket) {
        this.idMarket = idMarket;
    }

    public String getMarketName() {
        return marketName;
    }

    public void setMarketName(String marketName) {
        this.marketName = marketName;
    }

    public String getMarketImage() {
        return marketImage;
    }

    public void setMarketImage(String marketImage) {
        this.marketImage = marketImage;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getIdMainCat() {
        return idMainCat;
    }

    public void setIdMainCat(String idMainCat) {
        this.idMainCat = idMainCat;
    }

    public String getIdSubCat() {
        return idSubCat;
    }

    public void setIdSubCat(String idSubCat) {
        this.idSubCat = idSubCat;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getAlbumImg() {
        return albumImg;
    }

    public void setAlbumImg(String albumImg) {
        this.albumImg = albumImg;
    }

    public String getAllImages() {
        return allImages;
    }

    public void setAllImages(String allImages) {
        this.allImages = allImages;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getMiniMumQuantity() {
        return miniMumQuantity;
    }

    public void setMiniMumQuantity(String miniMumQuantity) {
        this.miniMumQuantity = miniMumQuantity;
    }

    public String getMyPlaceAndCity() {
        return MyPlaceAndCity;
    }

    public void setMyPlaceAndCity(String myPlaceAndCity) {
        MyPlaceAndCity = myPlaceAndCity;
    }

    public String getUserNameProduct() {
        return UserNameProduct;
    }

    public void setUserNameProduct(String userNameProduct) {
        UserNameProduct = userNameProduct;
    }

    public String getTimeAdd() {
        return TimeAdd;
    }

    public void setTimeAdd(String timeAdd) {
        TimeAdd = timeAdd;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getIsAllow() {
        return isAllow;
    }

    public void setIsAllow(String isAllow) {
        this.isAllow = isAllow;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public String getIsMarketWork() {
        return isMarketWork;
    }

    public void setIsMarketWork(String isMarketWork) {
        this.isMarketWork = isMarketWork;
    }

    public String getIsSum() {
        return isSum;
    }

    public void setIsSum(String isSum) {
        this.isSum = isSum;
    }

    public String getAlbumFullImg() {
        return albumFullImg;
    }

    public void setAlbumFullImg(String albumFullImg) {
        this.albumFullImg = albumFullImg;
    }

    public String getPackageData() {
        return packageData;
    }

    public void setPackageData(String packageData) {
        this.packageData = packageData;
    }

    public String getPackageId() {
        return packageId;
    }

    public void setPackageId(String packageId) {
        this.packageId = packageId;
    }

    public String getVideoName() {
        return videoName;
    }

    public void setVideoName(String videoName) {
        this.videoName = videoName;
    }

    public String getVideoImageName() {
        return videoImageName;
    }

    public void setVideoImageName(String videoImageName) {
        this.videoImageName = videoImageName;
    }
}
