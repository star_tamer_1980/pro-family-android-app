package productData;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.io.Serializable;
import java.util.List;

import libes.globalData;
import profamily.designsite.in.profamilies.HarajMenuProduct;
import profamily.designsite.in.profamilies.HarajProductDetails;
import profamily.designsite.in.profamilies.R;
import profamily.designsite.in.profamilies.marketListProduct;
import profamily.designsite.in.profamilies.marketProductDetails;

/**
 * Created by mac on 4/18/17.
 */

public class ProductMarketAdapter extends RecyclerView.Adapter<ProductMarketAdapter.ViewHolder> {
    private Context v;
    private List<Product> my_data;
    public ProductMarketAdapter(Context v, List<Product> data_list) {
        this.v = v;
        this.my_data = data_list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_product_test, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.title.setText(my_data.get(position).getName() + my_data.get(position).getIsAllow());
        holder.content.setText(my_data.get(position).getDescription());
        holder.price.setText(my_data.get(position).getPrice());
        holder.ratingBar.setRating(Integer.parseInt(my_data.get(position).getRating()));
        Glide.with(v).load(my_data.get(position).getImg()).into(holder.imageView);
        if(!my_data.get(position).getIsActive().equals("0") || !my_data.get(position).getIsAllow().equals("0")){
            holder.imageBlock.setVisibility(View.VISIBLE);
        }
        if(position % 2 == 1) {
            holder.rowData.setBackgroundColor(Color.parseColor("#E9EBEE"));
        }else{
            holder.rowData.setBackgroundColor(Color.parseColor("#ffffff"));
        }
        holder.rowData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    if(my_data.get(position).getActionType().equals("details")){
                        Intent i = new Intent(v.getContext(), marketProductDetails.class);
                        i.putExtra("product", (Serializable) my_data.get(position));
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        v.getContext().startActivity(i);
                        ((marketListProduct)v.getContext()).finish();
                    }else{
                        Toast.makeText(v.getContext(), "Market edit and id is : ", Toast.LENGTH_SHORT).show();
                    }
            }
        });
    }

    @Override
    public int getItemCount() {
        return my_data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView title;
        public TextView content;
        public TextView price;
        public RatingBar ratingBar;
        public ImageView imageView;
        public RelativeLayout rowData;
        public ImageView imageBlock;

        public ViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            title.setTypeface(Typeface.createFromAsset(itemView.getContext().getAssets(), globalData.getFont()));
            content = (TextView) itemView.findViewById(R.id.content);
            content.setTypeface(Typeface.createFromAsset(itemView.getContext().getAssets(), globalData.getFont()));
            price = (TextView) itemView.findViewById(R.id.price);
            price.setTypeface(Typeface.createFromAsset(itemView.getContext().getAssets(), globalData.getFont()));
            ratingBar = (RatingBar)itemView.findViewById(R.id.ratingBar);
            imageView = (ImageView) itemView.findViewById(R.id.image);
            rowData = (RelativeLayout) itemView.findViewById(R.id.rowData);
            imageBlock = (ImageView)itemView.findViewById(R.id.image_block);
        }
    }

}
