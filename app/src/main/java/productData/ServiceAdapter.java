package productData;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.List;

import libes.globalData;
import profamily.designsite.in.profamilies.HarajProductDetails;
import profamily.designsite.in.profamilies.R;
import profamily.designsite.in.profamilies.marketProductDetails;
import profamily.designsite.in.profamilies.serviceAdd;
import profamily.designsite.in.profamilies.serviceProductDetails;

/**
 * Created by mac on 4/18/17.
 */

public class ServiceAdapter extends RecyclerView.Adapter<ServiceAdapter.ViewHolder> {
    private Context v;
    private List<Product> my_data;
    public ServiceAdapter(Context v, List<Product> data_list) {
        this.v = v;
        this.my_data = data_list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_service, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.title.setText(my_data.get(position).getName());
        holder.PlaceAndCity.setText(my_data.get(position).getMyPlaceAndCity());
        holder.descrip.setText(my_data.get(position).getDescription());
        Glide.with(v).load(my_data.get(position).getImg()).into(holder.imageView);
        if(position % 2 == 1) {
            holder.rowData.setBackgroundColor(Color.parseColor("#E9EBEE"));
        }
        holder.rowData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    Intent i = new Intent(v.getContext(), serviceProductDetails.class);
                    i.putExtra("id", my_data.get(position).getId());
                    i.putExtra("title", my_data.get(position).getName());
                    i.putExtra("content", my_data.get(position).getDescription());
                    i.putExtra("tel", my_data.get(position).getContact());
                    i.putExtra("rating", my_data.get(position).getRating());
                    i.putExtra("idUser", my_data.get(position).getIdUser());
                    i.putExtra("imgName", my_data.get(position).getVideoImageName());
                    i.putExtra("img", my_data.get(position).getImg());
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    v.getContext().startActivity(i);

            }
        });
    }

    @Override
    public int getItemCount() {
        return my_data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView title;
        public ImageView imageView;
        public TextView PlaceAndCity;
        public TextView descrip;
        public RelativeLayout rowData;

        public ViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            title.setTypeface(Typeface.createFromAsset(itemView.getContext().getAssets(), globalData.getFont()));
            imageView = (ImageView) itemView.findViewById(R.id.image);
            PlaceAndCity = (TextView) itemView.findViewById(R.id.location);
            PlaceAndCity.setTypeface(Typeface.createFromAsset(itemView.getContext().getAssets(), globalData.getFont()));
            descrip = (TextView) itemView.findViewById(R.id.descrip);
            descrip.setTypeface(Typeface.createFromAsset(itemView.getContext().getAssets(), globalData.getFont()));
            rowData = (RelativeLayout) itemView.findViewById(R.id.rowData);

        }
    }

}
