package productData;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.List;

import libes.globalData;
import profamily.designsite.in.profamilies.HarajMenuProduct;
import profamily.designsite.in.profamilies.HarajProductDetails;
import profamily.designsite.in.profamilies.R;
import profamily.designsite.in.profamilies.marketProductDetails;

/**
 * Created by mac on 4/18/17.
 */

public class ProductSimilarAdapter extends RecyclerView.Adapter<ProductSimilarAdapter.ViewHolder> {
    private Context v;
    private List<Product> my_data;
    public ProductSimilarAdapter(Context v, List<Product> data_list) {
        this.v = v;
        this.my_data = data_list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_product_similar, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final String id = my_data.get(position).getId();
        Glide.with(v).load(my_data.get(position).getImg()).into(holder.imageView);
        Log.e("name", my_data.get(position).getName());
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                        Intent i = new Intent(v.getContext(), HarajProductDetails.class);
                        i.putExtra("id", id);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        v.getContext().startActivity(i);


            }
        });
    }

    @Override
    public int getItemCount() {
        return my_data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public ImageView imageView;

        public ViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.image);
        }
    }

}
