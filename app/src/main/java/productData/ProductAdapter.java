package productData;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.List;

import libes.globalData;
import profamily.designsite.in.profamilies.HarajMenuProduct;
import profamily.designsite.in.profamilies.HarajProductDetails;
import profamily.designsite.in.profamilies.R;
import profamily.designsite.in.profamilies.marketProductDetails;
import profamily.designsite.in.profamilies.notidetails;

/**
 * Created by mac on 4/18/17.
 */

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder> {
    private Context v;
    private List<Product> my_data;
    public ProductAdapter(Context v, List<Product> data_list) {
        this.v = v;
        this.my_data = data_list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_product, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final String id = my_data.get(position).getId();
        final String actionType = my_data.get(position).getActionType();
        final String idMainCat = my_data.get(position).getIdMainCat();
        final String title = my_data.get(position).getName();
        final String isActive = my_data.get(position).getIsActive();
        final String isAllow = my_data.get(position).getIsAllow();
        holder.idProduct.setText(my_data.get(position).getId());
        holder.title.setText(my_data.get(position).getName());
        holder.PlaceAndCity.setText(my_data.get(position).getMyPlaceAndCity());
        holder.TimeData.setText(my_data.get(position).getTimeAdd());
        holder.username.setText(my_data.get(position).getUserNameProduct());
        holder.idMainCat.setText(my_data.get(position).getIdMainCat());
        holder.actionType.setText(my_data.get(position).getActionType());
        Glide.with(v).load(my_data.get(position).getImg()).into(holder.imageView);
        Log.e("name", my_data.get(position).getName());
        if(!isActive.equals("0") || !isAllow.equals("0")){
            holder.imageBlock.setVisibility(View.VISIBLE);
        }
        if(position % 2 == 1) {
            holder.rowData.setBackgroundColor(Color.parseColor("#E9EBEE"));
        }
        holder.rowData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(idMainCat.equals("4")) {
                        Intent i = new Intent(v.getContext(), HarajProductDetails.class);
                        i.putExtra("id", id);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        v.getContext().startActivity(i);

                }else{
                    if(actionType.equals("details")){
                        Intent i = new Intent(v.getContext(), marketProductDetails.class);
                        i.putExtra("id", id);
                        i.putExtra("title", title);
                        i.putExtra("description", my_data.get(position).getDescription());
                        i.putExtra("price", my_data.get(position).getPrice());
                        i.putExtra("idMarket", my_data.get(position).getIdMarket());
                        i.putExtra("img", my_data.get(position).getImg());
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        v.getContext().startActivity(i);
                    }else{
                        Toast.makeText(v.getContext(), "Market edit and id is : " + id, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return my_data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView idProduct;
        public TextView title;
        public ImageView imageView;
        public TextView PlaceAndCity;
        public TextView TimeData;
        public TextView username;
        public RelativeLayout rowData;
        public TextView idMainCat;
        public TextView actionType;
        public ImageView imageBlock;

        public ViewHolder(View itemView) {
            super(itemView);
            idProduct = (TextView) itemView.findViewById(R.id.id_product);
            title = (TextView) itemView.findViewById(R.id.title);
            title.setTypeface(Typeface.createFromAsset(itemView.getContext().getAssets(), globalData.getFont()));
            imageView = (ImageView) itemView.findViewById(R.id.image);
            PlaceAndCity = (TextView) itemView.findViewById(R.id.location);
            PlaceAndCity.setTypeface(Typeface.createFromAsset(itemView.getContext().getAssets(), globalData.getFont()));
            TimeData = (TextView) itemView.findViewById(R.id.timeData);
            TimeData.setTypeface(Typeface.createFromAsset(itemView.getContext().getAssets(), globalData.getFont()));
            username = (TextView) itemView.findViewById(R.id.username);
            username.setTypeface(Typeface.createFromAsset(itemView.getContext().getAssets(), globalData.getFont()));
            rowData = (RelativeLayout) itemView.findViewById(R.id.rowData);
            idMainCat = (TextView) itemView.findViewById(R.id.id_mainCat);
            actionType = (TextView)itemView.findViewById(R.id.action_type);
            imageBlock = (ImageView)itemView.findViewById(R.id.image_block);
        }
    }

}
