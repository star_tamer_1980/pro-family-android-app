package menuSettings;

/**
 * Created by mac on 2/24/18.
 */

public class menuSettings {
    public String menuId;
    public String menuName;
    public String menuIcon;
    public menuSettings(){

    }

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getMenuIcon() {
        return menuIcon;
    }

    public void setMenuIcon(String menuIcon) {
        this.menuIcon = menuIcon;
    }

}
