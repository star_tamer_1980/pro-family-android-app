package menuSettings;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import libes.KeyValueShared;
import profamily.designsite.in.profamilies.HarajAddSwicherMedia;
import profamily.designsite.in.profamilies.HarajSettings;
import profamily.designsite.in.profamilies.MyAdv;
import profamily.designsite.in.profamilies.R;
import profamily.designsite.in.profamilies.harajFavProduct;
import profamily.designsite.in.profamilies.harajListChat;
import profamily.designsite.in.profamilies.pageDetails;
import profamily.designsite.in.profamilies.serviceAddAgreement;
import profamily.designsite.in.profamilies.serviceMe;
import profamily.designsite.in.profamilies.serviceMenu;
import profamily.designsite.in.profamilies.serviceMyFavourite;
import profamily.designsite.in.profamilies.settingsApp_myCity;
import profamily.designsite.in.profamilies.userLogin;
import profamily.designsite.in.profamilies.userRegister;

/**
 * Created by mac on 4/18/17.
 */

public class menuServiceAdapter extends RecyclerView.Adapter<menuServiceAdapter.ViewHolder> {
    private Context v;
    private List<menuSettings> my_data;
    public menuServiceAdapter(Context v, List<menuSettings> data_list) {
        this.v = v;
        this.my_data = data_list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_menu, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.title.setText(my_data.get(position).getMenuName());
        //Glide.with(v).load(v.getResources().getIdentifier(my_data.get(position).getMenuIcon(), "drawable", v.getPackageName()).into(holder.imgIcon);
        //Glide.with(v).load(my_data.get(position).getImg()).into(holder.imageView);
        //int imageId = v.getResources().getIdentifier(my_data.get(position).getMenuIcon(), "drawable", v.getPackageName());
        //holder.imgIcon.setImageResource(imageId);
        holder.rowData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(), my_data.get(position).getMenuName(), Toast.LENGTH_SHORT).show();
                Toast.makeText(v.getContext(), my_data.get(position).getMenuId(), Toast.LENGTH_SHORT).show();
                if(my_data.get(position).getMenuId().equals("0")){
                    Intent i = new Intent(v.getContext(), serviceAddAgreement.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    v.getContext().startActivity(i);
                }else if(my_data.get(position).getMenuId().equals("1")){
                    Intent i = new Intent(v.getContext(), serviceMyFavourite.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    v.getContext().startActivity(i);
                }else if(my_data.get(position).getMenuId().equals("2")){
                    Intent i = new Intent(v.getContext(), serviceMe.class);
                    i.putExtra("idUser", KeyValueShared.getId(v.getContext()));
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    v.getContext().startActivity(i);
                    ((serviceMenu)v.getContext()).finish();
                }else if(my_data.get(position).getMenuId().equals("3")){
                    Intent i = new Intent(v.getContext(), settingsApp_myCity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    v.getContext().startActivity(i);
                }else if(my_data.get(position).getMenuId().equals("4")){
                    Intent i = new Intent(v.getContext(), pageDetails.class);
                    i.putExtra("id", "8");
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    v.getContext().startActivity(i);
                }else if(my_data.get(position).getMenuId().equals("5")){
                    Intent i = new Intent(v.getContext(), pageDetails.class);
                    i.putExtra("id", "11");
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    v.getContext().startActivity(i);
                }else if(my_data.get(position).getMenuId().equals("6")){
                    Intent i = new Intent(v.getContext(), pageDetails.class);
                    i.putExtra("id", "12");
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    v.getContext().startActivity(i);
                }else if(my_data.get(position).getMenuId().equals("7")){
                    KeyValueShared.setStatue(v.getContext(), "0");
                    KeyValueShared.setId(v.getContext(), "0");
                    KeyValueShared.setName(v.getContext(), "");
                    KeyValueShared.setTel(v.getContext(), "");
                    KeyValueShared.setEmail(v.getContext(), "");
                    KeyValueShared.setSettingsAppisSMSComments(v.getContext(), "0");
                    KeyValueShared.setSettingsAppisSMSOrders(v.getContext(), "0");
                    KeyValueShared.setSettingsAppisNotifiComments(v.getContext(), "0");
                    KeyValueShared.setSettingsAppisNotifiOrders(v.getContext(), "0");
                    ((serviceMenu)v.getContext()).setMenu();
                }else if(my_data.get(position).getMenuId().equals("8")){
                    Intent i = new Intent(v.getContext(), userRegister.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    v.getContext().startActivity(i);
                }else if(my_data.get(position).getMenuId().equals("9")){
                    Intent i = new Intent(v.getContext(), userLogin.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    v.getContext().startActivity(i);
                }else if(my_data.get(position).getMenuId().equals("10")){
                    Toast.makeText(v.getContext(), "محادثة الإداره لم يتم عملها", Toast.LENGTH_SHORT).show();
                }else if(my_data.get(position).getMenuId().equals("10")){
                    Toast.makeText(v.getContext(), "محادثة الإداره لم يتم عملها", Toast.LENGTH_SHORT).show();
                }else if(my_data.get(position).getMenuId().equals("11")){
                    Intent i = new Intent(v.getContext(), harajListChat.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    v.getContext().startActivity(i);
                }



                //notifyDataSetChanged();
                /*
                    Intent i = new Intent(v.getContext(), HarajImageGalleryAlbum.class);
                    i.putExtra("idComment", id);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    v.getContext().startActivity(i);
                */
            }




        });
    }

    @Override
    public int getItemCount() {
        return my_data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView title;
        public ImageView imgIcon;
        public RelativeLayout rowData;
        public ViewHolder(View itemView) {
            super(itemView);
            title = (TextView)itemView.findViewById(R.id.title);
            imgIcon = (ImageView)itemView.findViewById(R.id.imgIcon);
            rowData = (RelativeLayout) itemView.findViewById(R.id.rowData);
        }
    }

}
