package menuSettings;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import libes.KeyValueShared;
import profamily.designsite.in.profamilies.HarajAddAgreement;
import profamily.designsite.in.profamilies.HarajAddSwicherMedia;
import profamily.designsite.in.profamilies.HarajSettings;
import profamily.designsite.in.profamilies.MyAdv;
import profamily.designsite.in.profamilies.R;
import profamily.designsite.in.profamilies.harajFavProduct;
import profamily.designsite.in.profamilies.harajListChat;
import profamily.designsite.in.profamilies.pageDetails;
import profamily.designsite.in.profamilies.settingsApp_myCity;
import profamily.designsite.in.profamilies.userLogin;
import profamily.designsite.in.profamilies.userRegister;

/**
 * Created by mac on 4/18/17.
 */

public class menuAddAdapter extends RecyclerView.Adapter<menuAddAdapter.ViewHolder> {
    private Context v;
    private List<menuSettings> my_data;
    public menuAddAdapter(Context v, List<menuSettings> data_list) {
        this.v = v;
        this.my_data = data_list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_menu, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.title.setText(my_data.get(position).getMenuName());
        holder.rowData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(), my_data.get(position).getMenuName(), Toast.LENGTH_SHORT).show();
                Toast.makeText(v.getContext(), my_data.get(position).getMenuId(), Toast.LENGTH_SHORT).show();


                Intent i = new Intent(v.getContext(), HarajAddAgreement.class);
                i.putExtra("idCat", my_data.get(position).getMenuId());
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                v.getContext().startActivity(i);
                ((Activity)v.getContext()).finish();

                //notifyDataSetChanged();
                /*
                    Intent i = new Intent(v.getContext(), HarajImageGalleryAlbum.class);
                    i.putExtra("idComment", id);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    v.getContext().startActivity(i);
                */
            }




        });
    }

    @Override
    public int getItemCount() {
        return my_data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView title;
        public ImageView imgIcon;
        public RelativeLayout rowData;
        public ViewHolder(View itemView) {
            super(itemView);
            title = (TextView)itemView.findViewById(R.id.title);
            imgIcon = (ImageView)itemView.findViewById(R.id.imgIcon);
            rowData = (RelativeLayout) itemView.findViewById(R.id.rowData);
        }
    }

}
