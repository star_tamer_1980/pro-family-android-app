package noti;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import libes.globalData;
import profamily.designsite.in.profamilies.R;
import profamily.designsite.in.profamilies.notidetails;

/**
 * Created by mac on 4/18/17.
 */

public class notificationAdminAdapter extends RecyclerView.Adapter<notificationAdminAdapter.ViewHolder> {
    private Context v;
    private List<notificationAdmin> my_data;
    public notificationAdminAdapter(Context v, List<notificationAdmin> data_list) {
        this.v = v;
        this.my_data = data_list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_noti, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final String id = my_data.get(position).getId();
        holder.title.setText(my_data.get(position).getTitle());
        holder.content.setText(my_data.get(position).getContent());
        holder.dateAdd.setText(my_data.get(position).getDateAdd());

        holder.rowData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(v.getContext(), notidetails.class);
                i.putExtra("id", id);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                v.getContext().startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return my_data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView title;
        public TextView content;
        public TextView dateAdd;
        public LinearLayout rowData;

        public ViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            title.setTypeface(Typeface.createFromAsset(itemView.getContext().getAssets(), globalData.getFont()));
            content = (TextView) itemView.findViewById(R.id.content);
            content.setTypeface(Typeface.createFromAsset(itemView.getContext().getAssets(), globalData.getFont()));
            dateAdd = (TextView) itemView.findViewById(R.id.timeData);
            dateAdd.setTypeface(Typeface.createFromAsset(itemView.getContext().getAssets(), globalData.getFont()));
            rowData = (LinearLayout) itemView.findViewById(R.id.rowData);
        }
    }

}
