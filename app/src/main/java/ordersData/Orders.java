package ordersData;

/**
 * Created by mac on 4/18/17.
 */

public class Orders {
   private String id, marketName, price, statue, statueTxt, img, dateAdd, delivery, statueForAction;

    public Orders() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMarketName() {
        return marketName;
    }

    public void setMarketName(String marketName) {
        this.marketName = marketName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getStatue() {
        return statue;
    }

    public void setStatue(String statue) {
        this.statue = statue;
    }

    public String getStatueTxt() {
        return statueTxt;
    }

    public void setStatueTxt(String statueTxt) {
        this.statueTxt = statueTxt;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getDateAdd() {
        return dateAdd;
    }

    public void setDateAdd(String dateAdd) {
        this.dateAdd = dateAdd;
    }

    public String getDelivery() {
        return delivery;
    }

    public void setDelivery(String delivery) {
        this.delivery = delivery;
    }

    public String getStatueForAction() {
        return statueForAction;
    }

    public void setStatueForAction(String statueForAction) {
        this.statueForAction = statueForAction;
    }
}
