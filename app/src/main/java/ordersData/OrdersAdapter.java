package ordersData;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.List;

import libes.globalData;
import profamily.designsite.in.profamilies.HarajMenuProduct;
import profamily.designsite.in.profamilies.HarajProductDetails;
import profamily.designsite.in.profamilies.OrdersDataForMarketDetails;
import profamily.designsite.in.profamilies.R;
import profamily.designsite.in.profamilies.marketProductDetails;
import profamily.designsite.in.profamilies.orderDetails;

/**
 * Created by mac on 4/18/17.
 */

public class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.ViewHolder> {
    private Context v;
    private List<Orders> my_data;
    public OrdersAdapter(Context v, List<Orders> data_list) {
        this.v = v;
        this.my_data = data_list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_order, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.title.setText(my_data.get(position).getMarketName());
        holder.PlaceAndCity.setText(my_data.get(position).getPrice());
        holder.TimeData.setText(my_data.get(position).getStatueTxt());
        holder.username.setText(my_data.get(position).getDateAdd());
        Glide.with(v).load(my_data.get(position).getImg()).into(holder.imageView);

        holder.rowData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(my_data.get(position).getStatueForAction() == "0") {
                    Intent i = new Intent(v.getContext(), orderDetails.class);
                    i.putExtra("id", my_data.get(position).getId());
                    i.putExtra("title", my_data.get(position).getMarketName());
                    i.putExtra("price", my_data.get(position).getPrice());
                    i.putExtra("statue", my_data.get(position).getStatue());
                    i.putExtra("statueTxt", my_data.get(position).getStatueTxt());
                    i.putExtra("timeAdd", my_data.get(position).getDateAdd());
                    i.putExtra("deliveryPrice", my_data.get(position).getDelivery());
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    v.getContext().startActivity(i);
                }else{
                    Intent i = new Intent(v.getContext(), OrdersDataForMarketDetails.class);
                    i.putExtra("id", my_data.get(position).getId());
                    i.putExtra("title", my_data.get(position).getMarketName());
                    i.putExtra("price", my_data.get(position).getPrice());
                    i.putExtra("statue", my_data.get(position).getStatue());
                    i.putExtra("statueTxt", my_data.get(position).getStatueTxt());
                    i.putExtra("timeAdd", my_data.get(position).getDateAdd());
                    i.putExtra("deliveryPrice", my_data.get(position).getDelivery());
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    v.getContext().startActivity(i);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return my_data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView idProduct;
        public TextView title;
        public ImageView imageView;
        public TextView PlaceAndCity;
        public TextView TimeData;
        public TextView username;
        public RelativeLayout rowData;
        public ImageView imageBlock;

        public ViewHolder(View itemView) {
            super(itemView);
            idProduct = (TextView) itemView.findViewById(R.id.id_product);
            title = (TextView) itemView.findViewById(R.id.title);
            title.setTypeface(Typeface.createFromAsset(itemView.getContext().getAssets(), globalData.getFont()));
            imageView = (ImageView) itemView.findViewById(R.id.image);
            PlaceAndCity = (TextView) itemView.findViewById(R.id.location);
            PlaceAndCity.setTypeface(Typeface.createFromAsset(itemView.getContext().getAssets(), globalData.getFont()));
            TimeData = (TextView) itemView.findViewById(R.id.timeData);
            TimeData.setTypeface(Typeface.createFromAsset(itemView.getContext().getAssets(), globalData.getFont()));
            username = (TextView) itemView.findViewById(R.id.username);
            username.setTypeface(Typeface.createFromAsset(itemView.getContext().getAssets(), globalData.getFont()));
            rowData = (RelativeLayout) itemView.findViewById(R.id.rowData);
            imageBlock = (ImageView)itemView.findViewById(R.id.image_block);
        }
    }

}
