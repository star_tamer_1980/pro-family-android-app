package ordersData;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import libes.globalData;
import profamily.designsite.in.profamilies.OrdersDataForMarketDetails;
import profamily.designsite.in.profamilies.R;
import profamily.designsite.in.profamilies.orderDetails;

/**
 * Created by mac on 4/18/17.
 */

public class OrdersProductAdapter extends RecyclerView.Adapter<OrdersProductAdapter.ViewHolder> {
    private Context v;
    private List<Orders> my_data;
    public OrdersProductAdapter(Context v, List<Orders> data_list) {
        this.v = v;
        this.my_data = data_list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_order_product, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.title.setText(my_data.get(position).getMarketName());
        holder.username.setText(my_data.get(position).getPrice());
        Glide.with(v).load(my_data.get(position).getImg()).into(holder.imageView);

    }

    @Override
    public int getItemCount() {
        return my_data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView idProduct;
        public TextView title;
        public ImageView imageView;
        public TextView username;
        public RelativeLayout rowData;

        public ViewHolder(View itemView) {
            super(itemView);
            idProduct = (TextView) itemView.findViewById(R.id.id_product);
            title = (TextView) itemView.findViewById(R.id.title);
            title.setTypeface(Typeface.createFromAsset(itemView.getContext().getAssets(), globalData.getFont()));
            imageView = (ImageView) itemView.findViewById(R.id.image);
            username = (TextView) itemView.findViewById(R.id.username);
            username.setTypeface(Typeface.createFromAsset(itemView.getContext().getAssets(), globalData.getFont()));
            rowData = (RelativeLayout) itemView.findViewById(R.id.rowData);
        }
    }

}
