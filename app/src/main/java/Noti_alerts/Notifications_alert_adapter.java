package Noti_alerts;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import libes.globalData;
import profamily.designsite.in.profamilies.HarajProductDetails;
import profamily.designsite.in.profamilies.OrdersDataForMarketDetails;
import profamily.designsite.in.profamilies.R;
import profamily.designsite.in.profamilies.chatUsers;
import profamily.designsite.in.profamilies.notidetails;
import profamily.designsite.in.profamilies.notification_alerts;

/**
 * Created by mac on 4/18/17.
 */

public class Notifications_alert_adapter extends RecyclerView.Adapter<Notifications_alert_adapter.ViewHolder> {
    private Context v;
    private List<Notifications_alert> my_data;
    public Notifications_alert_adapter(Context v, List<Notifications_alert> data_list) {
        this.v = v;
        this.my_data = data_list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_noti, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final String typeAlert = my_data.get(position).getTypeAlert();
        holder.title.setText(my_data.get(position).getTitle());
        holder.content.setText(my_data.get(position).getContent());
        holder.dateAdd.setText(my_data.get(position).getDateAdd());

        if(position % 2 == 1) {
            holder.rowData.setBackgroundColor(Color.parseColor("#fbefe7"));
        }else{
            holder.rowData.setBackgroundColor(Color.parseColor("#ffffff"));
        }
        holder.rowData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(typeAlert.equals("0") || typeAlert.equals("1")) {
                    Intent i = new Intent(v.getContext(), HarajProductDetails.class);
                    i.putExtra("id", my_data.get(position).getIdSecond());
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    v.getContext().startActivity(i);
                }else if(typeAlert.equals("2")) {
                    Intent i = new Intent(v.getContext(), chatUsers.class);
                    i.putExtra("otherUser", my_data.get(position).getIdSecond());
                    i.putExtra("username", my_data.get(position).getName());
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    v.getContext().startActivity(i);

                }else if(typeAlert.equals("5") || typeAlert.equals("6") || typeAlert.equals("7") || typeAlert.equals("8") || typeAlert.equals("11") || typeAlert.equals("25")) {
                    Toast.makeText(v.getContext(), "لم يتم الانتهاء منها", Toast.LENGTH_SHORT).show();
                }else if(typeAlert.equals("10")) {
                    Toast.makeText(v.getContext(), "لم يتم الانتهاء منها", Toast.LENGTH_SHORT).show();
                }else if(typeAlert.equals("19")) {
                    Toast.makeText(v.getContext(), "لم يتم الانتهاء منها", Toast.LENGTH_SHORT).show();
                }else if(typeAlert.equals("20") || typeAlert.equals("21") || typeAlert.equals("22") || typeAlert.equals("23")) {
                    Intent i = new Intent(v.getContext(), notidetails.class);
                    i.putExtra("id", my_data.get(position).getId());
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    v.getContext().startActivity(i);
                }
                Log.e("typealert", typeAlert);
                //((notification_alerts)v.getContext()).finish();
            }
        });
    }

    @Override
    public int getItemCount() {
        return my_data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView content;
        public TextView dateAdd;
        public TextView title;
        public LinearLayout rowData;

        public ViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            title.setTypeface(Typeface.createFromAsset(itemView.getContext().getAssets(), globalData.getFont()));
            content = (TextView) itemView.findViewById(R.id.content);
            content.setTypeface(Typeface.createFromAsset(itemView.getContext().getAssets(), globalData.getFont()));
            dateAdd = (TextView) itemView.findViewById(R.id.timeData);
            dateAdd.setTypeface(Typeface.createFromAsset(itemView.getContext().getAssets(), globalData.getFont()));
            rowData = (LinearLayout) itemView.findViewById(R.id.rowData);
        }
    }

}
