package Noti_alerts;

/**
 * Created by mac on 4/18/17.
 */

public class Notifications_alert {
    private String id, title, content, idFirst, idSecond, idThird, typeAlert, dateAdd, isView, name;
    public Notifications_alert() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTypeAlert() {
        return typeAlert;
    }

    public void setTypeAlert(String typeAlert) {
        this.typeAlert = typeAlert;
    }

    public String getDateAdd() {
        return dateAdd;
    }

    public void setDateAdd(String dateAdd) {
        this.dateAdd = dateAdd;
    }

    public String getIsView() {
        return isView;
    }

    public void setIsView(String isView) {
        this.isView = isView;
    }

    public String getIdFirst() {
        return idFirst;
    }

    public void setIdFirst(String idFirst) {
        this.idFirst = idFirst;
    }

    public String getIdSecond() {
        return idSecond;
    }

    public void setIdSecond(String idSecond) {
        this.idSecond = idSecond;
    }

    public String getIdThird() {
        return idThird;
    }

    public void setIdThird(String idThird) {
        this.idThird = idThird;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
