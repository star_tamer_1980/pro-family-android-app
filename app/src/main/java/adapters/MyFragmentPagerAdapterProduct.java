package adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

import static android.R.attr.fragment;

/**
 * Created by mac on 4/16/17.
 */

public class MyFragmentPagerAdapterProduct extends FragmentPagerAdapter {
    List<Fragment> listFragments;
    public MyFragmentPagerAdapterProduct(FragmentManager fm, List<Fragment> listFragments) {
        super(fm);
        this.listFragments = listFragments;
    }

    @Override
    public Fragment getItem(int position) {
        return listFragments.get(position);
    }

    @Override
    public int getCount() {
        return listFragments.size();
    }
}
