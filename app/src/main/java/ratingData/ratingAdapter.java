package ratingData;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import libes.globalData;
import profamily.designsite.in.profamilies.R;

/**
 * Created by mac on 4/18/17.
 */

public class ratingAdapter extends RecyclerView.Adapter<ratingAdapter.ViewHolder> {
    private Context v;
    private List<rating> my_data;
    public ratingAdapter(Context v, List<rating> data_list) {
        this.v = v;
        this.my_data = data_list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_comments, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.title.setText(my_data.get(position).getIdMember());
        holder.content.setText(my_data.get(position).getComment());
        holder.dateAdd.setText(my_data.get(position).getDateAdd());

    }

    @Override
    public int getItemCount() {
        return my_data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView title;
        public TextView content;
        public TextView dateAdd;
        public RelativeLayout rowData;

        public ViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.tv_username);
            title.setTypeface(Typeface.createFromAsset(itemView.getContext().getAssets(), globalData.getFont()));
            content = (TextView) itemView.findViewById(R.id.tv_comment);
            content.setTypeface(Typeface.createFromAsset(itemView.getContext().getAssets(), globalData.getFont()));
            dateAdd = (TextView) itemView.findViewById(R.id.tv_date_add);
            dateAdd.setTypeface(Typeface.createFromAsset(itemView.getContext().getAssets(), globalData.getFont()));
            rowData = (RelativeLayout) itemView.findViewById(R.id.rowData);
        }
    }

}
