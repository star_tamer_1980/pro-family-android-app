package categoryData;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import libes.globalData;
import profamily.designsite.in.profamilies.HarajImageRemove;
import profamily.designsite.in.profamilies.MarketCategoryRemove;
import profamily.designsite.in.profamilies.MarketEditSwitch;
import profamily.designsite.in.profamilies.R;
import profamily.designsite.in.profamilies.marketListProduct;

/**
 * Created by mac on 4/18/17.
 */

public class catAdapter extends RecyclerView.Adapter<catAdapter.ViewHolder> {
    private Context v;
    private List<category> my_data;
    public catAdapter(Context v, List<category> data_list) {
        this.v = v;
        this.my_data = data_list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_cat, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final String idSubCat = my_data.get(position).getId();
        holder.title.setText(my_data.get(position).getTitle());
        holder.rowData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {



                AlertDialog.Builder builder;
                builder = new AlertDialog.Builder(v.getContext());

                builder.setTitle(v.getResources().getString(R.string.alert_title_order_delete))
                        .setMessage(v.getResources().getString(R.string.alert_message_order_delete))
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Toast.makeText(v.getContext(), "id is: " + idSubCat, Toast.LENGTH_SHORT).show();
                                Intent i = new Intent(v.getContext(), MarketCategoryRemove.class);
                                i.putExtra("idSubCat", idSubCat);
                                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                v.getContext().startActivity(i);
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return my_data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView title;
        public RelativeLayout rowData;
        public ViewHolder(View itemView) {
            super(itemView);
            title = (TextView)itemView.findViewById(R.id.title);
            title.setTypeface(Typeface.createFromAsset(itemView.getContext().getAssets(), globalData.getFont()));
            rowData = (RelativeLayout) itemView.findViewById(R.id.rowData);

        }
    }

}
