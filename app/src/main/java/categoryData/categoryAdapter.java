package categoryData;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import libes.globalData;
import profamily.designsite.in.profamilies.HarajImageGalleryAlbum;
import profamily.designsite.in.profamilies.MarketDetails;
import profamily.designsite.in.profamilies.R;
import profamily.designsite.in.profamilies.marketListProduct;

/**
 * Created by mac on 4/18/17.
 */

public class categoryAdapter extends RecyclerView.Adapter<categoryAdapter.ViewHolder> {
    private Context v;
    private List<category> my_data;
    public categoryAdapter(Context v, List<category> data_list) {
        this.v = v;
        this.my_data = data_list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_title, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final String idSubCat = my_data.get(position).getId();
        final String idMarket = my_data.get(position).getIdMarket();
        holder.title.setText(my_data.get(position).getTitle());
        if(position % 2 == 1) {
            holder.rowData.setBackgroundColor(Color.parseColor("#E9EBEE"));
        }
        holder.rowData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    Intent i = new Intent(v.getContext(), marketListProduct.class);
                    i.putExtra("idMarket", idMarket);
                    i.putExtra("idSubCat", idSubCat);
                    i.putExtra("marketImage", my_data.get(position).getMarketImage());
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    v.getContext().startActivity(i);
                ((MarketDetails)v.getContext()).finish();
            }
        });
    }

    @Override
    public int getItemCount() {
        return my_data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView title;
        public RelativeLayout rowData;
        public ViewHolder(View itemView) {
            super(itemView);
            title = (TextView)itemView.findViewById(R.id.title);
            title.setTypeface(Typeface.createFromAsset(itemView.getContext().getAssets(), globalData.getFont()));
            rowData = (RelativeLayout) itemView.findViewById(R.id.rowData);

        }
    }

}
