package historyData;

/**
 * Created by mac on 4/18/17.
 */

public class history {
    private String id;
    private String title;
    history(String title){
        this.setTitle(title);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
