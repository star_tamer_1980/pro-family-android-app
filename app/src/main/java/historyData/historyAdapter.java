package historyData;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import libes.globalData;
import profamily.designsite.in.profamilies.HarajImageGalleryAlbum;
import profamily.designsite.in.profamilies.R;

/**
 * Created by mac on 4/18/17.
 */

public class historyAdapter extends ArrayAdapter<String> {
    private Context v;
    private List<String> my_data;
    public historyAdapter(Context v, String nameFilter) {
        super(v, android.R.layout.simple_dropdown_item_1line);
        my_data = new ArrayList<String>();
    }
    @Override
    public int getCount(){
        return my_data.size();
    }
    @Override
    public String getItem(int index){
        return my_data.get(index);
    }

    @Override
    public Filter getFilter() {
        Filter myFilter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                JsonParse jp=new JsonParse();
                if (constraint != null) {
                    // A class that queries a web API, parses the data and
                    // returns an ArrayList<GoEuroGetSet>
                    List<history> new_suggestions =jp.getParseJsonWCF(constraint.toString());
                    my_data.clear();
                    for (int i=0;i<new_suggestions.size();i++) {
                        my_data.add(new_suggestions.get(i).getTitle());
                    }

                    // Now assign the values and count to the FilterResults
                    // object
                    filterResults.values = my_data;
                    filterResults.count = my_data.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence contraint,
                                          FilterResults results) {
                if (results != null && results.count > 0) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        };
        return myFilter;
    }
}
